--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE analytical.t_plots_derived ADD COLUMN forest_subcategory4change integer[];

COMMENT ON COLUMN analytical.t_plots_derived.forest_subcategory4change IS 'Kategorie lesa z následujícího cyklu, pole cizích klíčů na tabulku c_forest_subcategory.';

UPDATE analytical.t_plots_derived SET forest_subcategory4change = t1.forest_subcategory
FROM analytical.t_plots_derived AS t1
WHERE
	t_plots_derived.nfi_grid = 200 AND
	t_plots_derived.nfi_cycle = 200 AND
	t_plots_derived.version IN (100, 200) AND
	t_plots_derived.is_latest = true AND
	t_plots_derived.pid = t1.pid AND
	t1.nfi_grid = 200 AND
	t1.nfi_cycle = 300 AND
	t1.version = 100 AND
	t1.is_latest = true;


