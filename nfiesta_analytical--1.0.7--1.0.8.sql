--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE TABLE analytical.c_natural_forest_region (
id 	integer,
label	varchar(100),
description text,
CONSTRAINT pkey__t_plots_derived__id PRIMARY KEY(id));

COMMENT ON TABLE analytical.c_natural_forest_region IS 'Číselník přírodních lesních oblastí.';
COMMENT ON COLUMN analytical.c_natural_forest_region.id IS 'Kód kategorie.';
COMMENT ON COLUMN analytical.c_natural_forest_region.label IS 'Popis kategorie.';
COMMENT ON COLUMN analytical.c_natural_forest_region.description IS 'Podrobná definice kategorie.';


ALTER TABLE analytical.t_plots_derived ADD COLUMN natural_forest_region integer;

ALTER TABLE analytical.t_plots_derived ADD CONSTRAINT fkey__t_plots_derived__c_natural_forest_region FOREIGN KEY (natural_forest_region)
REFERENCES analytical.c_natural_forest_region (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

INSERT INTO analytical.c_natural_forest_region (id, label, description)
VALUES
(100,'PLO 1','PLO 1 – Krušné hory'),
(200,'PLO 2','PLO 2 – Podkrušnohorské pánve'),
(300,'PLO 3','PLO 3 – Karlovarská vrchovina'),
(400,'PLO 4','PLO 4 – Doupovské hory'),
(500,'PLO 5','PLO 5 – České středohoří'),
(600,'PLO 6','PLO 6 – Západočeská pahorkatina'),
(700,'PLO 7','PLO 7 – Brdská vrchovina'),
(800,'PLO 8','PLO 8 – Křivoklátsko a Český kras'),
(900,'PLO 9','PLO 9 – Rakovnicko-kladenská pahorkatina'),
(1000,'PLO 10','PLO 10 – Středočeská pahorkatina'),
(1100,'PLO 11','PLO 11 – Český les'),
(1200,'PLO 12','PLO 12 – Předhoří Šumavy a Novohradských hor'),
(1300,'PLO 13','PLO 13 – Šumava'),
(1400,'PLO 14','PLO 14 – Novohradské hory'),
(1500,'PLO 15','PLO 15 – Jihočeské pánve'),
(1600,'PLO 16','PLO 16 – Českomoravská vrchovina'),
(1700,'PLO 17','PLO 17 – Polabí'),
(1800,'PLO 18','PLO 18 – Severočeská pískovcová plošina a Český ráj'),
(1900,'PLO 19','PLO 19 – Lužická pískovcová vrchovina'),
(2000,'PLO 20','PLO 20 – Lužická pahorkatina'),
(2100,'PLO 21','PLO 21 – Jizerské hory a Ještěd'),
(2200,'PLO 22','PLO 22 – Krkonoše'),
(2300,'PLO 23','PLO 23 – Podkrkonoší'),
(2400,'PLO 24','PLO 24 – Sudetské mezihoří'),
(2500,'PLO 25','PLO 25 – Orlické hory'),
(2600,'PLO 26','PLO 26 – Předhoří Orlických hor'),
(2700,'PLO 27','PLO 27 – Hrubý Jeseník'),
(2800,'PLO 28','PLO 28 – Předhoří Hrubého Jeseníku'),
(2900,'PLO 29','PLO 29 – Nízký Jeseník'),
(3000,'PLO 30','PLO 30 – Drahanská vrchovina'),
(3100,'PLO 31','PLO 31 – Českomoravské mezihoří'),
(3200,'PLO 32','PLO 32 – Slezská nížina'),
(3300,'PLO 33','PLO 33 – Předhoří Českomoravské vrchoviny'),
(3400,'PLO 34','PLO 34 – Hornomoravský úval'),
(3500,'PLO 35','PLO 35 – Jihomoravské úvaly'),
(3600,'PLO 36','PLO 36 – Středomoravské Karpaty'),
(3700,'PLO 37','PLO 37 – Kelečská pahorkatina'),
(3800,'PLO 38','PLO 38 – Bílé Karpaty a Vizovické vrchy'),
(3900,'PLO 39','PLO 39 – Podbeskydská pahorkatina'),
(4000,'PLO 40','PLO 40 – Moravskoslezské Beskydy'),
(4100,'PLO 41','PLO 41 – Hostýnskovsetínské vrchy a Javorníky');

