--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
ALTER TABLE analytical.f_p_stems_dynamic ADD COLUMN model_cp_area_mar_m2 double precision;
ALTER TABLE analytical.f_p_stems_dynamic ADD COLUMN model_cp_area_cal_m2 double precision;

COMMENT ON COLUMN analytical.f_p_stems_dynamic.model_cp_area_mar_m2 IS 'Modelová plocha korunové projekce stromu marginální.';
COMMENT ON COLUMN analytical.f_p_stems_dynamic.model_cp_area_cal_m2 IS 'Modelová plocha korunové projekce stromu kalibrovaná.';

INSERT INTO analytical.c_ldsity (id, ldsity_object, unit_of_measure, label, description, label_en, description_en, column_expression, area_domain_category, sub_population_category, definition_variant)
VALUES 
	(253, 900, 2200,
	'korunové projekce nehroubí - marginální model', 'Korunové projekce jedinců nehroubí. Pro výpočet korunových projekcí byl zvolen marginální model.',
	'crown projections of non-merchantable wood stems, marginal model', 'Crown projections of non-merchantable wood stems. For calculation of crown projections the marginal model was used.', 
	'model_cp_area_mar_m2/10000.0/izone_ha',
	'{911,914}', NULL, '{1400}'),
	(254, 1800, 2200,
	'korunové projekce hroubí (dynamické) - marginální', 'Korunové projekce jedinců hroubí (dynamická tabulka). Pro výpočet korunových projekcí byl zvolen marginální model.',
	'crown projections of merchantable wood stems (dynamic), marginal ', 'Crown projections of merchantable wood stems (dynamic table). For calculation of crown projections the marginal model was used.', 
	'model_cp_area_mar_m2/10000.0/izone_mid_ha',
	'{1111}', NULL, '{1400}'),
	(255, 1800, 2200,
	'korunové projekce hroubí (dynamické) - kalibrovaný', 'Korunové projekce jedinců hroubí (dynamická tabulka). Pro výpočet korunových projekcí byl zvolen kalibrovaný model.',
	'crown projections of merchantable wood stems (dynamic), calibrated', 'Crown projections of merchantable wood stems (dynamic table). For calculation of crown projections the calibrated model was used.', 
	'CASE    WHEN model_cp_area_cal_m2 <= 0.0 OR model_cp_area_cal_m2 IS NULL
	THEN (model_cp_area_mar_m2/10000.0/izone_mid_ha) 
	ELSE (model_cp_area_cal_m2/10000.0/izone_mid_ha) END',
	'{1111}', NULL, '{1500}');


