--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE analytical.c_natural_forest_region DROP CONSTRAINT pkey__t_plots_derived__id CASCADE;

ALTER TABLE analytical.c_natural_forest_region ADD CONSTRAINT pkey__c_natural_forest_region__id PRIMARY KEY (id);

ALTER TABLE analytical.t_plots_derived ADD CONSTRAINT fkey__t_plots_derived__c_natural_forest_region FOREIGN KEY (natural_forest_region)
REFERENCES analytical.c_natural_forest_region (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE TABLE analytical.c_european_forest_type_category (
id 	integer NOT NULL,
label	varchar(100) NOT NULL,
description text NOT NULL,
CONSTRAINT pkey__c_european_forest_type_category__id PRIMARY KEY(id));

COMMENT ON TABLE analytical.c_european_forest_type_category IS 'Číselník přírodních lesních oblastí.';
COMMENT ON COLUMN analytical.c_european_forest_type_category.id IS 'Kód kategorie.';
COMMENT ON COLUMN analytical.c_european_forest_type_category.label IS 'Popis kategorie.';
COMMENT ON COLUMN analytical.c_european_forest_type_category.description IS 'Podrobná definice kategorie.';

CREATE TABLE analytical.c_european_forest_type (
id 	integer NOT NULL,
label	varchar(100) NOT NULL,
description text NOT NULL,
european_forest_type_category integer NOT NULL,
CONSTRAINT pkey__c_european_forest_type__id PRIMARY KEY(id));

COMMENT ON TABLE analytical.c_european_forest_type IS 'Číselník přírodních lesních oblastí.';
COMMENT ON COLUMN analytical.c_european_forest_type.id IS 'Kód kategorie.';
COMMENT ON COLUMN analytical.c_european_forest_type.label IS 'Popis kategorie.';
COMMENT ON COLUMN analytical.c_european_forest_type.description IS 'Podrobná definice kategorie.';

ALTER TABLE analytical.c_european_forest_type ADD CONSTRAINT
fkey__c_european_forest_type__european_forest_type_category FOREIGN KEY (european_forest_type_category)
REFERENCES analytical.c_european_forest_type_category (id);

ALTER TABLE analytical.t_plots_field_data ADD COLUMN european_forest_type integer;
ALTER TABLE analytical.t_plots_field_data ADD CONSTRAINT fkey__t_plots_field_data__c_european_forest_type FOREIGN KEY (european_forest_type)
REFERENCES analytical.c_european_forest_type (id)
ON UPDATE CASCADE ON DELETE NO ACTION;


INSERT INTO analytical.c_european_forest_type_category (id, label, description)
VALUES
(1, 'boreal', '1. Boreal forest.'),
(2, 'hemiboreal, nemoral coniferous and mixed broadleaved-coniferous', '2. Hemiboreal forest and nemoral coniferous and mixed broadleaved-coniferous forest.'),
(3, 'Alpine coniferous', '3. Alpine coniferous forest.'),
(4, 'acidophilous oak and oak-birch', '4. Acidophilous oak and oak-birch forest.'),
(5, 'mesophytic deciduous', '5. Mesophytic deciduous forest.'),
(6, 'beech', '6. Beech forest.'),
(7, 'mountainous beech', '7. Mountainous beech forest.'),
(8, 'thermophilous deciduous', '8. Thermophilous deciduous forest.'),
(9, 'broadleaved evergreen', '9. Broadleaved evergreen forest.'),
(10, 'coniferous Mediterranean, Anatolian and Macaronesian', '10. Coniferous forest of the Mediterranean, Anatolian and Macaronesian regions.'),
(11, 'mire and swamp forest', '11. Mire and swamp forests.'),
(12, 'floodplain forest', '12. Floodplain forest.'),
(13, 'non riverine alder, birch, or aspen forest', '13. Non riverine alder, birch, or aspen forest.'),
(14, 'plantations and self-sown exotic forest', '14. Plantations and self-sown exotic forest.');


INSERT INTO analytical.c_european_forest_type (id, label, description, european_forest_type_category)
VALUES 
(1, 'spruce-dominated boreal', '1.1 Spruce-dominated boreal forest.', 1),
(2, 'pine-dominated boreal', '1.2 Pine-dominated boreal forest.', 1),
(3, 'hemiboreal', '2.1 Hemiboreal forest.', 2),
(4, 'nemoral Scots pine', '2.2 Nemoral Scots pine forest.', 2),
(5, 'nemoral spruce', '2.3 Nemoral spruce forest.', 2),
(6, 'nemoral Black pine', '2.4 Nemoral Black pine forest.', 2),
(7, 'mixed Scots pine-birch', '2.5 Mixed Scots pine-birch forest.', 2),
(8, 'mixed Scots pine-pedunculate oak', '2.6 Mixed Scots pine-pedunculate oak forest.', 2),
(9, 'subalpine larch-arolla pine and dwarf pine', '3.1 Subalpine larch-arolla pine and dwarf pine forest.', 3),
(10, 'subalpine and mountainous spruce and mountainous mixed spruce-silver fir', '3.2 Subalpine and mountainous spruce and mountainous mixed spruce-silver fir forest.', 3),
(11, 'Alpine Scots pine and Black pine', '3.3 Alpine Scots pine and Black pine forest.', 3),
(12, 'acidophylous oakwood', '4.1 Acidophylous oakwood.', 4),
(13, 'oak-birch forest', '4.2 Oak-birch forest.', 4),
(14, 'pedunculate oak-hornbeam', '5.1 Pedunculate oak-hornbeam forest.', 5),
(15, 'sessile oak-hornbeam', '5.2 Sessile oak-hornbeam forest.', 5),
(16, 'ashwood and oak-ash', '5.3 Ashwood and oak-ash forest.', 5),
(17, 'maple-oak', '5.4 Maple-oak forest.', 5),
(18, 'lime-oak', '5.5 Lime-oak forest.', 5),
(19, 'maple-lime', '5.6 Maple-lime forest.', 5),
(20, 'lime', '5.7 Lime forest.', 5),
(21, 'ravine and slope', '5.8 Ravine and slope forest.', 5),
(22, 'other mesophytic deciduous', '5.9 Other mesophytic deciduous forests.', 5),
(23, 'lowland beech of southern Scandinavia and north central Europe', '6.1 Lowland beech forest of southern Scandinavia and north central Europe.', 6),
(24, 'Atlantic and subatlantic lowland beech', '6.2 Atlantic and subatlantic lowland beech forest.', 6),
(25, 'subatlantic submountainous beech', '6.3 Subatlantic submountainous beech forest.', 6),
(26, 'central European submountainous beech', '6.4 Central European submountainous beech forest.', 6),
(27, 'Carpathian submountainous beech', '6.5 Carpathian submountainous beech forest.', 6),
(28, 'Illyrian submountainous beech', '6.6 Illyrian submountainous beech forest.', 6),
(29, 'Moesian submountainous beech', '6.7 Moesian submountainous beech forest.', 6),
(30, 'south western European mountainous beech', '7.1 South western European mountainous beech forest (Cantabrians, Pyrenees, central Massif, south western Alps).', 7),
(31, 'central European mountainous beech', '7.2 Central European mountainous beech forest.', 7),
(32, 'Apennine-Corsican mountainous beech', '7.3 Apennine-Corsican mountainous beech forest.', 7),
(33, 'Illyrian mountainous beech', '7.4 Illyrian mountainous beech forest.', 7),
(34, 'Carpathian mountainous beech', '7.5 Carpathian mountainous beech forest.', 7),
(35, 'Moesian mountainous beech', '7.6 Moesian mountainous beech forest.', 7),
(36, 'Crimean mountainous beech', '7.7 Crimean mountainous beech forest.', 7),
(37, 'oriental beech and hornbeam-oriental beech', '7.8 Oriental beech and hornbeam-oriental beech forest.', 7),
(38, 'downy oak', '8.1 Downy oak forest.', 8),
(39, 'Turkey oak, Hungarian oak and Sessile oak', '8.2 Turkey oak, Hungarian oak and Sessile oak forest.', 8),
(40, 'Pyrenean oak', '8.3 Pyrenean oak forest.', 8),
(41, 'Portuguese oak and Mirbeck''s oak Iberian', '8.4 Portuguese oak and Mirbeck''s oak Iberian forest.', 8),
(42, 'Macedonian oak', '8.5 Macedonian oak forest.', 8),
(43, 'Valonia oak', '8.6 Valonia oak forest.', 8),
(44, 'chestnut forest', '8.7 Chestnut forest.', 8),
(45, 'other thermophilous deciduous', '8.8 Other thermophilous deciduous forests.', 8),
(46, 'mediterranean evergreen oak, cork oak and holm oak forest Kermes and alder-leaved oak', '9.1 Mediterranean evergreen oak forest Cork oak and holm oak forest Kermes and alder-leaved oak forest.', 9),
(47, 'olive-carob', '9.2 Olive-carob forest.', 9),
(48, 'palm groves', '9.3 Palm groves.', 9),
(49, 'Macaronesian laurisilva', '9.4 Macaronesian laurisilva.', 9),
(50, 'other sclerophlyllous', '9.5 Other sclerophlyllous forests.', 9),
(51, 'thermophilous pine', '10.1 Thermophilous pine forest.', 10),
(52, 'Mediterranean and Anatolian Black pine', '10.2 Mediterranean and Anatolian Black pine forest.', 10),
(53, 'Canarian pine', '10.3 Canarian pine forest.', 10),
(54, 'Mediterranean and Anatolian Scots pine', '10.4 Mediterranean and Anatolian Scots pine forest.', 10),
(55, 'Alti-Mediterranean pine', '10.5 Alti-Mediterranean pine forest.', 10),
(56, 'Mediterranean and Anatolian fir', '10.6 Mediterranean and Anatolian fir forest.', 10),
(57, 'juniper forest', '10.7 Juniper forest.', 10),
(58, 'cypress forest', '10.8 Cypress forest.', 10),
(59, 'cedar forest', '10.9 Cedar forest.', 10),
(60, 'tetraclinis articulata stands', '10.10 Tetraclinis articulata stands.', 10),
(61, 'Mediterranean yew', '10.11 Mediterranean yew stands.', 10),
(62, 'conifer dominated or mixed mire', '11.1 Conifer dominated or mixed mire forest.', 11),
(63, 'alder swamp', '11.2 Alder swamp forest.', 11),
(64, 'birch swamp', '11.3 Birch swamp forest.', 11),
(65, 'pedunculate oak swamp', '11.4 Pedunculate oak swamp forest.', 11),
(66, 'aspen swamp', '11.5 Aspen swamp forest.', 11),
(67, 'riparian forest', '12.1 Riparian forest.', 12),
(68, 'fluvial forest', '12.2 Fluvial forest.', 12),
(69, 'Mediterranean and Macaronesian riparian', '12.3 Mediterranean and Macaronesian riparian forest.', 12),
(70, 'alder forest', '13.1 Alder forest.', 13),
(71, 'Italian alder forest', '13.2 Italian alder forest.', 13),
(72, 'boreal birch forest', '13.3 Boreal birch forest.', 13),
(73, 'southern boreal birch forest', '13.4 Southern boreal birch forest.', 13),
(74, 'aspen forest', '13.5 Aspen forest.', 13),
(75, 'plantations of site-native species', '14.1 Plantations of site-native species.', 14),
(76, 'plantations of not-site-native species and self-sown exotic forest', '14.2 Plantations of not-site-native species and self-sown exotic forest.', 14)
;
