--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE analytical.f_p_stems ADD COLUMN dead_stem_bio_t double precision;
ALTER TABLE analytical.f_p_stems ADD COLUMN dead_stem_bio_ldty_t_ha double precision;
ALTER TABLE analytical.t_cwd ADD COLUMN ldsity_bio_t_ha double precision;
ALTER TABLE analytical.t_fwd ADD COLUMN ldsity_bio_t_ha double precision;
ALTER TABLE analytical.t_plots_field_data ADD COLUMN european_forest_type_next integer;

ALTER TABLE analytical.t_plots_field_data ADD CONSTRAINT fkey__t_plots_field_data__c_european_forest_type__next FOREIGN KEY (european_forest_type_next)
REFERENCES analytical.c_european_forest_type (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

COMMENT ON COLUMN analytical.t_plots_field_data.european_forest_type IS 'Evropský lesní typ, cizí klíč do tabulky analytical.c_european_forest_type.';
COMMENT ON COLUMN analytical.t_plots_field_data.european_forest_type_next IS 'Evropský lesní typ v následujícím šetření, cizí klíč do tabulky analytical.c_european_forest_type.';

COMMENT ON COLUMN analytical.f_p_stems.dead_stem_bio_t IS 'Biomasa souše v tunách. Pro výpočet byl použit objem hroubí podle tabulek ČSOT s kůrou a s případnou redukcí na torzo. Tento byl vynásoben tabulkovou hodnotou hustoty dřeva.';
COMMENT ON COLUMN analytical.f_p_stems.dead_stem_bio_ldty_t_ha IS 'Lokální hustota biomasy souše.';
COMMENT ON COLUMN analytical.t_cwd.ldsity_m3_ha IS 'Lokální hustota objemu ležícího mrtvého dříví v metrech krychlových na hektar. Neobsahuje kompenzaci okrajového efektu.';
COMMENT ON COLUMN analytical.t_cwd.ldsity_bio_t_ha IS 'Lokální hustota biomasy ležícího mrtvého dříví v tunách na hektar. Neobsahuje kompenzaci okrajového efektu.';
COMMENT ON COLUMN analytical.t_fwd.ldsity_m3_ha IS 'Lokální hustota objemu ležícího mrtvého dříví v metrech krychlových na hektar. Neobsahuje kompenzaci okrajového efektu.';
COMMENT ON COLUMN analytical.t_fwd.ldsity_bio_t_ha IS 'Lokální hustota biomasy ležícího mrtvého dříví v tunách na hektar. Neobsahuje kompenzaci okrajového efektu.';

INSERT INTO analytical.c_ldsity (id, ldsity_object, column_expression, unit_of_measure, label, description, label_en, description_en, area_domain_category, sub_population_category, definition_variant)
VALUES 
	(248, 1400, '(ldsity_bio_t_ha * factor)/4.0', 1000,
		'biomasa ležícího mrtvého dříví hroubí', 'Biomasa ležícího mrtvého dříví hroubí.', 
		'lying merchantable deadwood biomass', 'Lying merchantable deadwood biomass.', 
		'{1119,1122}', NULL, NULL),
	(249, 1400, '(ldsity_bio_t_ha * factor)/4.0', 1000,
		'biomasa ležícího mrtvého dříví hroubí ≥ 1 m', 'Biomasa ležícího mrtvého dříví hroubí ≥ 1 m.', 
		'lying merchantable deadwood ≥ 1 m biomass', 'Lying merchantable deadwood ≥ 1 m biomass.', 
		'{1119,1122}', '{371}', NULL),
	(250, 1500, '(ldsity_bio_t_ha * factor)/4.0', 1000,
		'biomasa ležícího mrtvého dříví nehroubí', 'Biomasa ležícího mrtvého dříví nehroubí.', 
		'lying non-merchantable deadwood biomass', 'Lying non-merchantable deadwood biomass.', 
		'{1119,1122}', NULL, NULL),
	(251, 700, 'case when dead_stem IN (200,300, 500,600,700,800,900, 1100,1200,1300,1400,1500, 1700,1800,1900,2000,2100) then dead_stem_bio_ldty_t_ha else stump_bio_gldsity_t_ha end', 1000,
		'biomasa souše s redukcí na torzo a pařezu', 'Biomasa souše s redukcí na torzo a pařezu.',
		'dead stem and stump biomass', 'Dead stem and stump biomass.',
		'{1041,21,1036,1039}', '{1828}', '{1800,2000}'),
	(252, 700, 'dead_stem_bio_ldty_t_ha', 1000,
		'biomasa souše s redukcí na torzo', 'Biomasa souše s redukcí na torzo.',
		'dead stem biomass', 'Dead stem biomass.',
		'{1041,21,1036,1039}', '{1832}', '{1800}')
;

