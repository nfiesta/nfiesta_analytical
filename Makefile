#nfiesta_analytical/Makefile

EXTENSION = nfiesta_analytical # nazev extenze
DATA = nfiesta_analytical--1.0.0.sql \
	nfiesta_analytical--1.0.0--1.0.1.sql \
	nfiesta_analytical--1.0.1--1.0.2.sql \
	nfiesta_analytical--1.0.2--1.0.3.sql \
	nfiesta_analytical--1.0.3--1.0.4.sql \
	nfiesta_analytical--1.0.4--1.0.5.sql \
	nfiesta_analytical--1.0.5--1.0.6.sql \
	nfiesta_analytical--1.0.6--1.0.7.sql \
	nfiesta_analytical--1.0.7--1.0.8.sql \
	nfiesta_analytical--1.0.8--1.0.9.sql \
	nfiesta_analytical--1.0.9--1.0.10.sql \
	nfiesta_analytical--1.0.10--1.0.11.sql \
	nfiesta_analytical--1.0.11--1.0.12.sql \
	nfiesta_analytical--1.0.12--1.0.13.sql \
	nfiesta_analytical--1.0.13--1.0.14.sql \
	nfiesta_analytical--1.0.14--1.0.15.sql \
	nfiesta_analytical--1.0.15--1.0.16.sql \
	nfiesta_analytical--1.0.16--1.0.17.sql \
	nfiesta_analytical--1.0.17--1.0.18.sql

DOCS=README.md # readme

export SRC_DIR := $(shell pwd)

REGRESS = install

installcheck-all:
	make installcheck REGRESS_OPTS="--dbname=contrib_regression_analytical"
	make installcheck-sdesign
	make installcheck-data
	make installcheck-panelref
	make installcheck-etl

installcheck-sdesign:
	psql -d postgres -c "drop database if exists contrib_regression_sdesign;" -c "create database contrib_regression_sdesign template contrib_regression_analytical;"
	cd deps/nfiesta_sdesign && make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_sdesign" REGRESS="csv fn_import_data check_data check_triggers"

installcheck-data:
	psql -d postgres -c "drop database if exists contrib_regression_analytical_data;" -c "create database contrib_regression_analytical_data template contrib_regression_sdesign;"
	pg_restore -d contrib_regression_analytical_data --data-only < sql/data_t_plots_derived.dump
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_analytical_data" REGRESS="sdesign_metadata"
	pg_restore -d contrib_regression_analytical_data --data-only < sql/data_t_plots_field_data.dump
	pg_restore -d contrib_regression_analytical_data --data-only < sql/data_f_a_circle.dump
	pg_restore -d contrib_regression_analytical_data --data-only < sql/data_f_p_stems.dump
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_analytical_data" REGRESS="data_t_regeneration data_t_regenerations data_t_deadwood data_t_cwd"

installcheck-panelref:
	psql -d postgres -c "drop database if exists contrib_regression_analytical_panelref;" -c "create database contrib_regression_analytical_panelref template contrib_regression_analytical_data;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_analytical_panelref" REGRESS="analyze_data fn_save_panel_refyearset_mapping"

installcheck-etl:
	psql -d postgres -c "drop database if exists contrib_regression_analytical_etl;" -c "create database contrib_regression_analytical_etl template contrib_regression_analytical_panelref;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_analytical_etl" REGRESS="etl_fce_attr etl_fce_ldsity"

purge:
	rm -rf /usr/share/postgresql/16/extension/nfiesta_analytical--* /usr/share/postgresql/16/extension/nfiesta_analytical.control

# postgres build
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
