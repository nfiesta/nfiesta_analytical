--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE INDEX fki__t_plots_derived__panel ON analytical.t_plots_derived USING btree(panel);
CREATE INDEX fki__t_plots_derived__reference_year_set ON analytical.t_plots_derived USING btree(reference_year_set);

CREATE INDEX fki__t_plots_field_data__plots_derived ON analytical.t_plots_field_data USING btree(plots_derived);
CREATE INDEX fki__f_p_stems__circle ON analytical.f_p_stems USING btree(circle);
CREATE INDEX fki__t_regeneration__plots_field_data ON analytical.t_regeneration USING btree(plots_field_data);
CREATE INDEX fki__t_regenerations__regeneration ON analytical.t_regenerations USING btree(regeneration);


-- <function name="fn_get_ldsity_objects" schema="analytical" src="functions/fn_get_ldsity_objects.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_get_ldsity_objects(integer[]) CASCADE;

create or replace function analytical.fn_get_ldsity_objects(_ldsity_objects integer[])
returns integer[]
as
$$
declare
		_res_1	integer;
		_res	integer[];
begin
		if _ldsity_objects is null
		then
			raise exception 'Error 01: fn_get_ldsity_objects: The input argument _ldsity_objects must not be NULL !';
		end if;	
	
		select upper_object from analytical.c_ldsity_object where id = _ldsity_objects[1]
		into _res_1;	
	
		if _res_1 is null
		then
			_res := _ldsity_objects;
		else
			_res := analytical.fn_get_ldsity_objects(array[_res_1] || _ldsity_objects);
		end if;
	
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION analytical.fn_get_ldsity_objects(integer[]) IS
'The function for the specified ldsity object hierarchically returns a list of objects.';

GRANT EXECUTE ON FUNCTION analytical.fn_get_ldsity_objects(integer[]) TO public;

-- </function>

-- <function name="fn_check_classification_rule_syntax" schema="analytical" src="functions/fn_check_classification_rule_syntax.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule_syntax
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_check_classification_rule_syntax(integer, text) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_check_classification_rule_syntax(_ldsity_object integer, _rule text)
RETURNS boolean 
AS
$$
DECLARE
_table_name	varchar;
_test		boolean;
BEGIN
	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	SELECT table_name
	FROM analytical.c_ldsity_object
	WHERE id = _ldsity_object
	INTO _table_name;


	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN _test = true;
	ELSE
		BEGIN
		EXECUTE
		'SELECT EXISTS(SELECT *
		FROM '||_table_name||'
		WHERE '||_rule ||'
		)'
		INTO _test;

		IF _test IS NOT NULL 
		THEN _test := true;
		ELSE _test := false; 
		END IF;

		EXCEPTION WHEN OTHERS THEN
			_test := false;
		END;
	END IF;

	RETURN _test;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_check_classification_rule_syntax(integer, text) IS
'Function checks syntax in text field with classification rule.';

GRANT EXECUTE ON FUNCTION analytical.fn_check_classification_rule_syntax(integer, text) TO public;

-- </function>

-- <function name="fn_check_classification_rules" schema="analytical" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_check_classification_rules(_ldsity integer, _ldsity_object integer, _rules text[], _panel_refyearset integer DEFAULT NULL::integer, _adc integer[][] DEFAULT NULL::integer[][], _spc integer[][] DEFAULT NULL::integer[][])
--boolean
RETURNS TABLE (
result		boolean,
message_id	integer,
message		text
) 
AS
$$
DECLARE
_ldsity_object_ldsity	integer;
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_exist_test_all		boolean[];
--_qrule			text;
_adc4loop2d		integer[][];
_spc4loop2d		integer[][];
_adc4loop		integer[];
_spc4loop		integer[];
adcv			integer;
spcv			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	_ldsity_object_ldsity := (SELECT ldsity_object FROM analytical.c_ldsity WHERE id = _ldsity);

	IF NOT array[_ldsity_object] <@ (SELECT analytical.fn_get_ldsity_objects(array[_ldsity_object_ldsity]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM analytical.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
		THEN
			_exist_test := true; _rule = true;
		ELSE 	_exist_test := false;
		END IF;

		_exist_test_all := _exist_test_all || _exist_test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all @> array[false]
	THEN
		RAISE EXCEPTION 'In array of given rules there is an EXISTS or NOT EXISTS rule but there are also another rules, this rules have to be alone within one sub population.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all != ARRAY[true,true]
	THEN
		RAISE EXCEPTION 'The given array of rules has more or less then 2 rules (%). If this is a special case of EXISTS rule, then its counterpart NOT EXISTS has to be entered and nothing else.',_rules;
	END IF;

	_ldsity_objects := (SELECT analytical.fn_get_ldsity_objects(array[_ldsity_object]));

--raise notice 'ldos %', _ldsity_objects;
	IF _adc IS NOT NULL AND array_length(_rules,1) != array_length(_adc,1)
	THEN
		RAISE EXCEPTION 'Given array of area domain categories must be the same length as the given array of rules.';
	END IF;

	IF _spc IS NOT NULL AND array_length(_rules,1) != array_length(_spc,1)
	THEN
		RAISE EXCEPTION 'Given array of sub population categories must be the same length as the given array of rules.';
	END IF;

	IF _panel_refyearset IS NOT NULL AND NOT EXISTS (SELECT * FROM sdesign.cm_refyearset2panel_mapping AS t1 WHERE t1.id = _panel_refyearset)
	THEN RAISE EXCEPTION 'Given panel and reference year set combination does not exist in table sdesign.cm_refyearset2panel_mapping (%)', _panel_refyearset;
	END IF;

	IF _panel_refyearset IS NOT NULL
	THEN
	RAISE NOTICE 'panel_refyearset = %', _panel_refyearset;
	END IF;

-- raise notice '%', _adc;
-- get id from cm_adc2classification_rule id for each adc
	WITH RECURSIVE 
	w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.area_domain_category, t3.array_id, t3.id AS ordinality
			FROM
				analytical.cm_adc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  adc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc,1)),12) ) AS array_id
				FROM  unnest(_adc) WITH ORDINALITY AS t1(adc, id)
				) AS t3
			ON
				t1.area_domain_category = t3.adc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_adc,1) * array_length(_adc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id, 
 			CASE WHEN t1.variable_superior IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM 
			w_cm_ids AS t2
		LEFT JOIN
			(SELECT * FROM analytical.t_adc_hierarchy) AS t1 
		ON t1.variable = t2.area_domain_category
		LEFT JOIN
			(SELECT area_domain_category, id
			FROM
				(SELECT area_domain_category, id, row_number() over(partition by area_domain_category order by object_id DESC) AS row_id
				FROM
					(SELECT t4.id AS object_id, t3.* FROM analytical.cm_adc2classification_rule AS t3
					INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
					ON t3.ldsity_object = t4.ldsity_object) AS t3
				) AS t3
			WHERE row_id = 1
			) AS t3
		ON 	t1.variable_superior = t3.area_domain_category
	/*	UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM analytical.t_adc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN analytical.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
	*/
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		--LEFT JOIN analytical.t_adc_hierarchy AS t2 
	--	ON t1.variable_superior = t2.variable 
	--	WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _adc;
--raise notice '%', _adc;

--raise notice '%', _ldsity_objects;
	WITH --RECURSIVE 
	w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.sub_population_category, t3.array_id, t3.id AS ordinality
			FROM
				analytical.cm_spc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  spc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc,1)),12) ) AS array_id
				FROM  unnest(_spc) WITH ORDINALITY AS t1(spc, id)
				) AS t3
			ON
				t1.sub_population_category = t3.spc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_spc,1) * array_length(_spc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id, 
  			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM 
			w_cm_ids AS t2
		LEFT JOIN
			(SELECT * FROM analytical.t_spc_hierarchy) AS t1
		ON t1.variable = t2.sub_population_category
		LEFT JOIN
			(SELECT sub_population_category, id
			FROM
				(SELECT sub_population_category, id, row_number() over(partition by sub_population_category order by object_id DESC) AS row_id
				FROM
					(SELECT t4.id AS object_id, t3.* FROM analytical.cm_spc2classification_rule AS t3
					INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
					ON t3.ldsity_object = t4.ldsity_object
					WHERE t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
					) AS t3
				) AS t3
			WHERE row_id = 1
			) AS t3
		ON 	t1.variable_superior = t3.sub_population_category
		/*UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM analytical.t_spc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN analytical.cm_spc2classification_rule AS t3
		ON t1.variable_superior = t3.sub_population_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		WHERE t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
		*/
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		--LEFT JOIN analytical.t_spc_hierarchy AS t2 
		--ON t1.variable_superior = t2.variable 
		--WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _spc;


-- construct from part of the query
	WITH w AS (
		SELECT	analytical.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t2.id AS ord, t3.id, t2.ldsity_object, t3.table_name::text, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			analytical.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name::text,'.',1) AND t4.table_name = split_part(t3.table_name::text,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			(SELECT current_database()::information_schema.sql_identifier AS table_catalog,
				x.tblschema::information_schema.sql_identifier AS table_schema,
				x.tblname::information_schema.sql_identifier AS table_name,
				x.colname::information_schema.sql_identifier AS column_name,
				current_database()::information_schema.sql_identifier AS constraint_catalog,
				x.cstrschema::information_schema.sql_identifier AS constraint_schema,
				x.cstrname::information_schema.sql_identifier AS constraint_name
			FROM 
				(SELECT DISTINCT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
				FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_depend d,
					pg_namespace nc,
					pg_constraint c
			 	WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND 
					d.refclassid = 'pg_class'::regclass::oid AND d.refobjid = r.oid AND 
					d.refobjsubid = a.attnum AND d.classid = 'pg_constraint'::regclass::oid AND 
					d.objid = c.oid AND c.connamespace = nc.oid AND c.contype = 'c'::"char" AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND NOT a.attisdropped
				UNION ALL
				SELECT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
			   	FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_namespace nc,
					pg_constraint c
				WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND nc.oid = c.connamespace AND r.oid =
					CASE c.contype
					WHEN 'f'::"char" THEN c.confrelid
					ELSE c.conrelid
					END AND (a.attnum = ANY (
						CASE c.contype
						WHEN 'f'::"char" THEN c.confkey
				    		ELSE c.conkey
						END)
					) AND NOT a.attisdropped AND (c.contype = ANY (ARRAY['p'::"char", 'u'::"char", 'f'::"char"])) AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"]))
				) AS x(tblschema, tblname, tblowner, colname, cstrschema, cstrname)
			) AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY ord) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY ord) AS table_name_ws,
		array_agg(primary_key ORDER BY ord) AS primary_key,
		array_agg(column_name ORDER BY ord) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM analytical.c_ldsity_object WHERE id = _ldsity_object);

	FOR i IN 1..array_length(_cases,1)
	LOOP

		CASE
		WHEN _adc IS NOT NULL
		THEN

			-- 4D array, for each category build 2D array - each array within is an unique combination of categories/rules
			-- and each object must belong only into one combination
			_adc4loop2d :=  
					(WITH w_un AS (
						SELECT	adc,
							id AS ordinality,
							ceil( round((id/(count(*) OVER())::numeric * array_length(_adc[i:i],2) * array_length(_adc[i:i],3)),12) ) AS array_id
						FROM 
							unnest(_adc[i:i][:]) WITH ORDINALITY AS t(adc,id)
					), w_dist AS (
						SELECT array_agg(adc ORDER BY ordinality) AS adc, array_id
						FROM w_un
						GROUP BY array_id
					)
					SELECT array_agg(adc ORDER BY array_id) 
					FROM w_dist);
			--raise notice 'adc4loop2d %', _adc4loop2d;
			FOR y IN 1..array_length(_adc4loop2d,1)
			LOOP
				-- for 2d array, because slicing will result again in 2D array even with one element/dimension
				-- it is necessary to unnest it and aggregate again -> 1D array
				_adc4loop :=  (SELECT array_agg(adc) FROM unnest(_adc4loop2d[y:y][:]) AS t(adc));

				--raise notice 'y %, adc4loop %', y, _adc4loop;
				IF cardinality(array_remove(_adc4loop,NULL)) > 0 OR y = 1
				THEN
					--raise notice 'y %, adc4loop %', y, _adc4loop;
					--FOR adcv IN SELECT generate_subscripts(_adc4loop,1)
					--LOOP
					--raise notice 'adcv %', adcv;
					--IF adcv = 1 OR _adc4loop[adcv] IS NOT NULL THEN
						--raise notice '%', _adc4loop[adcv];
						WITH w_tables AS (
						SELECT
							t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'panel, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
								CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
								CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
								' FROM ', t1.table_name, 
								CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
								') AS ', _table_names_ws[t1.id]) AS table_select
						FROM
							unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
						LEFT JOIN
							(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
							FROM
								(SELECT t4.table_name, t3.classification_rule 
								FROM analytical.c_ldsity AS t1,
									unnest(t1.area_domain_category || _adc4loop) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN analytical.cm_adc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN analytical.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t4.table_name, t3.classification_rule 
								FROM analytical.c_ldsity AS t1,
									unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN analytical.cm_spc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN analytical.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t2.table_name, t2.filter 
								FROM analytical.c_ldsity_object AS t2
								--WHERE t1.id = _ldsity
								) AS t1
							WHERE t1.classification_rule IS NOT NULL
							GROUP BY table_name
							) AS t2
						ON t1.table_name = t2.table_name::varchar
					)
					SELECT array_agg(table_select ORDER BY id)
					FROM w_tables
					INTO _table_selects;
--raise notice 'ts = %', _table_selects;
					SELECT concat(' FROM (SELECT t2.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
						_table_names_ws[1], '.', _primary_keys[1], '
						FROM ', _table_selects[1], ' INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2 ON ', _table_names_ws[1],'.panel = t2.panel AND
						', _table_names_ws[1],'.reference_year_set = t2.reference_year_set) AS ', _table_names_ws[1]) 
					INTO _table1;

					_join_all := NULL; _join := NULL;

					FOR i IN 2..array_length(_table_selects,1) 
					LOOP
						SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
						INTO _join;

						_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
						--raise notice '%', _join_all;
					END LOOP;

					--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
					_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

					_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			--raise notice '%', _table1;
					_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL \n\n' ELSE '' END), _q);

					--END IF;
					--END LOOP;
				END IF;
			END LOOP;
			--_qrule := concat(CASE WHEN _qrule IS NOT NULL THEN concat(_qrule, ' UNION ALL ') ELSE '' END, _q_all);
			--raise notice '%',_q_all;
		WHEN _spc IS NOT NULL
		THEN
			-- 4D array, for each category build 2D array - each array within is an unique combination of categories/rules
			-- and each object must belong only into one combination
			_spc4loop2d :=  
					(WITH w_un AS (
						SELECT	spc,
							id AS ordinality,
							ceil( round((id/(count(*) OVER())::numeric * array_length(_spc[i:i],2) * array_length(_spc[i:i],3)),12) ) AS array_id
						FROM 
							unnest(_spc[i:i][:]) WITH ORDINALITY AS t(spc,id)
					), w_dist AS (
						SELECT array_agg(spc ORDER BY ordinality) AS spc, array_id
						FROM w_un
						GROUP BY array_id
					)
					SELECT array_agg(spc ORDER BY array_id) 
					FROM w_dist);
			--raise notice 'spc %', _spc;
			--raise notice 'spc4loop2d %', _spc4loop2d;
			FOR y IN 1..array_length(_spc4loop2d,1)
			LOOP
				-- for 2d array, because slicing will result again in 2D array even with one element/dimension
				-- it is necessary to unnest it and aggregate again -> 1D array
				_spc4loop :=  (SELECT array_agg(spc) FROM unnest(_spc4loop2d[y:y][:]) AS t(spc));
				--raise notice 'i = %, spc = %, spc4loop = %', i, _spc, _spc4loop;
				IF cardinality(array_remove(_spc4loop,NULL)) > 0 OR y = 1
				THEN
					--raise notice 'y %, spc4loop %', y, _spc4loop;
					--FOR spcv IN SELECT generate_subscripts(_spc4loop,1)
					--LOOP

					--IF spcv = 1 OR _spc4loop[spcv] IS NOT NULL THEN
					--raise notice 'spcv %', _spc4loop[spcv];
						WITH w_tables AS (
						SELECT
							t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'panel, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
								CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
								CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
								' FROM ', t1.table_name, 
								CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
								') AS ', _table_names_ws[t1.id]) AS table_select
						FROM
							unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
						LEFT JOIN
							(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
							FROM
								(SELECT t4.table_name, t3.classification_rule 
								FROM analytical.c_ldsity AS t1,
									unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN analytical.cm_adc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN analytical.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t4.table_name, t3.classification_rule 
								FROM analytical.c_ldsity AS t1,
									unnest(t1.sub_population_category || _spc4loop) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN analytical.cm_spc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN analytical.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t2.table_name, t2.filter 
								FROM analytical.c_ldsity_object AS t2
								--WHERE t1.id = _ldsity
								) AS t1
							WHERE t1.classification_rule IS NOT NULL
							GROUP BY table_name
							) AS t2
						ON t1.table_name = t2.table_name::varchar
					)
					SELECT array_agg(table_select ORDER BY id)
					FROM w_tables
					INTO _table_selects;

					SELECT concat(' FROM (SELECT t2.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
						_table_names_ws[1], '.', _primary_keys[1], '
						FROM ', _table_selects[1], ' INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2 ON ', _table_names_ws[1],'.panel = t2.panel AND
						', _table_names_ws[1],'.reference_year_set = t2.reference_year_set) AS ', _table_names_ws[1]) 
					INTO _table1;

					_join_all := NULL; _join := NULL;

					FOR i IN 2..array_length(_table_selects,1) 
					LOOP
						SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
						INTO _join;

						_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
						--raise notice '%', _join_all;
					END LOOP;

					--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
					_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

					_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			--raise notice '%', _table1;
					_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL \n\n' ELSE '' END), _q);
					--raise notice 'loop spcs, rule %, spc %', i, spcv;
					--raise notice '%', _q_all; 

					--END IF;
					--END LOOP;
				END IF;
			END LOOP;

			--_qrule := concat(concat(_qrule, CASE WHEN _qrule IS NOT NULL THEN ' UNION ALL ' ELSE '' END), _q_all);
		ELSE
			WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'panel, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM analytical.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN analytical.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN analytical.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM analytical.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN analytical.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN analytical.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM analytical.c_ldsity_object AS t2
						--WHERE t1.id = _ldsity
						) AS t1
					WHERE t1.classification_rule IS NOT NULL
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name::varchar
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t2.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2 ON ', _table_names_ws[1],'.panel = t2.panel AND
				', _table_names_ws[1],'.reference_year_set = t2.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
	--raise notice '%', _table1;
			_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

			--_qrule := _q_all;
		END CASE;
	END LOOP;

	--raise notice '%',_q_all;

	EXECUTE
	'WITH w AS NOT MATERIALIZED (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belogns to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			IF _exist_test_all = array[true, true]
			THEN
				RETURN QUERY
				SELECT true, 8, concat('Special case of EXISTS rules, implicitly true for all records.')::text;
			ELSE
				RETURN QUERY 
				SELECT false, 4, concat('Some of the objects were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
			END IF;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object (', _no_of_objects[1],') was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules for given ldsity contribution and ldsity object within its hierarchy. Optionally also for given panel x reference year set combination and/or  area domain/sub population category/ies.';

GRANT EXECUTE ON FUNCTION analytical.fn_check_classification_rules(integer, integer, text[], integer, integer[],  integer[]) TO public;

-- </function>

-- <function name="fn_save_ad_panel_refyearset_mapping" schema="analytical" src="functions/fn_save_ad_panel_refyearset_mapping.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ad_panel_refyearset_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(
	_area_domain	integer,
	_ldsity		integer,
	_ldsity_object	integer
	)
RETURNS void 
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.c_area_domain AS t1 WHERE t1.id = _area_domain)
	THEN RAISE EXCEPTION 'Given area_domain does not exist in table c_area_domain (%)', _area_domain;
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity_object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	WITH
	w_classrule_adc AS (
		SELECT 
			t1.id AS adc2classification_rule,
			t2.area_domain, t1.ldsity_object, 
			t4.area_domain AS area_domain_sup,
			array_agg(t3.variable_superior ORDER BY t3.variable_superior) FILTER (WHERE t3.variable_superior IS NOT NULL) AS adc_sup, 
			t2.id AS adc_rule, t1.classification_rule
			--array_agg(t1.classification_rule ORDER BY t2.id) AS rules
		FROM analytical.cm_adc2classification_rule AS t1
		INNER JOIN analytical.c_area_domain_category AS t2
		ON t1.area_domain_category = t2.id
		LEFT JOIN
			analytical.t_adc_hierarchy AS t3
		ON t2.id = t3.variable AND t3.dependent = true
		LEFT JOIN
			analytical.c_area_domain_category AS t4
		ON t3.variable_superior = t4.id
		WHERE t1.ldsity_object = _ldsity_object AND t2.area_domain = _area_domain
		GROUP BY t1.id, t1.ldsity_object, t2.area_domain, t4.area_domain, t1.ldsity_object, t2.id, t1.classification_rule
		ORDER BY t2.id
	), w_classrule AS (
		SELECT
			ldsity_object,
			area_domain,
			area_domain_sup,
			adc_sup,
			adc_rule,
			adc2classification_rule,
			coalesce(max(array_length(adc_sup,1)) OVER (PARTITION BY area_domain_sup, area_domain) - array_length(adc_sup,1),0) AS nulls2app,
			max(array_length(adc_sup,1)) OVER (PARTITION BY area_domain_sup, area_domain) AS max_len,
			classification_rule
		FROM
			w_classrule_adc
		ORDER BY
			adc_rule
	), w_classrule_null AS (
		SELECT
			ldsity_object, area_domain, area_domain_sup,
			adc_sup, adc_rule, adc2classification_rule, 
			CASE WHEN adc_sup IS NOT NULL THEN adc_sup || array_fill(NULL::int, ARRAY[nulls2app]) ELSE NULL END AS adc,
			classification_rule
		FROM
			w_classrule
	), w_classrule_agg AS (
		SELECT
			ldsity_object, area_domain, area_domain_sup,
			array_agg(adc ORDER BY adc_rule) FILTER (WHERE adc IS NOT NULL) AS adc, 
			array_agg(classification_rule ORDER BY adc_rule) AS rules,
			array_agg(adc2classification_rule ORDER BY adc_rule) AS adc2classification_rules
		FROM 
			w_classrule_null
		GROUP BY ldsity_object, area_domain, area_domain_sup
	), w_check AS (
	--select * from w_classrule_agg;
		SELECT  t2.ldsity_object, t1.id AS panel_refyearset, t2.area_domain, t2.area_domain_sup,
			t2.adc, t2.rules, t2.adc2classification_rules,
			t3.result, t3.message_id, t3.message
		FROM	sdesign.cm_refyearset2panel_mapping AS t1,
			w_classrule_agg AS t2,
			analytical.fn_check_classification_rules(_ldsity, t2.ldsity_object, t2.rules, t1.id, t2.adc) AS t3
		--WHERE t1.id = 20
	), w_true AS (
		SELECT DISTINCT ldsity_object, panel_refyearset, area_domain
		FROM w_check
		WHERE result = true
	)
	, w_final AS (
		SELECT t1.panel_refyearset, t3.adc2classification_rule
		FROM
			w_true AS t1
		INNER JOIN
			w_check AS t2
		ON t1.panel_refyearset = t2.panel_refyearset AND 
			t1.area_domain = t2.area_domain AND
			t1.ldsity_object = t2.ldsity_object,
			unnest(t2.adc2classification_rules) AS t3(adc2classification_rule)
	)
	INSERT INTO analytical.cm_adc2classrule2panel_refyearset (refyearset2panel, adc2classification_rule)
	SELECT panel_refyearset, adc2classification_rule
	FROM
		(SELECT panel_refyearset, adc2classification_rule
		FROM w_final
		EXCEPT 
		SELECT refyearset2panel, adc2classification_rule
		FROM analytical.cm_adc2classrule2panel_refyearset) AS t1
	ORDER BY panel_refyearset, adc2classification_rule
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(integer, integer, integer) IS
'Function for given ldsity contribution, ldsity object and area domain finds the maximum possible panel and reference year set combinations which is then inserted into cm_adc2classrule2panel_refyearset.';

GRANT EXECUTE ON FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(integer, integer, integer) TO public;

-- </function>

-- <function name="fn_save_sp_panel_refyearset_mapping" schema="analytical" src="functions/fn_save_sp_panel_refyearset_mapping.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_sp_panel_refyearset_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_save_sp_panel_refyearset_mapping(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_save_sp_panel_refyearset_mapping(
	_sub_population	integer,
	_ldsity		integer,
	_ldsity_object	integer
	)
RETURNS void 
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.c_sub_population AS t1 WHERE t1.id = _sub_population)
	THEN RAISE EXCEPTION 'Given sub_population does not exist in table c_sub_population (%)', _sub_population;
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity_object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	WITH
	w_classrule_spc AS (
		SELECT 
			t1.id AS spc2classification_rule,
			t2.sub_population, t1.ldsity_object, 
			t4.sub_population AS sub_population_sup,
			array_agg(t3.variable_superior ORDER BY t3.variable_superior) FILTER (WHERE t3.variable_superior IS NOT NULL) AS spc_sup, 
			t2.id AS spc_rule, t1.classification_rule
			--array_agg(t1.classification_rule ORDER BY t2.id) AS rules
		FROM analytical.cm_spc2classification_rule AS t1
		INNER JOIN analytical.c_sub_population_category AS t2
		ON t1.sub_population_category = t2.id
		LEFT JOIN
			analytical.t_spc_hierarchy AS t3
		ON t2.id = t3.variable AND t3.dependent = true
		LEFT JOIN
			analytical.c_sub_population_category AS t4
		ON t3.variable_superior = t4.id
		WHERE t1.ldsity_object = _ldsity_object AND t2.sub_population = _sub_population
		GROUP BY t1.id, t1.ldsity_object, t2.sub_population, t4.sub_population, t1.ldsity_object, t2.id, t1.classification_rule
		ORDER BY t2.id
	), w_classrule AS (
		SELECT
			ldsity_object,
			sub_population,
			sub_population_sup,
			spc_sup,
			spc_rule,
			spc2classification_rule,
			coalesce(max(array_length(spc_sup,1)) OVER (PARTITION BY sub_population_sup, sub_population) - array_length(spc_sup,1),0) AS nulls2app,
			max(array_length(spc_sup,1)) OVER (PARTITION BY sub_population_sup, sub_population) AS max_len,
			classification_rule
		FROM
			w_classrule_spc
		ORDER BY
			spc_rule
	), w_classrule_null AS (
		SELECT
			ldsity_object, sub_population, sub_population_sup,
			spc_sup, spc_rule, spc2classification_rule, 
			CASE WHEN spc_sup IS NOT NULL THEN spc_sup || array_fill(NULL::int, ARRAY[nulls2app]) ELSE NULL END AS spc,
			classification_rule
		FROM
			w_classrule
	), w_classrule_agg AS (
		SELECT
			ldsity_object, sub_population, sub_population_sup,
			array_agg(spc ORDER BY spc_rule) FILTER (WHERE spc IS NOT NULL) AS spc, 
			array_agg(classification_rule ORDER BY spc_rule) AS rules,
			array_agg(spc2classification_rule ORDER BY spc_rule) AS spc2classification_rules
		FROM 
			w_classrule_null
		GROUP BY ldsity_object, sub_population, sub_population_sup
	), w_check AS (
	--select * from w_classrule_agg;
		SELECT  t2.ldsity_object, t1.id AS panel_refyearset, t2.sub_population, t2.sub_population_sup,
			t2.spc, t2.rules, t2.spc2classification_rules,
			t3.result, t3.message_id, t3.message
		FROM	sdesign.cm_refyearset2panel_mapping AS t1,
			w_classrule_agg AS t2,
			analytical.fn_check_classification_rules(_ldsity, t2.ldsity_object, t2.rules, t1.id, t2.spc) AS t3
		--WHERE t1.id = 20
	), w_true AS (
		SELECT DISTINCT ldsity_object, panel_refyearset, sub_population
		FROM w_check
		WHERE result = true
	)
	, w_final AS (
		SELECT t1.panel_refyearset, t3.spc2classification_rule
		FROM
			w_true AS t1
		INNER JOIN
			w_check AS t2
		ON t1.panel_refyearset = t2.panel_refyearset AND 
			t1.sub_population = t2.sub_population AND
			t1.ldsity_object = t2.ldsity_object,
			unnest(t2.spc2classification_rules) AS t3(spc2classification_rule)
	)
	INSERT INTO analytical.cm_spc2classrule2panel_refyearset (refyearset2panel, spc2classification_rule)
	SELECT panel_refyearset, spc2classification_rule
	FROM
		(SELECT panel_refyearset, spc2classification_rule
		FROM w_final
		EXCEPT 
		SELECT refyearset2panel, spc2classification_rule
		FROM analytical.cm_spc2classrule2panel_refyearset) AS t1
	ORDER BY panel_refyearset, spc2classification_rule
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_save_sp_panel_refyearset_mapping(integer, integer, integer) IS
'Function for given ldsity contribution, ldsity object and sub population finds the maximum possible panel and reference year set combinations which is then inserted into cm_spc2classrule2panel_refyearset.';

GRANT EXECUTE ON FUNCTION analytical.fn_save_sp_panel_refyearset_mapping(integer, integer, integer) TO public;

-- </function>

-------------------------------- ETL functions -------------------------------

-- <function name="fn_etl_check_adc2classification_rule" schema="analytical" src="functions/etl/fn_etl_check_adc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc2classification_rule(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_adc2classification_rule
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc2classification_rule: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_adc2classification_rule: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_adc2classification_rule: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_adc2classification_rule: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc2classification_rule(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc2classification_rule(integer, integer) is
'Function checks that all area domain classification rules for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc2classification_rule(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_adc2classification_rule_attr" schema="analytical" src="functions/etl/fn_etl_check_adc2classification_rule_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc2classification_rule_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_adc2classification_rule_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc2classification_rule_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_adc2classification_rule_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_check_adc2classification_rule_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_check_adc2classification_rule_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is null
		then
			raise exception 'Error 05: fn_etl_check_adc2classification_rule_attr: Input argument _area_domain_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_area_domain_category,1)
		loop
			if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
			then
				raise exception 'Error 06: fn_etl_check_adc2classification_rule_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
			end if;
		end loop;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc2classification_rule_attr(_export_connection,_ldsity_object,_area_domain_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) is
'Function checks that all area domain classification rules for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc2classification_rule_attr(integer, integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_adc2classrule2panel_refyearset" schema="analytical" src="functions/etl/fn_etl_check_adc2classrule2panel_refyearset.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_adc2classrule2panel_refyearset
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc2classrule2panel_refyearset: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_adc2classrule2panel_refyearset: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_adc2classrule2panel_refyearset: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_adc2classrule2panel_refyearset: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc2classrule2panel_refyearset(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) is
'Function checks that all combinations of mapping between area domain classification rules and combinations of panels and reference year sets for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_adc2classrule2panel_refyearset_attr" schema="analytical" src="functions/etl/fn_etl_check_adc2classrule2panel_refyearset_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc2classrule2panel_refyearset_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc2classrule2panel_refyearset_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_adc2classrule2panel_refyearset_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_check_adc2classrule2panel_refyearset_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_check_adc2classrule2panel_refyearset_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is null
		then
			raise exception 'Error 05: fn_etl_check_adc2classrule2panel_refyearset_attr: Input argument _area_domain_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_area_domain_category,1)
		loop
			if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
			then
				raise exception 'Error 06: fn_etl_check_adc2classrule2panel_refyearset_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
			end if;
		end loop;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(_export_connection,_ldsity_object,_area_domain_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) is
'Function checks that all combinations of mapping between area domain classification rules and combinations of panels and reference year sets for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_adc_hierarchy" schema="analytical" src="functions/etl/fn_etl_check_adc_hierarchy.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc_hierarchy(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_adc_hierarchy
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc_hierarchy: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_adc_hierarchy: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_adc_hierarchy: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_adc_hierarchy: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc_hierarchy(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc_hierarchy(integer, integer) is
'Function checks that all area domain classification hierarchies for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc_hierarchy(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_adc_hierarchy_attr" schema="analytical" src="functions/etl/fn_etl_check_adc_hierarchy_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc_hierarchy_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_adc_hierarchy_attr
(
	_export_connection		integer,
	_area_domain_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc_hierarchy_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_adc_hierarchy_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _area_domain_category is null
		then
			raise exception 'Error 03: fn_etl_check_adc_hierarchy_attr: Input argument _area_domain_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_area_domain_category,1)
		loop
			if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
			then
				raise exception 'Error 04: fn_etl_check_adc_hierarchy_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
			end if;
		end loop;

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc_hierarchy_attr(_export_connection,_area_domain_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) is
'Function checks that all area domain classification hierarchies for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc_hierarchy_attr(integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_area_domain" schema="analytical" src="functions/etl/fn_etl_check_area_domain.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_area_domain(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_area_domain
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_area_domain: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_area_domain: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_area_domain(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_area_domain(integer, integer) is
'Function checks that all area domains for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_area_domain(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_area_domain(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_area_domain(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_area_domain_attr" schema="analytical" src="functions/etl/fn_etl_check_area_domain_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_area_domain_attr(integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_area_domain_attr
(
	_export_connection		integer,
	_area_domain_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_area_domain_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _area_domain_category is null
		then
			raise exception 'Error 03: fn_etl_check_area_domain_attr: Input argument _area_domain_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_area_domain_category,1)
		loop
			if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
			then
				raise exception 'Error 04: fn_etl_check_area_domain_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
			end if;
		end loop;

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_area_domain_attr(_export_connection,_area_domain_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_area_domain_attr(integer, integer[]) is
'Function checks that all area domains for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_area_domain_attr(integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain_attr(integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain_attr(integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_area_domain_attr(integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_area_domain_attr(integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_area_domain_category" schema="analytical" src="functions/etl/fn_etl_check_area_domain_category.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_area_domain_category(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_area_domain_category
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_category: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_category: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_area_domain_category: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_area_domain_category: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_area_domain_category(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_area_domain_category(integer, integer) is
'Function checks that all area domain categories for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_area_domain_category(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_area_domain_category_attr" schema="analytical" src="functions/etl/fn_etl_check_area_domain_category_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain_category_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_area_domain_category_attr
(
	_export_connection		integer,
	_area_domain_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_category_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_area_domain_category_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _area_domain_category is null
		then
			raise exception 'Error 03: fn_etl_check_area_domain_category_attr: Input argument _area_domain_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_area_domain_category,1)
		loop
			if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
			then
				raise exception 'Error 04: fn_etl_check_area_domain_category_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
			end if;
		end loop;

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_area_domain_category_attr(_export_connection,_area_domain_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) is
'Function checks that all area domain categories for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_area_domain_category_attr(integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_definition_variant" schema="analytical" src="functions/etl/fn_etl_check_definition_variant.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_definition_variant(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_definition_variant
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_definition_variant: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_definition_variant: Input argument _ldsity must not be NULL!';
		end if;
	
		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_definition_variant: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_definition_variant: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_definition_variant(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_definition_variant(integer, integer) is
'Function checks that all definition variants for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_definition_variant(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_definition_variant(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_definition_variant(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_definition_variant(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_definition_variant(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_ldsities4object" schema="analytical" src="functions/etl/fn_etl_check_ldsities4object.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_ldsities4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_ldsities4object(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_ldsities4object
(
	_export_connection	integer,
	_ldsity_object		integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_ldsities4object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_check_ldsities4object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_ldsities4object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_check_ldsities4object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_ldsities4object(_export_connection,_ldsity_object,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_ldsities4object(integer, integer) is
'Function checks that all ldsities for given input ldsity object was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_ldsities4object(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_ldsities4object(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_ldsities4object(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_ldsities4object(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_ldsities4object(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_ldsity2panel_refyearset_version" schema="analytical" src="functions/etl/fn_etl_check_ldsity2panel_refyearset_version.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_ldsity2panel_refyearset_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_ldsity2panel_refyearset_version
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_ldsity2panel_refyearset_version: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_ldsity2panel_refyearset_version: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_ldsity2panel_refyearset_version: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_ldsity2panel_refyearset_version: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_ldsity2panel_refyearset_version(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) is
'Function checks that all combinations of mapping between ldsity and panel, refyearsets and versions for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_ldsity2panel_refyearset_version(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_spc2classification_rule" schema="analytical" src="functions/etl/fn_etl_check_spc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_spc2classification_rule(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_spc2classification_rule
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_spc2classification_rule: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_spc2classification_rule: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_spc2classification_rule: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_spc2classification_rule: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_spc2classification_rule(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_spc2classification_rule(integer, integer) is
'Function checks that all sub population classification rules for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_spc2classification_rule(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_spc2classification_rule_attr" schema="analytical" src="functions/etl/fn_etl_check_spc2classification_rule_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_spc2classification_rule_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_spc2classification_rule_attr
(
	_export_connection			integer,
	_ldsity_object				integer,
	_sub_population_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_spc2classification_rule_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_spc2classification_rule_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_check_spc2classification_rule_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_check_spc2classification_rule_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _sub_population_category is null
		then
			raise exception 'Error 05: fn_etl_check_spc2classification_rule_attr: Input argument _sub_population_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_sub_population_category,1)
		loop
			if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
			then
				raise exception 'Error 06: fn_etl_check_spc2classification_rule_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
			end if;
		end loop;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_spc2classification_rule_attr(_export_connection,_ldsity_object,_sub_population_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) is
'Function checks that all sub population classification rules for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_spc2classification_rule_attr(integer, integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_spc2classrule2panel_refyearset" schema="analytical" src="functions/etl/fn_etl_check_spc2classrule2panel_refyearset.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_spc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_spc2classrule2panel_refyearset
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_spc2classrule2panel_refyearset: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_spc2classrule2panel_refyearset: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_spc2classrule2panel_refyearset: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_spc2classrule2panel_refyearset: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_spc2classrule2panel_refyearset(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) is
'Function checks that all combinations of mapping between sub population classification rules and combinations of panels and reference year sets for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_spc2classrule2panel_refyearset_attr" schema="analytical" src="functions/etl/fn_etl_check_spc2classrule2panel_refyearset_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_spc2classrule2panel_refyearset_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_sub_population_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_spc2classrule2panel_refyearset_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_spc2classrule2panel_refyearset_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_check_spc2classrule2panel_refyearset_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_check_spc2classrule2panel_refyearset_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _sub_population_category is null
		then
			raise exception 'Error 05: fn_etl_check_spc2classrule2panel_refyearset_attr: Input argument _sub_population_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_sub_population_category,1)
		loop
			if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
			then
				raise exception 'Error 06: fn_etl_check_spc2classrule2panel_refyearset_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
			end if;
		end loop;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(_export_connection,_ldsity_object,_sub_population_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) is
'Function checks that all combinations of mapping between sub population classification rules and combinations of panels and reference year sets for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(integer, integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_spc_hierarchy" schema="analytical" src="functions/etl/fn_etl_check_spc_hierarchy.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_spc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_spc_hierarchy(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_spc_hierarchy
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_spc_hierarchy: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_spc_hierarchy: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_spc_hierarchy: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_spc_hierarchy: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_spc_hierarchy(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_spc_hierarchy(integer, integer) is
'Function checks that all sub population classification hierarchies for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_spc_hierarchy(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_spc_hierarchy_attr" schema="analytical" src="functions/etl/fn_etl_check_spc_hierarchy_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_spc_hierarchy_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_spc_hierarchy_attr
(
	_export_connection			integer,
	_sub_population_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_spc_hierarchy_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_spc_hierarchy_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is null
		then
			raise exception 'Error 03: fn_etl_check_spc_hierarchy_attr: Input argument _sub_population_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_sub_population_category,1)
		loop
			if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
			then
				raise exception 'Error 04: fn_etl_check_spc_hierarchy_attr: Given area domain category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
			end if;
		end loop;

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_spc_hierarchy_attr(_export_connection,_sub_population_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) is
'Function checks that all sub population classification hierarchies for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_spc_hierarchy_attr(integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_sub_population" schema="analytical" src="functions/etl/fn_etl_check_sub_population.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_sub_population(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_sub_population
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_sub_population: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_sub_population: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_sub_population(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_sub_population(integer, integer) is
'Function checks that all sub populations for given input arguments was ETLed (TRUE) or not (FALSE).';

grant execute on function analytical.fn_etl_check_sub_population(integer, integer) to public;

alter function analytical.fn_etl_check_sub_population(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_sub_population(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_sub_population(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_sub_population_attr" schema="analytical" src="functions/etl/fn_etl_check_sub_population_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_sub_population_attr(integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_sub_population_attr
(
	_export_connection			integer,
	_sub_population_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_sub_population_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is null
		then
			raise exception 'Error 03: fn_etl_check_sub_population_attr: Input argument _sub_population_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_sub_population_category,1)
		loop
			if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
			then
				raise exception 'Error 04: fn_etl_check_sub_population_attr: Given area domain category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
			end if;
		end loop;

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_sub_population_attr(_export_connection,_sub_population_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_sub_population_attr(integer, integer[]) is
'Function checks that all sub populations for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_sub_population_attr(integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population_attr(integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population_attr(integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_sub_population_attr(integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_sub_population_attr(integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_sub_population_category" schema="analytical" src="functions/etl/fn_etl_check_sub_population_category.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_sub_population_category(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_sub_population_category
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_sub_population_category: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_sub_population_category: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_sub_population_category(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_sub_population_category(integer, integer) is
'Function checks that all sub population categories for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_sub_population_category(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_sub_population_category_attr" schema="analytical" src="functions/etl/fn_etl_check_sub_population_category_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population_category_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_sub_population_category_attr
(
	_export_connection		integer,
	_sub_population_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is null
		then
			raise exception 'Error 03: fn_etl_check_sub_population_category_attr: Input argument _sub_population_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_sub_population_category,1)
		loop
			if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
			then
				raise exception 'Error 04: fn_etl_check_sub_population_category_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
			end if;
		end loop;

		if	(
			select count(*) > 0
			from analytical.fn_etl_get_sub_population_category_attr(_export_connection,_sub_population_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) is
'Function checks that all area domain categories for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_sub_population_category_attr(integer, integer[]) to public;


-- </function>

-- <function name="fn_etl_check_unit_of_measure" schema="analytical" src="functions/etl/fn_etl_check_unit_of_measure.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_unit_of_measure(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_unit_of_measure
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_unit_of_measure: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_unit_of_measure: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_unit_of_measure: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_unit_of_measure: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_unit_of_measure(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_unit_of_measure(integer, integer) is
'Function checks that unit of measure for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_unit_of_measure(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_unit_of_measure(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_unit_of_measure(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_unit_of_measure(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_unit_of_measure(integer, integer) to public;


-- </function>

-- <function name="fn_etl_check_version" schema="analytical" src="functions/etl/fn_etl_check_version.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_version(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_version
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_version: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_version: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_version: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_version: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_version(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_version(integer, integer) is
'Function checks that all versions for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_version(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to public;


-- </function>

-- <function name="fn_etl_delete_area_domain" schema="analytical" src="functions/etl/fn_etl_delete_area_domain.sql">
--------------------------------------------------------------------------------
-- fn_etl_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_delete_area_domain(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN
	IF NOT EXISTS(SELECT * FROM analytical.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (id = %).', _id;
	END IF;

	-- delete ETL of some area_domain is possible only when it is not a part of dependent hierarchy
	IF EXISTS (	SELECT * 
			FROM analytical.t_adc_hierarchy AS t1
			WHERE 	variable_superior IN (	SELECT id FROM analytical.c_area_domain_category
							WHERE area_domain = _id) AND
				dependent = true
		)
	THEN 
		RAISE WARNING 'Given area domain have some categories present in table t_adc_hierarchy in superior position. It can destroy the dependency between the categories. This will not allow You to delete the area domain until you delete the inferior categories (area domains) first.';
	END IF;

	DELETE FROM analytical.t_etl_adc2classrule2panel_refyearset 
	WHERE adc2classrule2panel_refyearset IN 
		(SELECT id FROM analytical.cm_adc2classrule2panel_refyearset
		WHERE adc2classification_rule IN (
			SELECT id FROM analytical.cm_adc2classification_rule
			WHERE area_domain_category IN
				(SELECT id FROM analytical.c_area_domain_category 
					WHERE area_domain = _id)
						)
		); 

	DELETE FROM analytical.t_etl_adc2classification_rule
	WHERE adc2classification_rule IN
		(SELECT id FROM analytical.cm_adc2classification_rule 
		WHERE area_domain_category IN 
			(SELECT id FROM analytical.c_area_domain_category 
			WHERE area_domain = _id)
		);

	DELETE FROM analytical.t_etl_adc_hierarchy
	WHERE adc_hierarchy IN 
		(SELECT id FROM analytical.t_adc_hierarchy
		WHERE	variable IN (	SELECT id FROM analytical.c_area_domain_category
					WHERE area_domain = _id) OR
			variable_superior IN (	SELECT id FROM analytical.c_area_domain_category
						WHERE area_domain = _id)
		);

	DELETE FROM analytical.t_etl_area_domain_category
	WHERE area_domain_category IN 
		(SELECT id FROM analytical.c_area_domain_category 
		WHERE area_domain = _id);
	
	DELETE FROM analytical.t_etl_area_domain
	WHERE area_domain IN 
		(SELECT id FROM analytical.c_area_domain 
		WHERE id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION analytical.fn_etl_delete_area_domain(integer) TO public;

-- </function>

-- <function name="fn_etl_delete_export_connection" schema="analytical" src="functions/etl/fn_etl_delete_export_connection.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_delete_export_connection(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_export_connection: Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.t_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_export_connection: Given export connection (%) does not exist in t_export_connection table.', _id;
	END IF;
	
	DELETE FROM analytical.t_export_connection 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_delete_export_connection(integer) IS
'The function deletes a record from t_export_connection table.';

alter function analytical.fn_etl_delete_export_connection(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to adm_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to data_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to usr_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to public;
-- </function>

-- <function name="fn_etl_delete_sub_population" schema="analytical" src="functions/etl/fn_etl_delete_sub_population.sql">
--------------------------------------------------------------------------------
-- fn_etl_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_delete_sub_population(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_delete_sub_population(_id integer)
RETURNS void
AS
$$
BEGIN
	IF NOT EXISTS(SELECT * FROM analytical.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (id = %).', _id;
	END IF;

	-- delete ETL of some sub_population is possible only when it is not a part of dependent hierarchy
	IF EXISTS (	SELECT * 
			FROM analytical.t_spc_hierarchy AS t1
			WHERE 	variable_superior IN (	SELECT id FROM analytical.c_sub_population_category
							WHERE sub_population = _id) AND
				dependent = true
		)
	THEN 
		RAISE WARNING 'Given sub population have some categories present in table t_spc_hierarchy in superior position. It can destroy the dependency between the categories. This will not allow You to delete the sub population until you delete the inferior categories (sub populations) first.';
	END IF;

	DELETE FROM analytical.t_etl_spc2classrule2panel_refyearset 
	WHERE spc2classrule2panel_refyearset IN 
		(SELECT id FROM analytical.cm_spc2classrule2panel_refyearset
		WHERE spc2classification_rule IN (
			SELECT id FROM analytical.cm_spc2classification_rule
			WHERE sub_population_category IN
				(SELECT id FROM analytical.c_sub_population_category 
					WHERE sub_population = _id)
						)
		); 

	DELETE FROM analytical.t_etl_spc2classification_rule
	WHERE spc2classification_rule IN
		(SELECT id FROM analytical.cm_spc2classification_rule 
		WHERE sub_population_category IN 
			(SELECT id FROM analytical.c_sub_population_category 
			WHERE sub_population = _id)
		);

	DELETE FROM analytical.t_etl_spc_hierarchy
	WHERE spc_hierarchy IN 
		(SELECT id FROM analytical.t_spc_hierarchy
		WHERE	variable IN (	SELECT id FROM analytical.c_sub_population_category
					WHERE sub_population = _id) OR
			variable_superior IN (	SELECT id FROM analytical.c_sub_population_category
						WHERE sub_population = _id)
		);

	DELETE FROM analytical.t_etl_sub_population_category
	WHERE sub_population_category IN 
		(SELECT id FROM analytical.c_sub_population_category 
		WHERE sub_population = _id);
	
	DELETE FROM analytical.t_etl_sub_population
	WHERE sub_population IN 
		(SELECT id FROM analytical.c_sub_population 
		WHERE id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_delete_sub_population(integer) IS
'Function deletes records from c_sub_population, c_sub_population_category, cm_spc2classification_rule and cm_spc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION analytical.fn_etl_delete_sub_population(integer) TO public;

-- </function>

-- <function name="fn_etl_get_ad_attributes4ldsity_object" schema="analytical" src="functions/etl/fn_etl_get_ad_attributes4ldsity_object.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ad_attributes4ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_ad_attributes4ldsity_object
(
	_export_connection	integer,
	_ldsity_object		integer,
	_area_domain		integer[] default null::integer[],
	_check_attr			boolean default null::boolean		
)
returns table
(
	group_id									integer,
	lo__id										integer,
	lo__label									varchar,
	lo__description								text,
	lo__label_en								varchar,
	lo__description_en							text,
	aop__id										integer,
	aop__label									varchar,
	aop__description							text,
	ad__id										integer,
	ad__label									varchar,
	ad__description								text,
	ad__label_en								varchar,
	ad__description_en							text,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	adc__id										integer[],
	adc__label									varchar[],
	adc__description							text[],
	adc__label_en								varchar[],
	adc__description_en							text[],
	rule__id									integer[],
	rule__area_domain_category					integer[],
	rule__classification_rule					text[],
	check__area_domain							boolean,
	check__area_domain_category					boolean,
	check__adc_hierarchy						boolean,
	check__adc2classification_rule				boolean,
	check__adc2classrule2panel_refyearset		boolean,
	check__attr									boolean
)
as
$$
declare
		_cond_1	text;
		_cond_2	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ad_attributes4ldsity_object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_ad_attributes4ldsity_object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ad_attributes4ldsity_object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_ad_attributes4ldsity_object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if _area_domain is not null
		then
			for i in 1..array_length(_area_domain,1)
			loop
				if not exists (select t3.* from analytical.c_area_domain as t3 where t3.id = _area_domain[i])
				then
					raise exception 'Error 05: fn_etl_get_ad_attributes4ldsity_object: Given area domain (%) does not exist in c_area_domain table.', _area_domain[i];
				end if;
			end loop;
		end if;
	
		if _area_domain is not null and _check_attr is distinct from false
		then
			raise exception 'Error 06: fn_etl_get_ad_attributes4ldsity_object: If input argument _area_domain is not null then input argument _check_attr must be FALSE!';
		end if;		
		
		if _area_domain is not null
		then
			_cond_1 := 'w4.ad__id in (select unnest($3))';
		else
			_cond_1 := 'TRUE';
		end if;	
	
		if _check_attr is null
		then
			_cond_2 := 'TRUE';
		else
			if _check_attr = true
			then
				_cond_2 := 'w4.check__attr = true';
			else
				_cond_2 := 'w4.check__attr = false';
			end if;
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select * from analytical.fn_etl_get_area_domain_informations_attr($1,$2)
				)
		,w2 as	(
				select		
						w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.ad__id,
						w1.ad__label,
						w1.ad__description,
						w1.ad__label_en,
						w1.ad__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id,
						array_agg(w1.adc__id order by w1.rule__id) as adc__id,
						array_agg(w1.adc__label order by w1.rule__id) as adc__label,
						array_agg(w1.adc__description order by w1.rule__id) as adc__description,
						array_agg(w1.adc__label_en order by w1.rule__id) as adc__label_en,
						array_agg(w1.adc__description_en order by w1.rule__id) as adc__description_en,
						array_agg(w1.rule__id order by w1.rule__id) as rule__id,
						array_agg(w1.rule__area_domain_category order by w1.rule__id) as rule__area_domain_category,
						array_agg(w1.rule__classification_rule order by w1.rule__id) as rule__classification_rule
				from
						w1
				group
				by		w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.ad__id,
						w1.ad__label,
						w1.ad__description,
						w1.ad__label_en,
						w1.ad__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id
				)
		,w3 as	(
				select
						row_number() over() as group_id,
						w2.*,
						analytical.fn_etl_check_area_domain_attr($1,w2.adc__id)									as check__area_domain,
						analytical.fn_etl_check_area_domain_category_attr($1,w2.adc__id)						as check__area_domain_category,
						analytical.fn_etl_check_adc_hierarchy_attr($1,w2.adc__id)								as check__adc_hierarchy,
						analytical.fn_etl_check_adc2classification_rule_attr($1,w2.lo__id,w2.adc__id)			as check__adc2classification_rule,
						analytical.fn_etl_check_adc2classrule2panel_refyearset_attr($1,w2.lo__id,w2.adc__id)	as check__adc2classrule2panel_refyearset
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						case
							when
								w3.check__area_domain = true and
								w3.check__area_domain_category = true and
								w3.check__adc_hierarchy = true and
								w3.check__adc2classification_rule = true and
								w3.check__adc2classrule2panel_refyearset = true
							then
								true
							else
								false
						end as check__attr
							
				from
						w3
				)
		select
				w4.group_id::integer,
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.ad__id,
				w4.ad__label,
				w4.ad__description,
				w4.ad__label_en,
				w4.ad__description_en,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.adc__id,
				w4.adc__label,
				w4.adc__description,
				w4.adc__label_en,
				w4.adc__description_en,
				w4.rule__id,
				w4.rule__area_domain_category,
				w4.rule__classification_rule,
				w4.check__area_domain,
				w4.check__area_domain_category,
				w4.check__adc_hierarchy,
				w4.check__adc2classification_rule,
				w4.check__adc2classrule2panel_refyearset,
				w4.check__attr				
		from
				w4
		where
				'|| _cond_1 ||'
		and
				'|| _cond_2 ||'
		order
				by w4.group_id;
		'
		using _export_connection, _ldsity_object, _area_domain;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) is
'Function returns information of area domain attributes for given ldsity object.';

alter function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_adc2classification_rule" schema="analytical" src="functions/etl/fn_etl_get_adc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classification_rule
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_area_domain_category__id							integer,
	etl_area_domain_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_adc2classification_rule__id							integer,
	etl_adc2classification_rule__etl_id						integer,
	cm_adc2classrule2panel_refyearset__id					integer,
	cm_adc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond_1		text;
		_cond_2		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classification_rule: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_adc2classification_rule: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_adc2classification_rule: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_adc2classification_rule: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;		
	
		if _etl is null
		then
			_cond_1 := 'TRUE';
			_cond_2 := 'TRUE';
		else
			if _etl = true
			then
				_cond_1 := 'w8.etl_adc2classification_rule__etl_id is not null';
				_cond_2 := 'w10.etl_adc2classification_rule__etl_id is not null';
			else
				_cond_1 := 'w8.etl_adc2classification_rule__etl_id is null';
				_cond_2 := 'w10.etl_adc2classification_rule__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with 
		w1 as	(select * from analytical.fn_etl_get_area_domain_informations($1,$2))
		,w2 as 	(select distinct adc2classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id from w1)
		-----------------------------------------------
		,w3 as	(
				select
						w2.etl_area_domain_category__id,
						w2.etl_ldsity_object__id,
						teadc.area_domain_category,
						telo.ldsity_object
				from	w2
						inner join analytical.t_etl_area_domain_category as teadc on w2.etl_area_domain_category__id = teadc.id
						inner join analytical.t_etl_ldsity_object as telo on w2.etl_ldsity_object__id = telo.id
				)
		,w4 as	(		
				select distinct t1.ldsity_object, t4.id from
				(select w3.area_domain_category, w3.ldsity_object from w3) as t1
				inner join analytical.t_adc_hierarchy as t2 on t1.area_domain_category = t2.variable
				inner join analytical.c_area_domain_category as t3 on t2.variable_superior = t3.id
				inner join analytical.c_area_domain_category as t4 on t3.area_domain = t4.area_domain 
				)
		,w5 as	(
				select
						cacr.id as adc2classification_rule,
						cacr.classification_rule,
						cacr.area_domain_category,
						cacr.ldsity_object
				from w4
				inner join analytical.cm_adc2classification_rule as cacr 
				on w4.id = cacr.area_domain_category and w4.ldsity_object = cacr.ldsity_object
				)
		,w6 as	(
				select					
						w5.ldsity_object,
						cadc.area_domain,
						w5.area_domain_category,
						w5.adc2classification_rule,
						w5.classification_rule,
						telo.id as etl_ldsity_object__id,
						telo.etl_id as etl_ldsity_object__etl_id,
						tead.id as etl_area_domain__id,
						tead.etl_id as etl_area_domain__etl_id
				from
						w5
						
						inner join	analytical.c_area_domain_category as cadc on w5.area_domain_category = cadc.id
						inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
									on w5.ldsity_object = telo.ldsity_object
						inner join	(select * from analytical.t_etl_area_domain where export_connection = $1) as tead
									on cadc.area_domain = tead.area_domain
				)
		,w7 as	(
				select
						w6.*,
						teadc.id as etl_area_domain_category__id,
						teadc.etl_id as etl_area_domain_category__etl_id
				from
						w6
						inner join	(
									select * from analytical.t_etl_area_domain_category
									where etl_area_domain in (select etl_area_domain__id from w6)
									) as teadc
									on w6.area_domain_category = teadc.area_domain_category
				)
		,w8 as	(
				select
						w7.adc2classification_rule as id,
						w7.classification_rule,
						w7.etl_area_domain_category__id,
						w7.etl_area_domain_category__etl_id,
						w7.etl_ldsity_object__id,
						w7.etl_ldsity_object__etl_id,
						teacr.id as etl_adc2classification_rule__id,
						teacr.etl_id as etl_adc2classification_rule__etl_id,
						analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w7.adc2classification_rule) as res
				from 	w7
						left join analytical.t_etl_adc2classification_rule as teacr
						on w7.adc2classification_rule = teacr.adc2classification_rule
						and w7.etl_area_domain_category__id = teacr.etl_area_domain_category
						and w7.etl_ldsity_object__id = teacr.etl_ldsity_object
				)
		-----------------------------------------------
		,w9 as	(
				select
						w2.adc2classification_rule as id,
						cmadc.classification_rule,
						w2.etl_area_domain_category__id,
						w2.etl_area_domain_category__etl_id,
						w2.etl_ldsity_object__id,
						w2.etl_ldsity_object__etl_id,
						w2.etl_adc2classification_rule__id,
						w2.etl_adc2classification_rule__etl_id
				from
						w2 inner join analytical.cm_adc2classification_rule as cmadc on w2.adc2classification_rule = cmadc.id
				)
		,w10 as	(
				select
						w9.id,
						w9.classification_rule,
						w9.etl_area_domain_category__id,
						w9.etl_area_domain_category__etl_id,
						w9.etl_ldsity_object__id,
						w9.etl_ldsity_object__etl_id,
						w9.etl_adc2classification_rule__id,
						w9.etl_adc2classification_rule__etl_id,
						analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w9.id) as res
				from
						w9
				order
						by w9.id
				)
		-----------------------------------------------
		select
				w8.id,
				w8.classification_rule,
				w8.etl_area_domain_category__id,
				w8.etl_area_domain_category__etl_id,
				w8.etl_ldsity_object__id,
				w8.etl_ldsity_object__etl_id,
				w8.etl_adc2classification_rule__id,
				w8.etl_adc2classification_rule__etl_id,
				(w8.res).cm_adc2classrule2panel_refyearset__id,
				(w8.res).cm_adc2classrule2panel_refyearset__refyearset2panel
		from
				w8 where '|| _cond_1 ||'
		union
		select
				w10.id,
				w10.classification_rule,
				w10.etl_area_domain_category__id,
				w10.etl_area_domain_category__etl_id,
				w10.etl_ldsity_object__id,
				w10.etl_ldsity_object__etl_id,
				w10.etl_adc2classification_rule__id,
				w10.etl_adc2classification_rule__etl_id,
				(w10.res).cm_adc2classrule2panel_refyearset__id,
				(w10.res).cm_adc2classrule2panel_refyearset__refyearset2panel
		from
				w10 where '|| _cond_2 ||'
		order
				by id;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) is
'Function returns information records of area domain categories and rules.';

alter function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_adc2classification_rule_attr" schema="analytical" src="functions/etl/fn_etl_get_adc2classification_rule_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classification_rule_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classification_rule_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_area_domain_category__id							integer,
	etl_area_domain_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_adc2classification_rule__id							integer,
	etl_adc2classification_rule__etl_id						integer,
	cm_adc2classrule2panel_refyearset__id					integer,
	cm_adc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classification_rule_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_adc2classification_rule_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_adc2classification_rule_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_adc2classification_rule_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is not null
		then
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 05: fn_etl_get_adc2classification_rule_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_adc2classification_rule_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;		

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_adc2classification_rule__etl_id is not null';
			else
				_cond := 'w3.etl_adc2classification_rule__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_area_domain_category_attr($1,null::integer[],true)
					)
			,w2 as	(
					select
							w1.*,
							t.id as adc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_adc2classification_rule
										where area_domain_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.area_domain_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tear.id as etl_adc2classification_rule__id,
							tear.etl_id as etl_adc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w2.adc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_adc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_area_domain_category in (select w2.etl_area_domain_category__id from w2)
										and adc2classification_rule in (select w2.adc2classification_rule from w2)
										) as tear
							on w2.etl_ldsity_object__id = tear.etl_ldsity_object
							and w2.etl_area_domain_category__id = tear.etl_area_domain_category
							and w2.adc2classification_rule = tear.adc2classification_rule
					)
			select
					distinct
					w3.adc2classification_rule,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					(w3.res).cm_adc2classrule2panel_refyearset__id,
					(w3.res).cm_adc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.adc2classification_rule;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_area_domain_category_attr($1,$3,null::boolean)
					)
			,w2 as	(
					select
							w1.*,
							t.id as adc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_adc2classification_rule
										where area_domain_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.area_domain_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tear.id as etl_adc2classification_rule__id,
							tear.etl_id as etl_adc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w2.adc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_adc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_area_domain_category in (select w2.etl_area_domain_category__id from w2)
										and adc2classification_rule in (select w2.adc2classification_rule from w2)
										) as tear
							on w2.etl_ldsity_object__id = tear.etl_ldsity_object
							and w2.etl_area_domain_category__id = tear.etl_area_domain_category
							and w2.adc2classification_rule = tear.adc2classification_rule
					)
			select
					w3.adc2classification_rule,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					(w3.res).cm_adc2classrule2panel_refyearset__id,
					(w3.res).cm_adc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.adc2classification_rule;
			'
			using _export_connection, _ldsity_object, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) is
'Function returns information records of area domain categories and rules for attributes.';

alter function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_adc2classrule2panel_refyearset" schema="analytical" src="functions/etl/fn_etl_get_adc2classrule2panel_refyearset.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classrule2panel_refyearset
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id											integer,
	classification_rule							text,
	etl_area_domain_category__id				integer,
	etl_area_domain_category__etl_id			integer,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	etl_adc2classification_rule__id				integer,
	etl_adc2classification_rule__etl_id			integer,
	adc2classrule2panel_refyearset				integer,
	refyearset2panel							integer,
	etl_adc2classrule2panel_refyearset__id		integer,
	etl_adc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classrule2panel_refyearset: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_adc2classrule2panel_refyearset: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_adc2classrule2panel_refyearset: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_adc2classrule2panel_refyearset: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_adc2classrule2panel_refyearset__etl_id is not null';
			else
				_cond := 'w3.etl_adc2classrule2panel_refyearset__etl_id is null';
			end if;
		end if;

		return query execute
		'
		with
		w1 as	(select * from analytical.fn_etl_get_adc2classification_rule($1,$2,null::boolean))
		,w2 as	(
				select
						w1.id,
						w1.classification_rule,
						w1.etl_area_domain_category__id,
						w1.etl_area_domain_category__etl_id,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id,
						w1.etl_adc2classification_rule__id,
						w1.etl_adc2classification_rule__etl_id,
						t.id as adc2classrule2panel_refyearset,
						--t.adc2classification_rule,
						t.refyearset2panel
				from	w1
				inner join	analytical.cm_adc2classrule2panel_refyearset as t
							on w1.id = t.adc2classification_rule
				)
		,w3 as	(
				select
						w2.*,
						teacpr.id as etl_adc2classrule2panel_refyearset__id,
						teacpr.etl_id as etl_adc2classrule2panel_refyearset__etl_id
				from
						w2
				left join	analytical.t_etl_adc2classrule2panel_refyearset as teacpr
							on w2.adc2classrule2panel_refyearset = teacpr.adc2classrule2panel_refyearset
							and w2.etl_adc2classification_rule__id = teacpr.etl_adc2classification_rule
				)
		select
				w3.id,
				w3.classification_rule,
				w3.etl_area_domain_category__id,
				w3.etl_area_domain_category__etl_id,
				w3.etl_ldsity_object__id,
				w3.etl_ldsity_object__etl_id,
				w3.etl_adc2classification_rule__id,
				w3.etl_adc2classification_rule__etl_id,
				w3.adc2classrule2panel_refyearset,
				w3.refyearset2panel,
				w3.etl_adc2classrule2panel_refyearset__id,
				w3.etl_adc2classrule2panel_refyearset__etl_id
		from
				w3 where '|| _cond ||'
		order
				by w3.id, w3.refyearset2panel;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) is
'Function returns information records of mapping between area domain classification rules and combinations of panels and reference year sets.';

alter function analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_adc2classrule2panel_refyearset_attr" schema="analytical" src="functions/etl/fn_etl_get_adc2classrule2panel_refyearset_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classrule2panel_refyearset_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id											integer,
	classification_rule							text,
	etl_area_domain_category__id				integer,
	etl_area_domain_category__etl_id			integer,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	etl_adc2classification_rule__id				integer,
	etl_adc2classification_rule__etl_id			integer,
	adc2classrule2panel_refyearset				integer,
	refyearset2panel							integer,
	etl_adc2classrule2panel_refyearset__id		integer,
	etl_adc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classrule2panel_refyearset_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_adc2classrule2panel_refyearset_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_adc2classrule2panel_refyearset_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_adc2classrule2panel_refyearset_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is not null
		then	
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 05: fn_etl_get_adc2classrule2panel_refyearset_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_adc2classrule2panel_refyearset_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;		
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_adc2classrule2panel_refyearset__etl_id is not null';
			else
				_cond := 'w3.etl_adc2classrule2panel_refyearset__etl_id is null';
			end if;
		end if;

		if _area_domain_category is null
		then
			return query execute
			'	
			with
			w1 as	(select * from analytical.fn_etl_get_adc2classification_rule_attr($1,$2,null::integer[],true))
			,w2 as	(
					select
							w1.id,
							w1.classification_rule,
							w1.etl_area_domain_category__id,
							w1.etl_area_domain_category__etl_id,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
							w1.etl_adc2classification_rule__id,
							w1.etl_adc2classification_rule__etl_id,
							t.id as adc2classrule2panel_refyearset,
							t.refyearset2panel
					from	w1
					inner join	analytical.cm_adc2classrule2panel_refyearset as t
								on w1.id = t.adc2classification_rule
					)
			,w3 as	(
					select
							w2.*,
							teacpr.id as etl_adc2classrule2panel_refyearset__id,
							teacpr.etl_id as etl_adc2classrule2panel_refyearset__etl_id
					from
							w2
					left join	analytical.t_etl_adc2classrule2panel_refyearset as teacpr
								on w2.adc2classrule2panel_refyearset = teacpr.adc2classrule2panel_refyearset
								and w2.etl_adc2classification_rule__id = teacpr.etl_adc2classification_rule
					)
			select
					distinct
					w3.id,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					w3.adc2classrule2panel_refyearset,
					w3.refyearset2panel,
					w3.etl_adc2classrule2panel_refyearset__id,
					w3.etl_adc2classrule2panel_refyearset__etl_id
			from
					w3 where '|| _cond ||'
			order
					by w3.id, w3.refyearset2panel;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_adc2classification_rule_attr($1,$2,$3,null::boolean))
			,w2 as	(
					select
							w1.id,
							w1.classification_rule,
							w1.etl_area_domain_category__id,
							w1.etl_area_domain_category__etl_id,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
							w1.etl_adc2classification_rule__id,
							w1.etl_adc2classification_rule__etl_id,
							t.id as adc2classrule2panel_refyearset,
							t.refyearset2panel
					from	w1
					inner join	analytical.cm_adc2classrule2panel_refyearset as t
								on w1.id = t.adc2classification_rule
					)
			,w3 as	(
					select
							w2.*,
							teacpr.id as etl_adc2classrule2panel_refyearset__id,
							teacpr.etl_id as etl_adc2classrule2panel_refyearset__etl_id
					from
							w2
					left join	analytical.t_etl_adc2classrule2panel_refyearset as teacpr
								on w2.adc2classrule2panel_refyearset = teacpr.adc2classrule2panel_refyearset
								and w2.etl_adc2classification_rule__id = teacpr.etl_adc2classification_rule
					)
			select
					w3.id,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					w3.adc2classrule2panel_refyearset,
					w3.refyearset2panel,
					w3.etl_adc2classrule2panel_refyearset__id,
					w3.etl_adc2classrule2panel_refyearset__etl_id
			from
					w3 where '|| _cond ||'
			order
					by w3.id, w3.refyearset2panel;
			'
			using _export_connection, _ldsity_object, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) is
'Function returns information records of mapping between area domain classification rules and combinations of panels and reference year sets for attributes.';

alter function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer,integer[],  boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer,integer[],  boolean) to public;


-- </function>

-- <function name="fn_etl_get_adc_hierarchy" schema="analytical" src="functions/etl/fn_etl_get_adc_hierarchy.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc_hierarchy
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id													integer,
	variable_superior									integer,
	variable											integer,
	dependent											boolean,
	etl_area_domain_category__id__variable_superior		integer,
	etl_area_domain_category__etl_id__variable_superior	integer,
	etl_area_domain_category__id__variable				integer,
	etl_area_domain_category__etl_id__variable			integer,
	etl_adc_hierarchy__id								integer,
	etl_adc_hierarchy__etl_id							integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc_hierarchy: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_adc_hierarchy: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_adc_hierarchy: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_adc_hierarchy: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w4.etl_adc_hierarchy__etl_id is not null';
			else
				_cond := 'w4.etl_adc_hierarchy__etl_id is null';
			end if;
		end if;

		return query execute
		'
		with
		w1 as	(select * from analytical.fn_etl_get_area_domain_category($1,$2,null::boolean) where variable_superior = false)	-- variable
		,w2 as	(select * from analytical.fn_etl_get_area_domain_category($1,$2,null::boolean) where variable_superior = true)	-- variable superior
		,w3 as	(
				select
						tah.id as adc_hierarchy,
						tah.variable_superior,
						tah.variable,
						tah.dependent,
						w2.etl_area_domain_category__id as etl_area_domain_category__id__variable_superior,
						w2.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable_superior,
						w1.etl_area_domain_category__id as etl_area_domain_category__id__variable,
						w1.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable
				from
						analytical.t_adc_hierarchy as tah
						inner join	w1 on tah.variable = w1.id
						inner join	w2 on tah.variable_superior = w2.id
						where dependent = true
				)
		,w4 as	(
				select
						w3.adc_hierarchy,
						w3.variable_superior,
						w3.variable,
						w3.dependent,
						w3.etl_area_domain_category__id__variable_superior,
						w3.etl_area_domain_category__etl_id__variable_superior,
						w3.etl_area_domain_category__id__variable,
						w3.etl_area_domain_category__etl_id__variable,
						teah.id as etl_adc_hierarchy__id,
						teah.etl_id as etl_adc_hierarchy__etl_id
				from
						w3
				left join	analytical.t_etl_adc_hierarchy as teah
							on w3.adc_hierarchy = teah.adc_hierarchy
							and w3.etl_area_domain_category__id__variable_superior = teah.etl_area_domain_category_variable_superior
							and w3.etl_area_domain_category__id__variable = teah.etl_area_domain_category_variable
				)
		select
				w4.adc_hierarchy as id,
				w4.variable_superior,
				w4.variable,
				w4.dependent,
				w4.etl_area_domain_category__id__variable_superior,
				w4.etl_area_domain_category__etl_id__variable_superior,
				w4.etl_area_domain_category__id__variable,
				w4.etl_area_domain_category__etl_id__variable,
				w4.etl_adc_hierarchy__id,
				w4.etl_adc_hierarchy__etl_id
		from
				w4 where '|| _cond ||'
		order
				by w4.adc_hierarchy;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) is
'Function returns information records of area domain categories hierarchy.';

alter function analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_adc_hierarchy_attr" schema="analytical" src="functions/etl/fn_etl_get_adc_hierarchy_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc_hierarchy_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc_hierarchy_attr
(
	_export_connection		integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id													integer,
	variable_superior									integer,
	variable											integer,
	dependent											boolean,
	etl_area_domain_category__id__variable_superior		integer,
	etl_area_domain_category__etl_id__variable_superior	integer,
	etl_area_domain_category__id__variable				integer,
	etl_area_domain_category__etl_id__variable			integer,
	etl_adc_hierarchy__id								integer,
	etl_adc_hierarchy__etl_id							integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc_hierarchy_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_adc_hierarchy_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _area_domain_category is not null
		then	
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 03: fn_etl_get_adc_hierarchy_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_adc_hierarchy_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w4.etl_adc_hierarchy__etl_id is not null';
			else
				_cond := 'w4.etl_adc_hierarchy__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,null::integer[],true) where variable_superior = false)	-- variable
			,w2 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,null::integer[],true) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tah.id as adc_hierarchy,
							tah.variable_superior,
							tah.variable,
							tah.dependent,
							w2.etl_area_domain_category__id as etl_area_domain_category__id__variable_superior,
							w2.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable_superior,
							w1.etl_area_domain_category__id as etl_area_domain_category__id__variable,
							w1.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable
					from
							analytical.t_adc_hierarchy as tah
							inner join	w1 on tah.variable = w1.id
							inner join	w2 on tah.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.adc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_area_domain_category__id__variable_superior,
							w3.etl_area_domain_category__etl_id__variable_superior,
							w3.etl_area_domain_category__id__variable,
							w3.etl_area_domain_category__etl_id__variable,
							teah.id as etl_adc_hierarchy__id,
							teah.etl_id as etl_adc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_adc_hierarchy as teah
								on w3.adc_hierarchy = teah.adc_hierarchy
								and w3.etl_area_domain_category__id__variable_superior = teah.etl_area_domain_category_variable_superior
								and w3.etl_area_domain_category__id__variable = teah.etl_area_domain_category_variable
					)
			select
					w4.adc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_area_domain_category__id__variable_superior,
					w4.etl_area_domain_category__etl_id__variable_superior,
					w4.etl_area_domain_category__id__variable,
					w4.etl_area_domain_category__etl_id__variable,
					w4.etl_adc_hierarchy__id,
					w4.etl_adc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.adc_hierarchy;
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,$2,null::boolean) where variable_superior = false)	-- variable
			,w2 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,$2,null::boolean) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tah.id as adc_hierarchy,
							tah.variable_superior,
							tah.variable,
							tah.dependent,
							w2.etl_area_domain_category__id as etl_area_domain_category__id__variable_superior,
							w2.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable_superior,
							w1.etl_area_domain_category__id as etl_area_domain_category__id__variable,
							w1.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable
					from
							analytical.t_adc_hierarchy as tah
							inner join	w1 on tah.variable = w1.id
							inner join	w2 on tah.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.adc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_area_domain_category__id__variable_superior,
							w3.etl_area_domain_category__etl_id__variable_superior,
							w3.etl_area_domain_category__id__variable,
							w3.etl_area_domain_category__etl_id__variable,
							teah.id as etl_adc_hierarchy__id,
							teah.etl_id as etl_adc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_adc_hierarchy as teah
								on w3.adc_hierarchy = teah.adc_hierarchy
								and w3.etl_area_domain_category__id__variable_superior = teah.etl_area_domain_category_variable_superior
								and w3.etl_area_domain_category__id__variable = teah.etl_area_domain_category_variable
					)
			select
					w4.adc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_area_domain_category__id__variable_superior,
					w4.etl_area_domain_category__etl_id__variable_superior,
					w4.etl_area_domain_category__id__variable,
					w4.etl_area_domain_category__etl_id__variable,
					w4.etl_adc_hierarchy__id,
					w4.etl_adc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.adc_hierarchy;
			'
			using _export_connection, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories hierarchy for attributes.';

alter function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_area_domain" schema="analytical" src="functions/etl/fn_etl_get_area_domain.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_area_domain
(
	_export_connection	integer,
	_ldsity				integer default null::integer,
	_etl				boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_area_domain__id			integer,
	etl_area_domain__etl_id		integer,
	variable_superior			boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_area_domain: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if _ldsity is not null
		then
			if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
			then
				raise exception 'Error 03: fn_etl_get_area_domain: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
			end if;		
		end if;

		if _ldsity is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_area_domain: If input argument _ldsity is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w6.etl_area_domain__etl_id is not null';
			else
				_cond := 'w6.etl_area_domain__etl_id is null';
			end if;
		end if;
		
		if _ldsity is null
		then
			return query execute
			'
			with
			w1 as	(
					select tead.* from analytical.t_etl_area_domain as tead where tead.export_connection = $1
					)		
			,w2a as	(
					select tah.variable_superior from analytical.t_adc_hierarchy as tah
					where tah.variable_superior in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						) AND dependent = true
					)					
			,w2b as	(
					select tah.variable from analytical.t_adc_hierarchy as tah
					where tah.variable in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						) AND dependent = true
					)
			,w3a as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.area_domain, true as variable_superior from w3a union all
					select w3b.area_domain, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.area_domain, variable_superior from w4 union
					select w1.area_domain, false as variable_superior from w1
					)				
			select
					w5.area_domain as id,
					cad.label,
					cad.description,
					cad.label_en,
					cad.description_en,
					w1.id as etl_area_domain__id,
					w1.etl_id as etl_area_domain__etl_id,
					w5.variable_superior
			from
					w5
					inner join w1 on w5.area_domain = w1.area_domain
					inner join analytical.c_area_domain as cad on w1.area_domain = cad.id
			order
					by w5.area_domain, w5.variable_superior
			'
			using _export_connection;
		else
			return query execute
			'
			with 
			w1 as	(select * from analytical.fn_etl_get_area_domain_informations($1,$2))
			,w2 as 	(select distinct area_domain, etl_area_domain__id, etl_area_domain__etl_id from w1)
			,w3 as	(
					select
							w2.area_domain as id,
							cad.label,
							cad.description,
							cad.label_en,
							cad.description_en,
							w2.etl_area_domain__id,
							w2.etl_area_domain__etl_id
					from
							w2 inner join analytical.c_area_domain cad on w2.area_domain = cad.id
					)
			-----------------------------------------
			-- add type of hierarchy
			,w4 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en
					from
							analytical.c_area_domain
					where
							id in
									(
									select distinct area_domain from analytical.c_area_domain_category where id in			-- finding out of variable superior types
									(select distinct variable_superior from analytical.t_adc_hierarchy where variable in	-- finding out of variable superior categories
									(select id from analytical.c_area_domain_category
									where area_domain in (select distinct w3.id from w3) AND dependent = true))
									)
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_area_domain__id,
							t.etl_id as etl_area_domain__etl_id
					from
							w4
					left join	(select * from analytical.t_etl_area_domain where export_connection = $1) as t
								on w4.id = t.area_domain
					)
			-----------------------------------------
			,w6 as	(
					select
							w3.id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.etl_area_domain__id,
							w3.etl_area_domain__etl_id,
							false as variable_superior
					from
							w3 where w3.id not in (select w5.id from w5)
					union all
					select
							w5.id,
							w5.label,
							w5.description,
							w5.label_en,
							w5.description_en,
							w5.etl_area_domain__id,
							w5.etl_area_domain__etl_id,
							true as variable_superior
					from
							w5
					)
			select
					w6.id,
					w6.label,
					w6.description,
					w6.label_en,
					w6.description_en,
					w6.etl_area_domain__id,
					w6.etl_area_domain__etl_id,
					w6.variable_superior
			from
					w6 where '|| _cond ||'
			order
					by w6.id
			'
			using _export_connection, _ldsity;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain(integer, integer, boolean) is
'Function returns information records of area domains.';

alter function analytical.fn_etl_get_area_domain(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_area_domain_attr" schema="analytical" src="functions/etl/fn_etl_get_area_domain_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_attr
(
	_export_connection		integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_area_domain__id			integer,
	etl_area_domain__etl_id		integer,
	variable_superior			boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_area_domain_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _area_domain_category is not null
		then	
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 03: fn_etl_get_area_domain_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_area_domain_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w5.etl_area_domain__etl_id is not null';
			else
				_cond := 'w5.etl_area_domain__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'	
			with
			w1 as	(
					select tead.* from analytical.t_etl_area_domain as tead where tead.export_connection = $1
					)		
			,w2a as	(
					select tah.variable_superior from analytical.t_adc_hierarchy as tah
					where tah.variable_superior in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						)
					)					
			,w2b as	(
					select tah.variable from analytical.t_adc_hierarchy as tah
					where tah.variable in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						)
					)
			,w3a as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.area_domain, true as variable_superior from w3a union all
					select w3b.area_domain, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.area_domain, variable_superior from w4 union
					select w1.area_domain, false as variable_superior from w1
					)				
			select
					w5.area_domain as id,
					cad.label,
					cad.description,
					cad.label_en,
					cad.description_en,
					w1.id as etl_area_domain__id,
					w1.etl_id as etl_area_domain__etl_id,
					w5.variable_superior
			from
					w5
					inner join w1 on w5.area_domain = w1.area_domain
					inner join analytical.c_area_domain as cad on w1.area_domain = cad.id
			order
					by w5.area_domain, w5.variable_superior
			'
			using _export_connection;		
		else	
			return query execute
			'
			with
			w1 as	(
					select
							cad.id,
							cad.label,
							cad.description,
							cad.label_en,
							cad.description_en
					from
							analytical.c_area_domain cad
					where
							cad.id in	(
										select distinct cadc.area_domain
										from analytical.c_area_domain_category as cadc
										where cadc.id in (select unnest($2))
										)
					)
			,w2 as	(
					select
							w1.*,
							tead.id as etl_area_domain__id,
							tead.etl_id as etl_area_domain__etl_id
					from
							w1
					left
					join	(select * from analytical.t_etl_area_domain where export_connection = $1) as tead
							on w1.id = tead.area_domain
					)
			-----------------------------------------
			-- add type of hierarchy
			,w3 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en
					from
							analytical.c_area_domain
					where
							id in
									(
									select distinct area_domain from analytical.c_area_domain_category where id in			-- finding out of variable superior types
									(select distinct variable_superior from analytical.t_adc_hierarchy where variable in	-- finding out of variable superior categories
									(select id from analytical.c_area_domain_category
									where area_domain in (select distinct w2.id from w2)))
									)
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_area_domain__id,
							t.etl_id as etl_area_domain__etl_id
					from
							w3
					left join	(select * from analytical.t_etl_area_domain where export_connection = $1) as t
								on w3.id = t.area_domain
					)
			-----------------------------------------
			,w5 as	(
					select
							w2.id,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en,
							w2.etl_area_domain__id,
							w2.etl_area_domain__etl_id,
							false as variable_superior
					from
							w2 where w2.id not in (select w4.id from w4)
					union all
					select
							w4.id,
							w4.label,
							w4.description,
							w4.label_en,
							w4.description_en,
							w4.etl_area_domain__id,
							w4.etl_area_domain__etl_id,
							true as variable_superior
					from
							w4
					)
			select
					w5.id,
					w5.label,
					w5.description,
					w5.label_en,
					w5.description_en,
					w5.etl_area_domain__id,
					w5.etl_area_domain__etl_id,
					w5.variable_superior
			from
					w5 where '|| _cond ||'
			order
					by w5.id;
			'
			using _export_connection, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) is
'Function returns information records of area domains for given arguments.';

alter function analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_attr(integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_area_domain_category" schema="analytical" src="functions/etl/fn_etl_get_area_domain_category.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_category(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_category
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id									integer,
	label								varchar,
	description							text,
	label_en							varchar,
	description_en						text,
	etl_area_domain__id					integer,
	etl_area_domain__etl_id				integer,
	etl_area_domain_category__id		integer,
	etl_area_domain_category__etl_id	integer,
	variable_superior					boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category: Input argument _ldsity must not be NULL!';
		end if;
	
		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_area_domain_category: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w8.etl_area_domain_category__etl_id is not null';
			else
				_cond := 'w8.etl_area_domain_category__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with 
		w1 as	(select * from analytical.fn_etl_get_area_domain_informations($1,$2))
		,w2 as 	(select distinct area_domain_category, etl_area_domain__id, etl_area_domain_category__id, etl_area_domain_category__etl_id from w1)
		,w3 as	(
				select
						w2.area_domain_category as id,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en,
						w2.etl_area_domain__id as etl_area_domain,
						w2.etl_area_domain_category__id,
						w2.etl_area_domain_category__etl_id
				from
						w2 inner join analytical.c_area_domain_category as cadc on w2.area_domain_category = cadc.id
				)
		-----------------------------------------
		-- add CATEGORY from hierarchy
		,w4 as	(
				select
						id,
						label,
						description,
						label_en,
						description_en,
						area_domain
				from analytical.c_area_domain_category where area_domain in
				(select distinct area_domain from analytical.c_area_domain_category where id in
				(select distinct variable_superior from analytical.t_adc_hierarchy
				where variable in (select distinct w3.id from w3) AND dependent = true))
				)
		,w5 as	(
				select w4.*, t.id as etl_area_domain from w4
				left join
					(
					select * from analytical.t_etl_area_domain
					where export_connection = $1 and area_domain in (select w4.area_domain from w4)
					) as t
				on w4.area_domain = t.area_domain
				)
		,w6 as	(
				select
						w5.id,
						w5.label,
						w5.description,
						w5.label_en,
						w5.description_en,
						w5.etl_area_domain,
						t.id as etl_area_domain_category__id,
						t.etl_id as etl_area_domain_category__etl_id
				from w5
				left join	(
							select * from analytical.t_etl_area_domain_category
							where etl_area_domain in (select w5.etl_area_domain from w5)
							) as t
				on w5.etl_area_domain = t.etl_area_domain
				and w5.id = t.area_domain_category
				)
		-----------------------------------------
		,w7 as	(
				select
						w3.id,
						w3.label,
						w3.description,
						w3.label_en,
						w3.description_en,
						w3.etl_area_domain,
						w3.etl_area_domain_category__id,
						w3.etl_area_domain_category__etl_id,
						false as variable_superior
				from
						w3 where w3.id not in (select w6.id from w6)
				union all
				select
						w6.id,
						w6.label,
						w6.description,
						w6.label_en,
						w6.description_en,
						w6.etl_area_domain,
						w6.etl_area_domain_category__id,
						w6.etl_area_domain_category__etl_id,
						true as variable_superior
				from
						w6
				)
		-----------------------------------------
		-- add etl_id from t_etl_area_domain
		,w8 as	(
				select
						w7.id,
						w7.label,
						w7.description,
						w7.label_en,
						w7.description_en,
						w7.etl_area_domain as etl_area_domain__id,
						tt.etl_id as etl_area_domain__etl_id,
						w7.etl_area_domain_category__id,
						w7.etl_area_domain_category__etl_id,
						w7.variable_superior
				from
						w7
						left join
									(
									select tead.* from analytical.t_etl_area_domain as tead
									where tead.export_connection = $1
									and tead.id in (select w7.etl_area_domain from w7 where w7.etl_area_domain is not null)
									) as tt
						on w7.etl_area_domain = tt.id
				)
		-----------------------------------------
		select
				w8.id,
				w8.label,
				w8.description,
				w8.label_en,
				w8.description_en,
				w8.etl_area_domain__id,
				w8.etl_area_domain__etl_id,
				w8.etl_area_domain_category__id,
				w8.etl_area_domain_category__etl_id,	
				w8.variable_superior
		from
				w8 where '|| _cond ||'
		order
				by w8.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_category(integer, integer, boolean) is
'Function returns information records of area domain categories.';

alter function analytical.fn_etl_get_area_domain_category(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_area_domain_category_attr" schema="analytical" src="functions/etl/fn_etl_get_area_domain_category_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_category_attr
(
	_export_connection		integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id									integer,
	label								varchar,
	description							text,
	label_en							varchar,
	description_en						text,
	etl_area_domain__id					integer,
	etl_area_domain__etl_id				integer,
	etl_area_domain_category__id		integer,
	etl_area_domain_category__etl_id	integer,
	variable_superior					boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
		
		if _area_domain_category is not null
		then
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 03: fn_etl_get_area_domain_category_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w7.etl_area_domain_category__etl_id is not null';
			else
				_cond := 'w7.etl_area_domain_category__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'		
			with
			w1 as	(
					select tead.* from analytical.t_etl_area_domain as tead where tead.export_connection = $1
					)		
			,w2a as	(
					select tah.variable_superior from analytical.t_adc_hierarchy as tah
					where tah.variable_superior in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						)
					)					
			,w2b as	(
					select tah.variable from analytical.t_adc_hierarchy as tah
					where tah.variable in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						)
					)
			,w3a as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.area_domain, true as variable_superior from w3a union all
					select w3b.area_domain, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.area_domain, variable_superior from w4 union
					select w1.area_domain, false as variable_superior from w1
					)
			,w6 as	(
					select
							w5.area_domain as id,
							w1.id as etl_area_domain__id,
							w1.etl_id as etl_area_domain__etl_id,
							w5.variable_superior
					from
							w5
							inner join w1 on w5.area_domain = w1.area_domain
					)
			select
					t.area_domain_category as id,
					cadc.label,
					cadc.description,
					cadc.label_en,
					cadc.description_en,
					w6.etl_area_domain__id,
					w6.etl_area_domain__etl_id,
					t.id as etl_area_domain_category__id,
					t.etl_id as etl_area_domain_category__etl_id,
					w6.variable_superior
			from
					(
					select teadc.* from analytical.t_etl_area_domain_category as teadc
					where teadc.etl_area_domain in (select w6.etl_area_domain__id from w6)
					) as t
			inner join w6 on t.etl_area_domain = w6.etl_area_domain__id
			inner join analytical.c_area_domain_category as cadc on t.area_domain_category = cadc.id
			order
					by t.area_domain_category, w6.variable_superior
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(
					select
							cadc2.id,
							cadc2.label,
							cadc2.description,
							cadc2.label_en,
							cadc2.description_en,
							cadc2.area_domain
					from
							analytical.c_area_domain_category as cadc2
					where
							cadc2.area_domain in	(
													select distinct cadc1.area_domain
													from analytical.c_area_domain_category as cadc1
													where cadc1.id in (select unnest($2))
													)
					)
			,w2 as	(
					select
							w1.*,
							tead.id as etl_area_domain__id,
							tead.etl_id as etl_area_domain__etl_id
					from
							w1
					left
					join	(select * from analytical.t_etl_area_domain where export_connection = $1 and area_domain in (select distinct w1.area_domain from w1)) as tead
							on w1.area_domain = tead.area_domain
					)		
			,w3 as	(
					select
							w2.id,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en,
							w2.etl_area_domain__id,
							w2.etl_area_domain__etl_id,
							t.id as etl_area_domain_category__id,
							t.etl_id as etl_area_domain_category__etl_id
					from
							w2
							left join (select * from analytical.t_etl_area_domain_category as teadc where teadc.etl_area_domain in (select w2.etl_area_domain__id from w2)) as t
							on w2.etl_area_domain__id = t.etl_area_domain
							and w2.id = t.area_domain_category
					)
			-----------------------------------------	
			-----------------------------------------
			-- add CATEGORY from hierarchy
			,w4 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en,
							area_domain
					from analytical.c_area_domain_category where area_domain in
					(select distinct area_domain from analytical.c_area_domain_category where id in
					(select distinct variable_superior from analytical.t_adc_hierarchy
					where variable in (select distinct w3.id from w3)))
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_area_domain__id,
							t.etl_id as etl_area_domain__etl_id
					from w4
					left join
						(
						select * from analytical.t_etl_area_domain
						where export_connection = $1 and area_domain in (select w4.area_domain from w4)
						) as t
					on w4.area_domain = t.area_domain
					)
			,w6 as	(
					select
							w5.id,
							w5.label,
							w5.description,
							w5.label_en,
							w5.description_en,
							w5.etl_area_domain__id,
							w5.etl_area_domain__etl_id,
							t.id as etl_area_domain_category__id,
							t.etl_id as etl_area_domain_category__etl_id
					from w5
					left join	(
								select * from analytical.t_etl_area_domain_category
								where etl_area_domain in (select w5.etl_area_domain__id from w5)
								) as t
					on w5.etl_area_domain__id = t.etl_area_domain
					and w5.id = t.area_domain_category
					)
			-----------------------------------------
			,w7 as	(
					select
							w3.id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.etl_area_domain__id,
							w3.etl_area_domain__etl_id,
							w3.etl_area_domain_category__id,
							w3.etl_area_domain_category__etl_id,
							false as variable_superior
					from
							w3 where w3.id not in (select w6.id from w6)
					union all
					select
							w6.id,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en,
							w6.etl_area_domain__id,
							w6.etl_area_domain__etl_id,
							w6.etl_area_domain_category__id,
							w6.etl_area_domain_category__etl_id,
							true as variable_superior
					from
							w6
					)
			-----------------------------------------
			-----------------------------------------
			select
					w7.id,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					w7.etl_area_domain__id,
					w7.etl_area_domain__etl_id,
					w7.etl_area_domain_category__id,
					w7.etl_area_domain_category__etl_id,	
					w7.variable_superior
			from
					w7 where '|| _cond ||'
			order
					by w7.id
			'
			using _export_connection, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories for given arguments.';

alter function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_area_domain_informations" schema="analytical" src="functions/etl/fn_etl_get_area_domain_informations.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_informations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_informations(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_informations
(
	_export_connection	integer,
	_ldsity				integer
)
returns table
(
	ldsity_object							integer,
	area_domain								integer,
	area_domain_category					integer,
	adc2classification_rule					integer,
	------------------------------------------------
	etl_ldsity_object__id					integer,
	etl_ldsity_object__etl_id				integer,
	etl_area_domain__id						integer,
	etl_area_domain__etl_id					integer,
	etl_area_domain_category__id			integer,
	etl_area_domain_category__etl_id		integer,
	etl_adc2classification_rule__id			integer,
	etl_adc2classification_rule__etl_id		integer
)
as
$$
declare
		_adc_array	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_informations: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_informations: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_area_domain_informations: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_area_domain_informations: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;
	
		select cl.area_domain_category from analytical.c_ldsity as cl where cl.id = _ldsity
		into _adc_array;
	
		if _adc_array is null
		then
			raise exception 'Error 05: fn_etl_get_area_domain_informations: For input argument _ldsity = % not exists any reduction of area domain category!',_ldsity;
		else
			return query
			with
			w1 as	(						
					select
							t2.area_domain as rule__type,
							t1.rule__category,
							t1.rule__id,
							t1.rule__ldsity_object
					from
						(
						select
								cmadc2cr.id as rule__id,
								cmadc2cr.area_domain_category as rule__category,
								cmadc2cr.ldsity_object as rule__ldsity_object
						from analytical.cm_adc2classification_rule as cmadc2cr
						where cmadc2cr.id in (select unnest(_adc_array))
						) as t1
					inner join	analytical.c_area_domain_category as t2 on t1.rule__category = t2.id			
					)
			,w2 as	(
					select distinct rule__type, rule__category, rule__id, rule__ldsity_object from w1
					)
			,w3 as	(
					select
							w2.rule__type as rule__type_all,
							w2.rule__category,
							w2.rule__id,
							w2.rule__ldsity_object,
							cadc.id as rule__category_all,
							cacr.id as rule__id_all,
							telo.id as etl_ldsity_object__id,			-- id into table analytical.t_etl_ldsity_object
							telo.etl_id as etl_ldsity_object__etl_id	-- id into table target_data.c_ldsity_object
					from w2
					inner join analytical.c_area_domain_category as cadc on w2.rule__type = cadc.area_domain
					inner join analytical.cm_adc2classification_rule as cacr on w2.rule__ldsity_object = cacr.ldsity_object and cadc.id = cacr.area_domain_category
					left join (select * from analytical.t_etl_ldsity_object where export_connection = _export_connection) as telo on w2.rule__ldsity_object = telo.ldsity_object
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_area_domain__id,			-- id into table analytical.t_etl_area_domain
							t.etl_id as etl_area_domain__etl_id		-- id into table target_data.c_area_domain
					from 
							w3
					left join	(select * from analytical.t_etl_area_domain where export_connection = _export_connection) as t on w3.rule__type_all = t.area_domain
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_area_domain_category__id,			-- id into table analytical.t_etl_area_domain_category
							t.etl_id as etl_area_domain_category__etl_id	-- id into table target_data.c_area_domain_category
					from
							w4
					left join	(select * from analytical.t_etl_area_domain_category where etl_area_domain in (select w4.etl_area_domain__id from w4)) as t
								on w4.etl_area_domain__id = t.etl_area_domain
								and w4.rule__category_all = t.area_domain_category
					)					
			,w6 as	(
					select
							w5.*,
							t.id as etl_adc2classification_rule__id,
							t.etl_id as etl_adc2classification_rule__etl_id
					from
							w5
					left join	(select * from analytical.t_etl_adc2classification_rule where etl_area_domain_category in (select w5.etl_area_domain_category__id from w5)) as t
								on w5.etl_area_domain_category__id = t.etl_area_domain_category
								and w5.etl_ldsity_object__id = t.etl_ldsity_object
								and w5.rule__id_all = t.adc2classification_rule
					)				
			select
					w6.rule__ldsity_object			as ldsity_object,
					w6.rule__type_all				as area_domain,
					w6.rule__category_all			as area_domain_category,
					w6.rule__id_all					as adc2classification_rule,
					---------------------
					w6.etl_ldsity_object__id,
					w6.etl_ldsity_object__etl_id,
					w6.etl_area_domain__id,
					w6.etl_area_domain__etl_id,
					w6.etl_area_domain_category__id,
					w6.etl_area_domain_category__etl_id,
					w6.etl_adc2classification_rule__id,
					w6.etl_adc2classification_rule__etl_id
			from
					w6 order by w6.rule__id_all;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_informations(integer, integer) is
'Function returns area domain informations for given input arguments: export connection and ldsity.';

alter function analytical.fn_etl_get_area_domain_informations(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to public;


-- </function>

-- <function name="fn_etl_get_area_domain_informations_attr" schema="analytical" src="functions/etl/fn_etl_get_area_domain_informations_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_informations_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_informations_attr(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_informations_attr
(
	_export_connection	integer,
	_ldsity_object		integer
)
returns table
(
	lo__id								integer,
	lo__label							varchar,
	lo__description						text,
	lo__label_en						varchar,
	lo__description_en					text,
	aop__id								integer,
	aop__label							varchar,
	aop__description					text,
	ad__id								integer,
	ad__label							varchar,
	ad__description						text,
	ad__label_en						varchar,
	ad__description_en					text,
	adc__id								integer,
	adc__area_domain					integer,
	adc__label							varchar,
	adc__description					text,
	adc__label_en						varchar,
	adc__description_en					text,
	rule__id							integer,
	rule__area_domain_category			integer,
	rule__classification_rule			text,
	etl_ldsity_object__id				integer,
	etl_ldsity_object__etl_id			integer,
	etl_area_domain__id					integer,
	etl_area_domain__etl_id				integer,
	etl_area_domain_category__id		integer,
	etl_area_domain_category__etl_id	integer,
	etl_adc2classification_rule__id		integer,
	etl_adc2classification_rule__etl_id	integer
)
as
$$
declare
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_informations_attr: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_informations_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_area_domain_informations_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_area_domain_informations_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if not exists (select t3.* from analytical.t_etl_ldsity_object as t3 where t3.export_connection = _export_connection and t3.ldsity_object = _ldsity_object)
		then
			raise exception 'Error 05: fn_etl_get_area_domain_informations_attr: For given export connection (%) and ldsity object (%) does not exist record in t_etl_ldsity_object table.', _export_connection, _ldsity_object;
		end if;	
		
		return query
		with
		w1 as	(						
				select
						t4.id as lo__id,
						t4.label as lo__label,
						t4.description as lo__description,
						t4.label_en as lo__label_en,
						t4.description_en as lo__description_en,
						-----------------------------------------
						t5.id as aop__id,
						t5.label as aop__label,
						t5.description as aop__description,
						-----------------------------------------
						t3.id as ad__id,
						t3.label as ad__label,
						t3.description as ad__description,
						t3.label_en as ad__label_en,
						t3.description_en as ad__description_en,
						-----------------------------------------
						t2.id as adc__id,
						t2.area_domain as adc__area_domain,
						t2.label as adc__label,
						t2.description as adc__description,
						t2.label_en as adc__label_en,
						t2.description_en as adc__description_en,
						-----------------------------------------
						t1.rule__id,
						t1.rule__area_domain_category,
						t1.rule__classification_rule,
						-----------------------------------------
						t6.id as etl_ldsity_object__id,
						t6.etl_id as etl_ldsity_object__etl_id
				from
					(
					select
							cmadc2cr.id as rule__id,
							cmadc2cr.area_domain_category as rule__area_domain_category,
							cmadc2cr.ldsity_object as rule__ldsity_object,
							cmadc2cr.classification_rule as rule__classification_rule
					from
							analytical.cm_adc2classification_rule as cmadc2cr
					where
							cmadc2cr.ldsity_object = _ldsity_object
					) as t1
				
				inner join	analytical.c_area_domain_category as t2 on t1.rule__area_domain_category = t2.id
				inner join	analytical.c_area_domain as t3 on t2.area_domain = t3.id
				inner join	analytical.c_ldsity_object as t4 on t1.rule__ldsity_object = t4.id
				inner join	analytical.c_areal_or_population as t5 on t4.areal_or_population = t5.id
				inner join	(select telo.* from analytical.t_etl_ldsity_object as telo where telo.export_connection = _export_connection) as t6 on t1.rule__ldsity_object = t6.ldsity_object
				)
		,w2 as	(
				select
						w1.*,
						a1.id as etl_area_domain__id,
						a1.etl_id as etl_area_domain__etl_id
				from
						w1
				
				left join	(select * from analytical.t_etl_area_domain where export_connection = _export_connection) as a1 on w1.ad__id = a1.area_domain
				)
		,w3 as	(
				select
						w2.*,
						a2.id as etl_area_domain_category__id,
						a2.etl_id as etl_area_domain_category__etl_id
				from
						w2
						
				left join	(
							select teadc.* from analytical.t_etl_area_domain_category as teadc
							where teadc.etl_area_domain in (select w2.etl_area_domain__id from w2)
							) as a2
							on w2.etl_area_domain__id = a2.etl_area_domain and w2.adc__id = a2.area_domain_category
				)
		,w4 as	(
				select
						w3.*,
						a3.id as etl_adc2classification_rule__id,
						a3.etl_id as etl_adc2classification_rule__etl_id
				from
						w3
				left join	(
							select * from analytical.t_etl_adc2classification_rule
							where etl_ldsity_object in (select distinct w3.etl_ldsity_object__id from w3)
							and etl_area_domain_category in (select w3.etl_area_domain_category__id from w3)
							) as a3
							on w3.etl_area_domain_category__id = a3.etl_area_domain_category
							and w3.etl_ldsity_object__id = a3.etl_ldsity_object
							and w3.rule__id = a3.adc2classification_rule					
				)
		select
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.ad__id,
				w4.ad__label,
				w4.ad__description,
				w4.ad__label_en,
				w4.ad__description_en,
				w4.adc__id,
				w4.adc__area_domain,
				w4.adc__label,
				w4.adc__description,
				w4.adc__label_en,
				w4.adc__description_en,
				w4.rule__id,
				w4.rule__area_domain_category,
				w4.rule__classification_rule,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.etl_area_domain__id,
				w4.etl_area_domain__etl_id,
				w4.etl_area_domain_category__id,
				w4.etl_area_domain_category__etl_id,
				w4.etl_adc2classification_rule__id,
				w4.etl_adc2classification_rule__etl_id
		from
				w4 order by w4.rule__id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_informations_attr(integer, integer) is
'Function returns area domain informations for given input arguments: export connection and ldsity object.';

alter function analytical.fn_etl_get_area_domain_informations_attr(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations_attr(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations_attr(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations_attr(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations_attr(integer, integer) to public;


-- </function>

-- <function name="fn_etl_get_areal_or_population" schema="analytical" src="functions/etl/fn_etl_get_areal_or_population.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_areal_or_population(integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_areal_or_population
(
	_export_connection	integer,
	_etl				boolean default null::boolean
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	etl_areal_or_population__id		integer,
	etl_areal_or_population__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if not exists (select t.* from analytical.t_export_connection as t where t.id = _export_connection)
		then
			raise exception 'Error 01: fn_etl_get_areal_or_population: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_areal_or_population__etl_id is not null';
			else
				_cond := 'w3.etl_areal_or_population__etl_id is null';
			end if;
		end if;
				
		return query execute
		'
		with
		w1 as	(
				select * from analytical.c_areal_or_population
				)
		,w2 as	(
				select * from analytical.t_etl_areal_or_population
				where export_connection = $1
				)
		,w3 as	(
				select
					w1.*,
					w2.id as etl_areal_or_population__id,
					w2.etl_id as etl_areal_or_population__etl_id
				from
					w1 left join w2 on w1.id = w2.areal_or_population
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label as label_en,
				w3.description as description_en,
				w3.etl_areal_or_population__id,
				w3.etl_areal_or_population__etl_id
		from
				w3 where '|| _cond ||'
		order
				by w3.id
		'
		using _export_connection;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_areal_or_population(integer, boolean) is
'Function returns information records of areal or population.';

alter function analytical.fn_etl_get_areal_or_population(integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_definition_variant" schema="analytical" src="functions/etl/fn_etl_get_definition_variant.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_definition_variant(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_definition_variant
(
	_export_connection	integer,
	_ldsity				integer default null::integer,
	_etl				boolean default null::boolean
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	etl_definition_variant__id		integer,
	etl_definition_variant__etl_id	integer
)
as
$$
declare
		_cond_1		text;
		_cond_2		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_definition_variant: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_definition_variant: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if _ldsity is not null
		then
			if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
			then
				raise exception 'Error 03: fn_etl_get_unit_of_measure: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
			end if;		
		end if;
	
		if _ldsity is null
		then
			_cond_1 := 'TRUE';
		else
			_cond_1 := 'cdv.id in (select unnest(cl.definition_variant) as definition_variant from analytical.c_ldsity as cl where cl.id = $2)';
		end if;
	
		if _etl is null
		then
			_cond_2 := 'TRUE';
		else
			if _etl = true
			then
				_cond_2 := 'w3.etl_definition_variant__etl_id is not null';
			else
				_cond_2 := 'w3.etl_definition_variant__etl_id is null';
			end if;
		end if;
				
		return query execute
		'
		with
		w1 as	(
				select cdv.* from analytical.c_definition_variant as cdv
				where '|| _cond_1 ||'
				)
		,w2 as	(
				select * from analytical.t_etl_definition_variant
				where export_connection = $1
				)
		,w3 as	(
				select
					w1.*,
					w2.id as etl_definition_variant__id,
					w2.etl_id as etl_definition_variant__etl_id
				from
					w1 left join w2 on w1.id = w2.definition_variant
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label_en,
				w3.description_en,
				w3.etl_definition_variant__id,
				w3.etl_definition_variant__etl_id
		from
				w3 where '|| _cond_2 ||'
		order
				by w3.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_definition_variant(integer, integer, boolean) is
'Function returns information records of definition variants.';

alter function analytical.fn_etl_get_definition_variant(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_definition_variant(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_definition_variant(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_definition_variant(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_definition_variant(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_etl_ids_of_ldsities" schema="analytical" src="functions/etl/fn_etl_get_etl_ids_of_ldsities.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_ids_of_ldsities
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_etl_ids_of_ldsities(integer) CASCADE;

create or replace function analytical.fn_etl_get_etl_ids_of_ldsities(_export_connection integer)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_etl_ids_of_ldsities: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_etl_ids_of_ldsities: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		select array_agg(tel.etl_id order by tel.etl_id)
		from analytical.t_etl_ldsity as tel
		where tel.etl_ldsity_object in
			(
			select telo.id from analytical.t_etl_ldsity_object as telo
			where telo.export_connection = _export_connection
			)
		into _res;
	
		return _res;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) is
'Function returns ETL_IDs of ldsities that were ETL yet for given export connection.';

alter function analytical.fn_etl_get_etl_ids_of_ldsities(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to data_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to public;


-- </function>

-- <function name="fn_etl_get_export_connections" schema="analytical" src="functions/etl/fn_etl_get_export_connections.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_export_connections
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_export_connections() CASCADE;

create or replace function analytical.fn_etl_get_export_connections()
returns table
(
	id			integer,
	host		character varying,
	dbname		character varying,
	port		integer,
	comment		text
)
as
$$
begin
	return query
	select t.id, t.host, t.dbname, t.port, t.comment
	from analytical.t_export_connection as t
	order by t.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_export_connections() is
'The function returns records from t_export_connection table.';

alter function analytical.fn_etl_get_export_connections() owner to adm_analytical;
grant execute on function analytical.fn_etl_get_export_connections() to adm_analytical;
grant execute on function analytical.fn_etl_get_export_connections() to data_analytical;
grant execute on function analytical.fn_etl_get_export_connections() to usr_analytical;
grant execute on function analytical.fn_etl_get_export_connections() to public;


-- </function>

-- <function name="fn_etl_get_ldsities4object" schema="analytical" src="functions/etl/fn_etl_get_ldsities4object.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsities4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[]) CASCADE;

create or replace function analytical.fn_etl_get_ldsities4object
(
	_export_connection	integer,
	_ldsity_object		integer,
	_etl				boolean default null::boolean,
	_ldsities			integer[] default null::integer[],
	_etl_ldsity__etl_id	boolean default null::boolean

)
returns table
(
	export_connection						integer,
	------------------------------------------------
	ldsity_object							integer,
	ldsity_object_label						varchar,
	ldsity_object_description				text,
	ldsity_object_label_en					varchar,
	ldsity_object_description_en			text,
	etl_ldsity_object__id					integer,
	etl_ldsity_object__etl_id				integer,
	------------------------------------------------
	ldsity 									integer,
	ldsity_label							varchar,
	ldsity_description						text,
	ldsity_label_en							varchar,
	ldsity_description_en					text,
	ldsity_column_expression				text,
	------------------------------------------------
	check_unit_of_measure					boolean,
	check_definition_variant				boolean,
	------------------------------------------------
	check_area_domain						boolean,
	check_area_domain_category				boolean,
	check_adc2classification_rule			boolean,
	check_adc_hierarchy						boolean,
	check_adc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_sub_population					boolean,
	check_sub_population_category			boolean,
	check_spc2classification_rule			boolean,
	check_spc_hierarchy						boolean,
	check_spc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_version							boolean,
	check_ldsity2panel_refyearset_version	boolean,
	etl_ldsity__id							integer,
	etl_ldsity__etl_id						integer,
	check_ldsity							boolean,
	check_ldsity_part						boolean,
	------------------------------------------------
	ldsity_object__import					integer,
	unit_of_measure__import					integer,
	definition_variant__import				integer[],
	area_domain_category__import			integer[],
	sub_population_category__import			integer[]
)
as
$$
declare
		_ldsity_object_label				varchar;
		_ldsity_object_description			text;
		_ldsity_object_label_en				varchar;
		_ldsity_object_description_en		text;
		_etl_ldsity_object__id				integer;
		_etl_ldsity_object__etl_id			integer;
		_ldsity_object_upper_object			integer;
		_ldsity_array						integer[];
		_cond_1								text;
		_cond_2 							text;
		_data4import						boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsities4object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_ldsities4object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ldsities4object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_ldsities4object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		select
				clo.label,
				clo.description,
				clo.label_en,
				clo.description_en,
				clo.upper_object
		from
				analytical.c_ldsity_object as clo where clo.id = _ldsity_object
		into
				_ldsity_object_label,
				_ldsity_object_description,
				_ldsity_object_label_en,
				_ldsity_object_description_en,
				_ldsity_object_upper_object;

		select telo.id, telo.etl_id from analytical.t_etl_ldsity_object as telo
		where telo.export_connection = _export_connection
		and telo.ldsity_object = _ldsity_object
		into
			_etl_ldsity_object__id,
			_etl_ldsity_object__etl_id;
			
		if _ldsity_object_upper_object is null
		then
			return query
			with
			w1 as	(
					select
						0 as export_connection,
						------------------------------------------------
						null::integer as ldsity_object,
						null::varchar as ldsity_object_label,
						null::text as ldsity_object_description,
						null::varchar as ldsity_object_label_en,
						null::text as ldsity_object_description_en,
						null::integer as etl_ldsity_object__id,
						null::integer as etl_ldsity_object__etl_id,
						------------------------------------------------
						null::integer as ldsity,
						null::varchar as ldsity_label,
						null::text as ldsity_description,
						null::varchar as ldsity_label_en,
						null::text as ldsity_description_en,
						null::text as ldsity_column_expression,
						------------------------------------------------
						null::boolean as check_unit_of_measure,
						null::boolean as check_definition_variant,
						------------------------------------------------
						null::boolean as check_area_domain,
						null::boolean as check_area_domain_category,
						null::boolean as check_adc2classification_rule,
						null::boolean as check_adc_hierarchy,
						null::boolean as check_adc2classrule2panel_refyearset,
						------------------------------------------------
						null::boolean as check_sub_population,
						null::boolean as check_sub_population_category,
						null::boolean as check_spc2classification_rule,
						null::boolean as check_spc_hierarchy,
						null::boolean as check_spc2classrule2panel_refyearset,
						------------------------------------------------
						null::boolean as check_version,
						null::boolean as check_ldsity2panel_refyearset_version,
						null::integer as etl_ldsity__id,
						null::integer as etl_ldsity__etl_id,
						null::boolean as check_ldsity,
						null::boolean as check_ldsity_part,
						------------------------------------------------
						null::integer as ldsity_object__import,
						null::integer as unit_of_measure__import,
						null::integer[] as definition_variant__import,
						null::integer[] as area_domain_category__import,
						null::integer[] as sub_population_category__import
					)
			select * from w1 where w1.export_connection = _export_connection;
					
		else
			if _ldsities is null
			then
				select array_agg(cl.id order by cl.id) from analytical.c_ldsity as cl
				where cl.ldsity_object = _ldsity_object
				into _ldsity_array;

				_data4import := false;
			else
				_ldsity_array := _ldsities;
				_data4import := true;
			end if;
		
			if _ldsity_array is null
			then
				-- for given ldsity object not exist any ldsity => function must return zero rows
				return query
				with
				w1 as	(
						select
							0 as export_connection,
							------------------------------------------------
							null::integer as ldsity_object,
							null::varchar as ldsity_object_label,
							null::text as ldsity_object_description,
							null::varchar as ldsity_object_label_en,
							null::text as ldsity_object_description_en,
							null::integer as etl_ldsity_object__id,
							null::integer as etl_ldsity_object__etl_id,
							------------------------------------------------
							null::integer as ldsity,
							null::varchar as ldsity_label,
							null::text as ldsity_description,
							null::varchar as ldsity_label_en,
							null::text as ldsity_description_en,
							null::text as ldsity_column_expression,
							------------------------------------------------
							null::boolean as check_unit_of_measure,
							null::boolean as check_definition_variant,
							------------------------------------------------
							null::boolean as check_area_domain,
							null::boolean as check_area_domain_category,
							null::boolean as check_adc2classification_rule,
							null::boolean as check_adc_hierarchy,
							null::boolean as check_adc2classrule2panel_refyearset,
							------------------------------------------------
							null::boolean as check_sub_population,
							null::boolean as check_sub_population_category,
							null::boolean as check_spc2classification_rule,
							null::boolean as check_spc_hierarchy,
							null::boolean as check_spc2classrule2panel_refyearset,
							------------------------------------------------
							null::boolean as check_version,
							null::boolean as check_ldsity2panel_refyearset_version,
							null::integer as etl_ldsity__id,
							null::integer as etl_ldsity__etl_id,
							null::boolean as check_ldsity,
							null::boolean as check_ldsity_part,
							------------------------------------------------
							null::integer as ldsity_object__import,
							null::integer as unit_of_measure__import,
							null::integer[] as definition_variant__import,
							null::integer[] as area_domain_category__import,
							null::integer[] as sub_population_category__import
						)
				select * from w1 where w1.export_connection = _export_connection;				
			else
				if _etl is null
				then
					_cond_1 := 'TRUE';
				else
					if _etl = true
					then
						_cond_1 := 'w3.check_ldsity = true';
					else
						_cond_1 := 'w3.check_ldsity = false';
					end if;
				end if;

				if _etl_ldsity__etl_id is null
				then
					_cond_2 := 'TRUE';
				else
					if _etl_ldsity__etl_id = true
					then
						_cond_2 := 'w3.etl_ldsity__etl_id is not null';
					else
						_cond_2 := 'w3.etl_ldsity__etl_id is null';
					end if;
				end if;
			
				return query execute
				'
				with
				w1 as	(select unnest($2) as ldsities)
				,w2 as	(select analytical.fn_etl_get_ldsity($1,w1.ldsities,$10) as res from w1)
				,w3 as	(
						select		
								(w2.res).export_connection,
								-------------------------------------------
								$3 as ldsity_object,
								$4 as ldsity_object_label,
								$5 as ldsity_object_description,
								$6 as ldsity_object_label_en,
								$7 as ldsity_object_description_en,
								$8 as etl_ldsity_object__id,
								$9 as etl_ldsity_object__etl_id,	
								-------------------------------------------
								(w2.res).ldsity,
								(w2.res).label					as ldsity_label,
								(w2.res).description			as ldsity_description,
								(w2.res).label_en				as ldsity_label_en,
								(w2.res).description_en			as ldsity_description_en,
								(w2.res).column_expression		as ldsity_column_expression,
								-------------------------------------------
								(w2.res).check_unit_of_measure,
								(w2.res).check_definition_variant,
								-------------------------------------------
								(w2.res).check_area_domain,
								(w2.res).check_area_domain_category,
								(w2.res).check_adc2classification_rule,
								(w2.res).check_adc_hierarchy,
								(w2.res).check_adc2classrule2panel_refyearset,
								-------------------------------------------
								(w2.res).check_sub_population,
								(w2.res).check_sub_population_category,
								(w2.res).check_spc2classification_rule,
								(w2.res).check_spc_hierarchy,
								(w2.res).check_spc2classrule2panel_refyearset,
								-------------------------------------------
								(w2.res).check_version,
								(w2.res).check_ldsity2panel_refyearset_version,
								(w2.res).etl_ldsity__id,
								(w2.res).etl_ldsity__etl_id,
								(w2.res).check_ldsity,
								(w2.res).check_ldsity_part,
								-------------------------------------------
								(w2.res).ldsity_object__import,
								(w2.res).unit_of_measure__import,
								(w2.res).definition_variant__import,
								(w2.res).area_domain_category__import,
								(w2.res).sub_population_category__import
						from
								w2
						)
					select
							w3.export_connection,
							------------------------------------------------
							w3.ldsity_object,
							w3.ldsity_object_label,
							w3.ldsity_object_description,
							w3.ldsity_object_label_en,
							w3.ldsity_object_description_en,
							w3.etl_ldsity_object__id,
							w3.etl_ldsity_object__etl_id,
							------------------------------------------------
							w3.ldsity,
							w3.ldsity_label,
							w3.ldsity_description,
							w3.ldsity_label_en,
							w3.ldsity_description_en,
							w3.ldsity_column_expression,
							------------------------------------------------
							w3.check_unit_of_measure,
							w3.check_definition_variant,
							------------------------------------------------
							w3.check_area_domain,
							w3.check_area_domain_category,
							w3.check_adc2classification_rule,
							w3.check_adc_hierarchy,
							w3.check_adc2classrule2panel_refyearset,
							------------------------------------------------
							w3.check_sub_population,
							w3.check_sub_population_category,
							w3.check_spc2classification_rule,
							w3.check_spc_hierarchy,
							w3.check_spc2classrule2panel_refyearset,
							------------------------------------------------
							w3.check_version,
							w3.check_ldsity2panel_refyearset_version,
							w3.etl_ldsity__id,
							w3.etl_ldsity__etl_id,
							w3.check_ldsity,
							w3.check_ldsity_part,
							------------------------------------------------
							w3.ldsity_object__import,
							w3.unit_of_measure__import,
							w3.definition_variant__import,
							w3.area_domain_category__import,
							w3.sub_population_category__import
					from
							w3
					where
							'|| _cond_1 ||'
					and
							'|| _cond_2 ||'
					order
							by w3.ldsity
					'
					using
							_export_connection,
							_ldsity_array,
							_ldsity_object,
							_ldsity_object_label,
							_ldsity_object_description,
							_ldsity_object_label_en,
							_ldsity_object_description_en,
							_etl_ldsity_object__id,
							_etl_ldsity_object__etl_id,
							_data4import;
			end if;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) is
'Function returns information of ldsities for given ldsity object.';

alter function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_ldsity" schema="analytical" src="functions/etl/fn_etl_get_ldsity.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsity(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_ldsity
(
	_export_connection 	integer,
	_ldsity				integer,
	_data4import		boolean default false
)
returns table
(
	export_connection						integer,
	ldsity 									integer,
	label									varchar,
	description								text,
	label_en								varchar,
	description_en							text,
	column_expression						text,
	------------------------------------------------
	check_unit_of_measure					boolean,
	check_definition_variant				boolean,
	------------------------------------------------
	check_area_domain						boolean,
	check_area_domain_category				boolean,
	check_adc2classification_rule			boolean,
	check_adc_hierarchy						boolean,
	check_adc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_sub_population					boolean,
	check_sub_population_category			boolean,
	check_spc2classification_rule			boolean,
	check_spc_hierarchy						boolean,
	check_spc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_version							boolean,
	check_ldsity2panel_refyearset_version	boolean,
	etl_ldsity__id							integer,
	etl_ldsity__etl_id						integer,
	check_ldsity							boolean,
	check_ldsity_part						boolean,
	------------------------------------------------
	ldsity_object__import					integer,
	unit_of_measure__import					integer,
	definition_variant__import				integer[],
	area_domain_category__import			integer[],
	sub_population_category__import			integer[]
)
as
$$
declare
		_ldsity_object						integer;
		_ldsity_unit_of_measure				integer;
		_ldsity_definition_variant			integer[];
		_ldsity_area_domain_category		integer[];
		_ldsity_sub_population_category		integer[];
		_ldsity_object__import				integer;
		_unit_of_measure__import			integer;
		_definition_variant__import			integer[];
		_area_domain_category__import		integer[];
		_sub_population_category__import	integer[];
		_definition_variant__import_i		integer;
		_area_domain_category__import_i		integer;
		_sub_population_category__import_i	integer;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsity: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_ldsity: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ldsity: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_ldsity: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		select
				cl.ldsity_object,
				cl.unit_of_measure,
				cl.definition_variant,
				cl.area_domain_category,
				cl.sub_population_category
		from
				analytical.c_ldsity as cl where cl.id = _ldsity
		into
				_ldsity_object,
				_ldsity_unit_of_measure,
				_ldsity_definition_variant,
				_ldsity_area_domain_category,
				_ldsity_sub_population_category;


		if _data4import = true
		then
			-------------------------------------
			-- finding etl_id for ldsity object
			select telo.etl_id from analytical.t_etl_ldsity_object as telo
			where telo.export_connection = _export_connection
			and telo.ldsity_object = _ldsity_object
			into _ldsity_object__import;

			if _ldsity_object__import is null
			then
				raise exception 'Error 05: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and ldsity_object = % in table t_etl_ldsity_object!',_export_connection,_ldsity_object;
			end if;
			-------------------------------------
			-- unit_of_measure
			if _ldsity_unit_of_measure is null
			then
				_unit_of_measure__import := null::integer;
			else
				select teuom.etl_id from analytical.t_etl_unit_of_measure as teuom
				where teuom.export_connection = _export_connection
				and teuom.unit_of_measure = _ldsity_unit_of_measure
				into _unit_of_measure__import;

				if _unit_of_measure__import is null
				then
					raise exception 'Error 06: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and unit_of_measure = % in table t_etl_unit_of_measure!',_export_connection,_ldsity_unit_of_measure;
				end if;
			end if;
			-------------------------------------
			-- definition_variant
			if _ldsity_definition_variant is null
			then
				_definition_variant__import := null::integer[];
			else
				for i in 1..array_length(_ldsity_definition_variant,1)
				loop
					select tedf.etl_id from analytical.t_etl_definition_variant as tedf
					where tedf.export_connection = _export_connection
					and tedf.definition_variant = _ldsity_definition_variant[i]
					into _definition_variant__import_i;

					if _definition_variant__import_i is null
					then
						raise exception 'Error 07: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and definition_variant = % in table t_etl_definition_variant!',_export_connection,_ldsity_definition_variant[i];
					end if;

					if i = 1
					then
						_definition_variant__import := array[_definition_variant__import_i];
					else
						_definition_variant__import := _definition_variant__import || array[_definition_variant__import_i];
					end if;			
				end loop;
			end if;
			-------------------------------------
			-- area_domain_category
			if _ldsity_area_domain_category is null
			then
				_area_domain_category__import := null::integer[];
			else
				for i in 1..array_length(_ldsity_area_domain_category,1)
				loop
					select
							teacr.etl_id
					from
							(select cacr.* from analytical.cm_adc2classification_rule as cacr where cacr.id = _ldsity_area_domain_category[i]) as t
							inner join	analytical.c_area_domain_category as cadc on t.area_domain_category = cadc.id
							inner join	(select t1.* from analytical.t_etl_area_domain as t1 where t1.export_connection = _export_connection) as tead on cadc.area_domain = tead.area_domain
							inner join	(select t2.* from analytical.t_etl_ldsity_object as t2 where t2.export_connection = _export_connection) as telo on t.ldsity_object = telo.ldsity_object
							inner join	analytical.t_etl_area_domain_category as teadc
										on t.area_domain_category = teadc.area_domain_category
										and tead.id = teadc.etl_area_domain
							inner join	analytical.t_etl_adc2classification_rule as teacr
										on t.id = teacr.adc2classification_rule
										and teadc.id = teacr.etl_area_domain_category
										and telo.id = teacr.etl_ldsity_object
					into
						_area_domain_category__import_i;

					if _area_domain_category__import_i is null
					then
						raise exception 'Error 08: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and adc2classification_rule = % in table t_etl_adc2classification_rule!',_export_connection,_ldsity_area_domain_category[i];
					end if;

					if i = 1
					then
						_area_domain_category__import := array[_area_domain_category__import_i];
					else
						_area_domain_category__import := _area_domain_category__import || array[_area_domain_category__import_i];
					end if;	
				end loop;
			end if;
			-------------------------------------
			-- sub_population_category
			if _ldsity_sub_population_category is null
			then
				_sub_population_category__import := null::integer[];
			else
				for i in 1..array_length(_ldsity_sub_population_category,1)
				loop
					select
							tescr.etl_id
					from
							(select cscr.* from analytical.cm_spc2classification_rule as cscr where cscr.id = _ldsity_sub_population_category[i]) as t
							inner join	analytical.c_sub_population_category as cspc on t.sub_population_category = cspc.id
							inner join	(select t3.* from analytical.t_etl_sub_population as t3 where t3.export_connection = _export_connection) as tesp on cspc.sub_population = tesp.sub_population
							inner join	(select t4.* from analytical.t_etl_ldsity_object as t4 where t4.export_connection = _export_connection) as telo on t.ldsity_object = telo.ldsity_object
							inner join	analytical.t_etl_sub_population_category as tespc
										on t.sub_population_category = tespc.sub_population_category
										and tesp.id = tespc.etl_sub_population
							inner join	analytical.t_etl_spc2classification_rule as tescr
										on t.id = tescr.spc2classification_rule
										and tespc.id = tescr.etl_sub_population_category
										and telo.id = tescr.etl_ldsity_object
					into
						_sub_population_category__import_i;

					if _sub_population_category__import_i is null
					then
						raise exception 'Error 09: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and spc2classification_rule = % in table t_etl_spc2classification_rule!',_export_connection,_ldsity_sub_population_category[i];
					end if;

					if i = 1
					then
						_sub_population_category__import := array[_sub_population_category__import_i];
					else
						_sub_population_category__import := _sub_population_category__import || array[_sub_population_category__import_i];
					end if;	
				end loop;
			end if;
			-------------------------------------
		else
			_ldsity_object__import := null::integer;
			_unit_of_measure__import := null::integer;
			_definition_variant__import	:= null::integer[];
			_area_domain_category__import := null::integer[];
			_sub_population_category__import := null::integer[];
		end if;

		return query execute
		'
		with
		w1 as	(
				select
						$1 as export_connection,
						$2 as ldsity,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $4 is not null then (select * from analytical.fn_etl_check_unit_of_measure($1,$2)) else true end					as check_unit_of_measure,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $5 is not null then (select * from analytical.fn_etl_check_definition_variant($1,$2)) else true end				as check_definition_variant,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $6 is not null then (select * from analytical.fn_etl_check_area_domain($1,$2)) else true end						as check_area_domain,
						case when $6 is not null then (select * from analytical.fn_etl_check_area_domain_category($1,$2)) else true end				as check_area_domain_category,
						case when $6 is not null then (select * from analytical.fn_etl_check_adc2classification_rule($1,$2)) else true end			as check_adc2classification_rule,
						case when $6 is not null then (select * from analytical.fn_etl_check_adc_hierarchy($1,$2)) else true end					as check_adc_hierarchy,
						case when $6 is not null then (select * from analytical.fn_etl_check_adc2classrule2panel_refyearset($1,$2)) else true end	as check_adc2classrule2panel_refyearset,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $7 is not null then (select * from analytical.fn_etl_check_sub_population($1,$2)) else true end					as check_sub_population,
						case when $7 is not null then (select * from analytical.fn_etl_check_sub_population_category($1,$2)) else true end			as check_sub_population_category,
						case when $7 is not null then (select * from analytical.fn_etl_check_spc2classification_rule($1,$2)) else true end			as check_spc2classification_rule,
						case when $7 is not null then (select * from analytical.fn_etl_check_spc_hierarchy($1,$2)) else true end					as check_spc_hierarchy,
						case when $7 is not null then (select * from analytical.fn_etl_check_spc2classrule2panel_refyearset($1,$2)) else true end	as check_spc2classrule2panel_refyearset,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						(select * from analytical.fn_etl_check_version($1,$2))																		as check_version,
						(select * from analytical.fn_etl_check_ldsity2panel_refyearset_version($1,$2))												as check_ldsity2panel_refyearset_version
				)
		,w2 as	(
				select
						w1.export_connection,
						w1.ldsity,
						cl.label,
						cl.description,
						cl.label_en,
						cl.description_en,
						cl.column_expression::text as column_expression,
						---------------------------------------------
						w1.check_unit_of_measure,
						w1.check_definition_variant,
						---------------------------------------------
						w1.check_area_domain,
						w1.check_area_domain_category,
						w1.check_adc2classification_rule,
						w1.check_adc_hierarchy,
						w1.check_adc2classrule2panel_refyearset,
						---------------------------------------------
						w1.check_sub_population,
						w1.check_sub_population_category,
						w1.check_spc2classification_rule,
						w1.check_spc_hierarchy,
						w1.check_spc2classrule2panel_refyearset,
						---------------------------------------------
						w1.check_version,
						w1.check_ldsity2panel_refyearset_version,
						t.id as etl_ldsity__id,
						t.etl_id as etl_ldsity__etl_id
				from
						w1	inner join	analytical.c_ldsity as cl on w1.ldsity = cl.id
							left join	(
										select * from analytical.t_etl_ldsity where etl_ldsity_object =
										(select id from analytical.t_etl_ldsity_object as telo where telo.export_connection = $1 and ldsity_object = $3)
										and ldsity = $2
										) as t
										on w1.ldsity = t.ldsity
				)
		select
				w2.export_connection,
				w2.ldsity,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en,
				w2.column_expression,
				-------------------------------------------
				w2.check_unit_of_measure,
				w2.check_definition_variant,
				-------------------------------------------
				w2.check_area_domain,
				w2.check_area_domain_category,
				w2.check_adc2classification_rule,
				w2.check_adc_hierarchy,
				w2.check_adc2classrule2panel_refyearset,
				-------------------------------------------
				w2.check_sub_population,
				w2.check_sub_population_category,
				w2.check_spc2classification_rule,
				w2.check_spc_hierarchy,
				w2.check_spc2classrule2panel_refyearset,
				-------------------------------------------
				w2.check_version,
				w2.check_ldsity2panel_refyearset_version,
				w2.etl_ldsity__id,
				w2.etl_ldsity__etl_id,
				-------------------------------------------
				case
					when
						w2.check_unit_of_measure = true					and
						w2.check_definition_variant = true				and
						---------------------------------------------------
						w2.check_area_domain = true						and
						w2.check_area_domain_category = true			and
						w2.check_adc2classification_rule = true			and
						w2.check_adc_hierarchy = true					and
						w2.check_adc2classrule2panel_refyearset = true	and
						---------------------------------------------------
						w2.check_sub_population = true					and
						w2.check_sub_population_category = true			and
						w2.check_spc2classification_rule = true			and
						w2.check_spc_hierarchy = true					and
						w2.check_spc2classrule2panel_refyearset = true	and
						---------------------------------------------------
						w2.check_version = true							and
						w2.check_ldsity2panel_refyearset_version = true	and
						w2.etl_ldsity__id is not null					and
						w2.etl_ldsity__etl_id is not null
					then true
					else false
				end as check_ldsity,
				-------------------------------------------
				case
					when
						w2.check_unit_of_measure = true					and
						w2.check_definition_variant = true				and
						---------------------------------------------------
						w2.check_area_domain = true						and
						w2.check_area_domain_category = true			and
						w2.check_adc2classification_rule = true			and
						w2.check_adc_hierarchy = true					and
						w2.check_adc2classrule2panel_refyearset = true	and
						---------------------------------------------------
						w2.check_sub_population = true					and
						w2.check_sub_population_category = true			and
						w2.check_spc2classification_rule = true			and
						w2.check_spc_hierarchy = true					and
						w2.check_spc2classrule2panel_refyearset = true
					then true
					else false
				end as check_ldsity_part,
				$8 as ldsity_object__import,
				$9 as unit_of_measure__import,
				$10 as definition_variant__import,
				$11 as area_domain_category__import,
				$12 as sub_population_category__import
		from
				w2;
		'
		using
				_export_connection,
				_ldsity,
				_ldsity_object,
				_ldsity_unit_of_measure,
				_ldsity_definition_variant,
				_ldsity_area_domain_category,
				_ldsity_sub_population_category,
				_ldsity_object__import,
				_unit_of_measure__import,
				_definition_variant__import,
				_area_domain_category__import,
				_sub_population_category__import;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsity(integer, integer, boolean) is
'Function returns information records of ldsity.';

alter function analytical.fn_etl_get_ldsity(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_ldsity2panel_refyearset_version" schema="analytical" src="functions/etl/fn_etl_get_ldsity2panel_refyearset_version.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsity2panel_refyearset_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_ldsity2panel_refyearset_version
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id											integer,
	ldsity										integer,
	refyearset2panel							integer,
	version										integer,
	etl_ldsity__id								integer,
	etl_ldsity__etl_id							integer,
	etl_version__id								integer,
	etl_version__etl_id							integer,
	etl_ldsity2panel_refyearset_version__id		integer,
	etl_ldsity2panel_refyearset_version__etl_id	integer
)
as
$$
declare
		_ldsity_object	integer;
		_cond			text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsity2panel_refyearset_version: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_ldsity2panel_refyearset_version: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ldsity2panel_refyearset_version: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_ldsity2panel_refyearset_version: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		select cl.ldsity_object from analytical.c_ldsity as cl where cl.id = _ldsity
		into _ldsity_object;
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_ldsity2panel_refyearset_version__etl_id is not null';
			else
				_cond := 'w3.etl_ldsity2panel_refyearset_version__etl_id is null';
			end if;
		end if;	
			
		return query execute
		'
		with
		w1 as	(select * from analytical.cm_ldsity2panel_refyearset_version where ldsity = $2)
		,w2 as	(
				select
						w1.id,
						w1.ldsity,
						w1.refyearset2panel,
						w1.version,
						t1.id as etl_ldsity__id,
						t1.etl_id as etl_ldsity__etl_id,
						t2.id as etl_version__id,
						t2.etl_id as etl_version__etl_id
				from
						w1
				inner join	(	
							select * from analytical.t_etl_ldsity where etl_ldsity_object =
							(select id from analytical.t_etl_ldsity_object where export_connection = $1 and ldsity_object = $3)
							and ldsity = $2
							) as t1
							on w1.ldsity = t1.ldsity
							
				inner join	(select * from analytical.t_etl_version where export_connection = $1) as t2
							on w1.version = t2.version
				)
		,w3 as	(
				select
						w2.id,
						w2.ldsity,
						w2.refyearset2panel,
						w2.version,
						w2.etl_ldsity__id,
						w2.etl_ldsity__etl_id,
						w2.etl_version__id,
						w2.etl_version__etl_id,
						telprv.id as etl_ldsity2panel_refyearset_version__id,
						telprv.etl_id as etl_ldsity2panel_refyearset_version__etl_id
				from w2
				left join	analytical.t_etl_ldsity2panel_refyearset_version as telprv
							on w2.id = telprv.ldsity2panel_refyearset_version
							and w2.etl_ldsity__id = telprv.etl_ldsity
							and w2.etl_version__id = telprv.etl_version
				)
		select
				w3.id,
				w3.ldsity,
				w3.refyearset2panel,
				w3.version,
				w3.etl_ldsity__id,
				w3.etl_ldsity__etl_id,
				w3.etl_version__id,
				w3.etl_version__etl_id,
				w3.etl_ldsity2panel_refyearset_version__id,
				w3.etl_ldsity2panel_refyearset_version__etl_id							
		from
				w3 where '|| _cond ||'
		order
				by w3.ldsity, w3.refyearset2panel;
		'
		using _export_connection, _ldsity, _ldsity_object;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) is
'Function returns information records of mapping between ldsity and combinations of panel, reference year sets and versions.';

alter function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_ldsity_objects" schema="analytical" src="functions/etl/fn_etl_get_ldsity_objects.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsity_objects
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) CASCADE;

create or replace function analytical.fn_etl_get_ldsity_objects
(
	_export_connection		integer,
	_etl					boolean default null::boolean,	-- returns all ldsities for all objects [false returns all ldsities that are not etl yet]
	_checks					boolean default false,			-- if argument is true than are made checks for ldsities and checks for theirs upper objects
	_ldsity_object			integer default null::integer	-- function is calls for choosed ldsity object
)
returns table
(
	ldsity_object				integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	table_name					regclass,
	upper_object__id			integer,
	upper_object__etl_id		integer,
	areal_or_population__id		integer,
	areal_or_population__label	varchar,
	areal_or_population__etl_id	integer,
	column4upper_object			varchar,
	filter						text,
	etl_ldsity_object__id		integer,
	etl_ldsity_object__etl_id	integer,
	check_ldsities				boolean,
	check_upper_object			boolean		-- bude slouzit v etl procesu dale pri volani teto funkce na konkretni objekt
)
as
$$
declare
		_cond_1			text;
		_cond_2			text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsity_objects: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_ldsity_objects: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		-------------------------------
		if _ldsity_object is not null
		then
			_cond_1 := 'clo_int.id = $2';
		else
			_cond_1 := 'TRUE';
		end if;
		-------------------------------
		if _checks = false
		then
			if _etl is null
			then
				_cond_2 := 'TRUE'; -- bez kontrol, vypise vsechny zaznamy
			else
				if _etl = true
				then
					_cond_2 := 'w1.etl_ldsity_object__etl_id is not null'; -- bez kontrol, uz etl-kovany byly
				else
					_cond_2 := 'w1.etl_ldsity_object__etl_id is null'; -- bez kontrol, jeste etl-kovany nebyly
				end if;
			end if;
		else
			if _etl is null
			then
				_cond_2 := 'TRUE'; -- s kontrolama, vypise vsechny zaznamy
			else
				if _etl = true
				then
					_cond_2 :=	'
								(
								w2.etl_ldsity_object__etl_id is not null				and
								(w2.check_ldsities is null or w2.check_ldsities = true)	and
								w2.check_upper_object = true
								)
								';
				else
					_cond_2 :=	'
								(
								(w2.etl_ldsity_object__etl_id is null)																		or
								(w2.etl_ldsity_object__etl_id is not null and w2.check_ldsities = false and w2.check_upper_object =  true)	or
								(w2.etl_ldsity_object__etl_id is not null and w2.check_ldsities =  true and w2.check_upper_object = false)	or
								(w2.etl_ldsity_object__etl_id is not null and w2.check_ldsities = false and w2.check_upper_object = false)
								)
								';
				end if;
			end if;
		end if;
		-------------------------------	
		-------------------------------
		if	_checks = false
		then
			-- VYPIS BEZ KONTROL !!! => _checks = false
			-- + varianty pro vypis etl_ldsity_object__etl_id:
			-- 1. _etl is null::boolean -- vypis varianty 2. a 3. => etl_ldsity_object__etl_id is null or is not null
			-- 2. _etl = true  => etl_ldsity_object__etl_id =  true => vse co uz bylo na etl-kovano, bez ohledu na kontroly
			-- 3. _etl = false => etl_ldsity_object__etl_id = false => vse co jeste nebylo na etl-kovano, bez ohledu na kontroly
			return query execute
			'
			with
			w1 as	(
					select
							clo.id as ldsity_object,
							clo.label,
							clo.description,
							clo.label_en,
							clo.description_en,
							clo.table_name,
							clo.upper_object as upper_object__id,
							telo2.etl_id as upper_object__etl_id,				-- toto mi rika jestli uz upper object byl ci nebyl na etl-kovan
							clo.areal_or_population as areal_or_population__id,
							caop."label" as areal_or_population__label,
							teaop.etl_id as areal_or_population__etl_id,		-- toto mi rika jestli uz areal_or_population bylo ci nebylo na etl-kovano
							clo.column4upper_object,
							clo.filter,
							telo1.id as etl_ldsity_object__id,
							telo1.etl_id as etl_ldsity_object__etl_id,			-- toto mi rika jestli dany ldsity objekt uz byl ci nebyl na etl-kovan
							null::boolean as check_ldsities,
							null::boolean as check_upper_object
					from
							(select clo_int.* from analytical.c_ldsity_object as clo_int where '|| _cond_1 ||') as clo -- podminka _cond_1 pripadne omezuje jen na jeden konkretni ldsity objekt
							inner join analytical.c_areal_or_population as caop on clo.areal_or_population = caop.id
							left join (select * from analytical.t_etl_areal_or_population where export_connection = $1) as teaop on clo.areal_or_population = teaop.areal_or_population -- potom musi byt INNER JOIN
							left join (select a1.* from analytical.t_etl_ldsity_object as a1 where a1.export_connection = $1) as telo1 on clo.id = telo1.ldsity_object
							left join (select a2.* from analytical.t_etl_ldsity_object as a2 where a2.export_connection = $1) as telo2 on clo.upper_object = telo2.ldsity_object
					)
			select
					w1.ldsity_object,
					w1.label,
					w1.description,
					w1.label_en,
					w1.description_en,
					w1.table_name,
					w1.upper_object__id,
					w1.upper_object__etl_id,
					w1.areal_or_population__id,
					w1.areal_or_population__label,
					w1.areal_or_population__etl_id,
					w1.column4upper_object,
					w1.filter,
					w1.etl_ldsity_object__id,
					w1.etl_ldsity_object__etl_id,
					w1.check_ldsities,
					w1.check_upper_object
			from
					w1 where '|| _cond_2 ||'
			order
					by w1.ldsity_object;
			'
			using _export_connection, _ldsity_object;
		else
			-- VYPIS s KONTROLAMA !!! => _checks = true
			
			-- provadeni kontroly prispevku u daneno objektu: [vysledky muzou byt: null, true, false] !!!
			-- kontrola se neprovadi u objektu, ktery ma upper_object is null -- vysledkem bude NULL::boolean
			-- kontrola se neprovadi u objektu, ktery doposud nebyl etl-kovan -- vysledkem bude NULL::boolean
					
			-- provadeni kontroly prispevku u upper objektu: [vysledky muzou byt: true, false] !!!
			
			-- + varianty pro vypis:
			-- 1. _etl is null::boolean -- vypis vseho bez ohledu na vysledek kontrol => TRUE
				
			-- 2. _etl = true
			--		=> etl_ldsity_object__etl_id is not null and (check_ldsities is null or check_ldsities = true) and check_upper_object = true
			--		=> vse co uz bylo na etl-kovano, a obe kontroly jsou TRUE, pricemz check_ldsities u nejvyse postaveneho objektu je null
			
			-- 3. _etl = false
			--		=> etl_ldsity_object__etl_id is null => vse co jeste nebylo etl-kovano, bez ohledu na kontroly		OR
			--		=> etl_ldsity_object__etl_id is not null and check_ldsities = false and check_upper_object = true	OR
			--		=> etl_ldsity_object__etl_id is not null and check_ldsities =  true and check_upper_object = false
			
			return query execute
			'
			with
			w1 as	(
					select
							clo.id as ldsity_object,
							clo.label,
							clo.description,
							clo.label_en,
							clo.description_en,
							clo.table_name,
							clo.upper_object as upper_object__id,
							telo2.etl_id as upper_object__etl_id,				-- toto mi rika jestli uz upper object byl ci nebyl na etl-kovan
							clo.areal_or_population as areal_or_population__id,
							caop."label" as areal_or_population__label,
							teaop.etl_id as areal_or_population__etl_id,		-- toto mi rika jestli uz areal_or_population bylo ci nebylo na etl-kovano
							clo.column4upper_object,
							clo.filter,
							telo1.id as etl_ldsity_object__id,
							telo1.etl_id as etl_ldsity_object__etl_id			-- toto mi rika jestli dany ldsity objekt uz byl ci nebyl na etl-kovan
					from
							(select clo_int.* from analytical.c_ldsity_object as clo_int where '|| _cond_1 ||') as clo -- podminka _cond_1 pripadne omezuje jen na jeden konkretni ldsity objekt
							inner join analytical.c_areal_or_population as caop on clo.areal_or_population = caop.id
							left join (select * from analytical.t_etl_areal_or_population where export_connection = $1) as teaop on clo.areal_or_population = teaop.areal_or_population -- potom musi byt INNER JOIN
							left join (select a1.* from analytical.t_etl_ldsity_object as a1 where a1.export_connection = $1) as telo1 on clo.id = telo1.ldsity_object
							left join (select a2.* from analytical.t_etl_ldsity_object as a2 where a2.export_connection = $1) as telo2 on clo.upper_object = telo2.ldsity_object
					)
			,w2 as	(
					select
							w1.ldsity_object,
							w1.label,
							w1.description,
							w1.label_en,
							w1.description_en,
							w1.table_name,
							w1.upper_object__id,
							w1.upper_object__etl_id,
							w1.areal_or_population__id,
							w1.areal_or_population__label,
							w1.areal_or_population__etl_id,
							w1.column4upper_object,
							w1.filter,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
	
							-- provadeni kontroly prispevku u daneno objektu: [vysledky muzou byt: null, true, false] !!!
							-- kontrola se neprovadi u objektu, ktery ma upper_object is null -- vysledkem bude NULL::boolean
							-- kontrola se neprovadi u objektu, ktery doposud nebyl etl-kovan -- vysledkem bude NULL::boolean
							case
								when (w1.etl_ldsity_object__etl_id is null or w1.upper_object__id is null)
								then NULL::boolean
								else (select * from analytical.fn_etl_check_ldsities4object($1,w1.ldsity_object))
							end as check_ldsities,

							-- provadeni kontroly prispevku u upper objektu: [vysledky muzou byt: true, false] !!!
							case
								when w1.upper_object__id is null -- dany objekt je nejvyse postavenym objektem
								then TRUE
								else -- dany objekt neni nejvyse postavenym objektem
									case
										when
											(select clo1.upper_object from analytical.c_ldsity_object as clo1 where clo1.id = w1.upper_object__id)
											is null
										then -- nadrizeny objekt u daneho objektu je nejvyse postavenym objektem -- u nej se neprovadi kontrola na prispevky
												case
													when w1.upper_object__etl_id is null
													then FALSE -- nadrizeny objekt objektu jeste nebyl etl-kovan
													else TRUE -- nadrizeny objekt objektu uz byl etl-kovan
												end
										else -- nadrizeny objekt u daneho objektu neni nejvyse postavenym objektem
												case
													when w1.upper_object__etl_id is null
													then FALSE
													else (select * from analytical.fn_etl_check_ldsities4object($1,w1.upper_object__id))
												end
									end
							end
								as check_upper_object
					from
							w1
					)
			select
					w2.ldsity_object,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.table_name,
					w2.upper_object__id,
					w2.upper_object__etl_id,
					w2.areal_or_population__id,
					w2.areal_or_population__label,
					w2.areal_or_population__etl_id,
					w2.column4upper_object,
					w2.filter,
					w2.etl_ldsity_object__id,
					w2.etl_ldsity_object__etl_id,
					w2.check_ldsities,
					w2.check_upper_object
			from
					w2 where '|| _cond_2 ||'
			order
					by w2.ldsity_object
			'
			using _export_connection, _ldsity_object;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) is
'Function returns information of ldsity objects.';

alter function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to public;


-- </function>

-- <function name="fn_etl_get_refyearset2panel4adc2classification_rule" schema="analytical" src="functions/etl/fn_etl_get_refyearset2panel4adc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_refyearset2panel4adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) CASCADE;

create or replace function analytical.fn_etl_get_refyearset2panel4adc2classification_rule
(
	_adc2classification_rule	integer
)
returns table
(
	cm_adc2classrule2panel_refyearset__id					integer,
	cm_adc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_res_id					integer;
		_res_refyearset2panel	integer;
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_etl_get_refyearset2panel4adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		select
				cacpr.id,
				cacpr.refyearset2panel
		from
				analytical.cm_adc2classrule2panel_refyearset as cacpr
		where
				cacpr.adc2classification_rule = _adc2classification_rule
		order
				by cacpr.refyearset2panel
		limit
				1
		into
				_res_id,
				_res_refyearset2panel;
	
		if _res_id is null
		then
			raise exception 'Error 02: fn_etl_get_refyearset2panel4adc2classification_rule: For input argument _adc2classification_rule = % not exists any refyearset2panel in cm_adc2classrule2panel_refyearset table!',_adc2classification_rule;
		end if;
	
		return query select _res_id, _res_refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) is
'Function returns one row of id and refyearset2panel from cm_adc2classrule2panel_refyearset table for given adc2classification_rule.';

alter function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to data_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to public;


-- </function>

-- <function name="fn_etl_get_refyearset2panel4spc2classification_rule" schema="analytical" src="functions/etl/fn_etl_get_refyearset2panel4spc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_refyearset2panel4spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) CASCADE;

create or replace function analytical.fn_etl_get_refyearset2panel4spc2classification_rule
(
	_spc2classification_rule	integer
)
returns table
(
	cm_spc2classrule2panel_refyearset__id					integer,
	cm_spc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_res_id					integer;
		_res_refyearset2panel	integer;
begin
		if _spc2classification_rule is null
		then
			raise exception 'Error 01: fn_etl_get_refyearset2panel4spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
		end if;
	
		select
				cacpr.id,
				cacpr.refyearset2panel
		from
				analytical.cm_spc2classrule2panel_refyearset as cacpr
		where
				cacpr.spc2classification_rule = _spc2classification_rule
		order
				by cacpr.refyearset2panel
		limit
				1
		into
				_res_id,
				_res_refyearset2panel;
	
		if _res_id is null
		then
			raise exception 'Error 02: fn_etl_get_refyearset2panel4spc2classification_rule: For input argument _spc2classification_rule = % not exists any refyearset2panel in cm_spc2classrule2panel_refyearset table!',_spc2classification_rule;
		end if;
	
		return query select _res_id, _res_refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) is
'Function returns one row of id and refyearset2panel from cm_spc2classrule2panel_refyearset table for given spc2classification_rule.';

alter function analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) to data_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4spc2classification_rule(integer) to public;


-- </function>

-- <function name="fn_etl_get_sp_attributes4ldsity_object" schema="analytical" src="functions/etl/fn_etl_get_sp_attributes4ldsity_object.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sp_attributes4ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_sp_attributes4ldsity_object
(
	_export_connection	integer,
	_ldsity_object		integer,
	_sub_population		integer[] default null::integer[],
	_check_attr			boolean default null::boolean		
)
returns table
(
	group_id									integer,
	lo__id										integer,
	lo__label									varchar,
	lo__description								text,
	lo__label_en								varchar,
	lo__description_en							text,
	aop__id										integer,
	aop__label									varchar,
	aop__description							text,
	sp__id										integer,
	sp__label									varchar,
	sp__description								text,
	sp__label_en								varchar,
	sp__description_en							text,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	spc__id										integer[],
	spc__label									varchar[],
	spc__description							text[],
	spc__label_en								varchar[],
	spc__description_en							text[],
	rule__id									integer[],
	rule__sub_population_category				integer[],
	rule__classification_rule					text[],
	check__sub_population						boolean,
	check__sub_population_category				boolean,
	check__spc_hierarchy						boolean,
	check__spc2classification_rule				boolean,
	check__spc2classrule2panel_refyearset		boolean,
	check__attr									boolean
)
as
$$
declare
		_cond_1	text;
		_cond_2	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sp_attributes4ldsity_object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_sp_attributes4ldsity_object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sp_attributes4ldsity_object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_sp_attributes4ldsity_object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if _sub_population is not null
		then
			for i in 1..array_length(_sub_population,1)
			loop
				if not exists (select t3.* from analytical.c_sub_population as t3 where t3.id = _sub_population[i])
				then
					raise exception 'Error 05: fn_etl_get_sp_attributes4ldsity_object: Given sub population (%) does not exist in c_sub_population table.', _sub_population[i];
				end if;
			end loop;
		end if;
	
		if _sub_population is not null and _check_attr is distinct from false
		then
			raise exception 'Error 06: fn_etl_get_sp_attributes4ldsity_object: If input argument _sub_population is not null then input argument _check_attr must be FALSE!';
		end if;		
		
		if _sub_population is not null
		then
			_cond_1 := 'w4.sp__id in (select unnest($3))';
		else
			_cond_1 := 'TRUE';
		end if;	
	
		if _check_attr is null
		then
			_cond_2 := 'TRUE';
		else
			if _check_attr = true
			then
				_cond_2 := 'w4.check__attr = true';
			else
				_cond_2 := 'w4.check__attr = false';
			end if;
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select * from analytical.fn_etl_get_sub_population_informations_attr($1,$2)
				)
		,w2 as	(
				select		
						w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.sp__id,
						w1.sp__label,
						w1.sp__description,
						w1.sp__label_en,
						w1.sp__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id,
						array_agg(w1.spc__id order by w1.rule__id) as spc__id,
						array_agg(w1.spc__label order by w1.rule__id) as spc__label,
						array_agg(w1.spc__description order by w1.rule__id) as spc__description,
						array_agg(w1.spc__label_en order by w1.rule__id) as spc__label_en,
						array_agg(w1.spc__description_en order by w1.rule__id) as spc__description_en,
						array_agg(w1.rule__id order by w1.rule__id) as rule__id,
						array_agg(w1.rule__sub_population_category order by w1.rule__id) as rule__sub_population_category,
						array_agg(w1.rule__classification_rule order by w1.rule__id) as rule__classification_rule
				from
						w1
				group
				by		w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.sp__id,
						w1.sp__label,
						w1.sp__description,
						w1.sp__label_en,
						w1.sp__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id
				)
		,w3 as	(
				select
						row_number() over() as group_id,
						w2.*,
						analytical.fn_etl_check_sub_population_attr($1,w2.spc__id)								as check__sub_population,
						analytical.fn_etl_check_sub_population_category_attr($1,w2.spc__id)						as check__sub_population_category,
						analytical.fn_etl_check_spc_hierarchy_attr($1,w2.spc__id)								as check__spc_hierarchy,
						analytical.fn_etl_check_spc2classification_rule_attr($1,w2.lo__id,w2.spc__id)			as check__spc2classification_rule,
						analytical.fn_etl_check_spc2classrule2panel_refyearset_attr($1,w2.lo__id,w2.spc__id)	as check__spc2classrule2panel_refyearset
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						case
							when
								w3.check__sub_population = true and
								w3.check__sub_population_category = true and
								w3.check__spc_hierarchy = true and
								w3.check__spc2classification_rule = true and
								w3.check__spc2classrule2panel_refyearset = true
							then
								true
							else
								false
						end as check__attr
							
				from
						w3
				)
		select
				w4.group_id::integer,
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.sp__id,
				w4.sp__label,
				w4.sp__description,
				w4.sp__label_en,
				w4.sp__description_en,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.spc__id,
				w4.spc__label,
				w4.spc__description,
				w4.spc__label_en,
				w4.spc__description_en,
				w4.rule__id,
				w4.rule__sub_population_category,
				w4.rule__classification_rule,
				w4.check__sub_population,
				w4.check__sub_population_category,
				w4.check__spc_hierarchy,
				w4.check__spc2classification_rule,
				w4.check__spc2classrule2panel_refyearset,
				w4.check__attr				
		from
				w4
		where
				'|| _cond_1 ||'
		and
				'|| _cond_2 ||'
		order
				by w4.group_id;
		'
		using _export_connection, _ldsity_object, _sub_population;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) is
'Function returns information of sub population attributes for given ldsity object.';

alter function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_spc2classification_rule" schema="analytical" src="functions/etl/fn_etl_get_spc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc2classification_rule
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_sub_population_category__id							integer,
	etl_sub_population_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_spc2classification_rule__id							integer,
	etl_spc2classification_rule__etl_id						integer,
	cm_spc2classrule2panel_refyearset__id					integer,
	cm_spc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond_1		text;
		_cond_2		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc2classification_rule: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_spc2classification_rule: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_spc2classification_rule: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_spc2classification_rule: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;		
	
		if _etl is null
		then
			_cond_1 := 'TRUE';
			_cond_2 := 'TRUE';
		else
			if _etl = true
			then
				_cond_1 := 'w8.etl_spc2classification_rule__etl_id is not null';
				_cond_2 := 'w10.etl_spc2classification_rule__etl_id is not null';
			else
				_cond_1 := 'w8.etl_spc2classification_rule__etl_id is null';
				_cond_2 := 'w10.etl_spc2classification_rule__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with 
		w1 as	(select * from analytical.fn_etl_get_sub_population_informations($1,$2))
		,w2 as 	(select distinct spc2classification_rule, etl_sub_population_category__id, etl_sub_population_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_spc2classification_rule__id, etl_spc2classification_rule__etl_id from w1)
		-----------------------------------------------
		,w3 as	(
				select
						w2.etl_sub_population_category__id,
						w2.etl_ldsity_object__id,
						tespc.sub_population_category,
						telo.ldsity_object
				from	w2
						inner join analytical.t_etl_sub_population_category as tespc on w2.etl_sub_population_category__id = tespc.id
						inner join analytical.t_etl_ldsity_object as telo on w2.etl_ldsity_object__id = telo.id
				)
		,w4 as	(		
				select distinct t1.ldsity_object, t4.id from
				(select w3.sub_population_category, w3.ldsity_object from w3) as t1
				inner join analytical.t_spc_hierarchy as t2 on t1.sub_population_category = t2.variable
				inner join analytical.c_sub_population_category as t3 on t2.variable_superior = t3.id
				inner join analytical.c_sub_population_category as t4 on t3.sub_population = t4.sub_population 
				)
		,w5 as	(
				select
						cacr.id as spc2classification_rule,
						cacr.classification_rule,
						cacr.sub_population_category,
						cacr.ldsity_object
				from w4
				inner join analytical.cm_spc2classification_rule as cacr 
				on w4.id = cacr.sub_population_category and w4.ldsity_object = cacr.ldsity_object
				)
		,w6 as	(
				select					
						w5.ldsity_object,
						cspc.sub_population,
						w5.sub_population_category,
						w5.spc2classification_rule,
						w5.classification_rule,
						telo.id as etl_ldsity_object__id,
						telo.etl_id as etl_ldsity_object__etl_id,
						tead.id as etl_sub_population__id,
						tead.etl_id as etl_sub_population__etl_id
				from
						w5
						
						inner join	analytical.c_sub_population_category as cspc on w5.sub_population_category = cspc.id
						inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
									on w5.ldsity_object = telo.ldsity_object
						inner join	(select * from analytical.t_etl_sub_population where export_connection = $1) as tead
									on cspc.sub_population = tead.sub_population
				)
		,w7 as	(
				select
						w6.*,
						tespc.id as etl_sub_population_category__id,
						tespc.etl_id as etl_sub_population_category__etl_id
				from
						w6
						inner join	(
									select * from analytical.t_etl_sub_population_category
									where etl_sub_population in (select etl_sub_population__id from w6)
									) as tespc
									on w6.sub_population_category = tespc.sub_population_category
				)
		,w8 as	(
				select
						w7.spc2classification_rule as id,
						w7.classification_rule,
						w7.etl_sub_population_category__id,
						w7.etl_sub_population_category__etl_id,
						w7.etl_ldsity_object__id,
						w7.etl_ldsity_object__etl_id,
						teacr.id as etl_spc2classification_rule__id,
						teacr.etl_id as etl_spc2classification_rule__etl_id,
						analytical.fn_etl_get_refyearset2panel4spc2classification_rule(w7.spc2classification_rule) as res
				from 	w7
						left join analytical.t_etl_spc2classification_rule as teacr
						on w7.spc2classification_rule = teacr.spc2classification_rule
						and w7.etl_sub_population_category__id = teacr.etl_sub_population_category
						and w7.etl_ldsity_object__id = teacr.etl_ldsity_object
				)
		-----------------------------------------------
		,w9 as	(
				select
						w2.spc2classification_rule as id,
						cmspc.classification_rule,
						w2.etl_sub_population_category__id,
						w2.etl_sub_population_category__etl_id,
						w2.etl_ldsity_object__id,
						w2.etl_ldsity_object__etl_id,
						w2.etl_spc2classification_rule__id,
						w2.etl_spc2classification_rule__etl_id
				from
						w2 inner join analytical.cm_spc2classification_rule as cmspc on w2.spc2classification_rule = cmspc.id
				)
		,w10 as	(
				select
						w9.id,
						w9.classification_rule,
						w9.etl_sub_population_category__id,
						w9.etl_sub_population_category__etl_id,
						w9.etl_ldsity_object__id,
						w9.etl_ldsity_object__etl_id,
						w9.etl_spc2classification_rule__id,
						w9.etl_spc2classification_rule__etl_id,
						analytical.fn_etl_get_refyearset2panel4spc2classification_rule(w9.id) as res
				from
						w9
				order
						by w9.id
				)
		-----------------------------------------------
		select
				w8.id,
				w8.classification_rule,
				w8.etl_sub_population_category__id,
				w8.etl_sub_population_category__etl_id,
				w8.etl_ldsity_object__id,
				w8.etl_ldsity_object__etl_id,
				w8.etl_spc2classification_rule__id,
				w8.etl_spc2classification_rule__etl_id,
				(w8.res).cm_spc2classrule2panel_refyearset__id,
				(w8.res).cm_spc2classrule2panel_refyearset__refyearset2panel
		from
				w8 where '|| _cond_1 ||'
		union
		select
				w10.id,
				w10.classification_rule,
				w10.etl_sub_population_category__id,
				w10.etl_sub_population_category__etl_id,
				w10.etl_ldsity_object__id,
				w10.etl_ldsity_object__etl_id,
				w10.etl_spc2classification_rule__id,
				w10.etl_spc2classification_rule__etl_id,
				(w10.res).cm_spc2classrule2panel_refyearset__id,
				(w10.res).cm_spc2classrule2panel_refyearset__refyearset2panel
		from
				w10 where '|| _cond_2 ||'
		order
				by id;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) is
'Function returns information records of area domain categories and rules.';

alter function analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_spc2classification_rule_attr" schema="analytical" src="functions/etl/fn_etl_get_spc2classification_rule_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc2classification_rule_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc2classification_rule_attr
(
	_export_connection			integer,
	_ldsity_object				integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_sub_population_category__id							integer,
	etl_sub_population_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_spc2classification_rule__id							integer,
	etl_spc2classification_rule__etl_id						integer,
	cm_spc2classrule2panel_refyearset__id					integer,
	cm_spc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc2classification_rule_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_spc2classification_rule_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_spc2classification_rule_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_spc2classification_rule_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _sub_population_category is not null
		then
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 05: fn_etl_get_spc2classification_rule_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_spc2classification_rule_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;		

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_spc2classification_rule__etl_id is not null';
			else
				_cond := 'w3.etl_spc2classification_rule__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_sub_population_category_attr($1,null::integer[],true)
					)
			,w2 as	(
					select
							w1.*,
							t.id as spc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_spc2classification_rule
										where sub_population_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.sub_population_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tesr.id as etl_spc2classification_rule__id,
							tesr.etl_id as etl_spc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4spc2classification_rule(w2.spc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_spc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_sub_population_category in (select w2.etl_sub_population_category__id from w2)
										and spc2classification_rule in (select w2.spc2classification_rule from w2)
										) as tesr
							on w2.etl_ldsity_object__id = tesr.etl_ldsity_object
							and w2.etl_sub_population_category__id = tesr.etl_sub_population_category
							and w2.spc2classification_rule = tesr.spc2classification_rule
					)
			select
					distinct
					w3.spc2classification_rule,
					w3.classification_rule,
					w3.etl_sub_population_category__id,
					w3.etl_sub_population_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_spc2classification_rule__id,
					w3.etl_spc2classification_rule__etl_id,
					(w3.res).cm_spc2classrule2panel_refyearset__id,
					(w3.res).cm_spc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.spc2classification_rule;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_sub_population_category_attr($1,$3,null::boolean)
					)
			,w2 as	(
					select
							w1.*,
							t.id as spc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_spc2classification_rule
										where sub_population_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.sub_population_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tesr.id as etl_spc2classification_rule__id,
							tesr.etl_id as etl_spc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4spc2classification_rule(w2.spc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_spc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_sub_population_category in (select w2.etl_sub_population_category__id from w2)
										and spc2classification_rule in (select w2.spc2classification_rule from w2)
										) as tesr
							on w2.etl_ldsity_object__id = tesr.etl_ldsity_object
							and w2.etl_sub_population_category__id = tesr.etl_sub_population_category
							and w2.spc2classification_rule = tesr.spc2classification_rule
					)
			select
					w3.spc2classification_rule,
					w3.classification_rule,
					w3.etl_sub_population_category__id,
					w3.etl_sub_population_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_spc2classification_rule__id,
					w3.etl_spc2classification_rule__etl_id,
					(w3.res).cm_spc2classrule2panel_refyearset__id,
					(w3.res).cm_spc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.spc2classification_rule;
			'
			using _export_connection, _ldsity_object, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) is
'Function returns information records of area domain categories and rules for attributes.';

alter function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_spc2classrule2panel_refyearset" schema="analytical" src="functions/etl/fn_etl_get_spc2classrule2panel_refyearset.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc2classrule2panel_refyearset
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id											integer,
	classification_rule							text,
	etl_sub_population_category__id				integer,
	etl_sub_population_category__etl_id			integer,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	etl_spc2classification_rule__id				integer,
	etl_spc2classification_rule__etl_id			integer,
	spc2classrule2panel_refyearset				integer,
	refyearset2panel							integer,
	etl_spc2classrule2panel_refyearset__id		integer,
	etl_spc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc2classrule2panel_refyearset: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_spc2classrule2panel_refyearset: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_spc2classrule2panel_refyearset: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_spc2classrule2panel_refyearset: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_spc2classrule2panel_refyearset__etl_id is not null';
			else
				_cond := 'w3.etl_spc2classrule2panel_refyearset__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with
		w1 as	(select * from analytical.fn_etl_get_spc2classification_rule($1,$2,null::boolean))
		,w2 as	(
				select
						w1.id,
						w1.classification_rule,
						w1.etl_sub_population_category__id,
						w1.etl_sub_population_category__etl_id,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id,
						w1.etl_spc2classification_rule__id,
						w1.etl_spc2classification_rule__etl_id,
						t.id as spc2classrule2panel_refyearset,
						--t.spc2classification_rule,
						t.refyearset2panel
				from	w1
				inner join	analytical.cm_spc2classrule2panel_refyearset as t
							on w1.id = t.spc2classification_rule
				)
		,w3 as	(
				select
						w2.*,
						tescpr.id as etl_spc2classrule2panel_refyearset__id,
						tescpr.etl_id as etl_spc2classrule2panel_refyearset__etl_id
				from
						w2
				left join	analytical.t_etl_spc2classrule2panel_refyearset as tescpr
							on w2.spc2classrule2panel_refyearset = tescpr.spc2classrule2panel_refyearset
							and w2.etl_spc2classification_rule__id = tescpr.etl_spc2classification_rule
				)
		select
				w3.id,
				w3.classification_rule,
				w3.etl_sub_population_category__id,
				w3.etl_sub_population_category__etl_id,
				w3.etl_ldsity_object__id,
				w3.etl_ldsity_object__etl_id,
				w3.etl_spc2classification_rule__id,
				w3.etl_spc2classification_rule__etl_id,
				w3.spc2classrule2panel_refyearset,
				w3.refyearset2panel,
				w3.etl_spc2classrule2panel_refyearset__id,
				w3.etl_spc2classrule2panel_refyearset__etl_id
		from
				w3 where '|| _cond ||'
		order
				by w3.id, w3.refyearset2panel;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) is
'Function returns information records of mapping between sub population classification rules and combinations of panels and reference year sets.';

alter function analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_spc2classrule2panel_refyearset_attr" schema="analytical" src="functions/etl/fn_etl_get_spc2classrule2panel_refyearset_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc2classrule2panel_refyearset_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr
(
	_export_connection			integer,
	_ldsity_object				integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id											integer,
	classification_rule							text,
	etl_sub_population_category__id				integer,
	etl_sub_population_category__etl_id			integer,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	etl_spc2classification_rule__id				integer,
	etl_spc2classification_rule__etl_id			integer,
	spc2classrule2panel_refyearset				integer,
	refyearset2panel							integer,
	etl_spc2classrule2panel_refyearset__id		integer,
	etl_spc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc2classrule2panel_refyearset_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_spc2classrule2panel_refyearset_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_spc2classrule2panel_refyearset_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_spc2classrule2panel_refyearset_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _sub_population_category is not null
		then	
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 05: fn_etl_get_spc2classrule2panel_refyearset_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_spc2classrule2panel_refyearset_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;		
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_spc2classrule2panel_refyearset__etl_id is not null';
			else
				_cond := 'w3.etl_spc2classrule2panel_refyearset__etl_id is null';
			end if;
		end if;

		if _sub_population_category is null
		then
			return query execute
			'	
			with
			w1 as	(select * from analytical.fn_etl_get_spc2classification_rule_attr($1,$2,null::integer[],true))
			,w2 as	(
					select
							w1.id,
							w1.classification_rule,
							w1.etl_sub_population_category__id,
							w1.etl_sub_population_category__etl_id,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
							w1.etl_spc2classification_rule__id,
							w1.etl_spc2classification_rule__etl_id,
							t.id as spc2classrule2panel_refyearset,
							t.refyearset2panel
					from	w1
					inner join	analytical.cm_spc2classrule2panel_refyearset as t
								on w1.id = t.spc2classification_rule
					)
			,w3 as	(
					select
							w2.*,
							tescpr.id as etl_spc2classrule2panel_refyearset__id,
							tescpr.etl_id as etl_spc2classrule2panel_refyearset__etl_id
					from
							w2
					left join	analytical.t_etl_spc2classrule2panel_refyearset as tescpr
								on w2.spc2classrule2panel_refyearset = tescpr.spc2classrule2panel_refyearset
								and w2.etl_spc2classification_rule__id = tescpr.etl_spc2classification_rule
					)
			select
					distinct
					w3.id,
					w3.classification_rule,
					w3.etl_sub_population_category__id,
					w3.etl_sub_population_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_spc2classification_rule__id,
					w3.etl_spc2classification_rule__etl_id,
					w3.spc2classrule2panel_refyearset,
					w3.refyearset2panel,
					w3.etl_spc2classrule2panel_refyearset__id,
					w3.etl_spc2classrule2panel_refyearset__etl_id
			from
					w3 where '|| _cond ||'
			order
					by w3.id, w3.refyearset2panel;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_spc2classification_rule_attr($1,$2,$3,null::boolean))
			,w2 as	(
					select
							w1.id,
							w1.classification_rule,
							w1.etl_sub_population_category__id,
							w1.etl_sub_population_category__etl_id,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
							w1.etl_spc2classification_rule__id,
							w1.etl_spc2classification_rule__etl_id,
							t.id as spc2classrule2panel_refyearset,
							t.refyearset2panel
					from	w1
					inner join	analytical.cm_spc2classrule2panel_refyearset as t
								on w1.id = t.spc2classification_rule
					)
			,w3 as	(
					select
							w2.*,
							tescpr.id as etl_spc2classrule2panel_refyearset__id,
							tescpr.etl_id as etl_spc2classrule2panel_refyearset__etl_id
					from
							w2
					left join	analytical.t_etl_spc2classrule2panel_refyearset as tescpr
								on w2.spc2classrule2panel_refyearset = tescpr.spc2classrule2panel_refyearset
								and w2.etl_spc2classification_rule__id = tescpr.etl_spc2classification_rule
					)
			select
					w3.id,
					w3.classification_rule,
					w3.etl_sub_population_category__id,
					w3.etl_sub_population_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_spc2classification_rule__id,
					w3.etl_spc2classification_rule__etl_id,
					w3.spc2classrule2panel_refyearset,
					w3.refyearset2panel,
					w3.etl_spc2classrule2panel_refyearset__id,
					w3.etl_spc2classrule2panel_refyearset__etl_id
			from
					w3 where '|| _cond ||'
			order
					by w3.id, w3.refyearset2panel;
			'
			using _export_connection, _ldsity_object, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) is
'Function returns information records of mapping between sub population classification rules and combinations of panels and reference year sets for attributes.';

alter function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer,integer[],  boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(integer, integer,integer[],  boolean) to public;


-- </function>

-- <function name="fn_etl_get_spc_hierarchy" schema="analytical" src="functions/etl/fn_etl_get_spc_hierarchy.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc_hierarchy
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id														integer,
	variable_superior										integer,
	variable												integer,
	dependent												boolean,
	etl_sub_population_category__id__variable_superior		integer,
	etl_sub_population_category__etl_id__variable_superior	integer,
	etl_sub_population_category__id__variable				integer,
	etl_sub_population_category__etl_id__variable			integer,
	etl_spc_hierarchy__id									integer,
	etl_spc_hierarchy__etl_id								integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc_hierarchy: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_spc_hierarchy: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_spc_hierarchy: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_spc_hierarchy: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;			
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w4.etl_spc_hierarchy__etl_id is not null';
			else
				_cond := 'w4.etl_spc_hierarchy__etl_id is null';
			end if;
		end if;

		return query execute
		'
		with
		w1 as	(select * from analytical.fn_etl_get_sub_population_category($1,$2,null::boolean) where variable_superior = false)	-- variable
		,w2 as	(select * from analytical.fn_etl_get_sub_population_category($1,$2,null::boolean) where variable_superior = true)	-- variable superior
		,w3 as	(
				select
						tsh.id as spc_hierarchy,
						tsh.variable_superior,
						tsh.variable,
						tsh.dependent,
						w2.etl_sub_population_category__id as etl_sub_population_category__id__variable_superior,
						w2.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable_superior,
						w1.etl_sub_population_category__id as etl_sub_population_category__id__variable,
						w1.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable
				from
						analytical.t_spc_hierarchy as tsh
						inner join	w1 on tsh.variable = w1.id
						inner join	w2 on tsh.variable_superior = w2.id
				)
		,w4 as	(
				select
						w3.spc_hierarchy,
						w3.variable_superior,
						w3.variable,
						w3.dependent,
						w3.etl_sub_population_category__id__variable_superior,
						w3.etl_sub_population_category__etl_id__variable_superior,
						w3.etl_sub_population_category__id__variable,
						w3.etl_sub_population_category__etl_id__variable,
						tesh.id as etl_spc_hierarchy__id,
						tesh.etl_id as etl_spc_hierarchy__etl_id
				from
						w3
				left join	analytical.t_etl_spc_hierarchy as tesh
							on w3.spc_hierarchy = tesh.spc_hierarchy
							and w3.etl_sub_population_category__id__variable_superior = tesh.etl_sub_population_category_variable_superior
							and w3.etl_sub_population_category__id__variable = tesh.etl_sub_population_category_variable
				)
		select
				w4.spc_hierarchy as id,
				w4.variable_superior,
				w4.variable,
				w4.dependent,
				w4.etl_sub_population_category__id__variable_superior,
				w4.etl_sub_population_category__etl_id__variable_superior,
				w4.etl_sub_population_category__id__variable,
				w4.etl_sub_population_category__etl_id__variable,
				w4.etl_spc_hierarchy__id,
				w4.etl_spc_hierarchy__etl_id
		from
				w4 where '|| _cond ||'
		order
				by w4.spc_hierarchy;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) is
'Function returns information records of sub population categories hierarchy.';

alter function analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_spc_hierarchy_attr" schema="analytical" src="functions/etl/fn_etl_get_spc_hierarchy_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc_hierarchy_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc_hierarchy_attr
(
	_export_connection			integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id														integer,
	variable_superior										integer,
	variable												integer,
	dependent												boolean,
	etl_sub_population_category__id__variable_superior		integer,
	etl_sub_population_category__etl_id__variable_superior	integer,
	etl_sub_population_category__id__variable				integer,
	etl_sub_population_category__etl_id__variable			integer,
	etl_spc_hierarchy__id									integer,
	etl_spc_hierarchy__etl_id								integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc_hierarchy_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_spc_hierarchy_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is not null
		then	
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 03: fn_etl_get_spc_hierarchy_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_spc_hierarchy_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w4.etl_spc_hierarchy__etl_id is not null';
			else
				_cond := 'w4.etl_spc_hierarchy__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,null::integer[],true) where variable_superior = false)		-- variable
			,w2 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,null::integer[],true) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tsh.id as spc_hierarchy,
							tsh.variable_superior,
							tsh.variable,
							tsh.dependent,
							w2.etl_sub_population_category__id as etl_sub_population_category__id__variable_superior,
							w2.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable_superior,
							w1.etl_sub_population_category__id as etl_sub_population_category__id__variable,
							w1.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable
					from
							analytical.t_spc_hierarchy as tsh
							inner join	w1 on tsh.variable = w1.id
							inner join	w2 on tsh.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.spc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_sub_population_category__id__variable_superior,
							w3.etl_sub_population_category__etl_id__variable_superior,
							w3.etl_sub_population_category__id__variable,
							w3.etl_sub_population_category__etl_id__variable,
							tesh.id as etl_spc_hierarchy__id,
							tesh.etl_id as etl_spc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_spc_hierarchy as tesh
								on w3.spc_hierarchy = tesh.spc_hierarchy
								and w3.etl_sub_population_category__id__variable_superior = tesh.etl_sub_population_category_variable_superior
								and w3.etl_sub_population_category__id__variable = tesh.etl_sub_population_category_variable
					)
			select
					w4.spc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_sub_population_category__id__variable_superior,
					w4.etl_sub_population_category__etl_id__variable_superior,
					w4.etl_sub_population_category__id__variable,
					w4.etl_sub_population_category__etl_id__variable,
					w4.etl_spc_hierarchy__id,
					w4.etl_spc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.spc_hierarchy;
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,$2,null::boolean) where variable_superior = false)		-- variable
			,w2 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,$2,null::boolean) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tsh.id as spc_hierarchy,
							tsh.variable_superior,
							tsh.variable,
							tsh.dependent,
							w2.etl_sub_population_category__id as etl_sub_population_category__id__variable_superior,
							w2.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable_superior,
							w1.etl_sub_population_category__id as etl_sub_population_category__id__variable,
							w1.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable
					from
							analytical.t_spc_hierarchy as tsh
							inner join	w1 on tsh.variable = w1.id
							inner join	w2 on tsh.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.spc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_sub_population_category__id__variable_superior,
							w3.etl_sub_population_category__etl_id__variable_superior,
							w3.etl_sub_population_category__id__variable,
							w3.etl_sub_population_category__etl_id__variable,
							tesh.id as etl_spc_hierarchy__id,
							tesh.etl_id as etl_spc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_spc_hierarchy as tesh
								on w3.spc_hierarchy = tesh.spc_hierarchy
								and w3.etl_sub_population_category__id__variable_superior = tesh.etl_sub_population_category_variable_superior
								and w3.etl_sub_population_category__id__variable = tesh.etl_sub_population_category_variable
					)
			select
					w4.spc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_sub_population_category__id__variable_superior,
					w4.etl_sub_population_category__etl_id__variable_superior,
					w4.etl_sub_population_category__id__variable,
					w4.etl_sub_population_category__etl_id__variable,
					w4.etl_spc_hierarchy__id,
					w4.etl_spc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.spc_hierarchy;
			'
			using _export_connection, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories hierarchy for attributes.';

alter function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_sub_population" schema="analytical" src="functions/etl/fn_etl_get_sub_population.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_sub_population
(
	_export_connection	integer,
	_ldsity				integer default null::integer,
	_etl				boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_sub_population__id		integer,
	etl_sub_population__etl_id	integer,
	variable_superior			boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_sub_population: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if _ldsity is not null
		then
			if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
			then
				raise exception 'Error 03: fn_etl_get_sub_population: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
			end if;		
		end if;		

		if _ldsity is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_sub_population: If input argument _ldsity is null then input argument _etl must be TRUE!';
		end if;

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w6.etl_sub_population__etl_id is not null';
			else
				_cond := 'w6.etl_sub_population__etl_id is null';
			end if;
		end if;

		if _ldsity is null
		then
			return query execute
			'
			with
			w1 as	(
					select tesp.* from analytical.t_etl_sub_population as tesp where tesp.export_connection = $1
					)		
			,w2a as	(
					select tsh.variable_superior from analytical.t_spc_hierarchy as tsh
					where tsh.variable_superior in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)					
			,w2b as	(
					select tsh.variable from analytical.t_spc_hierarchy as tsh
					where tsh.variable in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)
			,w3a as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.sub_population, true as variable_superior from w3a union all
					select w3b.sub_population, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.sub_population, variable_superior from w4 union
					select w1.sub_population, false as variable_superior from w1
					)				
			select
					w5.sub_population as id,
					csp.label,
					csp.description,
					csp.label_en,
					csp.description_en,
					w1.id as etl_sub_population__id,
					w1.etl_id as etl_sub_population__etl_id,
					w5.variable_superior
			from
					w5
					inner join w1 on w5.sub_population = w1.sub_population
					inner join analytical.c_sub_population as csp on w1.sub_population = csp.id
			order
					by w5.sub_population, w5.variable_superior
			'
			using _export_connection;
		else		
			return query execute
			'
			with 
			w1 as	(select * from analytical.fn_etl_get_sub_population_informations($1,$2))
			,w2 as 	(select distinct sub_population, etl_sub_population__id, etl_sub_population__etl_id from w1)
			,w3 as	(
					select
							w2.sub_population as id,
							cad.label,
							cad.description,
							cad.label_en,
							cad.description_en,
							w2.etl_sub_population__id,
							w2.etl_sub_population__etl_id
					from
							w2 inner join analytical.c_sub_population cad on w2.sub_population = cad.id
					)
			-----------------------------------------
			-- add TYPE from hierarchy
			,w4 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en
					from
							analytical.c_sub_population
					where
							id in
									(
									select distinct sub_population from analytical.c_sub_population_category where id in	-- finding out variable superior types
									(select distinct variable_superior from analytical.t_spc_hierarchy where variable in	-- finding out variable superior categories
									(select id from analytical.c_sub_population_category
									where sub_population in (select distinct w3.id from w3)))
									)
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_sub_population__id,
							t.etl_id as etl_sub_population__etl_id
					from
							w4
					left join	(select * from analytical.t_etl_sub_population where export_connection = $1) as t
								on w4.id = t.sub_population
					)
			-----------------------------------------
			,w6 as	(
					select
							w3.id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.etl_sub_population__id,
							w3.etl_sub_population__etl_id,
							false as variable_superior
					from
							w3 where w3.id not in (select w5.id from w5)
					union all
					select
							w5.id,
							w5.label,
							w5.description,
							w5.label_en,
							w5.description_en,
							w5.etl_sub_population__id,
							w5.etl_sub_population__etl_id,
							true as variable_superior
					from
							w5
					)
			select
					w6.id,
					w6.label,
					w6.description,
					w6.label_en,
					w6.description_en,
					w6.etl_sub_population__id,
					w6.etl_sub_population__etl_id,
					w6.variable_superior
			from
					w6 where '|| _cond ||'
			order
					by w6.id
			'
			using _export_connection, _ldsity;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population(integer, integer, boolean) is
'Function returns information records of sub populations.';

alter function analytical.fn_etl_get_sub_population(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_sub_population_attr" schema="analytical" src="functions/etl/fn_etl_get_sub_population_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_attr
(
	_export_connection			integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_sub_population__id		integer,
	etl_sub_population__etl_id	integer,
	variable_superior			boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_sub_population_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is not null
		then	
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 03: fn_etl_get_sub_population_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_sub_population_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w5.etl_sub_population__etl_id is not null';
			else
				_cond := 'w5.etl_sub_population__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'	
			with
			w1 as	(
					select tesp.* from analytical.t_etl_sub_population as tesp where tesp.export_connection = $1
					)		
			,w2a as	(
					select tsh.variable_superior from analytical.t_spc_hierarchy as tsh
					where tsh.variable_superior in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)					
			,w2b as	(
					select tsh.variable from analytical.t_spc_hierarchy as tsh
					where tsh.variable in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)
			,w3a as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.sub_population, true as variable_superior from w3a union all
					select w3b.sub_population, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.sub_population, variable_superior from w4 union
					select w1.sub_population, false as variable_superior from w1
					)				
			select
					w5.sub_population as id,
					csp.label,
					csp.description,
					csp.label_en,
					csp.description_en,
					w1.id as etl_sub_population__id,
					w1.etl_id as etl_sub_population__etl_id,
					w5.variable_superior
			from
					w5
					inner join w1 on w5.sub_population = w1.sub_population
					inner join analytical.c_sub_population as csp on w1.sub_population = csp.id
			order
					by w5.sub_population, w5.variable_superior
			'
			using _export_connection;		
		else	
			return query execute
			'
			with
			w1 as	(
					select
							csp.id,
							csp.label,
							csp.description,
							csp.label_en,
							csp.description_en
					from
							analytical.c_sub_population csp
					where
							csp.id in	(
										select distinct cspc.sub_population
										from analytical.c_sub_population_category as cspc
										where cspc.id in (select unnest($2))
										)
					)
			,w2 as	(
					select
							w1.*,
							tesp.id as etl_sub_population__id,
							tesp.etl_id as etl_sub_population__etl_id
					from
							w1
					left
					join	(select * from analytical.t_etl_sub_population where export_connection = $1) as tesp
							on w1.id = tesp.sub_population
					)
			-----------------------------------------
			-- add type of hierarchy
			,w3 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en
					from
							analytical.c_sub_population
					where
							id in
									(
									select distinct sub_population from analytical.c_sub_population_category where id in	-- finding out of variable superior types
									(select distinct variable_superior from analytical.t_spc_hierarchy where variable in	-- finding out of variable superior categories
									(select id from analytical.c_sub_population_category
									where sub_population in (select distinct w2.id from w2)))
									)
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_sub_population__id,
							t.etl_id as etl_sub_population__etl_id
					from
							w3
					left join	(select * from analytical.t_etl_sub_population where export_connection = $1) as t
								on w3.id = t.sub_population
					)
			-----------------------------------------
			,w5 as	(
					select
							w2.id,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en,
							w2.etl_sub_population__id,
							w2.etl_sub_population__etl_id,
							false as variable_superior
					from
							w2 where w2.id not in (select w4.id from w4)
					union all
					select
							w4.id,
							w4.label,
							w4.description,
							w4.label_en,
							w4.description_en,
							w4.etl_sub_population__id,
							w4.etl_sub_population__etl_id,
							true as variable_superior
					from
							w4
					)
			select
					w5.id,
					w5.label,
					w5.description,
					w5.label_en,
					w5.description_en,
					w5.etl_sub_population__id,
					w5.etl_sub_population__etl_id,
					w5.variable_superior
			from
					w5 where '|| _cond ||'
			order
					by w5.id;
			'
			using _export_connection, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) is
'Function returns information records of sub populations for given arguments.';

alter function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_sub_population_category" schema="analytical" src="functions/etl/fn_etl_get_sub_population_category.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_category(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_category
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id									integer,
	label								varchar,
	description							text,
	label_en							varchar,
	description_en						text,
	etl_sub_population__id				integer,
	etl_sub_population__etl_id			integer,
	etl_sub_population_category__id		integer,
	etl_sub_population_category__etl_id	integer,
	variable_superior					boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;		
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w8.etl_sub_population_category__etl_id is not null';
			else
				_cond := 'w8.etl_sub_population_category__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with 
		w1 as	(select * from analytical.fn_etl_get_sub_population_informations($1,$2))
		,w2 as 	(select distinct sub_population_category, etl_sub_population__id, etl_sub_population_category__id, etl_sub_population_category__etl_id from w1)
		,w3 as	(
				select
						w2.sub_population_category as id,
						cspc.label,
						cspc.description,
						cspc.label_en,
						cspc.description_en,
						w2.etl_sub_population__id as etl_sub_population,
						w2.etl_sub_population_category__id,
						w2.etl_sub_population_category__etl_id
				from
						w2 inner join analytical.c_sub_population_category as cspc on w2.sub_population_category = cspc.id
				)
		-----------------------------------------
		-- add CATEGORY from hierarchy
		,w4 as	(
				select
						id,
						label,
						description,
						label_en,
						description_en,
						sub_population
				from analytical.c_sub_population_category where sub_population in
				(select distinct sub_population from analytical.c_sub_population_category where id in
				(select distinct variable_superior from analytical.t_spc_hierarchy
				where variable in (select distinct w3.id from w3)))
				)
		,w5 as	(
				select w4.*, t.id as etl_sub_population from w4
				left join
				(select * from analytical.t_etl_sub_population where export_connection = $1 and sub_population in (select w4.sub_population from w4)) as t
				on w4.sub_population = t.sub_population
				)

		,w6 as	(
				select
						w5.id,
						w5.label,
						w5.description,
						w5.label_en,
						w5.description_en,
						w5.etl_sub_population,
						t.id as etl_sub_population_category__id,
						t.etl_id as etl_sub_population_category__etl_id
				from w5
				left join	(select * from analytical.t_etl_sub_population_category where etl_sub_population in (select w5.etl_sub_population from w5)) as t
							on w5.etl_sub_population = t.etl_sub_population
							and w5.id = t.sub_population_category
				)
		-----------------------------------------
		,w7 as	(
				select
						w3.id,
						w3.label,
						w3.description,
						w3.label_en,
						w3.description_en,
						w3.etl_sub_population,
						w3.etl_sub_population_category__id,
						w3.etl_sub_population_category__etl_id,
						false as variable_superior
				from
						w3 where w3.id not in (select w6.id from w6)
				union all
				select
						w6.id,
						w6.label,
						w6.description,
						w6.label_en,
						w6.description_en,
						w6.etl_sub_population,
						w6.etl_sub_population_category__id,
						w6.etl_sub_population_category__etl_id,
						true as variable_superior
				from
						w6
				)
		-----------------------------------------
		-- add etl_id from t_etl_sub_population
		,w8 as	(
				select
						w7.id,
						w7.label,
						w7.description,
						w7.label_en,
						w7.description_en,
						w7.etl_sub_population as etl_sub_population__id,
						tt.etl_id as etl_sub_population__etl_id,
						w7.etl_sub_population_category__id,
						w7.etl_sub_population_category__etl_id,
						w7.variable_superior
				from
						w7
						left join
									(
									select tesp.* from analytical.t_etl_sub_population as tesp
									where tesp.export_connection = $1
									and tesp.id in (select w7.etl_sub_population from w7 where w7.etl_sub_population is not null)
									) as tt
						on w7.etl_sub_population = tt.id
				)
		-----------------------------------------
		select
				w8.id,
				w8.label,
				w8.description,
				w8.label_en,
				w8.description_en,
				w8.etl_sub_population__id,
				w8.etl_sub_population__etl_id,
				w8.etl_sub_population_category__id,
				w8.etl_sub_population_category__etl_id,	
				w8.variable_superior
		from
				w8 where '|| _cond ||'
		order
				by w8.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) is
'Function returns information records of sub population categories.';

alter function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_sub_population_category_attr" schema="analytical" src="functions/etl/fn_etl_get_sub_population_category_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_category_attr
(
	_export_connection			integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id										integer,
	label									varchar,
	description								text,
	label_en								varchar,
	description_en							text,
	etl_sub_population__id					integer,
	etl_sub_population__etl_id				integer,
	etl_sub_population_category__id			integer,
	etl_sub_population_category__etl_id		integer,
	variable_superior						boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
		
		if _sub_population_category is not null
		then
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 03: fn_etl_get_sub_population_category_attr: Given area domain category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w7.etl_sub_population_category__etl_id is not null';
			else
				_cond := 'w7.etl_sub_population_category__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'		
			with
			w1 as	(
					select tesp.* from analytical.t_etl_sub_population as tesp where tesp.export_connection = $1
					)		
			,w2a as	(
					select tsh.variable_superior from analytical.t_spc_hierarchy as tsh
					where tsh.variable_superior in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)					
			,w2b as	(
					select tsh.variable from analytical.t_spc_hierarchy as tsh
					where tsh.variable in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)
			,w3a as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.sub_population, true as variable_superior from w3a union all
					select w3b.sub_population, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.sub_population, variable_superior from w4 union
					select w1.sub_population, false as variable_superior from w1
					)
			,w6 as	(
					select
							w5.sub_population as id,
							w1.id as etl_sub_population__id,
							w1.etl_id as etl_sub_population__etl_id,
							w5.variable_superior
					from
							w5
							inner join w1 on w5.sub_population = w1.sub_population
					)
			select
					t.sub_population_category as id,
					cspc.label,
					cspc.description,
					cspc.label_en,
					cspc.description_en,
					w6.etl_sub_population__id,
					w6.etl_sub_population__etl_id,
					t.id as etl_sub_population_category__id,
					t.etl_id as etl_sub_population_category__etl_id,
					w6.variable_superior
			from
					(
					select tespc.* from analytical.t_etl_sub_population_category as tespc
					where tespc.etl_sub_population in (select w6.etl_sub_population__id from w6)
					) as t
			inner join w6 on t.etl_sub_population = w6.etl_sub_population__id
			inner join analytical.c_sub_population_category as cspc on t.sub_population_category = cspc.id
			order
					by t.sub_population_category, w6.variable_superior
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(
					select
							cspc2.id,
							cspc2.label,
							cspc2.description,
							cspc2.label_en,
							cspc2.description_en,
							cspc2.sub_population
					from
							analytical.c_sub_population_category as cspc2
					where
							cspc2.sub_population in	(
													select distinct cspc1.sub_population
													from analytical.c_sub_population_category as cspc1
													where cspc1.id in (select unnest($2))
													)
					)
			,w2 as	(
					select
							w1.*,
							tesp.id as etl_sub_population__id,
							tesp.etl_id as etl_sub_population__etl_id
					from
							w1
					left
					join	(select * from analytical.t_etl_sub_population where export_connection = $1 and sub_population in (select distinct w1.sub_population from w1)) as tesp
							on w1.sub_population = tesp.sub_population
					)		
			,w3 as	(
					select
							w2.id,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en,
							w2.etl_sub_population__id,
							w2.etl_sub_population__etl_id,
							t.id as etl_sub_population_category__id,
							t.etl_id as etl_sub_population_category__etl_id
					from
							w2
							left join (select * from analytical.t_etl_sub_population_category as tespc where tespc.etl_sub_population in (select w2.etl_sub_population__id from w2)) as t
							on w2.etl_sub_population__id = t.etl_sub_population
							and w2.id = t.sub_population_category
					)
			-----------------------------------------	
			-----------------------------------------
			-- add CATEGORY from hierarchy
			,w4 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en,
							sub_population
					from analytical.c_sub_population_category where sub_population in
					(select distinct sub_population from analytical.c_sub_population_category where id in
					(select distinct variable_superior from analytical.t_spc_hierarchy
					where variable in (select distinct w3.id from w3)))
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_sub_population__id,
							t.etl_id as etl_sub_population__etl_id
					from w4
					left join
						(
						select * from analytical.t_etl_sub_population
						where export_connection = $1 and sub_population in (select w4.sub_population from w4)
						) as t
					on w4.sub_population = t.sub_population
					)
			,w6 as	(
					select
							w5.id,
							w5.label,
							w5.description,
							w5.label_en,
							w5.description_en,
							w5.etl_sub_population__id,
							w5.etl_sub_population__etl_id,
							t.id as etl_sub_population_category__id,
							t.etl_id as etl_sub_population_category__etl_id
					from w5
					left join	(
								select * from analytical.t_etl_sub_population_category
								where etl_sub_population in (select w5.etl_sub_population__id from w5)
								) as t
					on w5.etl_sub_population__id = t.etl_sub_population
					and w5.id = t.sub_population_category
					)
			-----------------------------------------
			,w7 as	(
					select
							w3.id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.etl_sub_population__id,
							w3.etl_sub_population__etl_id,
							w3.etl_sub_population_category__id,
							w3.etl_sub_population_category__etl_id,
							false as variable_superior
					from
							w3 where w3.id not in (select w6.id from w6)
					union all
					select
							w6.id,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en,
							w6.etl_sub_population__id,
							w6.etl_sub_population__etl_id,
							w6.etl_sub_population_category__id,
							w6.etl_sub_population_category__etl_id,
							true as variable_superior
					from
							w6
					)
			-----------------------------------------
			-----------------------------------------
			select
					w7.id,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					w7.etl_sub_population__id,
					w7.etl_sub_population__etl_id,
					w7.etl_sub_population_category__id,
					w7.etl_sub_population_category__etl_id,	
					w7.variable_superior
			from
					w7 where '|| _cond ||'
			order
					by w7.id
			'
			using _export_connection, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories for given arguments.';

alter function analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category_attr(integer, integer[], boolean) to public;


-- </function>

-- <function name="fn_etl_get_sub_population_informations" schema="analytical" src="functions/etl/fn_etl_get_sub_population_informations.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_informations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_informations(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_informations
(
	_export_connection	integer,
	_ldsity				integer
)
returns table
(
	ldsity_object							integer,
	sub_population							integer,
	sub_population_category					integer,
	spc2classification_rule					integer,
	------------------------------------------------
	etl_ldsity_object__id					integer,
	etl_ldsity_object__etl_id				integer,
	etl_sub_population__id					integer,
	etl_sub_population__etl_id				integer,
	etl_sub_population_category__id			integer,
	etl_sub_population_category__etl_id		integer,
	etl_spc2classification_rule__id			integer,
	etl_spc2classification_rule__etl_id		integer
)
as
$$
declare
		_spc_array	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_informations: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_informations: Input argument _ldsity must not be NULL!';
		end if;
	
		select cl.sub_population_category from analytical.c_ldsity as cl where cl.id = _ldsity
		into _spc_array;
	
		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_informations: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_sub_population_informations: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;

		if _spc_array is null
		then
			raise exception 'Error 05: fn_etl_get_sub_population_informations: For input argument _ldsity = % not exists any reduction of sub population category!',_ldsity;
		else		
			return query
			with
			w1 as	(						
					select
							t2.sub_population as rule__type,
							t1.rule__category,
							t1.rule__id,
							t1.rule__ldsity_object
					from
						(
						select
								cmspc2cr.id as rule__id,
								cmspc2cr.sub_population_category as rule__category,
								cmspc2cr.ldsity_object as rule__ldsity_object
						from analytical.cm_spc2classification_rule as cmspc2cr
						where cmspc2cr.id in (select unnest(_spc_array))
						) as t1
					inner join	analytical.c_sub_population_category as t2 on t1.rule__category = t2.id			
					)
			,w2 as	(
					select distinct rule__type, rule__category, rule__id, rule__ldsity_object from w1
					)
			,w3 as	(
					select
							w2.rule__type as rule__type_all,
							w2.rule__category,
							w2.rule__id,
							w2.rule__ldsity_object,
							cspc.id as rule__category_all,
							cscr.id as rule__id_all,
							telo.id as etl_ldsity_object__id,			-- idecko do tabulky analytical.t_etl_ldsity_object
							telo.etl_id as etl_ldsity_object__etl_id	-- idecko do tabulky target_data.c_ldsity_object
					from w2
					inner join analytical.c_sub_population_category as cspc on w2.rule__type = cspc.sub_population
					inner join analytical.cm_spc2classification_rule as cscr on w2.rule__ldsity_object = cscr.ldsity_object and cspc.id = cscr.sub_population_category
					left join (select * from analytical.t_etl_ldsity_object where export_connection = _export_connection) as telo on w2.rule__ldsity_object = telo.ldsity_object
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_sub_population__id,			-- idecko do tabulky analytical.t_etl_sub_population
							t.etl_id as etl_sub_population__etl_id		-- idecko do tabulky target_data.c_sub_population
					from 
							w3
					left join	(select * from analytical.t_etl_sub_population where export_connection = _export_connection) as t on w3.rule__type_all = t.sub_population
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_sub_population_category__id,			-- idecko do tabulky analytical.t_etl_sub_population_category
							t.etl_id as etl_sub_population_category__etl_id	-- idecko do tabulky target_data.c_sub_population_category
					from
							w4
					left join	(select * from analytical.t_etl_sub_population_category where etl_sub_population in (select w4.etl_sub_population__id from w4)) as t
								on w4.etl_sub_population__id = t.etl_sub_population
								and w4.rule__category_all = t.sub_population_category
					)
			,w6 as	(
					select
							w5.*,
							t.id as etl_spc2classification_rule__id,
							t.etl_id as etl_spc2classification_rule__etl_id
					from
							w5
					left join	(select * from analytical.t_etl_spc2classification_rule where etl_sub_population_category in (select w5.etl_sub_population_category__id from w5)) as t
								on w5.etl_sub_population_category__id = t.etl_sub_population_category
								and w5.etl_ldsity_object__id = t.etl_ldsity_object
								and w5.rule__id_all = t.spc2classification_rule
					)
			select
					w6.rule__ldsity_object			as ldsity_object,
					w6.rule__type_all				as sub_population,
					w6.rule__category_all			as sub_population_category,
					w6.rule__id_all					as spc2classification_rule,
					---------------------
					w6.etl_ldsity_object__id,
					w6.etl_ldsity_object__etl_id,
					w6.etl_sub_population__id,
					w6.etl_sub_population__etl_id,
					w6.etl_sub_population_category__id,
					w6.etl_sub_population_category__etl_id,
					w6.etl_spc2classification_rule__id,
					w6.etl_spc2classification_rule__etl_id
			from
					w6 order by w6.rule__id_all;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_informations(integer, integer) is
'Function returns sub population informations for given input arguments: export connection and ldsity.';

alter function analytical.fn_etl_get_sub_population_informations(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to public;


-- </function>

-- <function name="fn_etl_get_sub_population_informations_attr" schema="analytical" src="functions/etl/fn_etl_get_sub_population_informations_attr.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_informations_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_informations_attr(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_informations_attr
(
	_export_connection	integer,
	_ldsity_object		integer
)
returns table
(
	lo__id								integer,
	lo__label							varchar,
	lo__description						text,
	lo__label_en						varchar,
	lo__description_en					text,
	aop__id								integer,
	aop__label							varchar,
	aop__description					text,
	sp__id								integer,
	sp__label							varchar,
	sp__description						text,
	sp__label_en						varchar,
	sp__description_en					text,
	spc__id								integer,
	spc__sub_population					integer,
	spc__label							varchar,
	spc__description					text,
	spc__label_en						varchar,
	spc__description_en					text,
	rule__id							integer,
	rule__sub_population_category		integer,
	rule__classification_rule			text,
	etl_ldsity_object__id				integer,
	etl_ldsity_object__etl_id			integer,
	etl_sub_population__id				integer,
	etl_sub_population__etl_id			integer,
	etl_sub_population_category__id		integer,
	etl_sub_population_category__etl_id	integer,
	etl_spc2classification_rule__id		integer,
	etl_spc2classification_rule__etl_id	integer
)
as
$$
declare
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_informations_attr: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_informations_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_informations_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_sub_population_informations_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if not exists (select t3.* from analytical.t_etl_ldsity_object as t3 where t3.export_connection = _export_connection and t3.ldsity_object = _ldsity_object)
		then
			raise exception 'Error 05: fn_etl_get_sub_population_informations_attr: For given export connection (%) and ldsity object (%) does not exist record in t_etl_ldsity_object table.', _export_connection, _ldsity_object;
		end if;	
		
		return query
		with
		w1 as	(						
				select
						t4.id as lo__id,
						t4.label as lo__label,
						t4.description as lo__description,
						t4.label_en as lo__label_en,
						t4.description_en as lo__description_en,
						-----------------------------------------
						t5.id as aop__id,
						t5.label as aop__label,
						t5.description as aop__description,
						-----------------------------------------
						t3.id as sp__id,
						t3.label as sp__label,
						t3.description as sp__description,
						t3.label_en as sp__label_en,
						t3.description_en as sp__description_en,
						-----------------------------------------
						t2.id as spc__id,
						t2.sub_population as spc__sub_population,
						t2.label as spc__label,
						t2.description as spc__description,
						t2.label_en as spc__label_en,
						t2.description_en as spc__description_en,
						-----------------------------------------
						t1.rule__id,
						t1.rule__sub_population_category,
						t1.rule__classification_rule,
						-----------------------------------------
						t6.id as etl_ldsity_object__id,
						t6.etl_id as etl_ldsity_object__etl_id
				from
					(
					select
							cmspc2cr.id as rule__id,
							cmspc2cr.sub_population_category as rule__sub_population_category,
							cmspc2cr.ldsity_object as rule__ldsity_object,
							cmspc2cr.classification_rule as rule__classification_rule
					from
							analytical.cm_spc2classification_rule as cmspc2cr
					inner join 	analytical.cm_spc2classrule2panel_refyearset as t2
					on 		cmspc2cr.id = t2.spc2classification_rule

					where
							cmspc2cr.ldsity_object = _ldsity_object
					) as t1
				
				inner join	analytical.c_sub_population_category as t2 on t1.rule__sub_population_category = t2.id
				inner join	analytical.c_sub_population as t3 on t2.sub_population = t3.id
				inner join	analytical.c_ldsity_object as t4 on t1.rule__ldsity_object = t4.id
				inner join	analytical.c_areal_or_population as t5 on t4.areal_or_population = t5.id
				inner join	(select telo.* from analytical.t_etl_ldsity_object as telo where telo.export_connection = _export_connection) as t6 on t1.rule__ldsity_object = t6.ldsity_object
				)
		,w2 as	(
				select
						w1.*,
						a1.id as etl_sub_population__id,
						a1.etl_id as etl_sub_population__etl_id
				from
						w1
				
				left join	(select * from analytical.t_etl_sub_population where export_connection = _export_connection) as a1 on w1.sp__id = a1.sub_population
				)
		,w3 as	(
				select
						w2.*,
						a2.id as etl_sub_population_category__id,
						a2.etl_id as etl_sub_population_category__etl_id
				from
						w2
						
				left join	(
							select tespc.* from analytical.t_etl_sub_population_category as tespc
							where tespc.etl_sub_population in (select w2.etl_sub_population__id from w2)
							) as a2
							on w2.etl_sub_population__id = a2.etl_sub_population and w2.spc__id = a2.sub_population_category
				)
		,w4 as	(
				select
						w3.*,
						a3.id as etl_spc2classification_rule__id,
						a3.etl_id as etl_spc2classification_rule__etl_id
				from
						w3
				left join	(
							select * from analytical.t_etl_spc2classification_rule
							where etl_ldsity_object in (select distinct w3.etl_ldsity_object__id from w3)
							and etl_sub_population_category in (select w3.etl_sub_population_category__id from w3)
							) as a3
							on w3.etl_sub_population_category__id = a3.etl_sub_population_category
							and w3.etl_ldsity_object__id = a3.etl_ldsity_object
							and w3.rule__id = a3.spc2classification_rule					
				)
		select
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.sp__id,
				w4.sp__label,
				w4.sp__description,
				w4.sp__label_en,
				w4.sp__description_en,
				w4.spc__id,
				w4.spc__sub_population,
				w4.spc__label,
				w4.spc__description,
				w4.spc__label_en,
				w4.spc__description_en,
				w4.rule__id,
				w4.rule__sub_population_category,
				w4.rule__classification_rule,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.etl_sub_population__id,
				w4.etl_sub_population__etl_id,
				w4.etl_sub_population_category__id,
				w4.etl_sub_population_category__etl_id,
				w4.etl_spc2classification_rule__id,
				w4.etl_spc2classification_rule__etl_id
		from
				w4 order by w4.rule__id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) is
'Function returns sub population informations for given input arguments: export connection and ldsity object.';

alter function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to public;


-- </function>

-- <function name="fn_etl_get_unit_of_measure" schema="analytical" src="functions/etl/fn_etl_get_unit_of_measure.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_unit_of_measure
(
	_export_connection	integer,
	_ldsity				integer default null::integer,
	_etl				boolean default null::boolean
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	etl_unit_of_measure__id			integer,
	etl_unit_of_measure__etl_id		integer
)
as
$$
declare
		_cond_1		text;
		_cond_2		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_unit_of_measure: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_unit_of_measure: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if _ldsity is not null
		then
			if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
			then
				raise exception 'Error 03: fn_etl_get_unit_of_measure: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
			end if;		
		end if;

		if _ldsity is null
		then
			_cond_1 := 'TRUE';
		else
			_cond_1 := 'cuom.id = (select cl.unit_of_measure from analytical.c_ldsity as cl where cl.id = $2)';
		end if;
	
		if _etl is null
		then
			_cond_2 := 'TRUE';
		else
			if _etl = true
			then
				_cond_2 := 'w3.etl_unit_of_measure__etl_id is not null';
			else
				_cond_2 := 'w3.etl_unit_of_measure__etl_id is null';
			end if;
		end if;
			
		return query execute
		'
		with
		w1 as	(
				select cuom.* from analytical.c_unit_of_measure as cuom
				where '|| _cond_1 ||'
				)
		,w2 as	(
				select * from analytical.t_etl_unit_of_measure
				where export_connection = $1
				)
		,w3 as	(
				select
					w1.*,
					w2.id as etl_unit_of_measure__id,
					w2.etl_id as etl_unit_of_measure__etl_id
				from
					w1 left join w2 on w1.id = w2.unit_of_measure
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label_en,
				w3.description_en,
				w3.etl_unit_of_measure__id,
				w3.etl_unit_of_measure__etl_id
		from
				w3 where '|| _cond_2 ||'
		order
				by w3.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) is
'Function returns information records of unit of measure.';

alter function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_get_version" schema="analytical" src="functions/etl/fn_etl_get_version.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_version(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_version
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id						integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_version__id			integer,
	etl_version__etl_id		integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_version: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_version: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_version: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_version: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_version__etl_id is not null';
			else
				_cond := 'w3.etl_version__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select * from analytical.c_version where id in
				(select version from analytical.cm_ldsity2panel_refyearset_version where ldsity = $2)	
				)
		,w2 as	(
				select * from analytical.t_etl_version
				where export_connection = $1
				)
		,w3 as	(
				select
					w1.*,
					w2.id as etl_version__id,
					w2.etl_id as etl_version__etl_id
				from
					w1 left join w2 on w1.id = w2.version
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label_en,
				w3.description_en,
				w3.etl_version__id,
				w3.etl_version__etl_id
		from
				w3 where '|| _cond ||'
		order
				by w3.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_version(integer, integer, boolean) is
'Function returns information records of versions.';

alter function analytical.fn_etl_get_version(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_version(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_version(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_version(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_version(integer, integer, boolean) to public;


-- </function>

-- <function name="fn_etl_save_adc2classification_rule" schema="analytical" src="functions/etl/fn_etl_save_adc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_adc2classification_rule
(
	_adc2classification_rule		integer,
	_etl_id							integer,
	_etl_area_domain_category		integer,
	_etl_ldsity_object				integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _adc2classification_rule is null
	then
		raise exception 'Error 01: fn_etl_save_adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_adc2classification_rule: Input argument _etl_id must not be NULL!';
	end if; 

	if _etl_area_domain_category is null
	then
		raise exception 'Error 03: fn_etl_save_adc2classification_rule: Input argument _etl_area_domain_category must not be NULL!';
	end if;

	if _etl_ldsity_object is null
	then
		raise exception 'Error 04: fn_etl_save_adc2classification_rule: Input argument _etl_ldsity_object must not be NULL!';
	end if; 

	insert into analytical.t_etl_adc2classification_rule(adc2classification_rule, etl_id, etl_area_domain_category, etl_ldsity_object)
	select _adc2classification_rule, _etl_id, _etl_area_domain_category, _etl_ldsity_object
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) is
'Function inserts a record into table t_etl_adc2classification_rule based on given parameters.';

alter function analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_adc2classification_rule(integer, integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_adc2classrule2panel_refyearset" schema="analytical" src="functions/etl/fn_etl_save_adc2classrule2panel_refyearset.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_adc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_adc2classrule2panel_refyearset
(
	_adc2classrule2panel_refyearset		integer,
	_etl_id								integer,
	_etl_adc2classification_rule		integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _adc2classrule2panel_refyearset is null
	then
		raise exception 'Error 01: fn_etl_save_adc2classrule2panel_refyearset: Input argument _adc2classrule2panel_refyearset must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_adc2classrule2panel_refyearset: Input argument _etl_id must not be NULL!';
	end if; 

	if _etl_adc2classification_rule is null
	then
		raise exception 'Error 03: fn_etl_save_adc2classrule2panel_refyearset: Input argument _etl_adc2classification_rule must not be NULL!';
	end if;

	insert into analytical.t_etl_adc2classrule2panel_refyearset(adc2classrule2panel_refyearset, etl_id, etl_adc2classification_rule)
	select _adc2classrule2panel_refyearset, _etl_id, _etl_adc2classification_rule
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) is
'Function inserts a record into table t_etl_adc2classrule2panel_refyearset based on given parameters.';

alter function analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_adc2classrule2panel_refyearset(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_adc_hierarchy" schema="analytical" src="functions/etl/fn_etl_save_adc_hierarchy.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_adc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_adc_hierarchy
(
	_adc_hierarchy									integer,
	_etl_area_domain_category_variable_superior		integer,
	_etl_area_domain_category_variable				integer,
	_etl_id											integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _adc_hierarchy is null
	then
		raise exception 'Error 01: fn_etl_save_adc_hierarchy: Input argument _adc_hierarchy must not be NULL!';
	end if; 

	if _etl_area_domain_category_variable_superior is null
	then
		raise exception 'Error 02: fn_etl_save_adc_hierarchy: Input argument _etl_area_domain_category_variable_superior must not be NULL!';
	end if; 

	if _etl_area_domain_category_variable is null
	then
		raise exception 'Error 03: fn_etl_save_adc_hierarchy: Input argument _etl_area_domain_category_variable must not be NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 04: fn_etl_save_adc_hierarchy: Input argument _etl_id must not be NULL!';
	end if; 

	insert into analytical.t_etl_adc_hierarchy(adc_hierarchy, etl_area_domain_category_variable_superior, etl_area_domain_category_variable, etl_id)
	select _adc_hierarchy, _etl_area_domain_category_variable_superior, _etl_area_domain_category_variable, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) is
'Function inserts a record into table t_etl_adc_hierarchy based on given parameters.';

alter function analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_adc_hierarchy(integer, integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_area_domain" schema="analytical" src="functions/etl/fn_etl_save_area_domain.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_area_domain(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_area_domain
(
	_export_connection		integer,
	_area_domain			integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_area_domain: Input argument _export_connection must not by NULL!';
	end if; 

	if _area_domain is null
	then
		raise exception 'Error 02: fn_etl_save_area_domain: Input argument _area_domain must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_area_domain: Input argument _etl_id must not by NULL!';
	end if;

	if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error 04: fn_etl_save_area_domain: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;

	if not exists (select t2.* from analytical.c_area_domain as t2 where t2.id = _area_domain)
	then
		raise exception 'Error 05: fn_etl_save_area_domain: Given area domain (%) does not exist in c_area_domain table.', _area_domain;
	end if;		

	insert into analytical.t_etl_area_domain(export_connection, area_domain, etl_id)
	select _export_connection, _area_domain, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_area_domain(integer, integer, integer) is
'Function inserts a record into table t_etl_area_domain based on given parameters.';

alter function analytical.fn_etl_save_area_domain(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_area_domain_category" schema="analytical" src="functions/etl/fn_etl_save_area_domain_category.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_area_domain_category(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_area_domain_category
(
	_area_domain_category	integer,
	_etl_id					integer,
	_etl_area_domain		integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _area_domain_category is null
	then
		raise exception 'Error 01: fn_etl_save_area_domain_category: Input argument _area_domain_category must not by NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_area_domain_category: Input argument _etl_id must not by NULL!';
	end if;

	if _etl_area_domain is null
	then
		raise exception 'Error 03: fn_etl_save_area_domain_category: Input argument _etl_area_domain must not by NULL!';
	end if;

	if not exists (select t.* from analytical.c_area_domain_category as t where t.id = _area_domain_category)
	then
		raise exception 'Error 04: fn_etl_save_area_domain_category: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category;
	end if;	

	insert into analytical.t_etl_area_domain_category(area_domain_category, etl_id, etl_area_domain)
	select _area_domain_category, _etl_id, _etl_area_domain
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) is
'Function inserts a record into table t_etl_area_domain_category based on given parameters.';

alter function analytical.fn_etl_save_area_domain_category(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_areal_or_population" schema="analytical" src="functions/etl/fn_etl_save_areal_or_population.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_areal_or_population(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_areal_or_population
(
	_export_connection		integer,
	_areal_or_population	integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id		integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_areal_or_population: Input argument _export_connection must not be NULL!';
	end if; 

	if _areal_or_population is null
	then
		raise exception 'Error 02: fn_etl_save_areal_or_population: Input argument _areal_or_population must not be NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_areal_or_population: Input argument _etl_id must not be NULL!';
	end if;

	if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error 04: fn_etl_save_areal_or_population: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;

	if not exists (select t2.* from analytical.c_areal_or_population as t2 where t2.id = _areal_or_population)
	then
		raise exception 'Error 05: fn_etl_save_areal_or_population: Given areal or population (%) does not exist in c_areal_or_population table.', _areal_or_population;
	end if;

	insert into analytical.t_etl_areal_or_population(export_connection, areal_or_population, etl_id)
	select _export_connection, _areal_or_population, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_areal_or_population(integer, integer, integer) is
'Function inserts a record into table t_etl_areal_or_population based on given parameters.';

alter function analytical.fn_etl_save_areal_or_population(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_areal_or_population(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_areal_or_population(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_areal_or_population(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_areal_or_population(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_definition_variant" schema="analytical" src="functions/etl/fn_etl_save_definition_variant.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_definition_variant(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_definition_variant
(
	_export_connection		integer,
	_definition_variant		integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_definition_variant: Input argument _export_connection must not by NULL!';
	end if; 

	if _definition_variant is null
	then
		raise exception 'Error 02: fn_etl_save_definition_variant: Input argument _definition_variant must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_definition_variant: Input argument _etl_id must not by NULL!';
	end if;

	if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error 04: fn_etl_save_definition_variant: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;

	if not exists (select t2.* from analytical.c_definition_variant as t2 where t2.id = _definition_variant)
	then
		raise exception 'Error 05: fn_etl_save_definition_variant: Given definition variant (%) does not exist in c_definition_variant table.', _definition_variant;
	end if;		

	insert into analytical.t_etl_definition_variant(export_connection, definition_variant, etl_id)
	select _export_connection, _definition_variant, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_definition_variant(integer, integer, integer) is
'Function inserts a record into table t_etl_definition_variant based on given parameters.';

alter function analytical.fn_etl_save_definition_variant(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_definition_variant(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_definition_variant(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_definition_variant(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_definition_variant(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_export_connection" schema="analytical" src="functions/etl/fn_etl_save_export_connection.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) CASCADE;

create or replace function analytical.fn_etl_save_export_connection
(
	_host		varchar,
	_dbname		varchar,
	_port		integer,
	_comment	text
)
returns integer
as
$$
declare
	_id		integer;
begin
	if _host is null or _dbname is null or _port is null or _comment is null
	then
		raise exception 'Error 01: fn_etl_save_export_connection: Mandatory input of host, dbname, port and comment is null (%, %, %, %).', _host, _dbname, _port, _comment;
	end if; 

	insert into analytical.t_export_connection(host, dbname, port, comment)
	select _host, _dbname, _port, _comment
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) is
'The function inserts a new record into t_export_connection table.';

alter function analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) to adm_analytical;
grant execute on function analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) to data_analytical;
grant execute on function analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) to usr_analytical;
grant execute on function analytical.fn_etl_save_export_connection(varchar, varchar, integer, text) to public;
-- </function>

-- <function name="fn_etl_save_ldsity" schema="analytical" src="functions/etl/fn_etl_save_ldsity.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_ldsity(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_ldsity
(
	_ldsity				integer,
	_etl_id				integer,
	_etl_ldsity_object	integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _ldsity is null
	then
		raise exception 'Error 01: fn_etl_save_ldsity: Input argument _ldsity must not by NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_ldsity: Input argument _etl_id must not by NULL!';
	end if; 

	if _etl_ldsity_object is null
	then
		raise exception 'Error 03: fn_etl_save_ldsity: Input argument _etl_ldsity_object must not by NULL!';
	end if;

	insert into analytical.t_etl_ldsity(ldsity, etl_id, etl_ldsity_object)
	select _ldsity, _etl_id, _etl_ldsity_object
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_ldsity(integer, integer, integer) is
'Function inserts a record into table t_etl_ldsity based on given parameters.';

alter function analytical.fn_etl_save_ldsity(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_ldsity2panel_refyearset_version" schema="analytical" src="functions/etl/fn_etl_save_ldsity2panel_refyearset_version.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_ldsity2panel_refyearset_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_ldsity2panel_refyearset_version
(
	_ldsity2panel_refyearset_version	integer,
	_etl_ldsity							integer,
	_etl_version						integer,
	_etl_id								integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _ldsity2panel_refyearset_version is null
	then
		raise exception 'Error 01: fn_etl_save_ldsity2panel_refyearset_version: Input argument _ldsity2panel_refyearset_version must not be NULL!';
	end if; 

	if _etl_ldsity is null
	then
		raise exception 'Error 02: fn_etl_save_ldsity2panel_refyearset_version: Input argument _etl_ldsity must not be NULL!';
	end if; 

	if _etl_version is null
	then
		raise exception 'Error 03: fn_etl_save_ldsity2panel_refyearset_version: Input argument _etl_version must not be NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 04: fn_etl_save_ldsity2panel_refyearset_version: Input argument _etl_id must not be NULL!';
	end if;	

	insert into analytical.t_etl_ldsity2panel_refyearset_version(ldsity2panel_refyearset_version, etl_ldsity, etl_version, etl_id)
	select _ldsity2panel_refyearset_version, _etl_ldsity, _etl_version, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) is
'Function inserts a record into table t_etl_adc2classrule2panel_refyearset based on given parameters.';

alter function analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_ldsity_object" schema="analytical" src="functions/etl/fn_etl_save_ldsity_object.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_ldsity_object(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_ldsity_object
(
	_export_connection		integer,
	_ldsity_object			integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_ldsity_object: Input argument _export_connection must not by NULL!';
	end if; 

	if _ldsity_object is null
	then
		raise exception 'Error 02: fn_etl_save_ldsity_object: Input argument _ldsity_object must not by NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_ldsity_object: Input argument _etl_id must not by NULL!';
	end if;

	insert into analytical.t_etl_ldsity_object(export_connection, ldsity_object, etl_id)
	select _export_connection, _ldsity_object, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_ldsity_object(integer, integer, integer) is
'Function inserts a record into table t_etl_ldsity_object based on given parameters.';

alter function analytical.fn_etl_save_ldsity_object(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity_object(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity_object(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_ldsity_object(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_ldsity_object(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_spc2classification_rule" schema="analytical" src="functions/etl/fn_etl_save_spc2classification_rule.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_spc2classification_rule
(
	_spc2classification_rule		integer,
	_etl_id							integer,
	_etl_sub_population_category	integer,
	_etl_ldsity_object				integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _spc2classification_rule is null
	then
		raise exception 'Error 01: fn_etl_save_spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_spc2classification_rule: Input argument _etl_id must not be NULL!';
	end if; 

	if _etl_sub_population_category is null
	then
		raise exception 'Error 03: fn_etl_save_spc2classification_rule: Input argument _etl_sub_population_category must not be NULL!';
	end if;

	if _etl_ldsity_object is null
	then
		raise exception 'Error 04: fn_etl_save_spc2classification_rule: Input argument _etl_ldsity_object must not be NULL!';
	end if; 

	insert into analytical.t_etl_spc2classification_rule(spc2classification_rule, etl_id, etl_sub_population_category, etl_ldsity_object)
	select _spc2classification_rule, _etl_id, _etl_sub_population_category, _etl_ldsity_object
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) is
'Function inserts a record into table t_etl_spc2classification_rule based on given parameters.';

alter function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_spc2classrule2panel_refyearset" schema="analytical" src="functions/etl/fn_etl_save_spc2classrule2panel_refyearset.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_spc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_spc2classrule2panel_refyearset
(
	_spc2classrule2panel_refyearset		integer,
	_etl_id								integer,
	_etl_spc2classification_rule		integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _spc2classrule2panel_refyearset is null
	then
		raise exception 'Error 01: fn_etl_save_spc2classrule2panel_refyearset: Input argument _spc2classrule2panel_refyearset must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_spc2classrule2panel_refyearset: Input argument _etl_id must not be NULL!';
	end if; 

	if _etl_spc2classification_rule is null
	then
		raise exception 'Error 03: fn_etl_save_spc2classrule2panel_refyearset: Input argument _etl_spc2classification_rule must not be NULL!';
	end if;

	insert into analytical.t_etl_spc2classrule2panel_refyearset(spc2classrule2panel_refyearset, etl_id, etl_spc2classification_rule)
	select _spc2classrule2panel_refyearset, _etl_id, _etl_spc2classification_rule
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) is
'Function inserts a record into table t_etl_spc2classrule2panel_refyearset based on given parameters.';

alter function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_spc_hierarchy" schema="analytical" src="functions/etl/fn_etl_save_spc_hierarchy.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_spc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_spc_hierarchy
(
	_spc_hierarchy										integer,
	_etl_sub_population_category_variable_superior		integer,
	_etl_sub_population_category_variable				integer,
	_etl_id												integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _spc_hierarchy is null
	then
		raise exception 'Error 01: fn_etl_save_spc_hierarchy: Input argument _spc_hierarchy must not be NULL!';
	end if; 

	if _etl_sub_population_category_variable_superior is null
	then
		raise exception 'Error 02: fn_etl_save_spc_hierarchy: Input argument _etl_sub_population_category_variable_superior must not be NULL!';
	end if; 

	if _etl_sub_population_category_variable is null
	then
		raise exception 'Error 03: fn_etl_save_spc_hierarchy: Input argument _etl_sub_population_category_variable must not be NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 04: fn_etl_save_spc_hierarchy: Input argument _etl_id must not be NULL!';
	end if; 

	insert into analytical.t_etl_spc_hierarchy(spc_hierarchy, etl_sub_population_category_variable_superior, etl_sub_population_category_variable, etl_id)
	select _spc_hierarchy, _etl_sub_population_category_variable_superior, _etl_sub_population_category_variable, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) is
'Function inserts a record into table t_etl_spc_hierarchy based on given parameters.';

alter function analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_spc_hierarchy(integer, integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_sub_population" schema="analytical" src="functions/etl/fn_etl_save_sub_population.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_sub_population(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_sub_population
(
	_export_connection		integer,
	_sub_population			integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_sub_population: Input argument _export_connection must not be NULL!';
	end if; 

	if _sub_population is null
	then
		raise exception 'Error 02: fn_etl_save_sub_population: Input argument _sub_population must not be NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_sub_population: Input argument _etl_id must not by NULL!';
	end if;

	if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error 04: fn_etl_save_sub_population: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;

	if not exists (select t2.* from analytical.c_sub_population as t2 where t2.id = _sub_population)
	then
		raise exception 'Error 05: fn_etl_save_sub_population: Given sub population (%) does not exist in c_sub_population table.', _sub_population;
	end if;		

	insert into analytical.t_etl_sub_population(export_connection, sub_population, etl_id)
	select _export_connection, _sub_population, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_sub_population(integer, integer, integer) is
'Function inserts a record into table t_etl_sub_population based on given parameters.';

alter function analytical.fn_etl_save_sub_population(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_sub_population(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_sub_population(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_sub_population(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_sub_population(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_sub_population_category" schema="analytical" src="functions/etl/fn_etl_save_sub_population_category.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_sub_population_category(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_sub_population_category
(
	_sub_population_category	integer,
	_etl_id						integer,
	_etl_sub_population			integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _sub_population_category is null
	then
		raise exception 'Error 01: fn_etl_save_sub_population_category: Input argument _sub_population must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_sub_population_category: Input argument _etl_id must not be NULL!';
	end if;

	if _etl_sub_population is null
	then
		raise exception 'Error 03: fn_etl_save_sub_population_category: Input argument _etl_sub_population must not be NULL!';
	end if;

	if not exists (select t.* from analytical.c_sub_population_category as t where t.id = _sub_population_category)
	then
		raise exception 'Error 04: fn_etl_save_sub_population_category: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category;
	end if;	

	insert into analytical.t_etl_sub_population_category(sub_population_category, etl_id, etl_sub_population)
	select _sub_population_category, _etl_id, _etl_sub_population
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_sub_population_category(integer, integer, integer) is
'Function inserts a record into table t_etl_sub_population_category based on given parameters.';

alter function analytical.fn_etl_save_sub_population_category(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_sub_population_category(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_sub_population_category(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_sub_population_category(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_sub_population_category(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_unit_of_measure" schema="analytical" src="functions/etl/fn_etl_save_unit_of_measure.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_unit_of_measure(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_unit_of_measure
(
	_export_connection		integer,
	_unit_of_measure		integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_unit_of_measure: Input argument _export_connection must not by NULL!';
	end if; 

	if _unit_of_measure is null
	then
		raise exception 'Error 02: fn_etl_save_unit_of_measure: Input argument _unit_of_measure must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_unit_of_measure: Input argument _etl_id must not by NULL!';
	end if;

	if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error 04: fn_etl_save_unit_of_measure: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;

	if not exists (select t2.* from analytical.c_unit_of_measure as t2 where t2.id = _unit_of_measure)
	then
		raise exception 'Error 05: fn_etl_save_unit_of_measure: Given unit of measure (%) does not exist in c_unit_of_measure table.', _unit_of_measure;
	end if;	

	insert into analytical.t_etl_unit_of_measure(export_connection, unit_of_measure, etl_id)
	select _export_connection, _unit_of_measure, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_unit_of_measure(integer, integer, integer) is
'Function inserts a record into table t_etl_unit_of_measure based on given parameters.';

alter function analytical.fn_etl_save_unit_of_measure(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_unit_of_measure(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_unit_of_measure(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_unit_of_measure(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_unit_of_measure(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_save_version" schema="analytical" src="functions/etl/fn_etl_save_version.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_version(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_version
(
	_export_connection		integer,
	_version				integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_version: Input argument _export_connection must not by NULL!';
	end if; 

	if _version is null
	then
		raise exception 'Error 02: fn_etl_save_version: Input argument _version must not by NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_version: Input argument _etl_id must not by NULL!';
	end if;

	insert into analytical.t_etl_version(export_connection, version, etl_id)
	select _export_connection, _version, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_version(integer, integer, integer) is
'Function inserts a record into table t_etl_version based on given parameters.';

alter function analytical.fn_etl_save_version(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_version(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_version(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_version(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_version(integer, integer, integer) to public;
-- </function>

-- <function name="fn_etl_try_delete_export_connection" schema="analytical" src="functions/etl/fn_etl_try_delete_export_connection.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_try_delete_export_connection(_id integer)
RETURNS boolean
AS
$$
BEGIN
	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT * FROM analytical.t_export_connection)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_try_delete_export_connection: Given export connection does not exist in table t_export_connection (%)', _id;
	END IF;

	return not exists (
	select telo.id from analytical.t_etl_ldsity_object as telo where telo.export_connection = _id			union all
	select tedv.id from analytical.t_etl_definition_variant as tedv where tedv.export_connection = _id		union all
	select tead.id from analytical.t_etl_area_domain as tead where tead.export_connection = _id				union all
	select tesp.id from analytical.t_etl_sub_population as tesp where tesp.export_connection = _id			union all
	select teuom.id from analytical.t_etl_unit_of_measure as teuom where teuom.export_connection = _id		union all
	select tev.id from analytical.t_etl_version as tev where tev.export_connection = _id					union all
	select teaop.id from analytical.t_etl_areal_or_population as teaop where teaop.export_connection = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_try_delete_export_connection(integer) IS
'The function provides test if it is possible to delete records from t_export_connection table.';

alter function analytical.fn_etl_try_delete_export_connection(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to adm_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to data_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to usr_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to public;
-- </function>

-- <function name="fn_etl_update_export_connection" schema="analytical" src="functions/etl/fn_etl_update_export_connection.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_update_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_update_export_connection(integer, text);

CREATE OR REPLACE FUNCTION analytical.fn_etl_update_export_connection
(
	_id integer,
	_comment text
)
RETURNS void
AS
$$
BEGIN

	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_update_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT t1.* FROM analytical.t_export_connection as t1 where t1.id = _id)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_update_export_connection: Given export connection (id = %) does not exist in t_export_connection table!', _id;
	END IF;

	if _comment is null
	then
		RAISE EXCEPTION 'Error 03: fn_etl_update_export_connection: Input argument _comment must not be NULL!';
	end if;

	update analytical.t_export_connection set comment = _comment where id = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_update_export_connection(integer, text) IS
'Function provides update in t_export_connection table.';

alter function analytical.fn_etl_update_export_connection(integer, text) owner to adm_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to adm_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to data_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to usr_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to public;
-- </function>
