--
-- Copyright 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE INDEX idx__t_plots_derived_dynamic__plot ON analytical.t_plots_derived_dynamic USING btree(plot) WHERE is_latest;
CREATE INDEX idx__t_plots_derived_dynamic__ref ON analytical.t_plots_derived_dynamic USING brin(reference_year_set) WHERE is_latest;
CREATE INDEX idx__t_plots_derived_dynamic__panel ON analytical.t_plots_derived_dynamic USING brin(panel) WHERE is_latest;

CREATE INDEX idx__t_plots_field_data_dynamic__pid ON analytical.t_plots_field_data_dynamic USING btree(nfi_cycle_result, pid);
CREATE INDEX idx__t_plots_field_data_dynamic__is_latest ON analytical.t_plots_field_data_dynamic USING btree(plots_derived_dynamic) WHERE is_latest;

CREATE INDEX idx__f_p_stems_dynamic__is_latest ON analytical.f_p_stems_dynamic USING btree(plots_field_data_dynamic) WHERE is_latest;
CREATE INDEX idx__f_p_stems_dynamic__stem_category ON analytical.f_p_stems_dynamic USING brin(stem_category) WHERE is_latest;


