--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

UPDATE analytical.c_ldsity
SET 	label = replace(label,'k.','k. s generalizovanou lokální hustotou'), 
	description = concat(description, ' Příspěvek je na všech kmenech počítán jako rozdíl mezi generalizovanou hustotou v druhém (okamžik těžby/mortality nebo koncové období) a prvním okamžiku (počáteční období).'), 
	label_en = replace(label_en, 'b.', 'b. with generalized local density'), 
	description_en = concat(description_en, ' Contribution of each stem is calculated as a difference of generalized local density in the second (time of cut/mortality or the end measurement) and the first measurement (begin measurement).')
WHERE id BETWEEN 172 AND 179;

-- 167,171
UPDATE analytical.c_ldsity SET definition_variant = array_replace(definition_variant,7200,7300)
WHERE id IN (167,171);

-- 170,171
UPDATE analytical.c_ldsity SET 
	label = concat(label, ', norm'),
	description = concat(description, ' Objem normalizován na jeden rok na základě skutečné délky periody mezi měřeními.'),
	label_en = concat(label_en, ', norm'),
	description_en = concat(description_en, ' Volume normalized to one year based on the real time span.')
WHERE id IN (170,171);

UPDATE analytical.c_ldsity SET definition_variant = array_append(definition_variant, 7200)
WHERE id BETWEEN 148 AND 155 OR id = 166;


UPDATE analytical.c_ldsity SET 
	label = concat(label, ', generalizovaná hustota'),
	description = concat(description, ' Generalizovaná lokální hustota.'),
	label_en = concat(label_en, ', generalized local density'),
	description_en = concat(description_en, ' Generalized local density.')
WHERE id IN (165,169);

UPDATE analytical.c_ldsity SET 
	label = concat(label, ', norm'),
	description = concat(description, ' Biomasa normalizována na jeden rok na základě skutečné délky periody mezi měřeními.'),
	label_en = concat(label_en, ', norm'),
	description_en = concat(description_en, ' Biomass normalized to one year based on the real time span.')
WHERE id IN (168,169);
