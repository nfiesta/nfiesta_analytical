SELECT
	t1.id,t1.nfi_grid,t1.nfi_cycle,t1.plots_field_data,t1.deadwood,t1.fid,t1.note,
	t1.accessibility,t1.fao_landuse,t1.transect_segment,t1.factor,t1.version,
	t1.time_span,t1.time_span_next,t1.pid,t1.status,t1.time_span_nominal,t1.time_span_nominal_next
	-- t1.point_geom
INTO public.t_deadwood
FROM
	analytical.t_deadwood AS t1
INNER JOIN
	analytical.t_plots_field_data AS t2
ON t1.plots_field_data = t2.id
WHERE t2.nfi_cycle IN (200,300) AND t2.is_latest;

