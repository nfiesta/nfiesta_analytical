SELECT
	t1.id,t1.gid,t1.plots_field_data,t1.circle,t1.area_m2,t1.area_share_perc,t1.accessibility,t1.fao_landuse,t1.land_category,t1.nfi_grid,t1.nfi_cycle,t1.stand_circle,
	t1.fid,
	--t1.stand_structure,t1.stand_naturalness_origin,t1.mixture_type,t1.species_composition,t1.growth_phase_origin,t1.stand_foundation_origin,t1.management_practice_origin,
	--t1.growth_phase,t1.stand_foundation,t1.stand_naturalness,t1.management_practice,t1.status,t1.stand_structure_origin,t1.mixture_type_origin,t1.fao_landuse_nil1,
	t1.land_category_nil1,t1.use_mode,t1.tol_character,t1.clearcut_origin,t1.clearcut,t1.clearcut_unsecured,t1.note,
	t1.version,t1.layer_count,t1.layer_count_or_mixed,
	--t1.replanting_cause_origin,t1.replanting_area_origin,t1.share_to_replant_perc_origin,t1.replanting_cause,t1.replanting_area,t1.share_to_replant_perc,
	t1.central_circle,t1.pid
	--t1.geom
INTO public.f_a_circle
FROM
	analytical.f_a_circle AS t1
INNER JOIN
	analytical.t_plots_field_data AS t2
ON t1.plots_field_data = t2.id
WHERE t2.nfi_cycle IN (100,200,300) AND t2.is_latest;
