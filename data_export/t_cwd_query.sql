SELECT
	t1.id,t1.nfi_grid,t1.nfi_cycle,t1.deadwood,t1.cwd,t1.fid,t1.diameter_mm,t1.decl_angle,
	t1.fresh_coniferous,t1.species_cwd,t1.piece_length,t1.decl_angle_deg,t1.ldsity_m3_ha,
	t1.species,t1.degradation,t1.deadwood_category,
	t1.version,t1.time_span,t1.time_span_next,t1.factor,t1.time_span_nominal,t1.time_span_nominal_next
INTO public.t_cwd
FROM 
	analytical.t_cwd AS t1
INNER JOIN
	analytical.t_deadwood AS t2
ON t1.deadwood = t2.id
INNER JOIN
	analytical.t_plots_field_data AS t3
ON t2.plots_field_data = t3.id
WHERE t3.nfi_cycle IN (200,300) AND t3.is_latest;

