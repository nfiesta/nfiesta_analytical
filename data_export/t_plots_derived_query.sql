SELECT 
	id,nfi_grid,nfi_cycle,pid,slhpo,land_tenure,field_survey,ps_nfi_luse,altitude_m,tract,plot,
	slope_dtm_perc,forest_subcategory,
	nfi_subgrid,slhpo4change,land_tenure4change,ps_lulucf_luse,
	--slope_dtm_deg,large_protected_area,small_protected_area,faws,azone,faws4change,taz_deg,transect_elevation_difference,
	myear,mseason,
	--threatened_stock_092019,
	--threatened_spruce_land_classification_042020,altitudinal_zone,edaphic_category,zonal_altitudinal_zone_mnm,open_area,
	--fire_damage,ps_wind_snow_damage,ps_growth_stage,species_composition,canopy_density,mixture_type,
	is_latest,inserted,ext_version,s2a,s2b,--strata_set,stratum,panel,cluster_configuration,cluster,reference_year_set,
	version
	--,iucn,evl,ptaci_obl,ramsar,biosf_rez,geopark
	--geom,square_geom,circle_geom,transect_geom
INTO public.t_plots_derived
FROM
	analytical.t_plots_derived 
WHERE
	nfi_cycle IN (100,200,300) AND is_latest;

