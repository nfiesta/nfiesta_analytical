SELECT
	t1.id,t1.nfi_grid,t1.nfi_cycle,t1.regeneration,t1.natural_regeneration_5dm,t1.natural_regeneration_5_13dm,t1.regeneration_7cm,t1.fid,
	t1.stem_age,t1.height_cm,t1.height_category_origin,t1.crown_width_min_cm,t1.crown_width_max_cm,t1.sample_small_tree,t1.tree_species,t1.vegetative_origin,t1.damage_or_protection_presence,
	t1.browsing_origin,t1.peeling_07,t1.other_damage,t1.other_damage_07,t1.protection,t1.damage,t1.height_category,t1.regeneration_origin,
	t1.species,
	t1.cp_area_m2,t1.izone_ha4rep,
	t1.regeneration_ldsity,t1.izone_ha,t1.regeneration_belonging,
	--t1.growth_phase,t1.peeling_all,t1.fraying,t1.browsing,t1.damage_rate,
	--t1.damage_rate_without_browsing,
	t1.timber_type,
	--t1.above_bio_t,t1.above_bio_model_t,t1.above_bio_ldty_t_ha,t1.above_bio_ldty_model_t_ha,t1.above_bio_sample_ldty_t_ha,t1.above_bio_sample_ldty_model_t_ha,
	--t1.above_bio_sample_edty_t_ha,
	t1.above_bio_gldsity_t_ha,
	--t1.damage_economic_activity,t1.stem_rot,
	--t1.volume_m3,t1.volume_categoryh_m3,t1.volume_categoryh_ldty_m3_ha,t1.volume_sample_ldty_m3_ha,t1.volume_categoryh_sample_ldty_m3_ha,t1.volume_sample_edty_m3_ha,
	t1.volume_gldsity_m3_ha,
	--t1.damage_rate_nil1,
	t1.cp_ranef,
	t1.model_cp_area_mar_m2,t1.model_cp_area_cal_m2,t1.stem_rot_all,
	t1.time_span,t1.stem_age_inc,t1.dbh_mm,t1.peeling,t1.version,
	t1.lateral_browsing,
	t1.time_span_next,
	t1.height_cat_cm,t1.clearcut,
	t1.time_span_nominal,t1.time_span_nominal_next,
	t1.species_con_dec,t1.carbon_gldsity_t_ha
INTO public.t_regenerations
FROM
	analytical.t_regenerations AS t1
INNER JOIN
	analytical.t_regeneration AS t2
ON t1.regeneration = t2.id
INNER JOIN
	analytical.t_plots_field_data AS t3
ON t2.plots_field_data = t3.id
WHERE t3.nfi_cycle IN (200,300) AND t3.is_latest;
