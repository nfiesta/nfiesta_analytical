SELECT
	t1.id,t1.nfi_grid,t1.nfi_cycle,t1.plots_field_data,t1.regeneration,t1.fid,t1.accessibility,t1.fao_landuse_reg,t1.land_category,t1.small_trees_presence,
	t1.natural_regeneration_potential,t1.exclosure,t1.regeneration_point_type,
	t1.factor,t1.regeneration_land,
	t1.growth_phase,t1.stand_foundation,t1.stand_naturalness,
	t1.stand_gap,t1.management_practice,
	t1.circle,
	t1.all_regeneration13_land_wsg,t1.all_regeneration13_firbroad_land_wsg,
	t1.version,
	t1.forest_area,t1.stand_area,
	t1.status,t1.pid
	--t1.geom
INTO public.t_regeneration
FROM
	analytical.t_regeneration AS t1
INNER JOIN
	analytical.t_plots_field_data AS t2
ON t1.plots_field_data = t2.id
WHERE t2.nfi_cycle IN (200,300) AND is_latest;
