--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE analytical.f_p_stems ADD COLUMN g13_ldsity_m2_ha double precision;

COMMENT ON COLUMN analytical.f_p_stems.g13_ldsity_m2_ha IS 'Příspěvek do lokální hustoty výčetní kruhové základny.';

INSERT INTO analytical.c_ldsity (id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
VALUES (247, 'výčetní kruhová základna', 'Výčetní kruhová základna.', 'basal area', 'Basal area at breast height.', 700, 'g13_ldsity_m2_ha', 2200,
	'{1041,21,1036,1039}', '{1827}', NULL);
