# turn off JIT
sudo sed -i "s/#jit = on/jit = off/" /etc/postgresql/16/main/postgresql.conf
sudo sed -i "s/#max_locks_per_transaction = 64/max_locks_per_transaction = 512/" /etc/postgresql/16/main/postgresql.conf
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/16/main/postgresql.conf
sudo sed -i "/host    all             all             127.0.0.1\/32            md5/ a host    all             vagrant         172.0.0.0\/8              md5" /etc/postgresql/16/main/pg_hba.conf
# start postgres
sudo service postgresql start
# create DB user (preferably with same name as Linux user to bypass password provision)
sudo -u postgres psql -c "CREATE USER vagrant SUPERUSER;"
sudo -u postgres psql -c "ALTER USER vagrant WITH PASSWORD 'vagrant';"
sudo -u postgres psql -c "CREATE ROLE adm_nfiesta; CREATE ROLE app_nfiesta;"
sudo -u postgres psql -c "CREATE ROLE adm_analytical; CREATE ROLE data_analytical; CREATE ROLE usr_analytical;"

# gitlab complains about dubious ownership in the repo, probably gitlab runner issue (on local machine this is not necessary):
git config --global --add safe.directory $(pwd)
git config --global --add safe.directory $(pwd)/deps/nfiesta_sdesign

# build install extensions
git submodule sync
git submodule update --init --progress
# ===============================nfiesta_sdesign==============================
# source code obtained by submodule
cd deps/nfiesta_sdesign
sudo make install
# - make installcheck
cd ../../
# ===============================nfiesta_analytical===============================
# no need to clone, sources already clonned in current directory (/builds)
sudo make install
# run all automated tests
make installcheck-all
