--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

INSERT INTO analytical.c_drupoz_kod (id, label, description) 
VALUES (15, 'neurčeno', 'neurčeno');

INSERT INTO analytical.c_zpvypa_kod (id, label, description) 
VALUES (31, 'neurčeno', 'neurčeno');

INSERT INTO analytical.c_area_domain_category (id, area_domain, label, description, label_en, description_en)
VALUES (1907, 1900, 'nezjištěno', 'Nezjištěno.', 'not identified', 'Not identified.');

INSERT INTO analytical.cm_adc2classification_rule (id, ldsity_object, area_domain_category, classification_rule)
VALUES 
(1214,200,1907,'false'),
(1215,100,1901,'land_category IN (10)'),
(1216,100,1902,'land_category IN (2,3,4,5,6)'),
(1217,100,1903,'land_category IN (7)'),
(1218,100,1904,'land_category IN (11)'),
(1219,100,1905,'land_category IN (13,14)'),
(1220,100,1906,'false'),
(1221,100,1907,'land_category IN (15)')
;

UPDATE analytical.cm_adc2classification_rule SET classification_rule = t1.classification_rule
FROM (	SELECT * 
	FROM 
	(VALUES
	(59, 'lulucf_category IN (100,700,1300)'),
	(60, 'lulucf_category IN (200,800,1400)'),
	(61, 'lulucf_category IN (300,900,1500)'),
	(62, 'lulucf_category IN (400,1000,1600)'),
	(63, 'lulucf_category IN (500,1100,1700)'),
	(64, 'lulucf_category IN (600,1200,1800)')) AS t(id, classification_rule)
) AS t1
WHERE cm_adc2classification_rule.id = t1.id;
