---------------------------------------------------------------------------------------------------
-- EXPORT CONNECTIONS
---------------------------------------------------------------------------------------------------
-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections() order by id;

-- save_EXPORT_CONNECTION
select * from analytical.fn_etl_save_export_connection
(
	'bran-pga-new'::varchar,
	'nfi_analytical'::varchar,
	5432::integer,
	'test'::text
);

-- save_EXPORT_CONNECTION
select * from analytical.fn_etl_save_export_connection
(
	'bran-pga-new'::varchar,
	'nfi_analytical'::varchar,
	5433::integer,
	'test'::text
);

-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections() order by id;

-- update_EXPORT_CONNECTION
select * from analytical.fn_etl_update_export_connection
(
	2,
	'test_test'::text
);

-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections() order by id;

-- try_delete_EXPORT_CONNECTION
select * from analytical.fn_etl_try_delete_export_connection(2);

-- delete_EXPORT_CONNECTION
select * from analytical.fn_etl_delete_export_connection(2);

-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections() order by id;
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- AREAL OR POPULATION
---------------------------------------------------------------------------------------------------
-- get_AREAL_OR_POPULATION
select * from analytical.fn_etl_get_areal_or_population(1);
select * from analytical.fn_etl_get_areal_or_population(1,false);
select * from analytical.fn_etl_get_areal_or_population(1,true);

-- save_AREAL_OR_POPULATION
select * from analytical.fn_etl_save_areal_or_population(1,1,100);
select * from analytical.fn_etl_save_areal_or_population(1,2,200);

-- etl_AREAL_OR_POPULARION
select * from analytical.t_etl_areal_or_population order by id;
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- LDSITY OBJECT
---------------------------------------------------------------------------------------------------
-- get_LDSITY_OBJECTS
select * from analytical.fn_etl_get_ldsity_objects(1,false,true);
select * from analytical.fn_etl_get_ldsity_objects(1,true,false);

-- save_LDSITY_OBJECT
select * from analytical.fn_etl_save_ldsity_object(1,100,100);

-- get_LDSITY_OBJECTS
select * from analytical.fn_etl_get_ldsity_objects(1,false,true);
select * from analytical.fn_etl_get_ldsity_objects(1,true,false);
select * from analytical.fn_etl_get_ldsity_objects(1,true,true);

-- save_LDSITY_OBJECT
select * from analytical.fn_etl_save_ldsity_object(1,200,200);
select * from analytical.fn_etl_save_ldsity_object(1,700,700);

-- etl_LDSITY_OBJECT
select * from analytical.t_etl_ldsity_object order by id;
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- LDSITIES
---------------------------------------------------------------------------------------------------
-- check_LDSITIES4OBJECT
select * from analytical.fn_etl_check_ldsities4object(1,100);
select * from analytical.fn_etl_check_ldsities4object(1,200);

-- get_LDSITIES4OBJECT
select * from analytical.fn_etl_get_ldsities4object(1,200,false,null::integer[]);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- UNIT OF MEASURE
---------------------------------------------------------------------------------------------------
-- get_UNIT_OF_MEASURE [with ldsity]
select * from analytical.fn_etl_get_unit_of_measure(1,1);
select * from analytical.fn_etl_get_unit_of_measure(1,1,false);
select * from analytical.fn_etl_get_unit_of_measure(1,1,true);

-- get_UNIT_OF_MEASURE [without ldsity]
select * from analytical.fn_etl_get_unit_of_measure(1,null::integer);
select * from analytical.fn_etl_get_unit_of_measure(1,null::integer,false);
select * from analytical.fn_etl_get_unit_of_measure(1,null::integer,true);

-- check_UNIT_OF_MEASURE
select * from analytical.fn_etl_check_unit_of_measure(1,1);

-- save_UNIT_OF_MEASURE
select * from analytical.fn_etl_save_unit_of_measure(1,100,100);

-- etl_UNIT_OF_MEASURE
select * from analytical.t_etl_unit_of_measure order by id;
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- DEFINITION VARIANT
---------------------------------------------------------------------------------------------------
-- get_DEFINITION_VARIANT [with ldsity]
select * from analytical.fn_etl_get_definition_variant(1,111);
select * from analytical.fn_etl_get_definition_variant(1,111,false);
select * from analytical.fn_etl_get_definition_variant(1,111,true);

select * from analytical.fn_etl_get_definition_variant(1,119);
select * from analytical.fn_etl_get_definition_variant(1,119,false);
select * from analytical.fn_etl_get_definition_variant(1,119,true);

-- get_DEFINITION_VARIANT [without ldsity]
select * from analytical.fn_etl_get_definition_variant(1,null::integer);
select * from analytical.fn_etl_get_definition_variant(1,null::integer,false);
select * from analytical.fn_etl_get_definition_variant(1,null::integer,true);

-- check_DEFINITION_VARIANT
select * from analytical.fn_etl_check_definition_variant(1,1);
select * from analytical.fn_etl_check_definition_variant(1,111);
select * from analytical.fn_etl_check_definition_variant(1,119);

-- save_DEFINITION_VARIANT
select * from analytical.fn_etl_save_definition_variant(1,100,100);
select * from analytical.fn_etl_save_definition_variant(1,200,200);

-- etl_DEFINITION_VARIANT
select * from analytical.t_etl_definition_variant order by id;
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- AREA DOMAIN INFORMATIONS
---------------------------------------------------------------------------------------------------
-- get_AREA_DOMAIN_INFORMATIONS
select * from analytical.fn_etl_get_area_domain_informations(1,2);
---------------------------------------------------------------------------------------------------

/*
---------------------------------------------------------------------------------------------------
-- simulation of changed value in c_ldsity for column sub_population_category for id 9
---------------------------------------------------------------------------------------------------
update analytical.c_ldsity set sub_population_category = array[66,2] where id = 9; -- original value = array[29101], 302
---------------------------------------------------------------------------------------------------
*/
/*
---------------------------------------------------------------------------------------------------
-- SUB POPULATION INFORMATIONS
---------------------------------------------------------------------------------------------------
-- get_SUB_POPULATION_INFORMATIONS
select * from analytical.fn_etl_get_sub_population_informations(1,9);
---------------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------------
-- AREA DOMAIN
---------------------------------------------------------------------------------------------------
-- get_AREA_DOMAIN [with ldsity]
select * from analytical.fn_etl_get_area_domain(1,2);
select * from analytical.fn_etl_get_area_domain(1,2,false);
select * from analytical.fn_etl_get_area_domain(1,2,true);

select * from analytical.fn_etl_get_area_domain(1,119);
select * from analytical.fn_etl_get_area_domain(1,119,false);
select * from analytical.fn_etl_get_area_domain(1,119,true);

-- get_AREA_DOMAIN [without ldsity]
select * from analytical.fn_etl_get_area_domain(1,null::integer,true);

-- check_AREA_DOMAIN
select * from analytical.fn_etl_check_area_domain(1,2);
select * from analytical.fn_etl_check_area_domain(1,119);

-- save_AREA_DOMAIN
select * from analytical.fn_etl_save_area_domain(1,100,100);
select * from analytical.fn_etl_save_area_domain(1,200,200);
select * from analytical.fn_etl_save_area_domain(1,400,400);
select * from analytical.fn_etl_save_area_domain(1,700,700);
select * from analytical.fn_etl_save_area_domain(1,32700,32700);
select * from analytical.fn_etl_save_area_domain(1,4,4);
select * from analytical.fn_etl_save_area_domain(1,6,6);
select * from analytical.fn_etl_save_area_domain(1,37000,37000);

-- get_AREA_DOMAIN [without ldsity]
select * from analytical.fn_etl_get_area_domain(1,null::integer,true);

-- etl_AREA_DOMAIN
select * from analytical.t_etl_area_domain order by id;

-- check_AREA_DOMAIN
select * from analytical.fn_etl_check_area_domain(1,2);
select * from analytical.fn_etl_check_area_domain(1,119);
---------------------------------------------------------------------------------------------------

/*
---------------------------------------------------------------------------------------------------
-- SUB POPULATION
---------------------------------------------------------------------------------------------------
-- get_SUB_POPULATION [with ldsity]
select * from analytical.fn_etl_get_sub_population(1,9);
select * from analytical.fn_etl_get_sub_population(1,9,false);
select * from analytical.fn_etl_get_sub_population(1,9,true);

-- get_SUB_POPULATION [without ldsity]
select * from analytical.fn_etl_get_sub_population(1,null::integer,true);

-- check_SUB_POPULATION
select * from analytical.fn_etl_check_sub_population(1,9);

-- save_SUB_POPULATION
select * from analytical.fn_etl_save_sub_population(1,500,500);
select * from analytical.fn_etl_save_sub_population(1,900,900);

-- get_SUB_POPULATION [without ldsity]
select * from analytical.fn_etl_get_sub_population(1,null::integer,true);

-- etl_SUB_POPULATION
select * from analytical.t_etl_sub_population order by id;

-- check_SUB_POPULATION
select * from analytical.fn_etl_check_sub_population(1,9);
---------------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------------
-- AREA DOMAIN CATEGORY
---------------------------------------------------------------------------------------------------
-- get_AREA_DOMAIN_CATEGORY
select * from analytical.fn_etl_get_area_domain_category(1,2);
select * from analytical.fn_etl_get_area_domain_category(1,2,false);
select * from analytical.fn_etl_get_area_domain_category(1,2,true);

select * from analytical.fn_etl_get_area_domain_category(1,119);
select * from analytical.fn_etl_get_area_domain_category(1,119,false);
select * from analytical.fn_etl_get_area_domain_category(1,119,true);

-- check_AREA_DOMAIN_CATEGORY
select * from analytical.fn_etl_check_area_domain_category(1,2);
select * from analytical.fn_etl_check_area_domain_category(1,119);

-- save_AREA_DOMAIN_CATEGORY
select * from analytical.fn_etl_save_area_domain_category(101,101,1);
select * from analytical.fn_etl_save_area_domain_category(102,102,1);
select * from analytical.fn_etl_save_area_domain_category(103,103,1);
select * from analytical.fn_etl_save_area_domain_category(104,104,1);
select * from analytical.fn_etl_save_area_domain_category(201,201,2);
select * from analytical.fn_etl_save_area_domain_category(202,202,2);
select * from analytical.fn_etl_save_area_domain_category(401,401,3);
select * from analytical.fn_etl_save_area_domain_category(402,402,3);
select * from analytical.fn_etl_save_area_domain_category(403,403,3);
select * from analytical.fn_etl_save_area_domain_category(701,701,4);
select * from analytical.fn_etl_save_area_domain_category(702,702,4);
select * from analytical.fn_etl_save_area_domain_category(32701,32701,5);
select * from analytical.fn_etl_save_area_domain_category(32702,32702,5);
select * from analytical.fn_etl_save_area_domain_category(32703,32703,5);
select * from analytical.fn_etl_save_area_domain_category(32704,32704,5);
select * from analytical.fn_etl_save_area_domain_category(32705,32705,5);

select * from analytical.fn_etl_save_area_domain_category(6,6,6);
select * from analytical.fn_etl_save_area_domain_category(7,7,6);
select * from analytical.fn_etl_save_area_domain_category(8,8,6);
select * from analytical.fn_etl_save_area_domain_category(9,9,7);
select * from analytical.fn_etl_save_area_domain_category(10,10,7);

select * from analytical.fn_etl_save_area_domain_category(37001,37001,8);
select * from analytical.fn_etl_save_area_domain_category(37002,37002,8);

-- etl_AREA_DOMAIN_CATEGORY
select * from analytical.t_etl_area_domain_category order by id;

-- check_AREA_DOMAIN_CATEGORY
select * from analytical.fn_etl_check_area_domain_category(1,2);
select * from analytical.fn_etl_check_area_domain_category(1,119);
---------------------------------------------------------------------------------------------------

/*
---------------------------------------------------------------------------------------------------
-- SUB POPULATION CATEGORY
---------------------------------------------------------------------------------------------------
-- get_SUB_POPULATION_CATEGORY
select * from analytical.fn_etl_get_sub_population_category(1,9);
select * from analytical.fn_etl_get_sub_population_category(1,9,false);
select * from analytical.fn_etl_get_sub_population_category(1,9,true);

-- check_SUB_POPULATION_CATEGORY
select * from analytical.fn_etl_check_sub_population_category(1,9);

-- save_SUB_POPULATION_CATEGORY
select * from analytical.fn_etl_save_sub_population_category(501,501,1);
select * from analytical.fn_etl_save_sub_population_category(502,502,1);
select * from analytical.fn_etl_save_sub_population_category(503,503,1);
select * from analytical.fn_etl_save_sub_population_category(504,504,1);
select * from analytical.fn_etl_save_sub_population_category(505,505,1);
select * from analytical.fn_etl_save_sub_population_category(506,506,1);
select * from analytical.fn_etl_save_sub_population_category(507,507,1);
select * from analytical.fn_etl_save_sub_population_category(508,508,1);
select * from analytical.fn_etl_save_sub_population_category(901,901,2);
select * from analytical.fn_etl_save_sub_population_category(902,902,2);

-- etl_SUB_POPULATION_CATEGORY
select * from analytical.t_etl_sub_population_category order by id;

-- check_SUB_POPULATION_CATEGORY
select * from analytical.fn_etl_check_sub_population_category(1,9);
---------------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------------
-- ADC HIERARCHY
---------------------------------------------------------------------------------------------------
-- get_ADC_HIERARCHY
select * from analytical.fn_etl_get_adc_hierarchy(1,2);
select * from analytical.fn_etl_get_adc_hierarchy(1,2,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,2,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,3);
select * from analytical.fn_etl_get_adc_hierarchy(1,3,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,3,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,4);
select * from analytical.fn_etl_get_adc_hierarchy(1,4,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,4,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,5);
select * from analytical.fn_etl_get_adc_hierarchy(1,5,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,5,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,111);
select * from analytical.fn_etl_get_adc_hierarchy(1,111,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,111,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,119);
select * from analytical.fn_etl_get_adc_hierarchy(1,119,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,119,true);

-- check_ADC_HIERARCHY
select * from analytical.fn_etl_check_adc_hierarchy(1,2);
select * from analytical.fn_etl_check_adc_hierarchy(1,3);
select * from analytical.fn_etl_check_adc_hierarchy(1,4);
select * from analytical.fn_etl_check_adc_hierarchy(1,5);
select * from analytical.fn_etl_check_adc_hierarchy(1,111);
select * from analytical.fn_etl_check_adc_hierarchy(1,119);

-- save_ADC_HIERARCHY
SELECT t4.*
FROM analytical.t_adc_hierarchy AS t1
INNER JOIN analytical.t_etl_area_domain_category AS t2
ON t1.variable_superior = t2.area_domain_category
INNER JOIN analytical.t_etl_area_domain_category AS t3
ON t1.variable = t3.area_domain_category, 
analytical.fn_etl_save_adc_hierarchy (t1.id, t2.id, t3.id, t1.id ) AS t4
WHERE --t1.id IN (1,2,3,4,5,48,49,69,70,402,403,513,514)
	t1.variable IN (101,102,103,104,201,202,401,402,403,701,702,32701,32702,32703,32704,32705,6,7,8,9,10,37001,37002)
ORDER BY t1.variable, t1.variable_superior;

/*select * from analytical.fn_etl_save_adc_hierarchy(3,5,7,3);
select * from analytical.fn_etl_save_adc_hierarchy(4,5,8,4);
select * from analytical.fn_etl_save_adc_hierarchy(5,5,9,5);
select * from analytical.fn_etl_save_adc_hierarchy(1,1,5,1);
select * from analytical.fn_etl_save_adc_hierarchy(2,1,6,2);
select * from analytical.fn_etl_save_adc_hierarchy(48,2,5,48);
select * from analytical.fn_etl_save_adc_hierarchy(49,2,6,49);
select * from analytical.fn_etl_save_adc_hierarchy(69,10,5,69);
select * from analytical.fn_etl_save_adc_hierarchy(70,10,6,70);
select * from analytical.fn_etl_save_adc_hierarchy(402,12,5,402);
select * from analytical.fn_etl_save_adc_hierarchy(403,12,6,403);
*/

-- etl_ADC_HIERARACHY
select * from analytical.t_etl_adc_hierarchy order by id;

-- check_ADC_HIERARCHY
select * from analytical.fn_etl_check_adc_hierarchy(1,2);
select * from analytical.fn_etl_check_adc_hierarchy(1,3);
select * from analytical.fn_etl_check_adc_hierarchy(1,4);
select * from analytical.fn_etl_check_adc_hierarchy(1,5);
select * from analytical.fn_etl_check_adc_hierarchy(1,111);
select * from analytical.fn_etl_check_adc_hierarchy(1,119);

-- get_ADC_HIERARCHY
select * from analytical.fn_etl_get_adc_hierarchy(1,2);
select * from analytical.fn_etl_get_adc_hierarchy(1,2,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,2,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,3);
select * from analytical.fn_etl_get_adc_hierarchy(1,3,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,3,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,4);
select * from analytical.fn_etl_get_adc_hierarchy(1,4,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,4,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,5);
select * from analytical.fn_etl_get_adc_hierarchy(1,5,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,5,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,111);
select * from analytical.fn_etl_get_adc_hierarchy(1,111,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,111,true);
select * from analytical.fn_etl_get_adc_hierarchy(1,119);
select * from analytical.fn_etl_get_adc_hierarchy(1,119,false);
select * from analytical.fn_etl_get_adc_hierarchy(1,119,true);
---------------------------------------------------------------------------------------------------

/*
---------------------------------------------------------------------------------------------------
-- SPC HIERARCHY
---------------------------------------------------------------------------------------------------
-- get_SPC_HIERARCHY
select * from analytical.fn_etl_get_spc_hierarchy(1,9);
select * from analytical.fn_etl_get_spc_hierarchy(1,9,false);
select * from analytical.fn_etl_get_spc_hierarchy(1,9,true);

-- check_SPC_HIERARCHY
select * from analytical.fn_etl_check_spc_hierarchy(1,9);

-- save_SPC_HIERARCHY
select * from analytical.fn_etl_save_spc_hierarchy(1,9,1,1);
select * from analytical.fn_etl_save_spc_hierarchy(2,9,2,2);
select * from analytical.fn_etl_save_spc_hierarchy(3,9,3,3);
select * from analytical.fn_etl_save_spc_hierarchy(4,9,4,4);
select * from analytical.fn_etl_save_spc_hierarchy(5,10,5,5);
select * from analytical.fn_etl_save_spc_hierarchy(6,10,6,6);
select * from analytical.fn_etl_save_spc_hierarchy(7,9,7,7);
select * from analytical.fn_etl_save_spc_hierarchy(8,10,8,8);

-- etl_SPC_HIERARACHY
select * from analytical.t_etl_spc_hierarchy order by id;

-- check_SPC_HIERARCHY
select * from analytical.fn_etl_check_spc_hierarchy(1,9);

-- get_SPC_HIERARCHY
select * from analytical.fn_etl_get_spc_hierarchy(1,9);
select * from analytical.fn_etl_get_spc_hierarchy(1,9,false);
select * from analytical.fn_etl_get_spc_hierarchy(1,9,true);
---------------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------------
-- ADC2CLASSIFICATION_RULE
---------------------------------------------------------------------------------------------------
-- get_ADC2CLASSIFICATION_RULE
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,2);
select 
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,2,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,2,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,111);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,111,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,111,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,119);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,119,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,119,true);

-- check_ADC2CLASSIFICATION_RULE
select * from analytical.fn_etl_check_adc2classification_rule(1,2);
select * from analytical.fn_etl_check_adc2classification_rule(1,111);
select * from analytical.fn_etl_check_adc2classification_rule(1,119);

-- save_ADC2CLASSIFICATION_RULE
SELECT t5.*
FROM analytical.cm_adc2classification_rule AS t1
INNER JOIN analytical.t_etl_area_domain_category AS t2
ON t1.area_domain_category = t2.area_domain_category
INNER JOIN analytical.cm_adc2classification_rule AS t3
ON t2.area_domain_category = t3.area_domain_category
INNER JOIN analytical.t_etl_ldsity_object AS t4
ON t3.ldsity_object = t4.ldsity_object,
analytical.fn_etl_save_adc2classification_rule(t1.id, t1.id, t2.id, t4.id) AS t5
WHERE --t1.id IN (1,6,11,16,21,22,23,26,29,32,33,814,819,824,829,834)
	t3.area_domain_category IN (101,102,103,104,201,202,401,402,403,701,702,32701,32702,32703,32704,32705,6,7,8,9,10,37001,37002)
ORDER BY t1.variable, t1.variable_superior;

/*
select * from analytical.fn_etl_save_adc2classification_rule(1,1,1,2);
select * from analytical.fn_etl_save_adc2classification_rule(6,6,2,2);
select * from analytical.fn_etl_save_adc2classification_rule(11,11,3,2);
select * from analytical.fn_etl_save_adc2classification_rule(16,16,4,2);
select * from analytical.fn_etl_save_adc2classification_rule(21,21,5,2);
select * from analytical.fn_etl_save_adc2classification_rule(22,22,6,2);
select * from analytical.fn_etl_save_adc2classification_rule(23,23,7,2);
select * from analytical.fn_etl_save_adc2classification_rule(26,26,8,2);
select * from analytical.fn_etl_save_adc2classification_rule(29,29,9,2);
select * from analytical.fn_etl_save_adc2classification_rule(32,32,10,2);
select * from analytical.fn_etl_save_adc2classification_rule(33,33,11,2);
select * from analytical.fn_etl_save_adc2classification_rule(814,814,12,2);
select * from analytical.fn_etl_save_adc2classification_rule(819,819,13,2);
select * from analytical.fn_etl_save_adc2classification_rule(824,824,14,2);
select * from analytical.fn_etl_save_adc2classification_rule(829,829,15,2);
select * from analytical.fn_etl_save_adc2classification_rule(834,834,16,2);
*/
-- etl_ADC2CLASSIFICATION_RULE
select * from analytical.t_etl_adc2classification_rule order by id;

-- check_ADC2CLASSIFICATION_RULE
select * from analytical.fn_etl_check_adc2classification_rule(1,2);
select * from analytical.fn_etl_check_adc2classification_rule(1,111);
select * from analytical.fn_etl_check_adc2classification_rule(1,119);

-- get_ADC2CLASSIFICATION_RULE
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,2);
select 
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,2,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,2,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,111);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,111,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,111,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,119);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,119,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, cm_adc2classrule2panel_refyearset__refyearset2panel 
from analytical.fn_etl_get_adc2classification_rule(1,119,true);

---------------------------------------------------------------------------------------------------

/*
---------------------------------------------------------------------------------------------------
-- SPC2CLASSIFICATION_RULE
---------------------------------------------------------------------------------------------------
-- save_LDSITY_OBJECT
select * from analytical.fn_etl_save_ldsity_object(1,700,700);

-- get_SPC2CLASSIFICATION_RULE
select * from analytical.fn_etl_get_spc2classification_rule(1,9);
select * from analytical.fn_etl_get_spc2classification_rule(1,9,false);
select * from analytical.fn_etl_get_spc2classification_rule(1,9,true);

-- check_SPC2CLASSIFICATION_RULE
select * from analytical.fn_etl_check_spc2classification_rule(1,9);

-- save_SPC2CLASSIFICATION_RULE
select * from analytical.fn_etl_save_spc2classification_rule(2,2,1,3);
select * from analytical.fn_etl_save_spc2classification_rule(7,7,2,3);
select * from analytical.fn_etl_save_spc2classification_rule(12,12,3,3);
select * from analytical.fn_etl_save_spc2classification_rule(17,17,4,3);
select * from analytical.fn_etl_save_spc2classification_rule(22,22,5,3);
select * from analytical.fn_etl_save_spc2classification_rule(27,27,6,3);
select * from analytical.fn_etl_save_spc2classification_rule(32,32,7,3);
select * from analytical.fn_etl_save_spc2classification_rule(37,37,8,3);
select * from analytical.fn_etl_save_spc2classification_rule(66,66,9,3);
select * from analytical.fn_etl_save_spc2classification_rule(71,71,10,3);

-- etl_SPC2CLASSIFICATION_RULE
select * from analytical.t_etl_spc2classification_rule order by id;

-- check_SPC2CLASSIFICATION_RULE
select * from analytical.fn_etl_check_spc2classification_rule(1,9);

-- get_SPC2CLASSIFICATION_RULE
select * from analytical.fn_etl_get_spc2classification_rule(1,9);
select * from analytical.fn_etl_get_spc2classification_rule(1,9,false);
select * from analytical.fn_etl_get_spc2classification_rule(1,9,true);
---------------------------------------------------------------------------------------------------
*/

---------------------------------------------------------------------------------------------------
-- ADC2CLASSRULE2PANEL_REFYEARSET
---------------------------------------------------------------------------------------------------
-- get_ADC2CLASSRULE2PANEL_REFYEARSET
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,2);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,2,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,2,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,111);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,111,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,111,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,119);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,119,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,119,true);

-- check_ADC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset(1,2);
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset(1,111);
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset(1,119);

-- save_ADC2CLASSRULE2PANEL_REFYEARSET

SELECT analytical.fn_etl_save_adc2classrule2panel_refyearset(t1.id,t1.id,t2.id)
FROM analytical.cm_adc2classrule2panel_refyearset AS t1
INNER JOIN
	analytical.t_etl_adc2classification_rule AS t2
ON t1.adc2classification_rule = t2.adc2classification_rule
ORDER BY t1.adc2classification_rule, t1.refyearset2panel;

-- etl_ADC2CLASSRULE2PANEL_REFYEARSET
SELECT etl_adc2classification_rule, count(*) AS total 
FROM analytical.t_etl_adc2classrule2panel_refyearset 
GROUP BY etl_adc2classification_rule
ORDER BY etl_adc2classification_rule;

-- check_ADC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset(1,2);
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset(1,111);
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset(1,119);

-- get_ADC2CLASSRULE2PANEL_REFYEARSET
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,2);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,2,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,2,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,111);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,111,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,111,true);

select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,119);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,119,false);
select
	id, classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id, refyearset2panel, etl_adc2classrule2panel_refyearset__id, etl_adc2classrule2panel_refyearset__etl_id
from analytical.fn_etl_get_adc2classrule2panel_refyearset(1,119,true);
---------------------------------------------------------------------------------------------------

/*
---------------------------------------------------------------------------------------------------
-- SPC2CLASSRULE2PANEL_REFYEARSET
---------------------------------------------------------------------------------------------------*
-- get_SPC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(1,9);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(1,9,false);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(1,9,true);

-- check_SPC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_check_spc2classrule2panel_refyearset(1,9);

-- save_SPC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(1,1,1);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(2,2,1);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(3,3,1);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(4,4,2);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(5,5,2);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(6,6,2);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(7,7,3);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(8,8,3);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(9,9,3);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(10,10,4);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(11,11,4);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(12,12,4);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(13,13,5);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(14,14,5);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(15,15,5);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(16,16,6);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(17,17,6);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(18,18,6);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(19,19,7);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(20,20,7);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(21,21,7);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(22,22,8);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(23,23,8);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(24,24,8);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(25,25,9);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(26,26,9);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(27,27,9);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(28,28,10);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(29,29,10);
select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(30,30,10);

-- etl_SPC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.t_etl_spc2classrule2panel_refyearset order by id;

-- check_SPC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_check_spc2classrule2panel_refyearset(1,9);

-- get_SPC2CLASSRULE2PANEL_REFYEARSET
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(1,9);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(1,9,false);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(1,9,true);
---------------------------------------------------------------------------------------------------
*/
