/*
	test for filling panel reference_year_set combinations
*/

---------------------------------------------------------------------------------------------------
-- data for cm_adc2classrule2panel_refyearset 
---------------------------------------------------------------------------------------------------
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(100, 1, 200);
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(4, 1, 200);
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(200, 1, 200);
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(400, 1, 200);
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(700, 1, 200);
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(32700, 1, 200);
SELECT * FROM analytical.fn_save_ad_panel_refyearset_mapping(37000, 5, 200);

---------------------------------------------------------------------------------------------------
-- cm_spc2classrule2panel_refyearset
---------------------------------------------------------------------------------------------------
SELECT * FROM analytical.fn_save_sp_panel_refyearset_mapping(500, 116, 700);
SELECT * FROM analytical.fn_save_sp_panel_refyearset_mapping(900, 116, 700);
SELECT * FROM analytical.fn_save_sp_panel_refyearset_mapping(36600, 116, 700);
SELECT * FROM analytical.fn_save_sp_panel_refyearset_mapping(29100, 116, 700);
SELECT * FROM analytical.fn_save_sp_panel_refyearset_mapping(36800, 116, 700);

---------------------------------------------------------------------------------------------------
SELECT adc2classification_rule, refyearset2panel 
FROM analytical.cm_adc2classrule2panel_refyearset 
ORDER BY adc2classification_rule, refyearset2panel;

---------------------------------------------------------------------------------------------------
SELECT spc2classification_rule, refyearset2panel 
FROM analytical.cm_spc2classrule2panel_refyearset 
ORDER BY spc2classification_rule, refyearset2panel;


