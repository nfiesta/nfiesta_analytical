-- panel
WITH w_data AS (
	SELECT
		t1.gid AS plot,
		split_part(t1.plot,'_',2)::integer AS pid,
		t1.geom,
		t2.id AS cluster, 
		t5.id AS cluster_configuration, 
		t4.id AS panel,
		t7.id AS stratum,
		t8.id AS strata_set,
		t8.strata_set AS strata_set_char
	FROM
		sdesign.f_p_plot AS t1
	INNER JOIN sdesign.t_cluster AS t2
	ON 	t1.cluster = t2.id
	INNER JOIN sdesign.cm_cluster2panel_mapping AS t3
	ON 	t2.id = t3.cluster
	INNER JOIN sdesign.t_panel AS t4
	ON 	t3.panel = t4.id
	INNER JOIN sdesign.t_cluster_configuration AS t5
	ON 	t4.cluster_configuration = t5.id
	INNER JOIN sdesign.cm_plot2cluster_config_mapping AS t6
	ON	t5.id = t6.cluster_configuration AND
		t1.gid = t6.plot
	INNER JOIN sdesign.t_stratum AS t7
	ON 	t4.stratum = t7.id
	INNER JOIN sdesign.t_strata_set AS t8
	ON 	t7.strata_set = t8.id
)
UPDATE analytical.t_plots_derived SET strata_set = t1.strata_set, stratum = t1.stratum,
panel = t1.panel, cluster_configuration = t1.cluster_configuration, cluster = t1.cluster, plot = t1.plot
FROM w_data AS t1
WHERE t_plots_derived.pid = t1.pid AND CASE WHEN t_plots_derived.nfi_grid = 100 THEN t1.strata_set_char = 'CZ-STS-SNIL1' ELSE t1.strata_set_char = 'CZ-STS-SNIL2' END;

--  reference_year_set
UPDATE analytical.t_plots_derived SET reference_year_set = t1.id
FROM
	(SELECT t1.id, t1.reference_year_set, t2.id AS myear, t3.id AS mseason
	FROM sdesign.t_reference_year_set AS t1
	INNER JOIN analytical.c_myear AS t2
	ON split_part(t1.reference_year_set,'-',1) = t2.label
	INNER JOIN analytical.c_mseason AS t3
	ON t2.id = t3.myear AND split_part(t1.reference_year_set,'-',2) = t3.label
	) AS t1
WHERE
	t_plots_derived.myear = t1.myear AND 
	t_plots_derived.mseason = t1.mseason;
-- nfi1
UPDATE analytical.t_plots_derived SET reference_year_set = t1.id
FROM sdesign.t_reference_year_set AS t1 
WHERE t1.reference_year_set = '2001-2004' AND
t_plots_derived.nfi_grid = 100 AND t_plots_derived.nfi_cycle = 100 AND t_plots_derived.panel IS NOT NULL;



