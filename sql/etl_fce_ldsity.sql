---------------------------------------------------------------------------------------------------
-- LDSITIES
---------------------------------------------------------------------------------------------------
-- get_LDSITIES4OBJECT
select * from analytical.fn_etl_get_ldsities4object(1,200,false,array[1,2,3,4,5,111,119,122]);


-- get_LDSITY [the third argument is _data4import boolean default false]
select * from analytical.fn_etl_get_ldsity(1,1,false);
select * from analytical.fn_etl_get_ldsity(1,2,false);
select * from analytical.fn_etl_get_ldsity(1,3,false);
select * from analytical.fn_etl_get_ldsity(1,4,false);
select * from analytical.fn_etl_get_ldsity(1,5,false);
select * from analytical.fn_etl_get_ldsity(1,111,false);
select * from analytical.fn_etl_get_ldsity(1,119,false);
select * from analytical.fn_etl_get_ldsity(1,122,false);

select * from analytical.fn_etl_get_ldsity(1,1,true);
select * from analytical.fn_etl_get_ldsity(1,2,true);
select * from analytical.fn_etl_get_ldsity(1,3,true);
select * from analytical.fn_etl_get_ldsity(1,4,true);
select * from analytical.fn_etl_get_ldsity(1,5,true);
select * from analytical.fn_etl_get_ldsity(1,111,true);
select * from analytical.fn_etl_get_ldsity(1,119,true);
select * from analytical.fn_etl_get_ldsity(1,122,true);

-- save_LDSITY
select * from analytical.fn_etl_save_ldsity(1,1,2);
select * from analytical.fn_etl_save_ldsity(2,2,2);
select * from analytical.fn_etl_save_ldsity(3,3,2);
select * from analytical.fn_etl_save_ldsity(4,4,2);
select * from analytical.fn_etl_save_ldsity(5,5,2);
select * from analytical.fn_etl_save_ldsity(111,111,2);
select * from analytical.fn_etl_save_ldsity(119,119,2);
select * from analytical.fn_etl_save_ldsity(122,122,2);

-- etl_LDSITY
select * from analytical.t_etl_ldsity order by id;
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- datas for cm_ldsity2panel_refyearset_version
---------------------------------------------------------------------------------------------------
insert into analytical.cm_ldsity2panel_refyearset_version(ldsity,refyearset2panel,version) values
(2,5,100),
(111,5,100),
(119,5,100);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- VERSION
---------------------------------------------------------------------------------------------------
-- get_VERSION
select * from analytical.fn_etl_get_version(1,2);
select * from analytical.fn_etl_get_version(1,2,false);
select * from analytical.fn_etl_get_version(1,2,true);

select * from analytical.fn_etl_get_version(1,111);
select * from analytical.fn_etl_get_version(1,111,false);
select * from analytical.fn_etl_get_version(1,111,true);

select * from analytical.fn_etl_get_version(1,119);
select * from analytical.fn_etl_get_version(1,119,false);
select * from analytical.fn_etl_get_version(1,119,true);

-- check_VERSION
select * from analytical.fn_etl_check_version(1,2);
select * from analytical.fn_etl_check_version(1,111);
select * from analytical.fn_etl_check_version(1,119);

-- save_VERSION
select * from analytical.fn_etl_save_version(1,100,100);

-- etl_VERSION
select * from analytical.t_etl_version order by id;

-- check_VERSION
select * from analytical.fn_etl_check_version(1,2);
select * from analytical.fn_etl_check_version(1,111);
select * from analytical.fn_etl_check_version(1,119);

-- get_VERSION
select * from analytical.fn_etl_get_version(1,2);
select * from analytical.fn_etl_get_version(1,2,false);
select * from analytical.fn_etl_get_version(1,2,true);

select * from analytical.fn_etl_get_version(1,111);
select * from analytical.fn_etl_get_version(1,111,false);
select * from analytical.fn_etl_get_version(1,111,true);

select * from analytical.fn_etl_get_version(1,119);
select * from analytical.fn_etl_get_version(1,119,false);
select * from analytical.fn_etl_get_version(1,119,true);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- LDSITY2PANEL_REFYEARSET_VERSION
---------------------------------------------------------------------------------------------------
-- get_LDSITY2PANEL_REFYEARSET_VERSION
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,2);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,2,false);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,2,true);

select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,111);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,111,false);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,111,true);

select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,119);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,119,false);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,119,true);

-- check_LDSITY2PANEL_REFYEARSET_VERSION
select * from analytical.fn_etl_check_ldsity2panel_refyearset_version(1,2);
select * from analytical.fn_etl_check_ldsity2panel_refyearset_version(1,111);
select * from analytical.fn_etl_check_ldsity2panel_refyearset_version(1,119);

-- save_LDSITY2PANEL_REFYEARSET_VERSION
select * from analytical.fn_etl_save_ldsity2panel_refyearset_version(1,2,1,1);
select * from analytical.fn_etl_save_ldsity2panel_refyearset_version(2,6,1,2);
select * from analytical.fn_etl_save_ldsity2panel_refyearset_version(3,7,1,3);

-- etl_LDSITY2PANEL_REFYEARSET_VERSION
select * from analytical.t_etl_ldsity2panel_refyearset_version order by id;

-- check_LDSITY2PANEL_REFYEARSET_VERSION
select * from analytical.fn_etl_check_ldsity2panel_refyearset_version(1,2);
select * from analytical.fn_etl_check_ldsity2panel_refyearset_version(1,111);
select * from analytical.fn_etl_check_ldsity2panel_refyearset_version(1,119);

-- get_LDSITY2PANEL_REFYEARSET_VERSION
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,2);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,2,false);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,2,true);

select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,111);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,111,false);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,111,true);

select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,119);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,119,false);
select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(1,119,true);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- LDSITY OBJECT
---------------------------------------------------------------------------------------------------
-- get_LDSITIES4OBJECT
select * from analytical.fn_etl_get_ldsities4object(1,200);
select * from analytical.fn_etl_get_ldsities4object(1,200,true);
select * from analytical.fn_etl_get_ldsities4object(1,200,false);

select * from analytical.fn_etl_get_ldsities4object(1,100);
select * from analytical.fn_etl_get_ldsities4object(1,100,true);
select * from analytical.fn_etl_get_ldsities4object(1,100,false);

select * from analytical.fn_etl_get_ldsities4object(1,200,null::boolean,null::integer[],true);
select * from analytical.fn_etl_get_ldsities4object(1,200,null::boolean,array[2,111,119],true);

-- check_LDSITIES4OBJECT
select * from analytical.fn_etl_check_ldsities4object(1,200);
select * from analytical.fn_etl_check_ldsities4object(1,100);
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- ETL ATTR FUNCTIONS
---------------------------------------------------------------------------------------------------
-- adding testing datas for ADC
insert into analytical.cm_adc2classrule2panel_refyearset(adc2classification_rule,refyearset2panel)
select id, 5 from analytical.cm_adc2classification_rule cacr where ldsity_object = 200
and area_domain_category in (select distinct adc__id from analytical.fn_etl_get_area_domain_informations_attr(1,200))
except select adc2classification_rule, refyearset2panel from analytical.cm_adc2classrule2panel_refyearset
order by id;

select * from analytical.cm_adc2classrule2panel_refyearset order by adc2classification_rule, refyearset2panel;

-- adding testing datas for SPC
insert into analytical.cm_spc2classrule2panel_refyearset(spc2classification_rule,refyearset2panel)
select id, 5 from analytical.cm_spc2classification_rule cacr where ldsity_object = 200
and sub_population_category in (select distinct spc__id from analytical.fn_etl_get_sub_population_informations_attr(1,200))
except select spc2classification_rule, refyearset2panel from analytical.cm_spc2classrule2panel_refyearset
order by id;

select * from analytical.cm_spc2classrule2panel_refyearset order by spc2classification_rule, refyearset2panel;

-- get_area_domain_informations_attr
select * from analytical.fn_etl_get_area_domain_informations_attr(1,200);

-- get_sub_population_informations_attr
select * from analytical.fn_etl_get_sub_population_informations_attr(1,200);

-- get_ad_attributes4ldsity_object
select * from analytical.fn_etl_get_ad_attributes4ldsity_object(1,200,null::integer[],null::boolean);
select * from analytical.fn_etl_get_ad_attributes4ldsity_object(1,200,null::integer[],false);
select * from analytical.fn_etl_get_ad_attributes4ldsity_object(1,200,null::integer[],true);
select * from analytical.fn_etl_get_ad_attributes4ldsity_object(1,200,array[200,400],false);

-- get_sp_attributes4ldsity_object
/*select * from analytical.fn_etl_get_sp_attributes4ldsity_object(1,700,null::integer[],null::boolean);
select * from analytical.fn_etl_get_sp_attributes4ldsity_object(1,700,null::integer[],false);
select * from analytical.fn_etl_get_sp_attributes4ldsity_object(1,700,null::integer[],true);
select * from analytical.fn_etl_get_sp_attributes4ldsity_object(1,700,array[36600],false);
*/
-- get_area_domain_attr
select * from analytical.fn_etl_get_area_domain_attr(1,array[201,202]);
select * from analytical.fn_etl_get_area_domain_attr(1,array[201,202],false);
select * from analytical.fn_etl_get_area_domain_attr(1,array[201,202],true);
select * from analytical.fn_etl_get_area_domain_attr(1,null::integer[],true);

-- get_sub_population_attr
select * from analytical.fn_etl_get_sub_population_attr(1,array[36601,36602]);
select * from analytical.fn_etl_get_sub_population_attr(1,array[36601,36602],false);
select * from analytical.fn_etl_get_sub_population_attr(1,array[36601,36602],true);
select * from analytical.fn_etl_get_sub_population_attr(1,null::integer[],true);

-- check_area_domain_attr
select * from analytical.fn_etl_check_area_domain_attr(1,array[201,202]);

-- check_sub_population_attr
select * from analytical.fn_etl_check_sub_population_attr(1,array[36601,36602]);

-- get_area_domain_category_attr
select * from analytical.fn_etl_get_area_domain_category_attr(1,array[201,202]);
select * from analytical.fn_etl_get_area_domain_category_attr(1,array[201,202],false);
select * from analytical.fn_etl_get_area_domain_category_attr(1,array[201,202],true);
select * from analytical.fn_etl_get_area_domain_category_attr(1,null::integer[],true);

-- get_sub_population_category_attr
select * from analytical.fn_etl_get_sub_population_category_attr(1,array[36601,36602]);
select * from analytical.fn_etl_get_sub_population_category_attr(1,array[36601,36602],false);
select * from analytical.fn_etl_get_sub_population_category_attr(1,array[36601,36602],true);
select * from analytical.fn_etl_get_sub_population_category_attr(1,null::integer[],true);

-- check_area_domain_category_attr
select * from analytical.fn_etl_check_area_domain_category_attr(1,array[201,202]);

-- check_sub_population_category_attr
select * from analytical.fn_etl_check_sub_population_category_attr(1,array[36601,36602]);

-- get_adc_hierarchy_attr
select * from analytical.fn_etl_get_adc_hierarchy_attr(1,array[201,202]);
select * from analytical.fn_etl_get_adc_hierarchy_attr(1,array[201,202],false);
select * from analytical.fn_etl_get_adc_hierarchy_attr(1,array[201,202],true);
select * from analytical.fn_etl_get_adc_hierarchy_attr(1,null::integer[],true);

-- get_spc_hierarchy_attr
select * from analytical.fn_etl_get_spc_hierarchy_attr(1,array[36601,36602]);
select * from analytical.fn_etl_get_spc_hierarchy_attr(1,array[36601,36602],false);
select * from analytical.fn_etl_get_spc_hierarchy_attr(1,array[36601,36602],true);
select * from analytical.fn_etl_get_spc_hierarchy_attr(1,null::integer[],true);	

-- check_adc_hierarchy_attr
select * from analytical.fn_etl_check_adc_hierarchy_attr(1,array[201,202]);

-- check_spc_hierarchy_attr
select * from analytical.fn_etl_check_spc_hierarchy_attr(1,array[36601,36602]);

-- get_adc2classification_rule_attr
select * from analytical.fn_etl_get_adc2classification_rule_attr(1,200, array[201,202]);
select * from analytical.fn_etl_get_adc2classification_rule_attr(1,200, array[201,202],false);
select * from analytical.fn_etl_get_adc2classification_rule_attr(1,200, array[201,202],true);
select * from analytical.fn_etl_get_adc2classification_rule_attr(1,200, null::integer[],true);

-- get_spc2classification_rule_attr
select * from analytical.fn_etl_get_spc2classification_rule_attr(1,700, array[36601,36602]);
select * from analytical.fn_etl_get_spc2classification_rule_attr(1,700, array[36601,36602],false);
select * from analytical.fn_etl_get_spc2classification_rule_attr(1,700, array[36601,36602],true);
select * from analytical.fn_etl_get_spc2classification_rule_attr(1,700, null::integer[],true);

-- check_adc2classification_rule_attr
select * from analytical.fn_etl_check_adc2classification_rule_attr(1,200,array[201,202]);

-- check_spc2classification_rule_attr
select * from analytical.fn_etl_check_spc2classification_rule_attr(1,700,array[36601,36602]);

-- get_adc2classrule2panel_refyearset_attr
select * from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(1,200,array[201,202]);
select * from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(1,200,array[201,202],false);
select * from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(1,200,array[201,202],true);
select * from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(1,200,null::integer[],true);

-- get_spc2classrule2panel_refyearset_attr
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(1,700,array[36601,36602]);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(1,700,array[36601,36602],false);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(1,700,array[36601,36602],true);
select * from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(1,700,null::integer[],true);

-- check_adc2classrule2panel_refyearset_attr
select * from analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(1,200,array[201,202]);

-- check_spc2classrule2panel_refyearset_attr
select * from analytical.fn_etl_check_spc2classrule2panel_refyearset_attr(1,700,array[36601,36602]);
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
