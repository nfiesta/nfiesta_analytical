--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ad_panel_refyearset_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(
	_area_domain	integer,
	_ldsity		integer,
	_ldsity_object	integer
	)
RETURNS void 
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.c_area_domain AS t1 WHERE t1.id = _area_domain)
	THEN RAISE EXCEPTION 'Given area_domain does not exist in table c_area_domain (%)', _area_domain;
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity_object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	WITH
	w_classrule_adc AS (
		SELECT 
			t1.id AS adc2classification_rule,
			t2.area_domain, t1.ldsity_object, 
			t4.area_domain AS area_domain_sup,
			array_agg(t3.variable_superior ORDER BY t3.variable_superior) FILTER (WHERE t3.variable_superior IS NOT NULL) AS adc_sup, 
			t2.id AS adc_rule, t1.classification_rule
			--array_agg(t1.classification_rule ORDER BY t2.id) AS rules
		FROM analytical.cm_adc2classification_rule AS t1
		INNER JOIN analytical.c_area_domain_category AS t2
		ON t1.area_domain_category = t2.id
		LEFT JOIN
			analytical.t_adc_hierarchy AS t3
		ON t2.id = t3.variable AND t3.dependent = true
		LEFT JOIN
			analytical.c_area_domain_category AS t4
		ON t3.variable_superior = t4.id
		WHERE t1.ldsity_object = _ldsity_object AND t2.area_domain = _area_domain
		GROUP BY t1.id, t1.ldsity_object, t2.area_domain, t4.area_domain, t1.ldsity_object, t2.id, t1.classification_rule
		ORDER BY t2.id
	), w_classrule AS (
		SELECT
			ldsity_object,
			area_domain,
			area_domain_sup,
			adc_sup,
			adc_rule,
			adc2classification_rule,
			coalesce(max(array_length(adc_sup,1)) OVER (PARTITION BY area_domain_sup, area_domain) - array_length(adc_sup,1),0) AS nulls2app,
			max(array_length(adc_sup,1)) OVER (PARTITION BY area_domain_sup, area_domain) AS max_len,
			classification_rule
		FROM
			w_classrule_adc
		ORDER BY
			adc_rule
	), w_classrule_null AS (
		SELECT
			ldsity_object, area_domain, area_domain_sup,
			adc_sup, adc_rule, adc2classification_rule, 
			CASE WHEN adc_sup IS NOT NULL THEN adc_sup || array_fill(NULL::int, ARRAY[nulls2app]) ELSE NULL END AS adc,
			classification_rule
		FROM
			w_classrule
	), w_classrule_agg AS (
		SELECT
			ldsity_object, area_domain, area_domain_sup,
			array_agg(adc ORDER BY adc_rule) FILTER (WHERE adc IS NOT NULL) AS adc, 
			array_agg(classification_rule ORDER BY adc_rule) AS rules,
			array_agg(adc2classification_rule ORDER BY adc_rule) AS adc2classification_rules
		FROM 
			w_classrule_null
		GROUP BY ldsity_object, area_domain, area_domain_sup
	), w_check AS (
	--select * from w_classrule_agg;
		SELECT  t2.ldsity_object, t1.id AS panel_refyearset, t2.area_domain, t2.area_domain_sup,
			t2.adc, t2.rules, t2.adc2classification_rules,
			t3.result, t3.message_id, t3.message
		FROM	sdesign.cm_refyearset2panel_mapping AS t1,
			w_classrule_agg AS t2,
			analytical.fn_check_classification_rules(_ldsity, t2.ldsity_object, t2.rules, t1.id, t2.adc) AS t3
		--WHERE t1.id = 20
	), w_true AS (
		SELECT DISTINCT ldsity_object, panel_refyearset, area_domain
		FROM w_check
		WHERE result = true
	)
	, w_final AS (
		SELECT t1.panel_refyearset, t3.adc2classification_rule
		FROM
			w_true AS t1
		INNER JOIN
			w_check AS t2
		ON t1.panel_refyearset = t2.panel_refyearset AND 
			t1.area_domain = t2.area_domain AND
			t1.ldsity_object = t2.ldsity_object,
			unnest(t2.adc2classification_rules) AS t3(adc2classification_rule)
	)
	INSERT INTO analytical.cm_adc2classrule2panel_refyearset (refyearset2panel, adc2classification_rule)
	SELECT panel_refyearset, adc2classification_rule
	FROM
		(SELECT panel_refyearset, adc2classification_rule
		FROM w_final
		EXCEPT 
		SELECT refyearset2panel, adc2classification_rule
		FROM analytical.cm_adc2classrule2panel_refyearset) AS t1
	ORDER BY panel_refyearset, adc2classification_rule
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(integer, integer, integer) IS
'Function for given ldsity contribution, ldsity object and area domain finds the maximum possible panel and reference year set combinations which is then inserted into cm_adc2classrule2panel_refyearset.';

GRANT EXECUTE ON FUNCTION analytical.fn_save_ad_panel_refyearset_mapping(integer, integer, integer) TO public;
