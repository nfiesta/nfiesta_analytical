--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc_hierarchy_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc_hierarchy_attr
(
	_export_connection			integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id														integer,
	variable_superior										integer,
	variable												integer,
	dependent												boolean,
	etl_sub_population_category__id__variable_superior		integer,
	etl_sub_population_category__etl_id__variable_superior	integer,
	etl_sub_population_category__id__variable				integer,
	etl_sub_population_category__etl_id__variable			integer,
	etl_spc_hierarchy__id									integer,
	etl_spc_hierarchy__etl_id								integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc_hierarchy_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_spc_hierarchy_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is not null
		then	
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 03: fn_etl_get_spc_hierarchy_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_spc_hierarchy_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w4.etl_spc_hierarchy__etl_id is not null';
			else
				_cond := 'w4.etl_spc_hierarchy__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,null::integer[],true) where variable_superior = false)		-- variable
			,w2 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,null::integer[],true) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tsh.id as spc_hierarchy,
							tsh.variable_superior,
							tsh.variable,
							tsh.dependent,
							w2.etl_sub_population_category__id as etl_sub_population_category__id__variable_superior,
							w2.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable_superior,
							w1.etl_sub_population_category__id as etl_sub_population_category__id__variable,
							w1.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable
					from
							analytical.t_spc_hierarchy as tsh
							inner join	w1 on tsh.variable = w1.id
							inner join	w2 on tsh.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.spc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_sub_population_category__id__variable_superior,
							w3.etl_sub_population_category__etl_id__variable_superior,
							w3.etl_sub_population_category__id__variable,
							w3.etl_sub_population_category__etl_id__variable,
							tesh.id as etl_spc_hierarchy__id,
							tesh.etl_id as etl_spc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_spc_hierarchy as tesh
								on w3.spc_hierarchy = tesh.spc_hierarchy
								and w3.etl_sub_population_category__id__variable_superior = tesh.etl_sub_population_category_variable_superior
								and w3.etl_sub_population_category__id__variable = tesh.etl_sub_population_category_variable
					)
			select
					w4.spc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_sub_population_category__id__variable_superior,
					w4.etl_sub_population_category__etl_id__variable_superior,
					w4.etl_sub_population_category__id__variable,
					w4.etl_sub_population_category__etl_id__variable,
					w4.etl_spc_hierarchy__id,
					w4.etl_spc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.spc_hierarchy;
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,$2,null::boolean) where variable_superior = false)		-- variable
			,w2 as	(select * from analytical.fn_etl_get_sub_population_category_attr($1,$2,null::boolean) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tsh.id as spc_hierarchy,
							tsh.variable_superior,
							tsh.variable,
							tsh.dependent,
							w2.etl_sub_population_category__id as etl_sub_population_category__id__variable_superior,
							w2.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable_superior,
							w1.etl_sub_population_category__id as etl_sub_population_category__id__variable,
							w1.etl_sub_population_category__etl_id as etl_sub_population_category__etl_id__variable
					from
							analytical.t_spc_hierarchy as tsh
							inner join	w1 on tsh.variable = w1.id
							inner join	w2 on tsh.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.spc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_sub_population_category__id__variable_superior,
							w3.etl_sub_population_category__etl_id__variable_superior,
							w3.etl_sub_population_category__id__variable,
							w3.etl_sub_population_category__etl_id__variable,
							tesh.id as etl_spc_hierarchy__id,
							tesh.etl_id as etl_spc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_spc_hierarchy as tesh
								on w3.spc_hierarchy = tesh.spc_hierarchy
								and w3.etl_sub_population_category__id__variable_superior = tesh.etl_sub_population_category_variable_superior
								and w3.etl_sub_population_category__id__variable = tesh.etl_sub_population_category_variable
					)
			select
					w4.spc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_sub_population_category__id__variable_superior,
					w4.etl_sub_population_category__etl_id__variable_superior,
					w4.etl_sub_population_category__id__variable,
					w4.etl_sub_population_category__etl_id__variable,
					w4.etl_spc_hierarchy__id,
					w4.etl_spc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.spc_hierarchy;
			'
			using _export_connection, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories hierarchy for attributes.';

alter function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc_hierarchy_attr(integer, integer[], boolean) to public;

