--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsity(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_ldsity
(
	_export_connection 	integer,
	_ldsity				integer,
	_data4import		boolean default false
)
returns table
(
	export_connection						integer,
	ldsity 									integer,
	label									varchar,
	description								text,
	label_en								varchar,
	description_en							text,
	column_expression						text,
	------------------------------------------------
	check_unit_of_measure					boolean,
	check_definition_variant				boolean,
	------------------------------------------------
	check_area_domain						boolean,
	check_area_domain_category				boolean,
	check_adc2classification_rule			boolean,
	check_adc_hierarchy						boolean,
	check_adc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_sub_population					boolean,
	check_sub_population_category			boolean,
	check_spc2classification_rule			boolean,
	check_spc_hierarchy						boolean,
	check_spc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_version							boolean,
	check_ldsity2panel_refyearset_version	boolean,
	etl_ldsity__id							integer,
	etl_ldsity__etl_id						integer,
	check_ldsity							boolean,
	check_ldsity_part						boolean,
	------------------------------------------------
	ldsity_object__import					integer,
	unit_of_measure__import					integer,
	definition_variant__import				integer[],
	area_domain_category__import			integer[],
	sub_population_category__import			integer[]
)
as
$$
declare
		_ldsity_object						integer;
		_ldsity_unit_of_measure				integer;
		_ldsity_definition_variant			integer[];
		_ldsity_area_domain_category		integer[];
		_ldsity_sub_population_category		integer[];
		_ldsity_object__import				integer;
		_unit_of_measure__import			integer;
		_definition_variant__import			integer[];
		_area_domain_category__import		integer[];
		_sub_population_category__import	integer[];
		_definition_variant__import_i		integer;
		_area_domain_category__import_i		integer;
		_sub_population_category__import_i	integer;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsity: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_ldsity: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ldsity: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_ldsity: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		select
				cl.ldsity_object,
				cl.unit_of_measure,
				cl.definition_variant,
				cl.area_domain_category,
				cl.sub_population_category
		from
				analytical.c_ldsity as cl where cl.id = _ldsity
		into
				_ldsity_object,
				_ldsity_unit_of_measure,
				_ldsity_definition_variant,
				_ldsity_area_domain_category,
				_ldsity_sub_population_category;


		if _data4import = true
		then
			-------------------------------------
			-- finding etl_id for ldsity object
			select telo.etl_id from analytical.t_etl_ldsity_object as telo
			where telo.export_connection = _export_connection
			and telo.ldsity_object = _ldsity_object
			into _ldsity_object__import;

			if _ldsity_object__import is null
			then
				raise exception 'Error 05: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and ldsity_object = % in table t_etl_ldsity_object!',_export_connection,_ldsity_object;
			end if;
			-------------------------------------
			-- unit_of_measure
			if _ldsity_unit_of_measure is null
			then
				_unit_of_measure__import := null::integer;
			else
				select teuom.etl_id from analytical.t_etl_unit_of_measure as teuom
				where teuom.export_connection = _export_connection
				and teuom.unit_of_measure = _ldsity_unit_of_measure
				into _unit_of_measure__import;

				if _unit_of_measure__import is null
				then
					raise exception 'Error 06: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and unit_of_measure = % in table t_etl_unit_of_measure!',_export_connection,_ldsity_unit_of_measure;
				end if;
			end if;
			-------------------------------------
			-- definition_variant
			if _ldsity_definition_variant is null
			then
				_definition_variant__import := null::integer[];
			else
				for i in 1..array_length(_ldsity_definition_variant,1)
				loop
					select tedf.etl_id from analytical.t_etl_definition_variant as tedf
					where tedf.export_connection = _export_connection
					and tedf.definition_variant = _ldsity_definition_variant[i]
					into _definition_variant__import_i;

					if _definition_variant__import_i is null
					then
						raise exception 'Error 07: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and definition_variant = % in table t_etl_definition_variant!',_export_connection,_ldsity_definition_variant[i];
					end if;

					if i = 1
					then
						_definition_variant__import := array[_definition_variant__import_i];
					else
						_definition_variant__import := _definition_variant__import || array[_definition_variant__import_i];
					end if;			
				end loop;
			end if;
			-------------------------------------
			-- area_domain_category
			if _ldsity_area_domain_category is null
			then
				_area_domain_category__import := null::integer[];
			else
				for i in 1..array_length(_ldsity_area_domain_category,1)
				loop
					select
							teacr.etl_id
					from
							(select cacr.* from analytical.cm_adc2classification_rule as cacr where cacr.id = _ldsity_area_domain_category[i]) as t
							inner join	analytical.c_area_domain_category as cadc on t.area_domain_category = cadc.id
							inner join	(select t1.* from analytical.t_etl_area_domain as t1 where t1.export_connection = _export_connection) as tead on cadc.area_domain = tead.area_domain
							inner join	(select t2.* from analytical.t_etl_ldsity_object as t2 where t2.export_connection = _export_connection) as telo on t.ldsity_object = telo.ldsity_object
							inner join	analytical.t_etl_area_domain_category as teadc
										on t.area_domain_category = teadc.area_domain_category
										and tead.id = teadc.etl_area_domain
							inner join	analytical.t_etl_adc2classification_rule as teacr
										on t.id = teacr.adc2classification_rule
										and teadc.id = teacr.etl_area_domain_category
										and telo.id = teacr.etl_ldsity_object
					into
						_area_domain_category__import_i;

					if _area_domain_category__import_i is null
					then
						raise exception 'Error 08: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and adc2classification_rule = % in table t_etl_adc2classification_rule!',_export_connection,_ldsity_area_domain_category[i];
					end if;

					if i = 1
					then
						_area_domain_category__import := array[_area_domain_category__import_i];
					else
						_area_domain_category__import := _area_domain_category__import || array[_area_domain_category__import_i];
					end if;	
				end loop;
			end if;
			-------------------------------------
			-- sub_population_category
			if _ldsity_sub_population_category is null
			then
				_sub_population_category__import := null::integer[];
			else
				for i in 1..array_length(_ldsity_sub_population_category,1)
				loop
					select
							tescr.etl_id
					from
							(select cscr.* from analytical.cm_spc2classification_rule as cscr where cscr.id = _ldsity_sub_population_category[i]) as t
							inner join	analytical.c_sub_population_category as cspc on t.sub_population_category = cspc.id
							inner join	(select t3.* from analytical.t_etl_sub_population as t3 where t3.export_connection = _export_connection) as tesp on cspc.sub_population = tesp.sub_population
							inner join	(select t4.* from analytical.t_etl_ldsity_object as t4 where t4.export_connection = _export_connection) as telo on t.ldsity_object = telo.ldsity_object
							inner join	analytical.t_etl_sub_population_category as tespc
										on t.sub_population_category = tespc.sub_population_category
										and tesp.id = tespc.etl_sub_population
							inner join	analytical.t_etl_spc2classification_rule as tescr
										on t.id = tescr.spc2classification_rule
										and tespc.id = tescr.etl_sub_population_category
										and telo.id = tescr.etl_ldsity_object
					into
						_sub_population_category__import_i;

					if _sub_population_category__import_i is null
					then
						raise exception 'Error 09: fn_etl_get_ldsity: Not exists any etl_id for export_connection = % and spc2classification_rule = % in table t_etl_spc2classification_rule!',_export_connection,_ldsity_sub_population_category[i];
					end if;

					if i = 1
					then
						_sub_population_category__import := array[_sub_population_category__import_i];
					else
						_sub_population_category__import := _sub_population_category__import || array[_sub_population_category__import_i];
					end if;	
				end loop;
			end if;
			-------------------------------------
		else
			_ldsity_object__import := null::integer;
			_unit_of_measure__import := null::integer;
			_definition_variant__import	:= null::integer[];
			_area_domain_category__import := null::integer[];
			_sub_population_category__import := null::integer[];
		end if;

		return query execute
		'
		with
		w1 as	(
				select
						$1 as export_connection,
						$2 as ldsity,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $4 is not null then (select * from analytical.fn_etl_check_unit_of_measure($1,$2)) else true end					as check_unit_of_measure,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $5 is not null then (select * from analytical.fn_etl_check_definition_variant($1,$2)) else true end				as check_definition_variant,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $6 is not null then (select * from analytical.fn_etl_check_area_domain($1,$2)) else true end						as check_area_domain,
						case when $6 is not null then (select * from analytical.fn_etl_check_area_domain_category($1,$2)) else true end				as check_area_domain_category,
						case when $6 is not null then (select * from analytical.fn_etl_check_adc2classification_rule($1,$2)) else true end			as check_adc2classification_rule,
						case when $6 is not null then (select * from analytical.fn_etl_check_adc_hierarchy($1,$2)) else true end					as check_adc_hierarchy,
						case when $6 is not null then (select * from analytical.fn_etl_check_adc2classrule2panel_refyearset($1,$2)) else true end	as check_adc2classrule2panel_refyearset,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						case when $7 is not null then (select * from analytical.fn_etl_check_sub_population($1,$2)) else true end					as check_sub_population,
						case when $7 is not null then (select * from analytical.fn_etl_check_sub_population_category($1,$2)) else true end			as check_sub_population_category,
						case when $7 is not null then (select * from analytical.fn_etl_check_spc2classification_rule($1,$2)) else true end			as check_spc2classification_rule,
						case when $7 is not null then (select * from analytical.fn_etl_check_spc_hierarchy($1,$2)) else true end					as check_spc_hierarchy,
						case when $7 is not null then (select * from analytical.fn_etl_check_spc2classrule2panel_refyearset($1,$2)) else true end	as check_spc2classrule2panel_refyearset,
						-----------------------------------------------------------------------------------------------------------------------------------------------------------
						(select * from analytical.fn_etl_check_version($1,$2))																		as check_version,
						(select * from analytical.fn_etl_check_ldsity2panel_refyearset_version($1,$2))												as check_ldsity2panel_refyearset_version
				)
		,w2 as	(
				select
						w1.export_connection,
						w1.ldsity,
						cl.label,
						cl.description,
						cl.label_en,
						cl.description_en,
						cl.column_expression::text as column_expression,
						---------------------------------------------
						w1.check_unit_of_measure,
						w1.check_definition_variant,
						---------------------------------------------
						w1.check_area_domain,
						w1.check_area_domain_category,
						w1.check_adc2classification_rule,
						w1.check_adc_hierarchy,
						w1.check_adc2classrule2panel_refyearset,
						---------------------------------------------
						w1.check_sub_population,
						w1.check_sub_population_category,
						w1.check_spc2classification_rule,
						w1.check_spc_hierarchy,
						w1.check_spc2classrule2panel_refyearset,
						---------------------------------------------
						w1.check_version,
						w1.check_ldsity2panel_refyearset_version,
						t.id as etl_ldsity__id,
						t.etl_id as etl_ldsity__etl_id
				from
						w1	inner join	analytical.c_ldsity as cl on w1.ldsity = cl.id
							left join	(
										select * from analytical.t_etl_ldsity where etl_ldsity_object =
										(select id from analytical.t_etl_ldsity_object as telo where telo.export_connection = $1 and ldsity_object = $3)
										and ldsity = $2
										) as t
										on w1.ldsity = t.ldsity
				)
		select
				w2.export_connection,
				w2.ldsity,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en,
				w2.column_expression,
				-------------------------------------------
				w2.check_unit_of_measure,
				w2.check_definition_variant,
				-------------------------------------------
				w2.check_area_domain,
				w2.check_area_domain_category,
				w2.check_adc2classification_rule,
				w2.check_adc_hierarchy,
				w2.check_adc2classrule2panel_refyearset,
				-------------------------------------------
				w2.check_sub_population,
				w2.check_sub_population_category,
				w2.check_spc2classification_rule,
				w2.check_spc_hierarchy,
				w2.check_spc2classrule2panel_refyearset,
				-------------------------------------------
				w2.check_version,
				w2.check_ldsity2panel_refyearset_version,
				w2.etl_ldsity__id,
				w2.etl_ldsity__etl_id,
				-------------------------------------------
				case
					when
						w2.check_unit_of_measure = true					and
						w2.check_definition_variant = true				and
						---------------------------------------------------
						w2.check_area_domain = true						and
						w2.check_area_domain_category = true			and
						w2.check_adc2classification_rule = true			and
						w2.check_adc_hierarchy = true					and
						w2.check_adc2classrule2panel_refyearset = true	and
						---------------------------------------------------
						w2.check_sub_population = true					and
						w2.check_sub_population_category = true			and
						w2.check_spc2classification_rule = true			and
						w2.check_spc_hierarchy = true					and
						w2.check_spc2classrule2panel_refyearset = true	and
						---------------------------------------------------
						w2.check_version = true							and
						w2.check_ldsity2panel_refyearset_version = true	and
						w2.etl_ldsity__id is not null					and
						w2.etl_ldsity__etl_id is not null
					then true
					else false
				end as check_ldsity,
				-------------------------------------------
				case
					when
						w2.check_unit_of_measure = true					and
						w2.check_definition_variant = true				and
						---------------------------------------------------
						w2.check_area_domain = true						and
						w2.check_area_domain_category = true			and
						w2.check_adc2classification_rule = true			and
						w2.check_adc_hierarchy = true					and
						w2.check_adc2classrule2panel_refyearset = true	and
						---------------------------------------------------
						w2.check_sub_population = true					and
						w2.check_sub_population_category = true			and
						w2.check_spc2classification_rule = true			and
						w2.check_spc_hierarchy = true					and
						w2.check_spc2classrule2panel_refyearset = true
					then true
					else false
				end as check_ldsity_part,
				$8 as ldsity_object__import,
				$9 as unit_of_measure__import,
				$10 as definition_variant__import,
				$11 as area_domain_category__import,
				$12 as sub_population_category__import
		from
				w2;
		'
		using
				_export_connection,
				_ldsity,
				_ldsity_object,
				_ldsity_unit_of_measure,
				_ldsity_definition_variant,
				_ldsity_area_domain_category,
				_ldsity_sub_population_category,
				_ldsity_object__import,
				_unit_of_measure__import,
				_definition_variant__import,
				_area_domain_category__import,
				_sub_population_category__import;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsity(integer, integer, boolean) is
'Function returns information records of ldsity.';

alter function analytical.fn_etl_get_ldsity(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsity(integer, integer, boolean) to public;

