--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_refyearset2panel4adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) CASCADE;

create or replace function analytical.fn_etl_get_refyearset2panel4adc2classification_rule
(
	_adc2classification_rule	integer
)
returns table
(
	cm_adc2classrule2panel_refyearset__id					integer,
	cm_adc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_res_id					integer;
		_res_refyearset2panel	integer;
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_etl_get_refyearset2panel4adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		select
				cacpr.id,
				cacpr.refyearset2panel
		from
				analytical.cm_adc2classrule2panel_refyearset as cacpr
		where
				cacpr.adc2classification_rule = _adc2classification_rule
		order
				by cacpr.refyearset2panel
		limit
				1
		into
				_res_id,
				_res_refyearset2panel;
	
		if _res_id is null
		then
			raise exception 'Error 02: fn_etl_get_refyearset2panel4adc2classification_rule: For input argument _adc2classification_rule = % not exists any refyearset2panel in cm_adc2classrule2panel_refyearset table!',_adc2classification_rule;
		end if;
	
		return query select _res_id, _res_refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) is
'Function returns one row of id and refyearset2panel from cm_adc2classrule2panel_refyearset table for given adc2classification_rule.';

alter function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to data_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_refyearset2panel4adc2classification_rule(integer) to public;

