--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_area_domain(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_area_domain
(
	_export_connection		integer,
	_area_domain			integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_area_domain: Input argument _export_connection must not by NULL!';
	end if; 

	if _area_domain is null
	then
		raise exception 'Error 02: fn_etl_save_area_domain: Input argument _area_domain must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_area_domain: Input argument _etl_id must not by NULL!';
	end if;

	if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error 04: fn_etl_save_area_domain: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;

	if not exists (select t2.* from analytical.c_area_domain as t2 where t2.id = _area_domain)
	then
		raise exception 'Error 05: fn_etl_save_area_domain: Given area domain (%) does not exist in c_area_domain table.', _area_domain;
	end if;		

	insert into analytical.t_etl_area_domain(export_connection, area_domain, etl_id)
	select _export_connection, _area_domain, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_area_domain(integer, integer, integer) is
'Function inserts a record into table t_etl_area_domain based on given parameters.';

alter function analytical.fn_etl_save_area_domain(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_area_domain(integer, integer, integer) to public;