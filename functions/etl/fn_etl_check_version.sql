--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_version(integer, integer) CASCADE;

create or replace function analytical.fn_etl_check_version
(
	_export_connection	integer,
	_ldsity				integer
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_version: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_check_version: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_check_version: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_check_version: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_version(_export_connection,_ldsity,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_version(integer, integer) is
'Function checks that all versions for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_version(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_check_version(integer, integer) to public;

