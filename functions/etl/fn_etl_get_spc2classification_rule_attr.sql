--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_spc2classification_rule_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_spc2classification_rule_attr
(
	_export_connection			integer,
	_ldsity_object				integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_sub_population_category__id							integer,
	etl_sub_population_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_spc2classification_rule__id							integer,
	etl_spc2classification_rule__etl_id						integer,
	cm_spc2classrule2panel_refyearset__id					integer,
	cm_spc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_spc2classification_rule_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_spc2classification_rule_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_spc2classification_rule_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_spc2classification_rule_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _sub_population_category is not null
		then
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 05: fn_etl_get_spc2classification_rule_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_spc2classification_rule_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;		

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_spc2classification_rule__etl_id is not null';
			else
				_cond := 'w3.etl_spc2classification_rule__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_sub_population_category_attr($1,null::integer[],true)
					)
			,w2 as	(
					select
							w1.*,
							t.id as spc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_spc2classification_rule
										where sub_population_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.sub_population_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tesr.id as etl_spc2classification_rule__id,
							tesr.etl_id as etl_spc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4spc2classification_rule(w2.spc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_spc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_sub_population_category in (select w2.etl_sub_population_category__id from w2)
										and spc2classification_rule in (select w2.spc2classification_rule from w2)
										) as tesr
							on w2.etl_ldsity_object__id = tesr.etl_ldsity_object
							and w2.etl_sub_population_category__id = tesr.etl_sub_population_category
							and w2.spc2classification_rule = tesr.spc2classification_rule
					)
			select
					distinct
					w3.spc2classification_rule,
					w3.classification_rule,
					w3.etl_sub_population_category__id,
					w3.etl_sub_population_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_spc2classification_rule__id,
					w3.etl_spc2classification_rule__etl_id,
					(w3.res).cm_spc2classrule2panel_refyearset__id,
					(w3.res).cm_spc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.spc2classification_rule;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_sub_population_category_attr($1,$3,null::boolean)
					)
			,w2 as	(
					select
							w1.*,
							t.id as spc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_spc2classification_rule
										where sub_population_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.sub_population_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tesr.id as etl_spc2classification_rule__id,
							tesr.etl_id as etl_spc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4spc2classification_rule(w2.spc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_spc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_sub_population_category in (select w2.etl_sub_population_category__id from w2)
										and spc2classification_rule in (select w2.spc2classification_rule from w2)
										) as tesr
							on w2.etl_ldsity_object__id = tesr.etl_ldsity_object
							and w2.etl_sub_population_category__id = tesr.etl_sub_population_category
							and w2.spc2classification_rule = tesr.spc2classification_rule
					)
			select
					w3.spc2classification_rule,
					w3.classification_rule,
					w3.etl_sub_population_category__id,
					w3.etl_sub_population_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_spc2classification_rule__id,
					w3.etl_spc2classification_rule__etl_id,
					(w3.res).cm_spc2classrule2panel_refyearset__id,
					(w3.res).cm_spc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.spc2classification_rule;
			'
			using _export_connection, _ldsity_object, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) is
'Function returns information records of area domain categories and rules for attributes.';

alter function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_spc2classification_rule_attr(integer, integer, integer[], boolean) to public;

