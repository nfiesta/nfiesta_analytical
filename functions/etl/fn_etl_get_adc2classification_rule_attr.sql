--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classification_rule_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classification_rule_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_area_domain_category__id							integer,
	etl_area_domain_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_adc2classification_rule__id							integer,
	etl_adc2classification_rule__etl_id						integer,
	cm_adc2classrule2panel_refyearset__id					integer,
	cm_adc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classification_rule_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_adc2classification_rule_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_adc2classification_rule_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_adc2classification_rule_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is not null
		then
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 05: fn_etl_get_adc2classification_rule_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_adc2classification_rule_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;		

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_adc2classification_rule__etl_id is not null';
			else
				_cond := 'w3.etl_adc2classification_rule__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_area_domain_category_attr($1,null::integer[],true)
					)
			,w2 as	(
					select
							w1.*,
							t.id as adc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_adc2classification_rule
										where area_domain_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.area_domain_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tear.id as etl_adc2classification_rule__id,
							tear.etl_id as etl_adc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w2.adc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_adc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_area_domain_category in (select w2.etl_area_domain_category__id from w2)
										and adc2classification_rule in (select w2.adc2classification_rule from w2)
										) as tear
							on w2.etl_ldsity_object__id = tear.etl_ldsity_object
							and w2.etl_area_domain_category__id = tear.etl_area_domain_category
							and w2.adc2classification_rule = tear.adc2classification_rule
					)
			select
					distinct
					w3.adc2classification_rule,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					(w3.res).cm_adc2classrule2panel_refyearset__id,
					(w3.res).cm_adc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.adc2classification_rule;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(
					select * from analytical.fn_etl_get_area_domain_category_attr($1,$3,null::boolean)
					)
			,w2 as	(
					select
							w1.*,
							t.id as adc2classification_rule,
							t.ldsity_object,
							t.classification_rule,
							telo.id as etl_ldsity_object__id,
							telo.etl_id as etl_ldsity_object__etl_id
					from
							w1
							inner join	(
										select * from analytical.cm_adc2classification_rule
										where area_domain_category in (select id from w1)
										and ldsity_object = $2
										) as t
							on w1.id = t.area_domain_category
							
							inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
							on t.ldsity_object = telo.ldsity_object
					)
			,w3 as	(
					select
							w2.*,
							tear.id as etl_adc2classification_rule__id,
							tear.etl_id as etl_adc2classification_rule__etl_id,
							analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w2.adc2classification_rule) as res
					from
							w2
							left join	(
										select * from analytical.t_etl_adc2classification_rule
										where etl_ldsity_object in (select w2.etl_ldsity_object__id from w2)
										and etl_area_domain_category in (select w2.etl_area_domain_category__id from w2)
										and adc2classification_rule in (select w2.adc2classification_rule from w2)
										) as tear
							on w2.etl_ldsity_object__id = tear.etl_ldsity_object
							and w2.etl_area_domain_category__id = tear.etl_area_domain_category
							and w2.adc2classification_rule = tear.adc2classification_rule
					)
			select
					w3.adc2classification_rule,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					(w3.res).cm_adc2classrule2panel_refyearset__id,
					(w3.res).cm_adc2classrule2panel_refyearset__refyearset2panel
			from
					w3 where '|| _cond ||'
			order
					by w3.adc2classification_rule;
			'
			using _export_connection, _ldsity_object, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) is
'Function returns information records of area domain categories and rules for attributes.';

alter function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule_attr(integer, integer, integer[], boolean) to public;

