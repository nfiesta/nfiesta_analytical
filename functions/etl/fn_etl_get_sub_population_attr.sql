--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_attr
(
	_export_connection			integer,
	_sub_population_category	integer[] default null::integer[],
	_etl						boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_sub_population__id		integer,
	etl_sub_population__etl_id	integer,
	variable_superior			boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_sub_population_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _sub_population_category is not null
		then	
			for i in 1..array_length(_sub_population_category,1)
			loop
				if not exists (select t2.* from analytical.c_sub_population_category as t2 where t2.id = _sub_population_category[i])
				then
					raise exception 'Error 03: fn_etl_get_sub_population_attr: Given sub population category (%) does not exist in c_sub_population_category table.', _sub_population_category[i];
				end if;
			end loop;
		end if;
	
		if _sub_population_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_sub_population_attr: If input argument _sub_population_category is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w5.etl_sub_population__etl_id is not null';
			else
				_cond := 'w5.etl_sub_population__etl_id is null';
			end if;
		end if;
	
		if _sub_population_category is null
		then
			return query execute
			'	
			with
			w1 as	(
					select tesp.* from analytical.t_etl_sub_population as tesp where tesp.export_connection = $1
					)		
			,w2a as	(
					select tsh.variable_superior from analytical.t_spc_hierarchy as tsh
					where tsh.variable_superior in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)					
			,w2b as	(
					select tsh.variable from analytical.t_spc_hierarchy as tsh
					where tsh.variable in
						(
						select cspc.id from analytical.c_sub_population_category as cspc
						where cspc.sub_population in (select w1.sub_population from w1)
						)
					)
			,w3a as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cspc1.sub_population from analytical.c_sub_population_category as cspc1
					where cspc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.sub_population, true as variable_superior from w3a union all
					select w3b.sub_population, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.sub_population, variable_superior from w4 union
					select w1.sub_population, false as variable_superior from w1
					)				
			select
					w5.sub_population as id,
					csp.label,
					csp.description,
					csp.label_en,
					csp.description_en,
					w1.id as etl_sub_population__id,
					w1.etl_id as etl_sub_population__etl_id,
					w5.variable_superior
			from
					w5
					inner join w1 on w5.sub_population = w1.sub_population
					inner join analytical.c_sub_population as csp on w1.sub_population = csp.id
			order
					by w5.sub_population, w5.variable_superior
			'
			using _export_connection;		
		else	
			return query execute
			'
			with
			w1 as	(
					select
							csp.id,
							csp.label,
							csp.description,
							csp.label_en,
							csp.description_en
					from
							analytical.c_sub_population csp
					where
							csp.id in	(
										select distinct cspc.sub_population
										from analytical.c_sub_population_category as cspc
										where cspc.id in (select unnest($2))
										)
					)
			,w2 as	(
					select
							w1.*,
							tesp.id as etl_sub_population__id,
							tesp.etl_id as etl_sub_population__etl_id
					from
							w1
					left
					join	(select * from analytical.t_etl_sub_population where export_connection = $1) as tesp
							on w1.id = tesp.sub_population
					)
			-----------------------------------------
			-- add type of hierarchy
			,w3 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en
					from
							analytical.c_sub_population
					where
							id in
									(
									select distinct sub_population from analytical.c_sub_population_category where id in	-- finding out of variable superior types
									(select distinct variable_superior from analytical.t_spc_hierarchy where variable in	-- finding out of variable superior categories
									(select id from analytical.c_sub_population_category
									where sub_population in (select distinct w2.id from w2)))
									)
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_sub_population__id,
							t.etl_id as etl_sub_population__etl_id
					from
							w3
					left join	(select * from analytical.t_etl_sub_population where export_connection = $1) as t
								on w3.id = t.sub_population
					)
			-----------------------------------------
			,w5 as	(
					select
							w2.id,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en,
							w2.etl_sub_population__id,
							w2.etl_sub_population__etl_id,
							false as variable_superior
					from
							w2 where w2.id not in (select w4.id from w4)
					union all
					select
							w4.id,
							w4.label,
							w4.description,
							w4.label_en,
							w4.description_en,
							w4.etl_sub_population__id,
							w4.etl_sub_population__etl_id,
							true as variable_superior
					from
							w4
					)
			select
					w5.id,
					w5.label,
					w5.description,
					w5.label_en,
					w5.description_en,
					w5.etl_sub_population__id,
					w5.etl_sub_population__etl_id,
					w5.variable_superior
			from
					w5 where '|| _cond ||'
			order
					by w5.id;
			'
			using _export_connection, _sub_population_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) is
'Function returns information records of sub populations for given arguments.';

alter function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_attr(integer, integer[], boolean) to public;

