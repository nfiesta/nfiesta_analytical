--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_unit_of_measure
(
	_export_connection	integer,
	_ldsity				integer default null::integer,
	_etl				boolean default null::boolean
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	etl_unit_of_measure__id			integer,
	etl_unit_of_measure__etl_id		integer
)
as
$$
declare
		_cond_1		text;
		_cond_2		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_unit_of_measure: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_unit_of_measure: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if _ldsity is not null
		then
			if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
			then
				raise exception 'Error 03: fn_etl_get_unit_of_measure: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
			end if;		
		end if;

		if _ldsity is null
		then
			_cond_1 := 'TRUE';
		else
			_cond_1 := 'cuom.id = (select cl.unit_of_measure from analytical.c_ldsity as cl where cl.id = $2)';
		end if;
	
		if _etl is null
		then
			_cond_2 := 'TRUE';
		else
			if _etl = true
			then
				_cond_2 := 'w3.etl_unit_of_measure__etl_id is not null';
			else
				_cond_2 := 'w3.etl_unit_of_measure__etl_id is null';
			end if;
		end if;
			
		return query execute
		'
		with
		w1 as	(
				select cuom.* from analytical.c_unit_of_measure as cuom
				where '|| _cond_1 ||'
				)
		,w2 as	(
				select * from analytical.t_etl_unit_of_measure
				where export_connection = $1
				)
		,w3 as	(
				select
					w1.*,
					w2.id as etl_unit_of_measure__id,
					w2.etl_id as etl_unit_of_measure__etl_id
				from
					w1 left join w2 on w1.id = w2.unit_of_measure
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label_en,
				w3.description_en,
				w3.etl_unit_of_measure__id,
				w3.etl_unit_of_measure__etl_id
		from
				w3 where '|| _cond_2 ||'
		order
				by w3.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) is
'Function returns information records of unit of measure.';

alter function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_unit_of_measure(integer, integer, boolean) to public;

