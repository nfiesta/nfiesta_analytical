--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_informations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_informations(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_informations
(
	_export_connection	integer,
	_ldsity				integer
)
returns table
(
	ldsity_object							integer,
	sub_population							integer,
	sub_population_category					integer,
	spc2classification_rule					integer,
	------------------------------------------------
	etl_ldsity_object__id					integer,
	etl_ldsity_object__etl_id				integer,
	etl_sub_population__id					integer,
	etl_sub_population__etl_id				integer,
	etl_sub_population_category__id			integer,
	etl_sub_population_category__etl_id		integer,
	etl_spc2classification_rule__id			integer,
	etl_spc2classification_rule__etl_id		integer
)
as
$$
declare
		_spc_array	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_informations: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_informations: Input argument _ldsity must not be NULL!';
		end if;
	
		select cl.sub_population_category from analytical.c_ldsity as cl where cl.id = _ldsity
		into _spc_array;
	
		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_informations: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_sub_population_informations: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;

		if _spc_array is null
		then
			raise exception 'Error 05: fn_etl_get_sub_population_informations: For input argument _ldsity = % not exists any reduction of sub population category!',_ldsity;
		else		
			return query
			with
			w1 as	(						
					select
							t2.sub_population as rule__type,
							t1.rule__category,
							t1.rule__id,
							t1.rule__ldsity_object
					from
						(
						select
								cmspc2cr.id as rule__id,
								cmspc2cr.sub_population_category as rule__category,
								cmspc2cr.ldsity_object as rule__ldsity_object
						from analytical.cm_spc2classification_rule as cmspc2cr
						where cmspc2cr.id in (select unnest(_spc_array))
						) as t1
					inner join	analytical.c_sub_population_category as t2 on t1.rule__category = t2.id			
					)
			,w2 as	(
					select distinct rule__type, rule__category, rule__id, rule__ldsity_object from w1
					)
			,w3 as	(
					select
							w2.rule__type as rule__type_all,
							w2.rule__category,
							w2.rule__id,
							w2.rule__ldsity_object,
							cspc.id as rule__category_all,
							cscr.id as rule__id_all,
							telo.id as etl_ldsity_object__id,			-- idecko do tabulky analytical.t_etl_ldsity_object
							telo.etl_id as etl_ldsity_object__etl_id	-- idecko do tabulky target_data.c_ldsity_object
					from w2
					inner join analytical.c_sub_population_category as cspc on w2.rule__type = cspc.sub_population
					inner join analytical.cm_spc2classification_rule as cscr on w2.rule__ldsity_object = cscr.ldsity_object and cspc.id = cscr.sub_population_category
					left join (select * from analytical.t_etl_ldsity_object where export_connection = _export_connection) as telo on w2.rule__ldsity_object = telo.ldsity_object
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_sub_population__id,			-- idecko do tabulky analytical.t_etl_sub_population
							t.etl_id as etl_sub_population__etl_id		-- idecko do tabulky target_data.c_sub_population
					from 
							w3
					left join	(select * from analytical.t_etl_sub_population where export_connection = _export_connection) as t on w3.rule__type_all = t.sub_population
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_sub_population_category__id,			-- idecko do tabulky analytical.t_etl_sub_population_category
							t.etl_id as etl_sub_population_category__etl_id	-- idecko do tabulky target_data.c_sub_population_category
					from
							w4
					left join	(select * from analytical.t_etl_sub_population_category where etl_sub_population in (select w4.etl_sub_population__id from w4)) as t
								on w4.etl_sub_population__id = t.etl_sub_population
								and w4.rule__category_all = t.sub_population_category
					)
			,w6 as	(
					select
							w5.*,
							t.id as etl_spc2classification_rule__id,
							t.etl_id as etl_spc2classification_rule__etl_id
					from
							w5
					left join	(select * from analytical.t_etl_spc2classification_rule where etl_sub_population_category in (select w5.etl_sub_population_category__id from w5)) as t
								on w5.etl_sub_population_category__id = t.etl_sub_population_category
								and w5.etl_ldsity_object__id = t.etl_ldsity_object
								and w5.rule__id_all = t.spc2classification_rule
					)
			select
					w6.rule__ldsity_object			as ldsity_object,
					w6.rule__type_all				as sub_population,
					w6.rule__category_all			as sub_population_category,
					w6.rule__id_all					as spc2classification_rule,
					---------------------
					w6.etl_ldsity_object__id,
					w6.etl_ldsity_object__etl_id,
					w6.etl_sub_population__id,
					w6.etl_sub_population__etl_id,
					w6.etl_sub_population_category__id,
					w6.etl_sub_population_category__etl_id,
					w6.etl_spc2classification_rule__id,
					w6.etl_spc2classification_rule__etl_id
			from
					w6 order by w6.rule__id_all;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_informations(integer, integer) is
'Function returns sub population informations for given input arguments: export connection and ldsity.';

alter function analytical.fn_etl_get_sub_population_informations(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations(integer, integer) to public;

