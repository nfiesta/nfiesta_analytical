--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsity_objects
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) CASCADE;

create or replace function analytical.fn_etl_get_ldsity_objects
(
	_export_connection		integer,
	_etl					boolean default null::boolean,	-- returns all ldsities for all objects [false returns all ldsities that are not etl yet]
	_checks					boolean default false,			-- if argument is true than are made checks for ldsities and checks for theirs upper objects
	_ldsity_object			integer default null::integer	-- function is calls for choosed ldsity object
)
returns table
(
	ldsity_object				integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	table_name					regclass,
	upper_object__id			integer,
	upper_object__etl_id		integer,
	areal_or_population__id		integer,
	areal_or_population__label	varchar,
	areal_or_population__etl_id	integer,
	column4upper_object			varchar,
	filter						text,
	etl_ldsity_object__id		integer,
	etl_ldsity_object__etl_id	integer,
	check_ldsities				boolean,
	check_upper_object			boolean		-- bude slouzit v etl procesu dale pri volani teto funkce na konkretni objekt
)
as
$$
declare
		_cond_1			text;
		_cond_2			text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsity_objects: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_ldsity_objects: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		-------------------------------
		if _ldsity_object is not null
		then
			_cond_1 := 'clo_int.id = $2';
		else
			_cond_1 := 'TRUE';
		end if;
		-------------------------------
		if _checks = false
		then
			if _etl is null
			then
				_cond_2 := 'TRUE'; -- bez kontrol, vypise vsechny zaznamy
			else
				if _etl = true
				then
					_cond_2 := 'w1.etl_ldsity_object__etl_id is not null'; -- bez kontrol, uz etl-kovany byly
				else
					_cond_2 := 'w1.etl_ldsity_object__etl_id is null'; -- bez kontrol, jeste etl-kovany nebyly
				end if;
			end if;
		else
			if _etl is null
			then
				_cond_2 := 'TRUE'; -- s kontrolama, vypise vsechny zaznamy
			else
				if _etl = true
				then
					_cond_2 :=	'
								(
								w2.etl_ldsity_object__etl_id is not null				and
								(w2.check_ldsities is null or w2.check_ldsities = true)	and
								w2.check_upper_object = true
								)
								';
				else
					_cond_2 :=	'
								(
								(w2.etl_ldsity_object__etl_id is null)																		or
								(w2.etl_ldsity_object__etl_id is not null and w2.check_ldsities = false and w2.check_upper_object =  true)	or
								(w2.etl_ldsity_object__etl_id is not null and w2.check_ldsities =  true and w2.check_upper_object = false)	or
								(w2.etl_ldsity_object__etl_id is not null and w2.check_ldsities = false and w2.check_upper_object = false)
								)
								';
				end if;
			end if;
		end if;
		-------------------------------	
		-------------------------------
		if	_checks = false
		then
			-- VYPIS BEZ KONTROL !!! => _checks = false
			-- + varianty pro vypis etl_ldsity_object__etl_id:
			-- 1. _etl is null::boolean -- vypis varianty 2. a 3. => etl_ldsity_object__etl_id is null or is not null
			-- 2. _etl = true  => etl_ldsity_object__etl_id =  true => vse co uz bylo na etl-kovano, bez ohledu na kontroly
			-- 3. _etl = false => etl_ldsity_object__etl_id = false => vse co jeste nebylo na etl-kovano, bez ohledu na kontroly
			return query execute
			'
			with
			w1 as	(
					select
							clo.id as ldsity_object,
							clo.label,
							clo.description,
							clo.label_en,
							clo.description_en,
							clo.table_name,
							clo.upper_object as upper_object__id,
							telo2.etl_id as upper_object__etl_id,				-- toto mi rika jestli uz upper object byl ci nebyl na etl-kovan
							clo.areal_or_population as areal_or_population__id,
							caop."label" as areal_or_population__label,
							teaop.etl_id as areal_or_population__etl_id,		-- toto mi rika jestli uz areal_or_population bylo ci nebylo na etl-kovano
							clo.column4upper_object,
							clo.filter,
							telo1.id as etl_ldsity_object__id,
							telo1.etl_id as etl_ldsity_object__etl_id,			-- toto mi rika jestli dany ldsity objekt uz byl ci nebyl na etl-kovan
							null::boolean as check_ldsities,
							null::boolean as check_upper_object
					from
							(select clo_int.* from analytical.c_ldsity_object as clo_int where '|| _cond_1 ||') as clo -- podminka _cond_1 pripadne omezuje jen na jeden konkretni ldsity objekt
							inner join analytical.c_areal_or_population as caop on clo.areal_or_population = caop.id
							left join (select * from analytical.t_etl_areal_or_population where export_connection = $1) as teaop on clo.areal_or_population = teaop.areal_or_population -- potom musi byt INNER JOIN
							left join (select a1.* from analytical.t_etl_ldsity_object as a1 where a1.export_connection = $1) as telo1 on clo.id = telo1.ldsity_object
							left join (select a2.* from analytical.t_etl_ldsity_object as a2 where a2.export_connection = $1) as telo2 on clo.upper_object = telo2.ldsity_object
					)
			select
					w1.ldsity_object,
					w1.label,
					w1.description,
					w1.label_en,
					w1.description_en,
					w1.table_name,
					w1.upper_object__id,
					w1.upper_object__etl_id,
					w1.areal_or_population__id,
					w1.areal_or_population__label,
					w1.areal_or_population__etl_id,
					w1.column4upper_object,
					w1.filter,
					w1.etl_ldsity_object__id,
					w1.etl_ldsity_object__etl_id,
					w1.check_ldsities,
					w1.check_upper_object
			from
					w1 where '|| _cond_2 ||'
			order
					by w1.ldsity_object;
			'
			using _export_connection, _ldsity_object;
		else
			-- VYPIS s KONTROLAMA !!! => _checks = true
			
			-- provadeni kontroly prispevku u daneno objektu: [vysledky muzou byt: null, true, false] !!!
			-- kontrola se neprovadi u objektu, ktery ma upper_object is null -- vysledkem bude NULL::boolean
			-- kontrola se neprovadi u objektu, ktery doposud nebyl etl-kovan -- vysledkem bude NULL::boolean
					
			-- provadeni kontroly prispevku u upper objektu: [vysledky muzou byt: true, false] !!!
			
			-- + varianty pro vypis:
			-- 1. _etl is null::boolean -- vypis vseho bez ohledu na vysledek kontrol => TRUE
				
			-- 2. _etl = true
			--		=> etl_ldsity_object__etl_id is not null and (check_ldsities is null or check_ldsities = true) and check_upper_object = true
			--		=> vse co uz bylo na etl-kovano, a obe kontroly jsou TRUE, pricemz check_ldsities u nejvyse postaveneho objektu je null
			
			-- 3. _etl = false
			--		=> etl_ldsity_object__etl_id is null => vse co jeste nebylo etl-kovano, bez ohledu na kontroly		OR
			--		=> etl_ldsity_object__etl_id is not null and check_ldsities = false and check_upper_object = true	OR
			--		=> etl_ldsity_object__etl_id is not null and check_ldsities =  true and check_upper_object = false
			
			return query execute
			'
			with
			w1 as	(
					select
							clo.id as ldsity_object,
							clo.label,
							clo.description,
							clo.label_en,
							clo.description_en,
							clo.table_name,
							clo.upper_object as upper_object__id,
							telo2.etl_id as upper_object__etl_id,				-- toto mi rika jestli uz upper object byl ci nebyl na etl-kovan
							clo.areal_or_population as areal_or_population__id,
							caop."label" as areal_or_population__label,
							teaop.etl_id as areal_or_population__etl_id,		-- toto mi rika jestli uz areal_or_population bylo ci nebylo na etl-kovano
							clo.column4upper_object,
							clo.filter,
							telo1.id as etl_ldsity_object__id,
							telo1.etl_id as etl_ldsity_object__etl_id			-- toto mi rika jestli dany ldsity objekt uz byl ci nebyl na etl-kovan
					from
							(select clo_int.* from analytical.c_ldsity_object as clo_int where '|| _cond_1 ||') as clo -- podminka _cond_1 pripadne omezuje jen na jeden konkretni ldsity objekt
							inner join analytical.c_areal_or_population as caop on clo.areal_or_population = caop.id
							left join (select * from analytical.t_etl_areal_or_population where export_connection = $1) as teaop on clo.areal_or_population = teaop.areal_or_population -- potom musi byt INNER JOIN
							left join (select a1.* from analytical.t_etl_ldsity_object as a1 where a1.export_connection = $1) as telo1 on clo.id = telo1.ldsity_object
							left join (select a2.* from analytical.t_etl_ldsity_object as a2 where a2.export_connection = $1) as telo2 on clo.upper_object = telo2.ldsity_object
					)
			,w2 as	(
					select
							w1.ldsity_object,
							w1.label,
							w1.description,
							w1.label_en,
							w1.description_en,
							w1.table_name,
							w1.upper_object__id,
							w1.upper_object__etl_id,
							w1.areal_or_population__id,
							w1.areal_or_population__label,
							w1.areal_or_population__etl_id,
							w1.column4upper_object,
							w1.filter,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
	
							-- provadeni kontroly prispevku u daneno objektu: [vysledky muzou byt: null, true, false] !!!
							-- kontrola se neprovadi u objektu, ktery ma upper_object is null -- vysledkem bude NULL::boolean
							-- kontrola se neprovadi u objektu, ktery doposud nebyl etl-kovan -- vysledkem bude NULL::boolean
							case
								when (w1.etl_ldsity_object__etl_id is null or w1.upper_object__id is null)
								then NULL::boolean
								else (select * from analytical.fn_etl_check_ldsities4object($1,w1.ldsity_object))
							end as check_ldsities,

							-- provadeni kontroly prispevku u upper objektu: [vysledky muzou byt: true, false] !!!
							case
								when w1.upper_object__id is null -- dany objekt je nejvyse postavenym objektem
								then TRUE
								else -- dany objekt neni nejvyse postavenym objektem
									case
										when
											(select clo1.upper_object from analytical.c_ldsity_object as clo1 where clo1.id = w1.upper_object__id)
											is null
										then -- nadrizeny objekt u daneho objektu je nejvyse postavenym objektem -- u nej se neprovadi kontrola na prispevky
												case
													when w1.upper_object__etl_id is null
													then FALSE -- nadrizeny objekt objektu jeste nebyl etl-kovan
													else TRUE -- nadrizeny objekt objektu uz byl etl-kovan
												end
										else -- nadrizeny objekt u daneho objektu neni nejvyse postavenym objektem
												case
													when w1.upper_object__etl_id is null
													then FALSE
													else (select * from analytical.fn_etl_check_ldsities4object($1,w1.upper_object__id))
												end
									end
							end
								as check_upper_object
					from
							w1
					)
			select
					w2.ldsity_object,
					w2.label,
					w2.description,
					w2.label_en,
					w2.description_en,
					w2.table_name,
					w2.upper_object__id,
					w2.upper_object__etl_id,
					w2.areal_or_population__id,
					w2.areal_or_population__label,
					w2.areal_or_population__etl_id,
					w2.column4upper_object,
					w2.filter,
					w2.etl_ldsity_object__id,
					w2.etl_ldsity_object__etl_id,
					w2.check_ldsities,
					w2.check_upper_object
			from
					w2 where '|| _cond_2 ||'
			order
					by w2.ldsity_object
			'
			using _export_connection, _ldsity_object;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) is
'Function returns information of ldsity objects.';

alter function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsity_objects(integer, boolean, boolean, integer) to public;

