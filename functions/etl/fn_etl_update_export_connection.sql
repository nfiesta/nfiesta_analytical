--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_update_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_update_export_connection(integer, text);

CREATE OR REPLACE FUNCTION analytical.fn_etl_update_export_connection
(
	_id integer,
	_comment text
)
RETURNS void
AS
$$
BEGIN

	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_update_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT t1.* FROM analytical.t_export_connection as t1 where t1.id = _id)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_update_export_connection: Given export connection (id = %) does not exist in t_export_connection table!', _id;
	END IF;

	if _comment is null
	then
		RAISE EXCEPTION 'Error 03: fn_etl_update_export_connection: Input argument _comment must not be NULL!';
	end if;

	update analytical.t_export_connection set comment = _comment where id = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_update_export_connection(integer, text) IS
'Function provides update in t_export_connection table.';

alter function analytical.fn_etl_update_export_connection(integer, text) owner to adm_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to adm_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to data_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to usr_analytical;
grant execute on function analytical.fn_etl_update_export_connection(integer, text) to public;