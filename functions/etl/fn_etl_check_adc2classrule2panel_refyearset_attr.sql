--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_adc2classrule2panel_refyearset_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) CASCADE;

create or replace function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[]
)
returns boolean
as
$$
declare
		_res	boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_adc2classrule2panel_refyearset_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_check_adc2classrule2panel_refyearset_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_check_adc2classrule2panel_refyearset_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_check_adc2classrule2panel_refyearset_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is null
		then
			raise exception 'Error 05: fn_etl_check_adc2classrule2panel_refyearset_attr: Input argument _area_domain_category must not be NULL!';
		end if;	
	
		for i in 1..array_length(_area_domain_category,1)
		loop
			if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
			then
				raise exception 'Error 06: fn_etl_check_adc2classrule2panel_refyearset_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
			end if;
		end loop;		
	
		if	(
			select count(*) > 0
			from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(_export_connection,_ldsity_object,_area_domain_category,false)
			)
		then
			_res := false;
		else
			_res := true;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) is
'Function checks that all combinations of mapping between area domain classification rules and combinations of panels and reference year sets for given input arguments was ETLed (TRUE) or not (FALSE).';

alter function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) owner to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to adm_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to data_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to usr_analytical;
grant execute on function analytical.fn_etl_check_adc2classrule2panel_refyearset_attr(integer, integer, integer[]) to public;

