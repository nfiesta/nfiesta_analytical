--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_spc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_spc2classrule2panel_refyearset
(
	_spc2classrule2panel_refyearset		integer,
	_etl_id								integer,
	_etl_spc2classification_rule		integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _spc2classrule2panel_refyearset is null
	then
		raise exception 'Error 01: fn_etl_save_spc2classrule2panel_refyearset: Input argument _spc2classrule2panel_refyearset must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_spc2classrule2panel_refyearset: Input argument _etl_id must not be NULL!';
	end if; 

	if _etl_spc2classification_rule is null
	then
		raise exception 'Error 03: fn_etl_save_spc2classrule2panel_refyearset: Input argument _etl_spc2classification_rule must not be NULL!';
	end if;

	insert into analytical.t_etl_spc2classrule2panel_refyearset(spc2classrule2panel_refyearset, etl_id, etl_spc2classification_rule)
	select _spc2classrule2panel_refyearset, _etl_id, _etl_spc2classification_rule
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) is
'Function inserts a record into table t_etl_spc2classrule2panel_refyearset based on given parameters.';

alter function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_spc2classrule2panel_refyearset(integer, integer, integer) to public;