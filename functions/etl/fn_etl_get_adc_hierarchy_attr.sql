--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc_hierarchy_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc_hierarchy_attr
(
	_export_connection		integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id													integer,
	variable_superior									integer,
	variable											integer,
	dependent											boolean,
	etl_area_domain_category__id__variable_superior		integer,
	etl_area_domain_category__etl_id__variable_superior	integer,
	etl_area_domain_category__id__variable				integer,
	etl_area_domain_category__etl_id__variable			integer,
	etl_adc_hierarchy__id								integer,
	etl_adc_hierarchy__etl_id							integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc_hierarchy_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_adc_hierarchy_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _area_domain_category is not null
		then	
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 03: fn_etl_get_adc_hierarchy_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_adc_hierarchy_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;	
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w4.etl_adc_hierarchy__etl_id is not null';
			else
				_cond := 'w4.etl_adc_hierarchy__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,null::integer[],true) where variable_superior = false)	-- variable
			,w2 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,null::integer[],true) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tah.id as adc_hierarchy,
							tah.variable_superior,
							tah.variable,
							tah.dependent,
							w2.etl_area_domain_category__id as etl_area_domain_category__id__variable_superior,
							w2.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable_superior,
							w1.etl_area_domain_category__id as etl_area_domain_category__id__variable,
							w1.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable
					from
							analytical.t_adc_hierarchy as tah
							inner join	w1 on tah.variable = w1.id
							inner join	w2 on tah.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.adc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_area_domain_category__id__variable_superior,
							w3.etl_area_domain_category__etl_id__variable_superior,
							w3.etl_area_domain_category__id__variable,
							w3.etl_area_domain_category__etl_id__variable,
							teah.id as etl_adc_hierarchy__id,
							teah.etl_id as etl_adc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_adc_hierarchy as teah
								on w3.adc_hierarchy = teah.adc_hierarchy
								and w3.etl_area_domain_category__id__variable_superior = teah.etl_area_domain_category_variable_superior
								and w3.etl_area_domain_category__id__variable = teah.etl_area_domain_category_variable
					)
			select
					w4.adc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_area_domain_category__id__variable_superior,
					w4.etl_area_domain_category__etl_id__variable_superior,
					w4.etl_area_domain_category__id__variable,
					w4.etl_area_domain_category__etl_id__variable,
					w4.etl_adc_hierarchy__id,
					w4.etl_adc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.adc_hierarchy;
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,$2,null::boolean) where variable_superior = false)	-- variable
			,w2 as	(select * from analytical.fn_etl_get_area_domain_category_attr($1,$2,null::boolean) where variable_superior = true)		-- variable superior
			,w3 as	(
					select
							tah.id as adc_hierarchy,
							tah.variable_superior,
							tah.variable,
							tah.dependent,
							w2.etl_area_domain_category__id as etl_area_domain_category__id__variable_superior,
							w2.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable_superior,
							w1.etl_area_domain_category__id as etl_area_domain_category__id__variable,
							w1.etl_area_domain_category__etl_id as etl_area_domain_category__etl_id__variable
					from
							analytical.t_adc_hierarchy as tah
							inner join	w1 on tah.variable = w1.id
							inner join	w2 on tah.variable_superior = w2.id
					)
			,w4 as	(
					select
							w3.adc_hierarchy,
							w3.variable_superior,
							w3.variable,
							w3.dependent,
							w3.etl_area_domain_category__id__variable_superior,
							w3.etl_area_domain_category__etl_id__variable_superior,
							w3.etl_area_domain_category__id__variable,
							w3.etl_area_domain_category__etl_id__variable,
							teah.id as etl_adc_hierarchy__id,
							teah.etl_id as etl_adc_hierarchy__etl_id
					from
							w3
					left join	analytical.t_etl_adc_hierarchy as teah
								on w3.adc_hierarchy = teah.adc_hierarchy
								and w3.etl_area_domain_category__id__variable_superior = teah.etl_area_domain_category_variable_superior
								and w3.etl_area_domain_category__id__variable = teah.etl_area_domain_category_variable
					)
			select
					w4.adc_hierarchy as id,
					w4.variable_superior,
					w4.variable,
					w4.dependent,
					w4.etl_area_domain_category__id__variable_superior,
					w4.etl_area_domain_category__etl_id__variable_superior,
					w4.etl_area_domain_category__id__variable,
					w4.etl_area_domain_category__etl_id__variable,
					w4.etl_adc_hierarchy__id,
					w4.etl_adc_hierarchy__etl_id
			from
					w4 where '|| _cond ||'
			order
					by w4.adc_hierarchy;
			'
			using _export_connection, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories hierarchy for attributes.';

alter function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc_hierarchy_attr(integer, integer[], boolean) to public;

