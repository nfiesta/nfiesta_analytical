--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_ids_of_ldsities
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_etl_ids_of_ldsities(integer) CASCADE;

create or replace function analytical.fn_etl_get_etl_ids_of_ldsities(_export_connection integer)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_etl_ids_of_ldsities: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_etl_ids_of_ldsities: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		select array_agg(tel.etl_id order by tel.etl_id)
		from analytical.t_etl_ldsity as tel
		where tel.etl_ldsity_object in
			(
			select telo.id from analytical.t_etl_ldsity_object as telo
			where telo.export_connection = _export_connection
			)
		into _res;
	
		return _res;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) is
'Function returns ETL_IDs of ldsities that were ETL yet for given export connection.';

alter function analytical.fn_etl_get_etl_ids_of_ldsities(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to data_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_etl_ids_of_ldsities(integer) to public;

