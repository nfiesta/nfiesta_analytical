--------------------------------------------------------------------------------
-- fn_etl_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_delete_sub_population(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_delete_sub_population(_id integer)
RETURNS void
AS
$$
BEGIN
	IF NOT EXISTS(SELECT * FROM analytical.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (id = %).', _id;
	END IF;

	-- delete ETL of some sub_population is possible only when it is not a part of dependent hierarchy
	IF EXISTS (	SELECT * 
			FROM analytical.t_spc_hierarchy AS t1
			WHERE 	variable_superior IN (	SELECT id FROM analytical.c_sub_population_category
							WHERE sub_population = _id) AND
				dependent = true
		)
	THEN 
		RAISE WARNING 'Given sub population have some categories present in table t_spc_hierarchy in superior position. It can destroy the dependency between the categories. This will not allow You to delete the sub population until you delete the inferior categories (sub populations) first.';
	END IF;

	DELETE FROM analytical.t_etl_spc2classrule2panel_refyearset 
	WHERE spc2classrule2panel_refyearset IN 
		(SELECT id FROM analytical.cm_spc2classrule2panel_refyearset
		WHERE spc2classification_rule IN (
			SELECT id FROM analytical.cm_spc2classification_rule
			WHERE sub_population_category IN
				(SELECT id FROM analytical.c_sub_population_category 
					WHERE sub_population = _id)
						)
		); 

	DELETE FROM analytical.t_etl_spc2classification_rule
	WHERE spc2classification_rule IN
		(SELECT id FROM analytical.cm_spc2classification_rule 
		WHERE sub_population_category IN 
			(SELECT id FROM analytical.c_sub_population_category 
			WHERE sub_population = _id)
		);

	DELETE FROM analytical.t_etl_spc_hierarchy
	WHERE spc_hierarchy IN 
		(SELECT id FROM analytical.t_spc_hierarchy
		WHERE	variable IN (	SELECT id FROM analytical.c_sub_population_category
					WHERE sub_population = _id) OR
			variable_superior IN (	SELECT id FROM analytical.c_sub_population_category
						WHERE sub_population = _id)
		);

	DELETE FROM analytical.t_etl_sub_population_category
	WHERE sub_population_category IN 
		(SELECT id FROM analytical.c_sub_population_category 
		WHERE sub_population = _id);
	
	DELETE FROM analytical.t_etl_sub_population
	WHERE sub_population IN 
		(SELECT id FROM analytical.c_sub_population 
		WHERE id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_delete_sub_population(integer) IS
'Function deletes records from c_sub_population, c_sub_population_category, cm_spc2classification_rule and cm_spc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION analytical.fn_etl_delete_sub_population(integer) TO public;
