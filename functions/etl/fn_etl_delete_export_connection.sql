--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_delete_export_connection(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_export_connection: Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM analytical.t_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_export_connection: Given export connection (%) does not exist in t_export_connection table.', _id;
	END IF;
	
	DELETE FROM analytical.t_export_connection 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_delete_export_connection(integer) IS
'The function deletes a record from t_export_connection table.';

alter function analytical.fn_etl_delete_export_connection(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to adm_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to data_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to usr_analytical;
grant execute on function analytical.fn_etl_delete_export_connection(integer) to public;