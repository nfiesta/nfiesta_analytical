--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_category(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_category
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id									integer,
	label								varchar,
	description							text,
	label_en							varchar,
	description_en						text,
	etl_sub_population__id				integer,
	etl_sub_population__etl_id			integer,
	etl_sub_population_category__id		integer,
	etl_sub_population_category__etl_id	integer,
	variable_superior					boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;		
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w8.etl_sub_population_category__etl_id is not null';
			else
				_cond := 'w8.etl_sub_population_category__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with 
		w1 as	(select * from analytical.fn_etl_get_sub_population_informations($1,$2))
		,w2 as 	(select distinct sub_population_category, etl_sub_population__id, etl_sub_population_category__id, etl_sub_population_category__etl_id from w1)
		,w3 as	(
				select
						w2.sub_population_category as id,
						cspc.label,
						cspc.description,
						cspc.label_en,
						cspc.description_en,
						w2.etl_sub_population__id as etl_sub_population,
						w2.etl_sub_population_category__id,
						w2.etl_sub_population_category__etl_id
				from
						w2 inner join analytical.c_sub_population_category as cspc on w2.sub_population_category = cspc.id
				)
		-----------------------------------------
		-- add CATEGORY from hierarchy
		,w4 as	(
				select
						id,
						label,
						description,
						label_en,
						description_en,
						sub_population
				from analytical.c_sub_population_category where sub_population in
				(select distinct sub_population from analytical.c_sub_population_category where id in
				(select distinct variable_superior from analytical.t_spc_hierarchy
				where variable in (select distinct w3.id from w3)))
				)
		,w5 as	(
				select w4.*, t.id as etl_sub_population from w4
				left join
				(select * from analytical.t_etl_sub_population where export_connection = $1 and sub_population in (select w4.sub_population from w4)) as t
				on w4.sub_population = t.sub_population
				)

		,w6 as	(
				select
						w5.id,
						w5.label,
						w5.description,
						w5.label_en,
						w5.description_en,
						w5.etl_sub_population,
						t.id as etl_sub_population_category__id,
						t.etl_id as etl_sub_population_category__etl_id
				from w5
				left join	(select * from analytical.t_etl_sub_population_category where etl_sub_population in (select w5.etl_sub_population from w5)) as t
							on w5.etl_sub_population = t.etl_sub_population
							and w5.id = t.sub_population_category
				)
		-----------------------------------------
		,w7 as	(
				select
						w3.id,
						w3.label,
						w3.description,
						w3.label_en,
						w3.description_en,
						w3.etl_sub_population,
						w3.etl_sub_population_category__id,
						w3.etl_sub_population_category__etl_id,
						false as variable_superior
				from
						w3 where w3.id not in (select w6.id from w6)
				union all
				select
						w6.id,
						w6.label,
						w6.description,
						w6.label_en,
						w6.description_en,
						w6.etl_sub_population,
						w6.etl_sub_population_category__id,
						w6.etl_sub_population_category__etl_id,
						true as variable_superior
				from
						w6
				)
		-----------------------------------------
		-- add etl_id from t_etl_sub_population
		,w8 as	(
				select
						w7.id,
						w7.label,
						w7.description,
						w7.label_en,
						w7.description_en,
						w7.etl_sub_population as etl_sub_population__id,
						tt.etl_id as etl_sub_population__etl_id,
						w7.etl_sub_population_category__id,
						w7.etl_sub_population_category__etl_id,
						w7.variable_superior
				from
						w7
						left join
									(
									select tesp.* from analytical.t_etl_sub_population as tesp
									where tesp.export_connection = $1
									and tesp.id in (select w7.etl_sub_population from w7 where w7.etl_sub_population is not null)
									) as tt
						on w7.etl_sub_population = tt.id
				)
		-----------------------------------------
		select
				w8.id,
				w8.label,
				w8.description,
				w8.label_en,
				w8.description_en,
				w8.etl_sub_population__id,
				w8.etl_sub_population__etl_id,
				w8.etl_sub_population_category__id,
				w8.etl_sub_population_category__etl_id,	
				w8.variable_superior
		from
				w8 where '|| _cond ||'
		order
				by w8.id
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) is
'Function returns information records of sub population categories.';

alter function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_category(integer, integer, boolean) to public;

