--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sp_attributes4ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_sp_attributes4ldsity_object
(
	_export_connection	integer,
	_ldsity_object		integer,
	_sub_population		integer[] default null::integer[],
	_check_attr			boolean default null::boolean		
)
returns table
(
	group_id									integer,
	lo__id										integer,
	lo__label									varchar,
	lo__description								text,
	lo__label_en								varchar,
	lo__description_en							text,
	aop__id										integer,
	aop__label									varchar,
	aop__description							text,
	sp__id										integer,
	sp__label									varchar,
	sp__description								text,
	sp__label_en								varchar,
	sp__description_en							text,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	spc__id										integer[],
	spc__label									varchar[],
	spc__description							text[],
	spc__label_en								varchar[],
	spc__description_en							text[],
	rule__id									integer[],
	rule__sub_population_category				integer[],
	rule__classification_rule					text[],
	check__sub_population						boolean,
	check__sub_population_category				boolean,
	check__spc_hierarchy						boolean,
	check__spc2classification_rule				boolean,
	check__spc2classrule2panel_refyearset		boolean,
	check__attr									boolean
)
as
$$
declare
		_cond_1	text;
		_cond_2	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sp_attributes4ldsity_object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_sp_attributes4ldsity_object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sp_attributes4ldsity_object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_sp_attributes4ldsity_object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if _sub_population is not null
		then
			for i in 1..array_length(_sub_population,1)
			loop
				if not exists (select t3.* from analytical.c_sub_population as t3 where t3.id = _sub_population[i])
				then
					raise exception 'Error 05: fn_etl_get_sp_attributes4ldsity_object: Given sub population (%) does not exist in c_sub_population table.', _sub_population[i];
				end if;
			end loop;
		end if;
	
		if _sub_population is not null and _check_attr is distinct from false
		then
			raise exception 'Error 06: fn_etl_get_sp_attributes4ldsity_object: If input argument _sub_population is not null then input argument _check_attr must be FALSE!';
		end if;		
		
		if _sub_population is not null
		then
			_cond_1 := 'w4.sp__id in (select unnest($3))';
		else
			_cond_1 := 'TRUE';
		end if;	
	
		if _check_attr is null
		then
			_cond_2 := 'TRUE';
		else
			if _check_attr = true
			then
				_cond_2 := 'w4.check__attr = true';
			else
				_cond_2 := 'w4.check__attr = false';
			end if;
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select * from analytical.fn_etl_get_sub_population_informations_attr($1,$2)
				)
		,w2 as	(
				select		
						w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.sp__id,
						w1.sp__label,
						w1.sp__description,
						w1.sp__label_en,
						w1.sp__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id,
						array_agg(w1.spc__id order by w1.rule__id) as spc__id,
						array_agg(w1.spc__label order by w1.rule__id) as spc__label,
						array_agg(w1.spc__description order by w1.rule__id) as spc__description,
						array_agg(w1.spc__label_en order by w1.rule__id) as spc__label_en,
						array_agg(w1.spc__description_en order by w1.rule__id) as spc__description_en,
						array_agg(w1.rule__id order by w1.rule__id) as rule__id,
						array_agg(w1.rule__sub_population_category order by w1.rule__id) as rule__sub_population_category,
						array_agg(w1.rule__classification_rule order by w1.rule__id) as rule__classification_rule
				from
						w1
				group
				by		w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.sp__id,
						w1.sp__label,
						w1.sp__description,
						w1.sp__label_en,
						w1.sp__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id
				)
		,w3 as	(
				select
						row_number() over() as group_id,
						w2.*,
						analytical.fn_etl_check_sub_population_attr($1,w2.spc__id)								as check__sub_population,
						analytical.fn_etl_check_sub_population_category_attr($1,w2.spc__id)						as check__sub_population_category,
						analytical.fn_etl_check_spc_hierarchy_attr($1,w2.spc__id)								as check__spc_hierarchy,
						analytical.fn_etl_check_spc2classification_rule_attr($1,w2.lo__id,w2.spc__id)			as check__spc2classification_rule,
						analytical.fn_etl_check_spc2classrule2panel_refyearset_attr($1,w2.lo__id,w2.spc__id)	as check__spc2classrule2panel_refyearset
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						case
							when
								w3.check__sub_population = true and
								w3.check__sub_population_category = true and
								w3.check__spc_hierarchy = true and
								w3.check__spc2classification_rule = true and
								w3.check__spc2classrule2panel_refyearset = true
							then
								true
							else
								false
						end as check__attr
							
				from
						w3
				)
		select
				w4.group_id::integer,
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.sp__id,
				w4.sp__label,
				w4.sp__description,
				w4.sp__label_en,
				w4.sp__description_en,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.spc__id,
				w4.spc__label,
				w4.spc__description,
				w4.spc__label_en,
				w4.spc__description_en,
				w4.rule__id,
				w4.rule__sub_population_category,
				w4.rule__classification_rule,
				w4.check__sub_population,
				w4.check__sub_population_category,
				w4.check__spc_hierarchy,
				w4.check__spc2classification_rule,
				w4.check__spc2classrule2panel_refyearset,
				w4.check__attr				
		from
				w4
		where
				'|| _cond_1 ||'
		and
				'|| _cond_2 ||'
		order
				by w4.group_id;
		'
		using _export_connection, _ldsity_object, _sub_population;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) is
'Function returns information of sub population attributes for given ldsity object.';

alter function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_sp_attributes4ldsity_object(integer, integer, integer[], boolean) to public;

