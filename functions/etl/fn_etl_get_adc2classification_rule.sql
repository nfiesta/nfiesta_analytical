--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classification_rule
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id														integer,
	classification_rule										text,
	etl_area_domain_category__id							integer,
	etl_area_domain_category__etl_id						integer,
	etl_ldsity_object__id									integer,
	etl_ldsity_object__etl_id								integer,
	etl_adc2classification_rule__id							integer,
	etl_adc2classification_rule__etl_id						integer,
	cm_adc2classrule2panel_refyearset__id					integer,
	cm_adc2classrule2panel_refyearset__refyearset2panel		integer
)
as
$$
declare
		_cond_1		text;
		_cond_2		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classification_rule: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_adc2classification_rule: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_adc2classification_rule: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_adc2classification_rule: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;		
	
		if _etl is null
		then
			_cond_1 := 'TRUE';
			_cond_2 := 'TRUE';
		else
			if _etl = true
			then
				_cond_1 := 'w8.etl_adc2classification_rule__etl_id is not null';
				_cond_2 := 'w10.etl_adc2classification_rule__etl_id is not null';
			else
				_cond_1 := 'w8.etl_adc2classification_rule__etl_id is null';
				_cond_2 := 'w10.etl_adc2classification_rule__etl_id is null';
			end if;
		end if;
	
		return query execute
		'
		with 
		w1 as	(select * from analytical.fn_etl_get_area_domain_informations($1,$2))
		,w2 as 	(select distinct adc2classification_rule, etl_area_domain_category__id, etl_area_domain_category__etl_id, etl_ldsity_object__id, etl_ldsity_object__etl_id, etl_adc2classification_rule__id, etl_adc2classification_rule__etl_id from w1)
		-----------------------------------------------
		,w3 as	(
				select
						w2.etl_area_domain_category__id,
						w2.etl_ldsity_object__id,
						teadc.area_domain_category,
						telo.ldsity_object
				from	w2
						inner join analytical.t_etl_area_domain_category as teadc on w2.etl_area_domain_category__id = teadc.id
						inner join analytical.t_etl_ldsity_object as telo on w2.etl_ldsity_object__id = telo.id
				)
		,w4 as	(		
				select distinct t1.ldsity_object, t4.id from
				(select w3.area_domain_category, w3.ldsity_object from w3) as t1
				inner join analytical.t_adc_hierarchy as t2 on t1.area_domain_category = t2.variable
				inner join analytical.c_area_domain_category as t3 on t2.variable_superior = t3.id
				inner join analytical.c_area_domain_category as t4 on t3.area_domain = t4.area_domain 
				)
		,w5 as	(
				select
						cacr.id as adc2classification_rule,
						cacr.classification_rule,
						cacr.area_domain_category,
						cacr.ldsity_object
				from w4
				inner join analytical.cm_adc2classification_rule as cacr 
				on w4.id = cacr.area_domain_category and w4.ldsity_object = cacr.ldsity_object
				)
		,w6 as	(
				select					
						w5.ldsity_object,
						cadc.area_domain,
						w5.area_domain_category,
						w5.adc2classification_rule,
						w5.classification_rule,
						telo.id as etl_ldsity_object__id,
						telo.etl_id as etl_ldsity_object__etl_id,
						tead.id as etl_area_domain__id,
						tead.etl_id as etl_area_domain__etl_id
				from
						w5
						
						inner join	analytical.c_area_domain_category as cadc on w5.area_domain_category = cadc.id
						inner join	(select * from analytical.t_etl_ldsity_object where export_connection = $1) as telo
									on w5.ldsity_object = telo.ldsity_object
						inner join	(select * from analytical.t_etl_area_domain where export_connection = $1) as tead
									on cadc.area_domain = tead.area_domain
				)
		,w7 as	(
				select
						w6.*,
						teadc.id as etl_area_domain_category__id,
						teadc.etl_id as etl_area_domain_category__etl_id
				from
						w6
						inner join	(
									select * from analytical.t_etl_area_domain_category
									where etl_area_domain in (select etl_area_domain__id from w6)
									) as teadc
									on w6.area_domain_category = teadc.area_domain_category
				)
		,w8 as	(
				select
						w7.adc2classification_rule as id,
						w7.classification_rule,
						w7.etl_area_domain_category__id,
						w7.etl_area_domain_category__etl_id,
						w7.etl_ldsity_object__id,
						w7.etl_ldsity_object__etl_id,
						teacr.id as etl_adc2classification_rule__id,
						teacr.etl_id as etl_adc2classification_rule__etl_id,
						analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w7.adc2classification_rule) as res
				from 	w7
						left join analytical.t_etl_adc2classification_rule as teacr
						on w7.adc2classification_rule = teacr.adc2classification_rule
						and w7.etl_area_domain_category__id = teacr.etl_area_domain_category
						and w7.etl_ldsity_object__id = teacr.etl_ldsity_object
				)
		-----------------------------------------------
		,w9 as	(
				select
						w2.adc2classification_rule as id,
						cmadc.classification_rule,
						w2.etl_area_domain_category__id,
						w2.etl_area_domain_category__etl_id,
						w2.etl_ldsity_object__id,
						w2.etl_ldsity_object__etl_id,
						w2.etl_adc2classification_rule__id,
						w2.etl_adc2classification_rule__etl_id
				from
						w2 inner join analytical.cm_adc2classification_rule as cmadc on w2.adc2classification_rule = cmadc.id
				)
		,w10 as	(
				select
						w9.id,
						w9.classification_rule,
						w9.etl_area_domain_category__id,
						w9.etl_area_domain_category__etl_id,
						w9.etl_ldsity_object__id,
						w9.etl_ldsity_object__etl_id,
						w9.etl_adc2classification_rule__id,
						w9.etl_adc2classification_rule__etl_id,
						analytical.fn_etl_get_refyearset2panel4adc2classification_rule(w9.id) as res
				from
						w9
				order
						by w9.id
				)
		-----------------------------------------------
		select
				w8.id,
				w8.classification_rule,
				w8.etl_area_domain_category__id,
				w8.etl_area_domain_category__etl_id,
				w8.etl_ldsity_object__id,
				w8.etl_ldsity_object__etl_id,
				w8.etl_adc2classification_rule__id,
				w8.etl_adc2classification_rule__etl_id,
				(w8.res).cm_adc2classrule2panel_refyearset__id,
				(w8.res).cm_adc2classrule2panel_refyearset__refyearset2panel
		from
				w8 where '|| _cond_1 ||'
		union
		select
				w10.id,
				w10.classification_rule,
				w10.etl_area_domain_category__id,
				w10.etl_area_domain_category__etl_id,
				w10.etl_ldsity_object__id,
				w10.etl_ldsity_object__etl_id,
				w10.etl_adc2classification_rule__id,
				w10.etl_adc2classification_rule__etl_id,
				(w10.res).cm_adc2classrule2panel_refyearset__id,
				(w10.res).cm_adc2classrule2panel_refyearset__refyearset2panel
		from
				w10 where '|| _cond_2 ||'
		order
				by id;
		'
		using _export_connection, _ldsity;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) is
'Function returns information records of area domain categories and rules.';

alter function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classification_rule(integer, integer, boolean) to public;

