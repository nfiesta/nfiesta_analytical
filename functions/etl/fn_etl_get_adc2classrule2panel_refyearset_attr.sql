--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_adc2classrule2panel_refyearset_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr
(
	_export_connection		integer,
	_ldsity_object			integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id											integer,
	classification_rule							text,
	etl_area_domain_category__id				integer,
	etl_area_domain_category__etl_id			integer,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	etl_adc2classification_rule__id				integer,
	etl_adc2classification_rule__etl_id			integer,
	adc2classrule2panel_refyearset				integer,
	refyearset2panel							integer,
	etl_adc2classrule2panel_refyearset__id		integer,
	etl_adc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_adc2classrule2panel_refyearset_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_adc2classrule2panel_refyearset_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_etl_get_adc2classrule2panel_refyearset_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_adc2classrule2panel_refyearset_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;	
	
		if _area_domain_category is not null
		then	
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 05: fn_etl_get_adc2classrule2panel_refyearset_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 06: fn_etl_get_adc2classrule2panel_refyearset_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;		
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_adc2classrule2panel_refyearset__etl_id is not null';
			else
				_cond := 'w3.etl_adc2classrule2panel_refyearset__etl_id is null';
			end if;
		end if;

		if _area_domain_category is null
		then
			return query execute
			'	
			with
			w1 as	(select * from analytical.fn_etl_get_adc2classification_rule_attr($1,$2,null::integer[],true))
			,w2 as	(
					select
							w1.id,
							w1.classification_rule,
							w1.etl_area_domain_category__id,
							w1.etl_area_domain_category__etl_id,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
							w1.etl_adc2classification_rule__id,
							w1.etl_adc2classification_rule__etl_id,
							t.id as adc2classrule2panel_refyearset,
							t.refyearset2panel
					from	w1
					inner join	analytical.cm_adc2classrule2panel_refyearset as t
								on w1.id = t.adc2classification_rule
					)
			,w3 as	(
					select
							w2.*,
							teacpr.id as etl_adc2classrule2panel_refyearset__id,
							teacpr.etl_id as etl_adc2classrule2panel_refyearset__etl_id
					from
							w2
					left join	analytical.t_etl_adc2classrule2panel_refyearset as teacpr
								on w2.adc2classrule2panel_refyearset = teacpr.adc2classrule2panel_refyearset
								and w2.etl_adc2classification_rule__id = teacpr.etl_adc2classification_rule
					)
			select
					distinct
					w3.id,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					w3.adc2classrule2panel_refyearset,
					w3.refyearset2panel,
					w3.etl_adc2classrule2panel_refyearset__id,
					w3.etl_adc2classrule2panel_refyearset__etl_id
			from
					w3 where '|| _cond ||'
			order
					by w3.id, w3.refyearset2panel;
			'
			using _export_connection, _ldsity_object;
		else
			return query execute
			'
			with
			w1 as	(select * from analytical.fn_etl_get_adc2classification_rule_attr($1,$2,$3,null::boolean))
			,w2 as	(
					select
							w1.id,
							w1.classification_rule,
							w1.etl_area_domain_category__id,
							w1.etl_area_domain_category__etl_id,
							w1.etl_ldsity_object__id,
							w1.etl_ldsity_object__etl_id,
							w1.etl_adc2classification_rule__id,
							w1.etl_adc2classification_rule__etl_id,
							t.id as adc2classrule2panel_refyearset,
							t.refyearset2panel
					from	w1
					inner join	analytical.cm_adc2classrule2panel_refyearset as t
								on w1.id = t.adc2classification_rule
					)
			,w3 as	(
					select
							w2.*,
							teacpr.id as etl_adc2classrule2panel_refyearset__id,
							teacpr.etl_id as etl_adc2classrule2panel_refyearset__etl_id
					from
							w2
					left join	analytical.t_etl_adc2classrule2panel_refyearset as teacpr
								on w2.adc2classrule2panel_refyearset = teacpr.adc2classrule2panel_refyearset
								and w2.etl_adc2classification_rule__id = teacpr.etl_adc2classification_rule
					)
			select
					w3.id,
					w3.classification_rule,
					w3.etl_area_domain_category__id,
					w3.etl_area_domain_category__etl_id,
					w3.etl_ldsity_object__id,
					w3.etl_ldsity_object__etl_id,
					w3.etl_adc2classification_rule__id,
					w3.etl_adc2classification_rule__etl_id,
					w3.adc2classrule2panel_refyearset,
					w3.refyearset2panel,
					w3.etl_adc2classrule2panel_refyearset__id,
					w3.etl_adc2classrule2panel_refyearset__etl_id
			from
					w3 where '|| _cond ||'
			order
					by w3.id, w3.refyearset2panel;
			'
			using _export_connection, _ldsity_object, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) is
'Function returns information records of mapping between area domain classification rules and combinations of panels and reference year sets for attributes.';

alter function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer,integer[],  boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(integer, integer,integer[],  boolean) to public;

