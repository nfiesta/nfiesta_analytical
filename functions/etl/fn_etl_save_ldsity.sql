--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_ldsity(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_ldsity
(
	_ldsity				integer,
	_etl_id				integer,
	_etl_ldsity_object	integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _ldsity is null
	then
		raise exception 'Error 01: fn_etl_save_ldsity: Input argument _ldsity must not by NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_ldsity: Input argument _etl_id must not by NULL!';
	end if; 

	if _etl_ldsity_object is null
	then
		raise exception 'Error 03: fn_etl_save_ldsity: Input argument _etl_ldsity_object must not by NULL!';
	end if;

	insert into analytical.t_etl_ldsity(ldsity, etl_id, etl_ldsity_object)
	select _ldsity, _etl_id, _etl_ldsity_object
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_ldsity(integer, integer, integer) is
'Function inserts a record into table t_etl_ldsity based on given parameters.';

alter function analytical.fn_etl_save_ldsity(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_ldsity(integer, integer, integer) to public;