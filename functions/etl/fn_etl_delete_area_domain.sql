--------------------------------------------------------------------------------
-- fn_etl_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION analytical.fn_etl_delete_area_domain(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN
	IF NOT EXISTS(SELECT * FROM analytical.c_area_domain WHERE id = _id)
	THEN
		RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (id = %).', _id;
	END IF;

	-- delete ETL of some area_domain is possible only when it is not a part of dependent hierarchy
	IF EXISTS (	SELECT * 
			FROM analytical.t_adc_hierarchy AS t1
			WHERE 	variable_superior IN (	SELECT id FROM analytical.c_area_domain_category
							WHERE area_domain = _id) AND
				dependent = true
		)
	THEN 
		RAISE WARNING 'Given area domain have some categories present in table t_adc_hierarchy in superior position. It can destroy the dependency between the categories. This will not allow You to delete the area domain until you delete the inferior categories (area domains) first.';
	END IF;

	DELETE FROM analytical.t_etl_adc2classrule2panel_refyearset 
	WHERE adc2classrule2panel_refyearset IN 
		(SELECT id FROM analytical.cm_adc2classrule2panel_refyearset
		WHERE adc2classification_rule IN (
			SELECT id FROM analytical.cm_adc2classification_rule
			WHERE area_domain_category IN
				(SELECT id FROM analytical.c_area_domain_category 
					WHERE area_domain = _id)
						)
		); 

	DELETE FROM analytical.t_etl_adc2classification_rule
	WHERE adc2classification_rule IN
		(SELECT id FROM analytical.cm_adc2classification_rule 
		WHERE area_domain_category IN 
			(SELECT id FROM analytical.c_area_domain_category 
			WHERE area_domain = _id)
		);

	DELETE FROM analytical.t_etl_adc_hierarchy
	WHERE adc_hierarchy IN 
		(SELECT id FROM analytical.t_adc_hierarchy
		WHERE	variable IN (	SELECT id FROM analytical.c_area_domain_category
					WHERE area_domain = _id) OR
			variable_superior IN (	SELECT id FROM analytical.c_area_domain_category
						WHERE area_domain = _id)
		);

	DELETE FROM analytical.t_etl_area_domain_category
	WHERE area_domain_category IN 
		(SELECT id FROM analytical.c_area_domain_category 
		WHERE area_domain = _id);
	
	DELETE FROM analytical.t_etl_area_domain
	WHERE area_domain IN 
		(SELECT id FROM analytical.c_area_domain 
		WHERE id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION analytical.fn_etl_delete_area_domain(integer) TO public;
