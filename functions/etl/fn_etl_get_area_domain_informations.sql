--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_informations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_informations(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_informations
(
	_export_connection	integer,
	_ldsity				integer
)
returns table
(
	ldsity_object							integer,
	area_domain								integer,
	area_domain_category					integer,
	adc2classification_rule					integer,
	------------------------------------------------
	etl_ldsity_object__id					integer,
	etl_ldsity_object__etl_id				integer,
	etl_area_domain__id						integer,
	etl_area_domain__etl_id					integer,
	etl_area_domain_category__id			integer,
	etl_area_domain_category__etl_id		integer,
	etl_adc2classification_rule__id			integer,
	etl_adc2classification_rule__etl_id		integer
)
as
$$
declare
		_adc_array	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_informations: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_informations: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_area_domain_informations: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_area_domain_informations: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;
	
		select cl.area_domain_category from analytical.c_ldsity as cl where cl.id = _ldsity
		into _adc_array;
	
		if _adc_array is null
		then
			raise exception 'Error 05: fn_etl_get_area_domain_informations: For input argument _ldsity = % not exists any reduction of area domain category!',_ldsity;
		else
			return query
			with
			w1 as	(						
					select
							t2.area_domain as rule__type,
							t1.rule__category,
							t1.rule__id,
							t1.rule__ldsity_object
					from
						(
						select
								cmadc2cr.id as rule__id,
								cmadc2cr.area_domain_category as rule__category,
								cmadc2cr.ldsity_object as rule__ldsity_object
						from analytical.cm_adc2classification_rule as cmadc2cr
						where cmadc2cr.id in (select unnest(_adc_array))
						) as t1
					inner join	analytical.c_area_domain_category as t2 on t1.rule__category = t2.id			
					)
			,w2 as	(
					select distinct rule__type, rule__category, rule__id, rule__ldsity_object from w1
					)
			,w3 as	(
					select
							w2.rule__type as rule__type_all,
							w2.rule__category,
							w2.rule__id,
							w2.rule__ldsity_object,
							cadc.id as rule__category_all,
							cacr.id as rule__id_all,
							telo.id as etl_ldsity_object__id,			-- id into table analytical.t_etl_ldsity_object
							telo.etl_id as etl_ldsity_object__etl_id	-- id into table target_data.c_ldsity_object
					from w2
					inner join analytical.c_area_domain_category as cadc on w2.rule__type = cadc.area_domain
					inner join analytical.cm_adc2classification_rule as cacr on w2.rule__ldsity_object = cacr.ldsity_object and cadc.id = cacr.area_domain_category
					left join (select * from analytical.t_etl_ldsity_object where export_connection = _export_connection) as telo on w2.rule__ldsity_object = telo.ldsity_object
					)
			,w4 as	(
					select
							w3.*,
							t.id as etl_area_domain__id,			-- id into table analytical.t_etl_area_domain
							t.etl_id as etl_area_domain__etl_id		-- id into table target_data.c_area_domain
					from 
							w3
					left join	(select * from analytical.t_etl_area_domain where export_connection = _export_connection) as t on w3.rule__type_all = t.area_domain
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_area_domain_category__id,			-- id into table analytical.t_etl_area_domain_category
							t.etl_id as etl_area_domain_category__etl_id	-- id into table target_data.c_area_domain_category
					from
							w4
					left join	(select * from analytical.t_etl_area_domain_category where etl_area_domain in (select w4.etl_area_domain__id from w4)) as t
								on w4.etl_area_domain__id = t.etl_area_domain
								and w4.rule__category_all = t.area_domain_category
					)					
			,w6 as	(
					select
							w5.*,
							t.id as etl_adc2classification_rule__id,
							t.etl_id as etl_adc2classification_rule__etl_id
					from
							w5
					left join	(select * from analytical.t_etl_adc2classification_rule where etl_area_domain_category in (select w5.etl_area_domain_category__id from w5)) as t
								on w5.etl_area_domain_category__id = t.etl_area_domain_category
								and w5.etl_ldsity_object__id = t.etl_ldsity_object
								and w5.rule__id_all = t.adc2classification_rule
					)				
			select
					w6.rule__ldsity_object			as ldsity_object,
					w6.rule__type_all				as area_domain,
					w6.rule__category_all			as area_domain_category,
					w6.rule__id_all					as adc2classification_rule,
					---------------------
					w6.etl_ldsity_object__id,
					w6.etl_ldsity_object__etl_id,
					w6.etl_area_domain__id,
					w6.etl_area_domain__etl_id,
					w6.etl_area_domain_category__id,
					w6.etl_area_domain_category__etl_id,
					w6.etl_adc2classification_rule__id,
					w6.etl_adc2classification_rule__etl_id
			from
					w6 order by w6.rule__id_all;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_informations(integer, integer) is
'Function returns area domain informations for given input arguments: export connection and ldsity.';

alter function analytical.fn_etl_get_area_domain_informations(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_informations(integer, integer) to public;

