--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ad_attributes4ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_ad_attributes4ldsity_object
(
	_export_connection	integer,
	_ldsity_object		integer,
	_area_domain		integer[] default null::integer[],
	_check_attr			boolean default null::boolean		
)
returns table
(
	group_id									integer,
	lo__id										integer,
	lo__label									varchar,
	lo__description								text,
	lo__label_en								varchar,
	lo__description_en							text,
	aop__id										integer,
	aop__label									varchar,
	aop__description							text,
	ad__id										integer,
	ad__label									varchar,
	ad__description								text,
	ad__label_en								varchar,
	ad__description_en							text,
	etl_ldsity_object__id						integer,
	etl_ldsity_object__etl_id					integer,
	adc__id										integer[],
	adc__label									varchar[],
	adc__description							text[],
	adc__label_en								varchar[],
	adc__description_en							text[],
	rule__id									integer[],
	rule__area_domain_category					integer[],
	rule__classification_rule					text[],
	check__area_domain							boolean,
	check__area_domain_category					boolean,
	check__adc_hierarchy						boolean,
	check__adc2classification_rule				boolean,
	check__adc2classrule2panel_refyearset		boolean,
	check__attr									boolean
)
as
$$
declare
		_cond_1	text;
		_cond_2	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ad_attributes4ldsity_object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_ad_attributes4ldsity_object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ad_attributes4ldsity_object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_ad_attributes4ldsity_object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if _area_domain is not null
		then
			for i in 1..array_length(_area_domain,1)
			loop
				if not exists (select t3.* from analytical.c_area_domain as t3 where t3.id = _area_domain[i])
				then
					raise exception 'Error 05: fn_etl_get_ad_attributes4ldsity_object: Given area domain (%) does not exist in c_area_domain table.', _area_domain[i];
				end if;
			end loop;
		end if;
	
		if _area_domain is not null and _check_attr is distinct from false
		then
			raise exception 'Error 06: fn_etl_get_ad_attributes4ldsity_object: If input argument _area_domain is not null then input argument _check_attr must be FALSE!';
		end if;		
		
		if _area_domain is not null
		then
			_cond_1 := 'w4.ad__id in (select unnest($3))';
		else
			_cond_1 := 'TRUE';
		end if;	
	
		if _check_attr is null
		then
			_cond_2 := 'TRUE';
		else
			if _check_attr = true
			then
				_cond_2 := 'w4.check__attr = true';
			else
				_cond_2 := 'w4.check__attr = false';
			end if;
		end if;
	
		return query execute
		'
		with
		w1 as	(
				select * from analytical.fn_etl_get_area_domain_informations_attr($1,$2)
				)
		,w2 as	(
				select		
						w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.ad__id,
						w1.ad__label,
						w1.ad__description,
						w1.ad__label_en,
						w1.ad__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id,
						array_agg(w1.adc__id order by w1.rule__id) as adc__id,
						array_agg(w1.adc__label order by w1.rule__id) as adc__label,
						array_agg(w1.adc__description order by w1.rule__id) as adc__description,
						array_agg(w1.adc__label_en order by w1.rule__id) as adc__label_en,
						array_agg(w1.adc__description_en order by w1.rule__id) as adc__description_en,
						array_agg(w1.rule__id order by w1.rule__id) as rule__id,
						array_agg(w1.rule__area_domain_category order by w1.rule__id) as rule__area_domain_category,
						array_agg(w1.rule__classification_rule order by w1.rule__id) as rule__classification_rule
				from
						w1
				group
				by		w1.lo__id,
						w1.lo__label,
						w1.lo__description,
						w1.lo__label_en,
						w1.lo__description_en,
						w1.aop__id,
						w1.aop__label,
						w1.aop__description,
						w1.ad__id,
						w1.ad__label,
						w1.ad__description,
						w1.ad__label_en,
						w1.ad__description_en,
						w1.etl_ldsity_object__id,
						w1.etl_ldsity_object__etl_id
				)
		,w3 as	(
				select
						row_number() over() as group_id,
						w2.*,
						analytical.fn_etl_check_area_domain_attr($1,w2.adc__id)									as check__area_domain,
						analytical.fn_etl_check_area_domain_category_attr($1,w2.adc__id)						as check__area_domain_category,
						analytical.fn_etl_check_adc_hierarchy_attr($1,w2.adc__id)								as check__adc_hierarchy,
						analytical.fn_etl_check_adc2classification_rule_attr($1,w2.lo__id,w2.adc__id)			as check__adc2classification_rule,
						analytical.fn_etl_check_adc2classrule2panel_refyearset_attr($1,w2.lo__id,w2.adc__id)	as check__adc2classrule2panel_refyearset
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						case
							when
								w3.check__area_domain = true and
								w3.check__area_domain_category = true and
								w3.check__adc_hierarchy = true and
								w3.check__adc2classification_rule = true and
								w3.check__adc2classrule2panel_refyearset = true
							then
								true
							else
								false
						end as check__attr
							
				from
						w3
				)
		select
				w4.group_id::integer,
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.ad__id,
				w4.ad__label,
				w4.ad__description,
				w4.ad__label_en,
				w4.ad__description_en,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.adc__id,
				w4.adc__label,
				w4.adc__description,
				w4.adc__label_en,
				w4.adc__description_en,
				w4.rule__id,
				w4.rule__area_domain_category,
				w4.rule__classification_rule,
				w4.check__area_domain,
				w4.check__area_domain_category,
				w4.check__adc_hierarchy,
				w4.check__adc2classification_rule,
				w4.check__adc2classrule2panel_refyearset,
				w4.check__attr				
		from
				w4
		where
				'|| _cond_1 ||'
		and
				'|| _cond_2 ||'
		order
				by w4.group_id;
		'
		using _export_connection, _ldsity_object, _area_domain;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) is
'Function returns information of area domain attributes for given ldsity object.';

alter function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ad_attributes4ldsity_object(integer, integer, integer[], boolean) to public;

