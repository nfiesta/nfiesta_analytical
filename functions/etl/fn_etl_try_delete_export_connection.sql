--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION analytical.fn_etl_try_delete_export_connection(_id integer)
RETURNS boolean
AS
$$
BEGIN
	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT * FROM analytical.t_export_connection)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_try_delete_export_connection: Given export connection does not exist in table t_export_connection (%)', _id;
	END IF;

	return not exists (
	select telo.id from analytical.t_etl_ldsity_object as telo where telo.export_connection = _id			union all
	select tedv.id from analytical.t_etl_definition_variant as tedv where tedv.export_connection = _id		union all
	select tead.id from analytical.t_etl_area_domain as tead where tead.export_connection = _id				union all
	select tesp.id from analytical.t_etl_sub_population as tesp where tesp.export_connection = _id			union all
	select teuom.id from analytical.t_etl_unit_of_measure as teuom where teuom.export_connection = _id		union all
	select tev.id from analytical.t_etl_version as tev where tev.export_connection = _id					union all
	select teaop.id from analytical.t_etl_areal_or_population as teaop where teaop.export_connection = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION analytical.fn_etl_try_delete_export_connection(integer) IS
'The function provides test if it is possible to delete records from t_export_connection table.';

alter function analytical.fn_etl_try_delete_export_connection(integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to adm_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to data_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to usr_analytical;
grant execute on function analytical.fn_etl_try_delete_export_connection(integer) to public;