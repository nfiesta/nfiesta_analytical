--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) CASCADE;

create or replace function analytical.fn_etl_get_area_domain_category_attr
(
	_export_connection		integer,
	_area_domain_category	integer[] default null::integer[],
	_etl					boolean default null::boolean
)
returns table
(
	id									integer,
	label								varchar,
	description							text,
	label_en							varchar,
	description_en						text,
	etl_area_domain__id					integer,
	etl_area_domain__etl_id				integer,
	etl_area_domain_category__id		integer,
	etl_area_domain_category__etl_id	integer,
	variable_superior					boolean
)
as
$$
declare
		_cond		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category_attr: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
		
		if _area_domain_category is not null
		then
			for i in 1..array_length(_area_domain_category,1)
			loop
				if not exists (select t2.* from analytical.c_area_domain_category as t2 where t2.id = _area_domain_category[i])
				then
					raise exception 'Error 03: fn_etl_get_area_domain_category_attr: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category[i];
				end if;
			end loop;
		end if;
	
		if _area_domain_category is null and _etl is distinct from true
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category_attr: If input argument _area_domain_category is null then input argument _etl must be TRUE!';
		end if;
		
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w7.etl_area_domain_category__etl_id is not null';
			else
				_cond := 'w7.etl_area_domain_category__etl_id is null';
			end if;
		end if;
	
		if _area_domain_category is null
		then
			return query execute
			'		
			with
			w1 as	(
					select tead.* from analytical.t_etl_area_domain as tead where tead.export_connection = $1
					)		
			,w2a as	(
					select tah.variable_superior from analytical.t_adc_hierarchy as tah
					where tah.variable_superior in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						)
					)					
			,w2b as	(
					select tah.variable from analytical.t_adc_hierarchy as tah
					where tah.variable in
						(
						select cadc.id from analytical.c_area_domain_category as cadc
						where cadc.area_domain in (select w1.area_domain from w1)
						)
					)
			,w3a as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2a.variable_superior from w2a)
					)
			,w3b as	(
					select distinct cadc1.area_domain from analytical.c_area_domain_category as cadc1
					where cadc1.id in (select w2b.variable from w2b)
					)
			,w4 as	(
					select w3a.area_domain, true as variable_superior from w3a union all
					select w3b.area_domain, false as variable_superior from w3b
					)
			,w5 as	(
					select w4.area_domain, variable_superior from w4 union
					select w1.area_domain, false as variable_superior from w1
					)
			,w6 as	(
					select
							w5.area_domain as id,
							w1.id as etl_area_domain__id,
							w1.etl_id as etl_area_domain__etl_id,
							w5.variable_superior
					from
							w5
							inner join w1 on w5.area_domain = w1.area_domain
					)
			select
					t.area_domain_category as id,
					cadc.label,
					cadc.description,
					cadc.label_en,
					cadc.description_en,
					w6.etl_area_domain__id,
					w6.etl_area_domain__etl_id,
					t.id as etl_area_domain_category__id,
					t.etl_id as etl_area_domain_category__etl_id,
					w6.variable_superior
			from
					(
					select teadc.* from analytical.t_etl_area_domain_category as teadc
					where teadc.etl_area_domain in (select w6.etl_area_domain__id from w6)
					) as t
			inner join w6 on t.etl_area_domain = w6.etl_area_domain__id
			inner join analytical.c_area_domain_category as cadc on t.area_domain_category = cadc.id
			order
					by t.area_domain_category, w6.variable_superior
			'
			using _export_connection;		
		else
			return query execute
			'
			with
			w1 as	(
					select
							cadc2.id,
							cadc2.label,
							cadc2.description,
							cadc2.label_en,
							cadc2.description_en,
							cadc2.area_domain
					from
							analytical.c_area_domain_category as cadc2
					where
							cadc2.area_domain in	(
													select distinct cadc1.area_domain
													from analytical.c_area_domain_category as cadc1
													where cadc1.id in (select unnest($2))
													)
					)
			,w2 as	(
					select
							w1.*,
							tead.id as etl_area_domain__id,
							tead.etl_id as etl_area_domain__etl_id
					from
							w1
					left
					join	(select * from analytical.t_etl_area_domain where export_connection = $1 and area_domain in (select distinct w1.area_domain from w1)) as tead
							on w1.area_domain = tead.area_domain
					)		
			,w3 as	(
					select
							w2.id,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en,
							w2.etl_area_domain__id,
							w2.etl_area_domain__etl_id,
							t.id as etl_area_domain_category__id,
							t.etl_id as etl_area_domain_category__etl_id
					from
							w2
							left join (select * from analytical.t_etl_area_domain_category as teadc where teadc.etl_area_domain in (select w2.etl_area_domain__id from w2)) as t
							on w2.etl_area_domain__id = t.etl_area_domain
							and w2.id = t.area_domain_category
					)
			-----------------------------------------	
			-----------------------------------------
			-- add CATEGORY from hierarchy
			,w4 as	(
					select
							id,
							label,
							description,
							label_en,
							description_en,
							area_domain
					from analytical.c_area_domain_category where area_domain in
					(select distinct area_domain from analytical.c_area_domain_category where id in
					(select distinct variable_superior from analytical.t_adc_hierarchy
					where variable in (select distinct w3.id from w3)))
					)
			,w5 as	(
					select
							w4.*,
							t.id as etl_area_domain__id,
							t.etl_id as etl_area_domain__etl_id
					from w4
					left join
						(
						select * from analytical.t_etl_area_domain
						where export_connection = $1 and area_domain in (select w4.area_domain from w4)
						) as t
					on w4.area_domain = t.area_domain
					)
			,w6 as	(
					select
							w5.id,
							w5.label,
							w5.description,
							w5.label_en,
							w5.description_en,
							w5.etl_area_domain__id,
							w5.etl_area_domain__etl_id,
							t.id as etl_area_domain_category__id,
							t.etl_id as etl_area_domain_category__etl_id
					from w5
					left join	(
								select * from analytical.t_etl_area_domain_category
								where etl_area_domain in (select w5.etl_area_domain__id from w5)
								) as t
					on w5.etl_area_domain__id = t.etl_area_domain
					and w5.id = t.area_domain_category
					)
			-----------------------------------------
			,w7 as	(
					select
							w3.id,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							w3.etl_area_domain__id,
							w3.etl_area_domain__etl_id,
							w3.etl_area_domain_category__id,
							w3.etl_area_domain_category__etl_id,
							false as variable_superior
					from
							w3 where w3.id not in (select w6.id from w6)
					union all
					select
							w6.id,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en,
							w6.etl_area_domain__id,
							w6.etl_area_domain__etl_id,
							w6.etl_area_domain_category__id,
							w6.etl_area_domain_category__etl_id,
							true as variable_superior
					from
							w6
					)
			-----------------------------------------
			-----------------------------------------
			select
					w7.id,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					w7.etl_area_domain__id,
					w7.etl_area_domain__etl_id,
					w7.etl_area_domain_category__id,
					w7.etl_area_domain_category__etl_id,	
					w7.variable_superior
			from
					w7 where '|| _cond ||'
			order
					by w7.id
			'
			using _export_connection, _area_domain_category;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) is
'Function returns information records of area domain categories for given arguments.';

alter function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_area_domain_category_attr(integer, integer[], boolean) to public;

