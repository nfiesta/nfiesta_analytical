#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import psycopg2
import psycopg2.extras
import json
from psycopg2 import sql

###########################################################     DB CONN

connstr = input("Define a label of the new import connection:\nexample (without quotes): 'dbname=MyDB host=CanBeOmmitedForLinuxBasedAuthentication user=MyUser' for password use ~/.pgpass file\n")

try:
    # alter database contrib_regression_user rename to nfi_analytical;
    conn_src = psycopg2.connect(connstr)
except psycopg2.DatabaseError as e:
    print("nfiesta_parallel.single_query(), not possible to connect DB")
    print(e)
    sys.exit(1)

def run_sql(_conn, _sql_cmd, _data):
    cur = _conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        cur.execute(_sql_cmd, _data)
    except psycopg2.Error as e:
        print("etl_py, SQL error:")
        print(e)
        sys.exit(1)

    if cur.description != None:
        path_row_list = cur.fetchall()
    else:
        path_row_list = None

    return path_row_list

    cur.close()

###########################################################     ETL
print('--------------------------------------------------------------')
print('ETL')
print('--------------------------------------------------------------')

###############################################################################
#####################################################     Set National Language
print('--------------------------------------------------------------')
print('ETL - Set National Language')
print('--------------------------------------------------------------')
while True:
    national_language = input("Set national language (cs/en):\n")
    if not (national_language == 'cs' or national_language == 'en'):
        print("Sorry, not known.")
        continue
    else:
        break
print('National language was set to: %s' % (national_language))
print('--------------------------------------------------------------')
###############################################################################



# ###############################################################################
# #####################################################     Connections
# print('ETL - Export connections')
# print('--------------------------------------------------------------')
# ###############################################     Node 1
# print('Node 1 - get export connections')
# sql_cmd = '''select count(*) as res_conn_node_1 from analytical.fn_etl_get_export_connections();'''
# data = (None, )
# res_conn_node1 = run_sql(conn_src, sql_cmd, data)

# if res_conn_node1[0]['res_conn_node_1'] == 0:
#     ###############################################     Node 2
#     print('Node 2 - Exists any record? -> NO -> next step: Node 3')
#     ###############################################     Node 3
#     print('Node 3 - Define an attributes of the new export connection -> next step: Node 4')
#     node3_conn_host = input("Node 3 - Define a host of the new export connection:\nexample (without quotes): 'localhost'\n")
#     node3_conn_dbname = input("Node 3 - Define a dbname of the new export connection:\nexample (without quotes): 'contrib_regression_sdesign'\n")
#     node3_conn_port = input("Node 3 - Define a port of the new export connection:\nexample (without quotes): '5432'\n")
#     node3_conn_comment = input("Node 3 - Define a comment of the new export connection:\nexample (without quotes): 'test'\n")
#     ###############################################     Node 4
#     print('Node 4 - save export connection -> next step: Node 6')
#     sql_cmd = '''select analytical.fn_etl_save_export_connection(%s,%s,%s,%s) as res_conn_node_4;'''
#     data = (node3_conn_host,node3_conn_dbname,int(node3_conn_port),node3_conn_comment)
#     res_conn_node4 = run_sql(conn_src, sql_cmd, data)[0]['res_conn_node_4']    
# else:
#     ###############################################     Node 2
#     print('Node 2 - Exists any record? -> YES -> Display list of export connections -> next step: Node 5')
#     sql_cmd = '''select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections();'''
#     print('List of export connections:')
#     data = (None, )
#     res_conn_node2 = run_sql(conn_src, sql_cmd, data)
#     print('-------')
#     for i, r in enumerate(res_conn_node2):
#         print('ID: %s   HOST: %s    DBNAME: %s  PORT: %s    COMMENT: %s' % (i+1, r['host'], r['dbname'], r['port'], r['comment']))
#     print('-------')

#     while True:
#         ###############################################     Node 5
#         node5_conn = input("Node 5 - Is the current list of connections sufficient? (y/n):\n")
#         if not (node5_conn == 'y' or node5_conn == 'n'):
#             print("Sorry, not known.")
#             continue
#         else:
#             break

#     if node5_conn == 'y':
#         print('YES -> next step: Node 6')
#     elif node5_conn == 'n':
#         ###############################################     Node 3
#         print('Node 3 - Define an attributes of the new export connection -> next step: Node 4')
#         node3_conn_host = input("Node 3 - Define a host of the new export connection:\nexample (without quotes): 'localhost'\n")
#         node3_conn_dbname = input("Node 3 - Define a dbname of the new export connection:\nexample (without quotes): 'contrib_regression_sdesign'\n")
#         node3_conn_port = input("Node 3 - Define a port of the new export connection:\nexample (without quotes): '5432'\n")
#         node3_conn_comment = input("Node 3 - Define a comment of the new export connection:\nexample (without quotes): 'test'\n")    
#         ###############################################     Node 4
#         print('Node 4 - save export connection')
#         sql_cmd = '''select analytical.fn_etl_save_export_connection(%s,%s,%s,%s) as res_conn_node_4;'''
#         data = (node3_conn_host,node3_conn_dbname,int(node3_conn_port),node3_conn_comment)
#         res_conn_node4 = run_sql(conn_src, sql_cmd, data)[0]['res_conn_node_4']        

# ###############################################     Node 6
# print('Node 6 - Informative list of export connections:')
# sql_cmd = '''select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections();'''
# data = (None, )
# res_conn_node6 = run_sql(conn_src, sql_cmd, data)
# print('-------')
# for i, r in enumerate(res_conn_node6):
#     print('ID: %s   HOST: %s    DBNAME: %s  PORT: %s    COMMENT: %s' % (i+1, r['host'], r['dbname'], r['port'], r['comment']))
# print('-------')
# ###############################################     Node 7
# node7_conn_pyarray = input("Node 7 - Set ID of export connection:\n")
# node7_conn = res_conn_node6[int(node7_conn_pyarray)-1]['id']
# ###############################################     Node 8
# try:
#     # alter database contrib_regression_add rename to nfi_esta;
#     # connstr = 'dbname=nfi_esta user=vagrant' # for local linux based authentication (without password)
#     # connstr = dbname=contrib_regression_etl user=vagrant # for testing etl.py
#     connstr_host = res_conn_node6[int(node7_conn_pyarray)-1]['host']
#     connstr_dbname = res_conn_node6[int(node7_conn_pyarray)-1]['dbname']
#     connstr_port = res_conn_node6[int(node7_conn_pyarray)-1]['port']
#     connstr = "host=%s dbname=%s port=%s"%(connstr_host,connstr_dbname,connstr_port)
#     node8_conn_user = input("Node 8. Define user for selected connection:\nexample (without quotes): 'MyUser' for password use ~/.pgpass file\n")
#     connstr="%s user=%s"%(connstr, node8_conn_user)
#     conn_dest = psycopg2.connect(connstr)
# except psycopg2.DatabaseError as e:
#     print("nfiesta_parallel.single_query(), not possible to connect DB")
#     print(e)
#     sys.exit(1)
# conn_src.commit()

# print('--------------------------------------------------------------')
# ###############################################################################



###############################################################################
#####################################################     Connections
print('ETL - Export connections')
print('--------------------------------------------------------------')
###############################################     Node 1
print('Node 1 - get export connections')
sql_cmd_conn_node1_count = '''select count(*) as res_conn_node_1 from analytical.fn_etl_get_export_connections();'''
sql_cmd_conn_node1_list = '''select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections();'''
data_conn_node1 = (None, )
res_conn_node1_count = run_sql(conn_src, sql_cmd_conn_node1_count, data_conn_node1)[0]['res_conn_node_1'] 
res_conn_node1_list = run_sql(conn_src, sql_cmd_conn_node1_list, data_conn_node1)
###############################################     Node 2
print('Node 2 - Display list of export connections -> next step: Node 3')
if res_conn_node1_count == 0:
    print('-------')
    print('EMPTY')
    print('-------')
else:
    print('-------')
    for i, r in enumerate(res_conn_node1_list):
        print('ID: %s   HOST: %s    DBNAME: %s  PORT: %s    COMMENT: %s' % (i+1, r['host'], r['dbname'], r['port'], r['comment']))
    print('-------')
###############################################     Node 3
while True:
    node3_conn = input("Node 3 - Is the current list of connections sufficient and correct? (y/n):\n")
    if not (node3_conn == 'y' or node3_conn == 'n'):
        print("Sorry, not known.")
        continue
    else:
        break

if node3_conn == 'y':
    print('YES -> next step: Node 16')
else:
    print('NO -> next step: Node 4')
    ###############################################     Node 4
    while True:
        node4_conn_action = input("Node 4 - Set action: (insert/update/delete):\n")
        if not (node4_conn_action == 'insert' or node4_conn_action == 'update' or node4_conn_action == 'delete'):
            print("Sorry, not known.")
            continue
        else:
            break

    if node4_conn_action == 'insert':
        ###############################################     Node 5
        print('Node 5 - Define an attributes of the new export connection -> next step: Node 6')
        node5_conn_host = input("Node 5 - Define a host of the new export connection:\nexample (without quotes): 'localhost'\n")
        node5_conn_dbname = input("Node 5 - Define a dbname of the new export connection:\nexample (without quotes): 'contrib_regression_user'\n")
        node5_conn_port = input("Node 5 - Define a port of the new export connection:\nexample (without quotes): '5433'\n")
        node5_conn_comment = input("Node 5 - Define a comment of the new export connection:\nexample (without quotes): 'test'\n")        
        ###############################################     Node 6
        # TEST SIGN INTO TARGET DB
        print('Node 6 - TEST sign in target database')
        try:
            node5_connstr = "host=%s dbname=%s port=%s"%(node5_conn_host,node5_conn_dbname,node5_conn_port)
            node5_conn_user = input("Node 5 - Define user for the new export connection:\nexample (without quotes): 'MyUser' for password use ~/.pgpass file\n")
            node5_connstr="%s user=%s"%(node5_connstr, node5_conn_user)
            conn_dest = psycopg2.connect(node5_connstr)
        except psycopg2.DatabaseError as e:
            ###############################################     Node 7
            print("nfiesta_parallel.single_query(), not possible to connect DB")
            print(e)
            sys.exit(1)
        ###############################################     Node 7
        print('Node 7 - Connection succeeded')
        ###############################################     Node 8
        print('Node 8 - save export connection -> next step: Node 16 [over Nodes 9,1,2,3]')
        sql_cmd_conn_node8 = '''select analytical.fn_etl_save_export_connection(%s,%s,%s,%s) as res_conn_node_8;'''
        data_conn_node8 = (node5_conn_host,node5_conn_dbname,int(node5_conn_port),node5_conn_comment)
        res_conn_node8 = run_sql(conn_src, sql_cmd_conn_node8, data_conn_node8)[0]['res_conn_node_8']
        ###############################################     Node 9
        print('Node 9 - COMMIT [source DB]')
        conn_src.commit()
        conn_dest.close()
        ###############################################
    else:
        if node4_conn_action == 'update':
            node4_conn_pyarray = input("Node 4 - Set ID of export connection for UPDATE:\n")
        else:
            node4_conn_pyarray = input("Node 4 - Set ID of export connection for DELETE:\n")

        node4_conn_id = res_conn_node1_list[int(node4_conn_pyarray)-1]['id']

        #################################################################################### Node 10
        if node4_conn_action == 'update':
            ####################################### SET COMMENT
            node10_conn_comment = input("Node 10 - Define a new comment:\nexample (without quotes): 'test'\n")
            #######################################
            # UPDATE
            print('Node 10 - UPDATE export connection -> next step: Node 14')
            sql_cmd_conn_node10 = '''select * from analytical.fn_etl_update_export_connection(%s,%s);'''
            data_conn_node10 = (node4_conn_id,node10_conn_comment)
            res_conn_node10 = run_sql(conn_src, sql_cmd_conn_node10, data_conn_node10)
            #######################################
        #################################################################################### Node 11,12,13
        if node4_conn_action == 'delete':
            sql_cmd_conn_node11 = '''select count(t.*) from (select fn_etl_try_delete_export_connection from analytical.fn_etl_try_delete_export_connection(%s)) as t where t.fn_etl_try_delete_export_connection = true;'''
            data_conn_node11 = (node4_conn_id,)
            res_conn_node11 = run_sql(conn_src, sql_cmd_conn_node11, data_conn_node11)[0][0]
            #print(res_conn_node11)

            if res_conn_node11 == 0:
                print('Node 12 - EXPORT CONNECTION IS NOT ALLOWED TO DELETE !!! -> next step: STOP process ETL [In GUI apllication next step will be Node 1]')
                sys.exit(1)
            else:
                # DELETE
                print('Node 13 - DELETE export connection -> next step: Node 14')
                sql_cmd_conn_node13 = '''select * from analytical.fn_etl_delete_export_connection(%s);'''
                data_conn_node13 = (node4_conn_id,)
                res_conn_node13 = run_sql(conn_src, sql_cmd_conn_node13,data_conn_node13)
        ####################################################################################
        ###############################################################################
        #######################################################   CONFIRM CHANGES
        ##############################################     CHM - Node 14
        while True:
            commit_conn_chm = input("Node 14 - Confirm a commit of UPDATE or DELETE connection? (y/n):\n")
            if not (commit_conn_chm == 'y' or commit_conn_chm == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if commit_conn_chm == 'y':
            conn_src.commit()

            if node4_conn_action == 'update':
                print('Node 14 -> YES -> Process of UPDATE export connection was commited. Next step: Node 15')
            else:
                print('Node 14 -> YES -> Process of DELETE export connection was commited. Next step: Node 15')

        elif commit_conn_chm == 'n':
            if node4_conn_action == 'update':
                print('Node 14 -> NO -> Process of UPDATE export connection was NOT commited. Next step: Node 15')
            else:
                print('Node 14 -> NO -> Process of DELETE export connection was NOT commited. Next step: Node 15')

        while True:
            check_chm_continue = input("CHM - Node 15 - Continue with process ETL? (y/n):\n")
            if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if check_chm_continue == 'y':
            print('YES -> User wants to continue with process ETL. Next step: Node 16 [over Nodes 1,2,3]')
        else:
            ###############################################################################
            ###########################################################     DB CONN CLOSE
            print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
            conn_src.close()
            sys.exit(1)
        ###############################################################################

####################################################################################
# INFORMATIVE LIST OF CONNECTIONS for Node no. 16 if user solved some of actions
if node3_conn == 'n':
    sql_cmd_conn_node1_count = '''select count(*) from analytical.fn_etl_get_export_connections();'''
    sql_cmd_conn_node1_list = '''select id, host, dbname, port, comment from analytical.fn_etl_get_export_connections();'''
    data_conn_node1 = (None, )
    res_conn_node1_count = run_sql(conn_src, sql_cmd_conn_node1_count, data_conn_node1)[0][0]
    res_conn_node1_list = run_sql(conn_src, sql_cmd_conn_node1_list, data_conn_node1)
    if res_conn_node1_count == 0:
        print('List of export connections is EMPTY -> next step: STOP process ETL')
        sys.exit(1)
    else:
        print('List of export connections:')
        print('-------')
        for i, r in enumerate(res_conn_node1_list):
            print('ID: %s   HOST: %s    DBNAME: %s  PORT: %s    COMMENT: %s' % (i+1, r['host'], r['dbname'], r['port'], r['comment']))
        print('-------')
####################################################################################

###############################################     Node 16
node16_conn_pyarray = input("Node 16 - Set ID of export connection:\n")
node7_conn = res_conn_node1_list[int(node16_conn_pyarray)-1]['id']
###############################################     Node 17
try:
    # alter database contrib_regression_add rename to nfi_esta;
    # connstr = 'dbname=nfi_esta user=vagrant' # for local linux based authentication (without password)
    # connstr = dbname=contrib_regression_user host=localhost user=vagrant port=5433 # for testing etl.py [SOURCE DB]
    # connstr = dbname=contrib_regression_nfiesta_sdesign # for testing etl.py [TARGET DB]
    # connstr = dbname=contrib_regression_etl host=localhost user=vagrant port=5433 # for testing etl.py [SOURCE DB]
    connstr_host = res_conn_node1_list[int(node16_conn_pyarray)-1]['host']
    connstr_dbname = res_conn_node1_list[int(node16_conn_pyarray)-1]['dbname']
    connstr_port = res_conn_node1_list[int(node16_conn_pyarray)-1]['port']
    connstr = "host=%s dbname=%s port=%s"%(connstr_host,connstr_dbname,connstr_port)
    node8_conn_user = input("Node 17. Define user for selected connection:\nexample (without quotes): 'MyUser' for password use ~/.pgpass file\n")
    connstr="%s user=%s"%(connstr, node8_conn_user)
    conn_dest = psycopg2.connect(connstr)
except psycopg2.DatabaseError as e:
    print("nfiesta_parallel.single_query(), not possible to connect DB")
    print(e)
    sys.exit(1)
print('--------------------------------------------------------------')
###############################################################################



###############################################################################
# set schema name from target DB
###############################################################################
#schemaname = 'target_data'
#sql_cmd = sql.SQL('''select fn_etl_update_target_variable_metadata as res_chm_node_10 from {}.fn_etl_update_target_variable_metadata(%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
###############################################################################



###############################################################################
# AREAL OR POPULATION
###############################################################################
print('ETL - Areal or population')
print('--------------------------------------------------------------')
##############################################    Node 1
print('Node 1 - Informative list of areal or populations for ETL [SOURCE DB]')
sql_cmd_ap1_count = '''select count(*) from analytical.fn_etl_get_areal_or_population(%s,%s);'''
sql_cmd_ap1_list = '''select id, label, description, label_en, description_en, etl_areal_or_population__id, etl_areal_or_population__etl_id from analytical.fn_etl_get_areal_or_population(%s,%s) order by id;'''
data_false = 'false'
data_ap1 = (node7_conn,data_false)
res_ap1_count = run_sql(conn_src, sql_cmd_ap1_count, data_ap1)[0][0]
res_ap1_list = run_sql(conn_src, sql_cmd_ap1_list, data_ap1)
#print(res_ap1_count)
###############################################     Node 2
if res_ap1_count > 0:
    print('Node 2 - Exists any record? -> YES')
    ###############################################     Node 3
    print('Node 3 - List of areal or populations [SOURCE DB]:')
    for r in res_ap1_list:
        print('ID: %s   LABEL: %s   DESC: %s   LABEL_EN: %s    DESC_EN: %s  ETL_AoP__ID: %s ETL_Aop__ETL_ID: %s' % (r['id'], r['label'], r['description'], r['label_en'], r['description_en'], r['etl_areal_or_population__id'], r['etl_areal_or_population__etl_id']))

    # CYCLE for all rows from node 1
    for r in res_ap1_list:
        print('CYCLE for areal or population: LABEL = %s and LABEL_EN = %s' % (r['label'], r['label_en']))
        ###############################################     Node 4
        print('Node 4 - Get areal or population [SOURCE DB]')
        sql_cmd_ap4_count = '''select count(*) from analytical.fn_etl_get_areal_or_population(%s,%s);'''
        data_true = 'true'
        data_ap4 = (node7_conn,data_true)
        res_ap4_count = run_sql(conn_src, sql_cmd_ap4_count, data_ap4)[0][0]

        # set sql_cmd and data for node 5 based on result in node 4
        if res_ap4_count == 0:
            sql_cmd_ap5_count = '''select count(*) from target_data.fn_import_get_areal_or_populations(%s,null::int[]);'''
            sql_cmd_ap5_list = '''select id, etl_id, label, description, label_en, description_en from target_data.fn_import_get_areal_or_populations(%s,null::int[]);'''
            data_ap5 = (r['id'],)
        else:
            sql_cmd_ap4_array = '''select array_agg(etl_areal_or_population__etl_id) from analytical.fn_etl_get_areal_or_population(%s,%s);''' 
            res_ap4_array = run_sql(conn_src, sql_cmd_ap4_array, data_ap4)[0][0]
            #print(res_ap4_array)
            sql_cmd_ap5_count = '''select count(*) from target_data.fn_import_get_areal_or_populations(%s,%s);'''
            sql_cmd_ap5_list = '''select id, etl_id, label, description, label_en, description_en from target_data.fn_import_get_areal_or_populations(%s,%s);'''
            data_ap5 = (r['id'],res_ap4_array)

        ###############################################     Node 5
        print('Node 5 - Get areal or populations [TARGET DB]')
        res_ap5_count = run_sql(conn_dest, sql_cmd_ap5_count, data_ap5)[0][0]
        res_ap5_list = run_sql(conn_dest, sql_cmd_ap5_list, data_ap5)
        #print(res_ap5_count)

        ###############################################     Node 6
        if res_ap5_count == 0:
            print('Node 6 - Any record returned? -> NO')
            ###############################################     Node 11
            print('Node 11 - Save areal or population [TARGET DB]')
            sql_cmd_ap11_save = '''select etl_id from target_data.fn_import_save_areal_or_population(%s,%s,%s,%s,%s);'''
            data_ap11 = (r['id'],r['label'],r['description'],r['label_en'],r['description_en'])
            res_ap11_save = run_sql(conn_dest, sql_cmd_ap11_save, data_ap11)[0][0]
            ###############################################     Node 13
            print('Node 13 - Save areal or population [SOURCE DB]')
            sql_cmd_ap13_save = '''select * from analytical.fn_etl_save_areal_or_population(%s,%s,%s);'''
            data_ap13 = (node7_conn,r['id'],res_ap11_save)
            res_ap13_save = run_sql(conn_src,sql_cmd_ap13_save,data_ap13)
        else:
        ###############################################     Node 6
            print('Node 6 - Any record returned? -> YES')
            ###############################################     Node 7
            print('Node 7 - Check areal or population [TARGET DB]')
            sql_cmd_ap7_etl_id = '''select etl_id from target_data.fn_import_check_areal_or_population(%s,%s);'''
            sql_cmd_ap7_list =  '''select id, etl_id, label, description, label_en, description_en from target_data.fn_import_check_areal_or_population(%s,%s);'''
            data_ap7 = (r['id'],r['label_en'])
            res_ap7_etl_id = run_sql(conn_dest,sql_cmd_ap7_etl_id,data_ap7)[0][0]
            res_ap7_list = run_sql(conn_dest,sql_cmd_ap7_list,data_ap7)
            #print(res_ap7_etl_id)

            ###############################################     Node 8
            if res_ap7_etl_id == None:
                print('Node 8 - Exists etl_id? -> NO')
                ###############################################     Node 9
                print('Node 9 - List of areal or populations from node 5 [TARGET DB]:')
                for rr in res_ap5_list:
                    print('ID: %s   ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s  DESC_EN: %s' % (rr['id'], rr['etl_id'], rr['label'], rr['description'], rr['label_en'], rr['description_en']))
                ###############################################     Node 10
                while True:
                    ap10_input_y_or_no = input("Node 10 - Exists etl id for: LABEL = '%s' and LABEL_EN = '%s'? (y/n):\n" % (r['label'], r['label_en']))
                    if not (ap10_input_y_or_no == 'y' or ap10_input_y_or_no == 'n'):
                        print("Sorry, not known.")
                        continue
                    else:
                        break

                if ap10_input_y_or_no == 'n':
                    print('NO')
                    ###############################################     Node 11
                    print('Node 11 - Save areal or population [TARGET DB]')
                    sql_cmd_ap11_save = '''select etl_id from target_data.fn_import_save_areal_or_population(%s,%s,%s,%s,%s);'''
                    data_ap11 = (r['id'],r['label'],r['description'],r['label_en'],r['description_en'])
                    res_ap11_save = run_sql(conn_dest, sql_cmd_ap11_save, data_ap11)[0][0]
                    ###############################################     Node 13
                    print('Node 13 - Save areal or population [SOURCE DB]')
                    sql_cmd_ap13_save = '''select * from analytical.fn_etl_save_areal_or_population(%s,%s,%s);'''
                    data_ap13 = (node7_conn,r['id'],res_ap11_save)
                    res_ap13_save = run_sql(conn_src,sql_cmd_ap13_save,data_ap13)
                else:
                    print('YES')
                    ap10_input_etl_id = input("Set etl_id?\n")               
                    ###############################################     Node 13
                    print('Node 13 - Save areal or population [SOURCE DB]')
                    sql_cmd_ap13_save = '''select * from analytical.fn_etl_save_areal_or_population(%s,%s,%s);'''
                    data_ap13 = (node7_conn,r['id'],ap10_input_etl_id)
                    res_ap13_save = run_sql(conn_src,sql_cmd_ap13_save,data_ap13)

            else:
            ###############################################     Node 8
                print('Node 8 - Exists etl_id? -> YES')
                ###############################################     Node 12
                print('Areal or population from node 7 [TARGET DB]:')
                for rrr in res_ap7_list:
                    print('ID: %s   ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s  DESC_EN: %s' % (rrr['id'], rrr['etl_id'], rrr['label'], rrr['description'], rrr['label_en'], rr['description_en']))

                while True:
                    ap12_input_y_or_no = input("Node 12 - Is etl id correct? (y/n):\n")
                    if not (ap12_input_y_or_no == 'y' or ap12_input_y_or_no == 'n'):
                        print("Sorry, not known.")
                        continue
                    else:
                        break

                if ap12_input_y_or_no == 'y':
                    print('YES')
                    ###############################################     Node 13
                    print('Node 13 - Save areal or population [SOURCE DB]')
                    sql_cmd_ap13_save = '''select * from analytical.fn_etl_save_areal_or_population(%s,%s,%s);'''
                    data_ap13 = (node7_conn,r['id'],res_ap7_etl_id)
                    res_ap13_save = run_sql(conn_src,sql_cmd_ap13_save,data_ap13)
                else:
                    print('NO')          
                    ###############################################     Node 9
                    print('Node 9 - List of areal or populations from node 5 [TARGET DB]:')
                    for rr in res_ap5_list:
                        print('ID: %s   ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s  DESC_EN: %s' % (rr['id'], rr['etl_id'], rr['label'], rr['description'], rr['label_en'], rr['description_en']))
                    ###############################################     Node 10
                    while True:
                        ap10_input_y_or_no = input("Node 10 - Exists etl id for: LABEL = '%s' and LABEL_EN = '%s'? (y/n):\n" % (r['label'], r['label_en']))
                        if not (ap10_input_y_or_no == 'y' or ap10_input_y_or_no == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if ap10_input_y_or_no == 'n':
                        print('NO')
                        ###############################################     Node 11
                        print('Node 11 - Save areal or population [TARGET DB]')
                        sql_cmd_ap11_save = '''select etl_id from target_data.fn_import_save_areal_or_population(%s,%s,%s,%s,%s);'''
                        data_ap11 = (r['id'],r['label'],r['description'],r['label_en'],r['description_en'])
                        res_ap11_save = run_sql(conn_dest, sql_cmd_ap11_save, data_ap11)[0][0]
                        ###############################################     Node 13
                        print('Node 13 - Save areal or population [SOURCE DB]')
                        sql_cmd_ap13_save = '''select * from analytical.fn_etl_save_areal_or_population(%s,%s,%s);'''
                        data_ap13 = (node7_conn,r['id'],res_ap11_save)
                        res_ap13_save = run_sql(conn_src,sql_cmd_ap13_save,data_ap13)
                    else:
                        print('YES')
                        ap10_input_etl_id = input("Set etl_id?\n")               
                        ###############################################     Node 13
                        print('Node 13 - Save areal or population [SOURCE DB]')
                        sql_cmd_ap13_save = '''select * from analytical.fn_etl_save_areal_or_population(%s,%s,%s);'''
                        data_ap13 = (node7_conn,r['id'],ap10_input_etl_id)
                        res_ap13_save = run_sql(conn_src,sql_cmd_ap13_save,data_ap13)

###############################################     Node 2
else:
    print('Node 2 - Exists any record? -> NO -> next step: ETL Ldsity object')
print('--------------------------------------------------------------')
###############################################################################
###############################################################################



###############################################################################
# Node D
###############################################################################
while True:
    input_node_d = input("Node D - Do you want to solve an ETL of AREA DOMAIN or SUB POPULATION ATTRIBUTES for ldsity object that have already been ETLed? (y/n):\n")
    if not (input_node_d == 'y' or input_node_d == 'n'):
        print("Sorry, not known.")
        continue
    else:
        break
print('--------------------------------------------------------------')
###############################################################################



###############################################################################
# IF for Node D
###############################################################################
if input_node_d == 'n':
###############################################################################
    ###############################################################################
    # LDSITY OBJECT
    ###############################################################################
    print('ETL - LDSITY OBJECT')
    print('--------------------------------------------------------------')
    ##############################################    Node 1
    print('Node 1 - Informative list of ldsity objects for ETL [SOURCE DB]')
    sql_cmd_lo1_count = '''select count(*) from analytical.fn_etl_get_ldsity_objects(%s,%s,%s);'''
    sql_cmd_lo1_list = '''select ldsity_object,label,description,label_en,description_en,table_name,upper_object__id,upper_object__etl_id,areal_or_population__id,areal_or_population__label,areal_or_population__etl_id,column4upper_object,filter,etl_ldsity_object__id,etl_ldsity_object__etl_id,check_ldsities,check_upper_object from analytical.fn_etl_get_ldsity_objects(%s,%s,%s) order by ldsity_object;'''
    data_false = 'false'
    data_true = 'true'
    data_lo1 = (node7_conn,data_false,data_true)
    res_lo1_count = run_sql(conn_src, sql_cmd_lo1_count, data_lo1)[0][0]
    res_lo1_list = run_sql(conn_src, sql_cmd_lo1_list, data_lo1)
    #print(res_lo1_count)
    ###############################################     Node 2
    if res_lo1_count > 0:
        print('Node 2 - Exists any record? -> YES')
        ###############################################     Node 3
        print('Node 3 - List of ldsity objects for ETL [SOURCE DB]:')
        for r in res_lo1_list:
            print('LDSITY_OBJECT: %s   LABEL: %s   LABEL_EN: %s    AoP_LABEL: %s  ETL_LO__ETL_ID: %s CHECK_LDSITIES: %s CHECK_UPPER_OBJECT: %s' % (r['ldsity_object'], r['label'], r['label_en'], r['areal_or_population__label'], r['etl_ldsity_object__etl_id'], r['check_ldsities'], r['check_upper_object']))
        ###############################################     Node 4
        lo4_input_ldsity_object = input("Node 4 - Choose ldsity object for ETL?\n")
        sql_cmd_lo4_check_upper_object_true = '''select count(*) from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s) where check_upper_object = TRUE;'''
        data_lo4 = (node7_conn,data_false,data_true,lo4_input_ldsity_object)
        res_lo4_check_upper_object_true = run_sql(conn_src,sql_cmd_lo4_check_upper_object_true,data_lo4)[0][0]
        #print(res_lo4_check_upper_object_true)
        ###############################################     Node 5
        if res_lo4_check_upper_object_true == 0:
            print('Node 5 - Is check_upper_object TRUE? -> NO -> STOP ETL process!')
            sys.exit(1)
        else:
            print('Node 5 - Is check_upper_object TRUE? -> YES')
            ###############################################     Node 6
            sql_cmd_lo6_etl_id = '''select etl_ldsity_object__etl_id from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s) where check_upper_object = TRUE;'''
            data_lo6 = (node7_conn,data_false,data_true,lo4_input_ldsity_object)
            res_lo6_etl_id = run_sql(conn_src,sql_cmd_lo6_etl_id,data_lo6)[0][0]
            #print(res_lo6_etl_id)
            if res_lo6_etl_id == None:
                print('Node 6 - Exist etl_id of ldsity object? -> NO')
                ###################################################################
                ###################################################################
                ###############################################     Node 7
                print('Node 7 - Get ldsity objects [SOURCE DB]')
                sql_cmd_lo7_count = '''select count(*) from analytical.fn_etl_get_ldsity_objects(%s,%s,%s);'''
                data_true = 'true'
                data_false = 'false'
                data_lo7 = (node7_conn,data_true,data_false)
                res_lo7_count = run_sql(conn_src, sql_cmd_lo7_count, data_lo7)[0][0]

                # set sql_cmd and data for node 8 based on result in node 7
                if res_lo7_count == 0:
                    sql_cmd_lo8_count = '''select count(*) from target_data.fn_import_get_ldsity_objects(%s,null::int[]);'''
                    sql_cmd_lo8_list = '''select ldsity_object,id,label,description,label_en,description_en,table_name,upper_object,areal_or_population,column4upper_object,filter from target_data.fn_import_get_ldsity_objects(%s,null::int[]);'''
                    data_lo8 = (lo4_input_ldsity_object,)
                else:
                    sql_cmd_lo7_array = '''select array_agg(etl_ldsity_object__etl_id) from analytical.fn_etl_get_ldsity_objects(%s,%s,%s);''' 
                    res_lo7_array = run_sql(conn_src, sql_cmd_lo7_array, data_lo7)[0][0]
                    #print(res_lo7_array)
                    sql_cmd_lo8_count = '''select count(*) from target_data.fn_import_get_ldsity_objects(%s,%s);'''
                    sql_cmd_lo8_list = '''select ldsity_object,id,label,description,label_en,description_en,table_name,upper_object,areal_or_population,column4upper_object,filter from target_data.fn_import_get_ldsity_objects(%s,%s);'''
                    data_lo8 = (lo4_input_ldsity_object,res_lo7_array)

                ###############################################     Node 8
                print('Node 8 - Get ldsity objects [TARGET DB]')
                res_lo8_count = run_sql(conn_dest, sql_cmd_lo8_count, data_lo8)[0][0]
                res_lo8_list = run_sql(conn_dest, sql_cmd_lo8_list, data_lo8)
                #print(res_lo8_count)   
                ###############################################     Node 9
                if res_lo8_count == 0:
                    print('Node 9 - Any record returned? -> NO')
                    ###############################################     Node 14
                    print('Node 14 - Save ldsity object [TARGET DB]')
                    #sql_cmd_lo14_data4save = '''select ldsity_object, label, description, label_en, description_en, table_name, upper_object__etl_id, areal_or_population__etl_id, column4upper_object, filter from analytical.fn_etl_get_ldsity_objects(%s,%s,%s) where ldsity_object = %s;'''
                    sql_cmd_lo14_data4save = '''select ldsity_object, label, description, label_en, description_en, table_name, upper_object__etl_id, areal_or_population__etl_id, column4upper_object, filter from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s);'''
                    data_lo14_data4save = (node7_conn,data_false,data_false,lo4_input_ldsity_object)
                    res_lo14_data4save = run_sql(conn_src,sql_cmd_lo14_data4save,data_lo14_data4save)
                    #print(res_lo14_data4save[0][0])
                    #print(res_lo14_data4save[0][1])
                    sql_cmd_lo14_save = '''select * from target_data.fn_import_save_ldsity_object(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
                    data_lo14 = (res_lo14_data4save[0][0],res_lo14_data4save[0][1],res_lo14_data4save[0][2],res_lo14_data4save[0][3],res_lo14_data4save[0][4],res_lo14_data4save[0][5],res_lo14_data4save[0][6],res_lo14_data4save[0][7],res_lo14_data4save[0][8],res_lo14_data4save[0][9])
                    res_lo14_save = run_sql(conn_dest, sql_cmd_lo14_save, data_lo14)[0][0]
                    ###############################################     Node 16
                    print('Node 16 - Save ldsity object [SOURCE DB]')
                    sql_cmd_lo16_save = '''select * from analytical.fn_etl_save_ldsity_object(%s,%s,%s);'''
                    data_lo16 = (node7_conn,lo4_input_ldsity_object,res_lo14_save)
                    res_lo16_save = run_sql(conn_src,sql_cmd_lo16_save,data_lo16)[0][0]
                    #print(res_lo16_save)
                else:
                ###############################################     Node 9
                    print('Node 9 - Any record returned? -> YES')
                    ###############################################     Node 10
                    print('Node 10 - Check ldsity object [TARGET DB]')
                    sql_cmd_lo10_data4check = '''select ldsity_object, label_en from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s);'''
                    data_lo10_data4check = (node7_conn,data_false,data_false,lo4_input_ldsity_object)
                    res_lo10_data4check = run_sql(conn_src,sql_cmd_lo10_data4check,data_lo10_data4check)
                    sql_cmd_lo10_etl_id = '''select id from target_data.fn_import_check_ldsity_object(%s,%s);'''
                    sql_cmd_lo10_list =  '''select ldsity_object, id, label, description, label_en, description_en, areal_or_population__label, areal_or_population__label_en from target_data.fn_import_check_ldsity_object(%s,%s);'''
                    data_lo10 = (res_lo10_data4check[0][0],res_lo10_data4check[0][1])
                    res_lo10_etl_id = run_sql(conn_dest,sql_cmd_lo10_etl_id,data_lo10)[0][0]
                    res_lo10_list = run_sql(conn_dest,sql_cmd_lo10_list,data_lo10)
                    #print(res_lo10_etl_id)
                    ###############################################     Node 11
                    if res_lo10_etl_id == None:
                        print('Node 11 - Exists etl_id? -> NO')
                        ###############################################     Node 12
                        print('Node 12 - List of ldsity objects from node 8 [TARGET DB]:')
                        for rr in res_lo8_list:
                            print('LDSITY_OBJECT: %s   ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s  DESC_EN: %s' % (rr['ldsity_object'], rr['id'], rr['label'], rr['description'], rr['label_en'], rr['description_en']))
                        ###############################################     Node 13
                        while True:
                            lo13_input_y_or_no = input("Node 13 - Exists id for: LABEL_EN = '%s'? (y/n):\n" % (res_lo10_data4check[0][1]))
                            if not (lo13_input_y_or_no == 'y' or lo13_input_y_or_no == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if lo13_input_y_or_no == 'n':
                            print('NO')
                            ###############################################     Node 14
                            print('Node 14 - Save ldsity object [TARGET DB]')
                            sql_cmd_lo14_data4save = '''select ldsity_object, label, description, label_en, description_en, table_name, upper_object__etl_id, areal_or_population__etl_id, column4upper_object, filter from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s);'''
                            data_lo14_data4save = (node7_conn,data_false,data_false,lo4_input_ldsity_object)
                            res_lo14_data4save = run_sql(conn_src,sql_cmd_lo14_data4save,data_lo14_data4save)
                            sql_cmd_lo14_save = '''select * from target_data.fn_import_save_ldsity_object(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
                            data_lo14 = (res_lo14_data4save[0][0],res_lo14_data4save[0][1],res_lo14_data4save[0][2],res_lo14_data4save[0][3],res_lo14_data4save[0][4],res_lo14_data4save[0][5],res_lo14_data4save[0][6],res_lo14_data4save[0][7],res_lo14_data4save[0][8],res_lo14_data4save[0][9])
                            res_lo14_save = run_sql(conn_dest, sql_cmd_lo14_save, data_lo14)[0][0]
                            ###############################################     Node 16
                            print('Node 16 - Save ldsity object [SOURCE DB]')
                            sql_cmd_lo16_save = '''select * from analytical.fn_etl_save_ldsity_object(%s,%s,%s);'''
                            data_lo16 = (node7_conn,lo4_input_ldsity_object,res_lo14_save)
                            res_lo16_save = run_sql(conn_src,sql_cmd_lo16_save,data_lo16)[0][0]
                            #print(res_lo16_save)
                        else:
                            print('YES')
                            lo13_input_id = input("Set id?\n")               
                            ###############################################     Node 16
                            print('Node 16 - Save ldsity object [SOURCE DB]')
                            sql_cmd_lo16_save = '''select * from analytical.fn_etl_save_ldsity_object(%s,%s,%s);'''
                            data_lo16 = (node7_conn,lo4_input_ldsity_object,lo13_input_id)
                            res_lo16_save = run_sql(conn_src,sql_cmd_lo16_save,data_lo16)
                    else:
                    ###############################################     Node 11
                        print('Node 11 - Exists etl_id? -> YES')
                        ###############################################     Node 15
                        print('Ldsity object from node 10 [TARGET DB]:')
                        for rrr in res_lo10_list:
                            print('LDSITY_OBJECT: %s   ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s  DESC_EN: %s' % (rrr['ldsity_object'], rrr['id'], rrr['label'], rrr['description'], rrr['label_en'], rrr['description_en']))

                        while True:
                            lo15_input_y_or_no = input("Node 15 - Is etl id correct? (y/n):\n")
                            if not (lo15_input_y_or_no == 'y' or lo15_input_y_or_no == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if lo15_input_y_or_no == 'y':
                            print('YES')
                            ###############################################     Node 16
                            print('Node 16 - Save ldsity object [SOURCE DB]')
                            sql_cmd_lo16_save = '''select * from analytical.fn_etl_save_ldsity_object(%s,%s,%s);'''
                            data_lo16 = (node7_conn,lo4_input_ldsity_object,res_lo10_etl_id)
                            res_lo16_save = run_sql(conn_src,sql_cmd_lo16_save,data_lo16)
                        else:
                            print('NO')          
                            ###############################################     Node 12
                            print('Node 12 - List of ldsity objects from node 8 [TARGET DB]:')
                            for rr in res_lo8_list:
                                print('LDSITY_OBJECT: %s   ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s  DESC_EN: %s' % (rr['ldsity_object'], rr['id'], rr['label'], rr['description'], rr['label_en'], rr['description_en']))
                            ###############################################     Node 13
                            while True:
                                lo13_input_y_or_no = input("Node 13 - Exists id for: LABEL_EN = '%s'? (y/n):\n" % (res_lo10_data4check[0][1]))
                                if not (lo13_input_y_or_no == 'y' or lo13_input_y_or_no == 'n'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break

                            if lo13_input_y_or_no == 'n':
                                print('NO')
                                ###############################################     Node 14
                                print('Node 14 - Save ldsity object [TARGET DB]')
                                sql_cmd_lo14_data4save = '''select ldsity_object, label, description, label_en, description_en, table_name, upper_object__etl_id, areal_or_population__etl_id, column4upper_object, filter from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s);'''
                                data_lo14_data4save = (node7_conn,data_false,data_false,lo4_input_ldsity_object)
                                res_lo14_data4save = run_sql(conn_src,sql_cmd_lo14_data4save,data_lo14_data4save)
                                sql_cmd_lo14_save = '''select * from target_data.fn_import_save_ldsity_object(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
                                data_lo14 = (res_lo14_data4save[0][0],res_lo14_data4save[0][1],res_lo14_data4save[0][2],res_lo14_data4save[0][3],res_lo14_data4save[0][4],res_lo14_data4save[0][5],res_lo14_data4save[0][6],res_lo14_data4save[0][7],res_lo14_data4save[0][8],res_lo14_data4save[0][9])
                                res_lo14_save = run_sql(conn_dest, sql_cmd_lo14_save, data_lo14)[0][0]
                                ###############################################     Node 16
                                print('Node 16 - Save ldsity object [SOURCE DB]')
                                sql_cmd_lo16_save = '''select * from analytical.fn_etl_save_ldsity_object(%s,%s,%s);'''
                                data_lo16 = (node7_conn,lo4_input_ldsity_object,res_lo14_save)
                                res_lo16_save = run_sql(conn_src,sql_cmd_lo16_save,data_lo16)[0][0]
                                #print(res_lo16_save)
                            else:
                                print('YES')
                                lo13_input_id = input("Set id?\n")               
                                ###############################################     Node 16
                                print('Node 16 - Save ldsity object [SOURCE DB]')
                                sql_cmd_lo16_save = '''select * from analytical.fn_etl_save_ldsity_object(%s,%s,%s);'''
                                data_lo16 = (node7_conn,lo4_input_ldsity_object,lo13_input_id)
                                res_lo16_save = run_sql(conn_src,sql_cmd_lo16_save,data_lo16)
                ###################################################################
                ###################################################################
            else:
                print('Node 6 - Exist etl_id of ldsity object? -> YES -> next step: ETL Ldsities')
    ###############################################     Node 2
    else:
        print('Node 2 - Exists any record? -> NO -> next step: STOP ETL process. Nothing to ETL.')
        sys.exit(1)
    print('--------------------------------------------------------------')
    ###############################################################################
    ###############################################################################



    ###############################################################################
    # Node A
    ###############################################################################
    while True:
        input_node_a = input("Node A - Do you want to solve an ETL process of Ldsities? (y/n):\n")
        if not (input_node_a == 'y' or input_node_a == 'n'):
            print("Sorry, not known.")
            continue
        else:
            break
    print('--------------------------------------------------------------')
    ###############################################################################


    ###############################################################################
    # LDSITIES
    if input_node_a == 'y':
        ###############################################################################
        # LDSITIES [part from node 1 to node 5]
        ###############################################################################
        print('ETL - LDSITIES [part from node 1 to node 5]')
        print('--------------------------------------------------------------')
        ##############################################    Node 1
        print('Node 1 - Check ldsities for ldsity object [SOURCE DB]')
        sql_cmd_l1_check = '''select * from analytical.fn_etl_check_ldsities4object(%s,%s);'''
        data_l1 = (node7_conn,lo4_input_ldsity_object)
        res_l1_check = run_sql(conn_src, sql_cmd_l1_check, data_l1)[0][0]
        #print(res_l1_check)
        ###############################################     Node 2
        if res_l1_check == True:
            #print('Node 2 - Is returned TRUE? -> YES -> Commit and STOP ETL process. Nothing to ETL.')
            #print('ETL process was commited.')
            #conn_src.commit()
            #conn_dest.commit()
            #sys.exit(1)
            print('Node 2 - Is returned TRUE? -> YES -> Nothing LDSITY to ETL -> next step: Node B')
        else:
            print('Node 2 - Is returned TRUE? -> NO')
            ###############################################     Node 3
            print('Node 3 - Get ldsities for choosen ldsity object [SOURCE DB]')
            sql_cmd_l3_list = '''select * from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]);'''
            data_l3 = (node7_conn,lo4_input_ldsity_object)
            res_l3_list = run_sql(conn_src,sql_cmd_l3_list,data_l3)
            #print(res_l3_list)
            ###############################################     Node 4
            print('Node 4 - List of ldsities for ETL [SOURCE DB]')
            for r in res_l3_list:
                print('LO: %s LO_L: %s L: %s L_L: %s UoM: %s DF: %s AD: %s ADC: %s adc_R: %s adc_H: %s adc_PR: %s SP: %s SPC: %s spc_R: %s spc_H: %s spc_PR: %s V: %s RV: %s ETL_ID: %s ch_L: %s ch_LP: %s' % (r['ldsity_object'], r['ldsity_object_label_en'], r['ldsity'], r['ldsity_label_en'], r['check_unit_of_measure'], r['check_definition_variant'], r['check_area_domain'], r['check_area_domain_category'], r['check_adc2classification_rule'], r['check_adc_hierarchy'], r['check_adc2classrule2panel_refyearset'], r['check_sub_population'], r['check_sub_population_category'], r['check_spc2classification_rule'], r['check_spc_hierarchy'], r['check_spc2classrule2panel_refyearset'], r['check_version'], r['check_ldsity2panel_refyearset_version'], r['etl_ldsity__etl_id'], r['check_ldsity'], r['check_ldsity_part']))
            ###############################################     Node 5
            print('Node 5 - choose and set IDs of LDSITIES for ETL [SOURCE DB] -> next step: Node 3')
            lst = []
            # number of elements as input
            n = int(input("Enter number of ldsities and IDs [SOURCE DB]: "))
            # iterating till the range
            for i in range(0, n):
                ele = int(input())
                lst.append(ele) # adding the element
            print(lst)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 6 and ETL of UNIT OF MEASURE]
            ###############################################################################
            print('ETL - LDSITIES [node 6 and ETL of UNIT OF MEASURE]')
            print('--------------------------------------------------------------')
            ##############################################    Node 6
            sql_cmd_l6_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_unit_of_measure = false;'''
            #sql_cmd_l6_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_unit_of_measure = false;'''
            data_l6 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l6_check = run_sql(conn_src,sql_cmd_l6_check,data_l6)[0][0]
            #print(res_l6_check)
            if res_l6_check == 0:
                print('Node 6 - Is the TRUE value filled in for all selected ldsities in column check_unit_of_measure? -> YES -> next step: CHECK DEFINITION VARIANT')
            else:
                print('Node 6 - Is the TRUE value filled in for all selected ldsities in column check_unit_of_measure? -> NO')
                sql_cmd_l6_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_unit_of_measure = false order by ldsity;'''
                #sql_cmd_l6_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_unit_of_measure = false order by ldsity;'''
                res_l6_list = run_sql(conn_src,sql_cmd_l6_list,data_l6)
                ##############################################    CYCLE
                print('CYCLE - UNIT OF MEASURE [cycle for all selected ldsities where check_unit_of_measure = FALSE]')
                for r in res_l6_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## Cycle Node 1
                    print('Node 1 - Get unit of measure [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_unit_of_measure(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_unit_of_measure(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list) # tady v listu bude vzdy jen jedna jednotka
                    print('-----------------------------------')
                    print('UNIT_OF_MEASURE_ID: %s' % (res_c1_sdb_get_list[0]['id']))
                    print('UNIT_OF_MEASURE_LABEL_EN: %s' % (res_c1_sdb_get_list[0]['label_en']))
                    ########################################## Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## Cycle Node 3
                        print('Node 3 - Get unit of measures [TARGET DB]')
                        # preparing cmd and data for node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_unit_of_measure__etl_id order by etl_unit_of_measure__etl_id) from analytical.fn_etl_get_unit_of_measure(%s,null::integer,true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_unit_of_measures(%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_unit_of_measures(%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[0]['id'],)
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_unit_of_measures(%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_unit_of_measures(%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[0]['id'],res_c3_sdb_get_etl_id)
                        
                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle Node 9
                            print('Node 9 - Save unit of measure [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_unit_of_measure(%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[0]['id'],res_c1_sdb_get_list[0]['label'],res_c1_sdb_get_list[0]['description'],res_c1_sdb_get_list[0]['label_en'],res_c1_sdb_get_list[0]['description_en'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                            #print(res_c9_tdb_save)
                            ########################################## Cycle Node 11
                            print('Node 11 - Save unit of measure [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_unit_of_measure(%s,%s,%s);'''
                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[0]['id'],res_c9_tdb_save)
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## Cycle Node 5
                            print('Node 5 - Check unit of measure [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_unit_of_measure(%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_unit_of_measure(%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[0]['id'],res_c1_sdb_get_list[0]['label_en'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the unit of measure')
                                for cc in res_c3_tdb_get_list:
                                    print('UoM: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['unit_of_measure'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                ########################################## Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## Cycle Node 9
                                    print('Node 9 - Save unit of measure [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_unit_of_measure(%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[0]['id'],res_c1_sdb_get_list[0]['label'],res_c1_sdb_get_list[0]['description'],res_c1_sdb_get_list[0]['label_en'],res_c1_sdb_get_list[0]['description_en'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                    #print(res_c9_tdb_save)
                                    ########################################## Cycle Node 11
                                    print('Node 11 - Save unit of measure [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_unit_of_measure(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[0]['id'],res_c9_tdb_save)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## Cycle Node 11
                                    print('Node 11 - Save unit of measure [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of unit of measure [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_unit_of_measure(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[0]['id'],input_c11_tdb)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('UoM: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['unit_of_measure'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the unit of measure')
                                    for cc in res_c3_tdb_get_list:
                                        print('UoM: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['unit_of_measure'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## Cycle Node 9
                                        print('Node 9 - Save unit of measure [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_unit_of_measure(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[0]['id'],res_c1_sdb_get_list[0]['label'],res_c1_sdb_get_list[0]['description'],res_c1_sdb_get_list[0]['label_en'],res_c1_sdb_get_list[0]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## Cycle Node 11
                                        print('Node 11 - Save unit of measure [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_unit_of_measure(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[0]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## Cycle Node 11
                                        print('Node 11 - Save unit of measure [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of unit of measure [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_unit_of_measure(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[0]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## Cycle Node 11
                                    print('Node 11 - Save unit of measure [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_unit_of_measure(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[0]['id'],res_c5_tdb_check_etl_id)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 7 and ETL of DEFINITION VARIANT]
            ###############################################################################
            print('ETL - LDSITIES [node 7 and ETL of DEFINITION VARIANT]')
            print('--------------------------------------------------------------')
            ##############################################    Node 7
            sql_cmd_l7_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_definition_variant = false;'''
            #sql_cmd_l7_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_definition_variant = false;'''
            data_l7 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l7_check = run_sql(conn_src,sql_cmd_l7_check,data_l7)[0][0]
            #print(res_l7_check)
            if res_l7_check == 0:
                print('Node 7 - Is the TRUE value filled in for all selected ldsities in column check_definition_variant? -> YES -> next step: CHECK AREA DOMAIN')
            else:
                print('Node 7 - Is the TRUE value filled in for all selected ldsities in column check_definition_variant? -> NO')
                sql_cmd_l7_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_definition_variant = false order by ldsity;'''
                #sql_cmd_l7_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_definition_variant = false order by ldsity;'''
                res_l7_list = run_sql(conn_src,sql_cmd_l7_list,data_l7)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - DEFINITION VARIANT [cycle for all selected ldsities where check_definition_variant = FALSE]')
                for r in res_l7_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get definition variant [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_definition_variant(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_definition_variant(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2     #640r
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - DEFINITION VARIANT [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('DEFINITION_VARIANT_ID: %s' % (res_c1_sdb_get_list[i]['id']))
                            print('DEFINITION_VARIANT_LABEL_EN: %s' % (res_c1_sdb_get_list[i]['label_en']))
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_definition_variant__etl_id order by etl_definition_variant__etl_id) from analytical.fn_etl_get_definition_variant(%s,null::integer,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,)
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get definition variants [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_definition_variants(%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_definition_variants(%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],)
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_definition_variants(%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_definition_variants(%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4     #660r
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save definition variant [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_definition_variant(%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                #print(res_c9_tdb_save)
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save definition variant [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_definition_variant(%s,%s,%s);'''
                                data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                            else: #675r
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check definition variant [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_definition_variant(%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_definition_variant(%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label_en'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6     #686r
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the definiton variant')
                                    for cc in res_c3_tdb_get_list:
                                        print('DV: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['definition_variant'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save definition variant [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_definition_variant(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save definition variant [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_definition_variant(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save definition variant [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of definition varinat [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_definition_variant(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)    #724r
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('DV: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['definition_variant'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break   #737r

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the definition variant')
                                        for cc in res_c3_tdb_get_list:
                                            print('DV: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['definition_variant'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save definition variant [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_definition_variant(%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                            #print(res_c9_tdb_save)
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save definition variant [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_definition_variant(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save definition variant [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of definition variant [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_definition_variant(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:   #777r
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save definition variant [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_definition_variant(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################


            ### ADC ###
            ##############################################################################################################################################################
            ##############################################################################################################################################################
            ###############################################################################
            # LDSITIES [node 8 and ETL of AREA DOMAIN]
            ###############################################################################
            print('--------------------------------------------------------------')
            print('ETL - LDSITIES [node 8 and ETL of AREA DOMAIN]')
            print('--------------------------------------------------------------')
            ##############################################    Node 8
            sql_cmd_l8_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_area_domain = false;'''
            #sql_cmd_l8_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_area_domain = false;'''
            data_l8 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l8_check = run_sql(conn_src,sql_cmd_l8_check,data_l8)[0][0]
            #print(res_l8_check)
            if res_l8_check == 0:
                print('Node 8 - Is the TRUE value filled in for all selected ldsities in column check_area_domain? -> YES -> next step: CHECK AREA DOMAIN CATEGORY')
            else:
                print('Node 8 - Is the TRUE value filled in for all selected ldsities in column check_area_domain? -> NO')
                sql_cmd_l8_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_area_domain = false order by ldsity;'''
                #sql_cmd_l8_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_area_domain = false order by ldsity;'''
                res_l8_list = run_sql(conn_src,sql_cmd_l8_list,data_l8)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - AREA DOMAIN [cycle for all selected ldsities where check_area_domain = FALSE]')
                for r in res_l8_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get area domain [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_area_domain(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_area_domain(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - AREA DOMAIN [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('AD_LABEL_EN: %s' % (res_c1_sdb_get_list[i]['label_en']))
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_area_domain__etl_id order by etl_area_domain__etl_id) from analytical.fn_etl_get_area_domain(%s,null::integer,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,)
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get area domains [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domains(%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domains(%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],)
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domains(%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domains(%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save area domain [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain(%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                #print(res_c9_tdb_save)
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save area domain [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check area domain [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_area_domain(%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_area_domain(%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label_en'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the area domain')
                                    for cc in res_c3_tdb_get_list:
                                        print('AD: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save area domain [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area_domain [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('AD: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['area_domain'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the area domain')
                                        for cc in res_c3_tdb_get_list:
                                            print('AD: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save area domain [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain(%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                            #print(res_c9_tdb_save)
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save area domain [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save area domain [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 9 and ETL of AREA DOMAIN CATEGORY]
            ###############################################################################
            print('ETL - LDSITIES [node 9 and ETL of AREA DOMAIN CATEGORY]')
            print('--------------------------------------------------------------')
            ##############################################    Node 9
            sql_cmd_l9_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_area_domain_category = false;'''
            #sql_cmd_l9_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_area_domain_category = false;'''
            data_l9 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l9_check = run_sql(conn_src,sql_cmd_l9_check,data_l9)[0][0]
            #print(res_l9_check)
            if res_l9_check == 0:
                print('Node 9 - Is the TRUE value filled in for all selected ldsities in column check_area_domain_category? -> YES -> next step: CHECK AREA DOMAIN CATEGORY HIERARCHY')
            else:
                print('Node 9 - Is the TRUE value filled in for all selected ldsities in column check_area_domain_category? -> NO')
                sql_cmd_l9_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_area_domain_category = false order by ldsity;'''
                #sql_cmd_l9_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_area_domain_category = false order by ldsity;'''
                res_l9_list = run_sql(conn_src,sql_cmd_l9_list,data_l9)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - AREA DOMAIN CATEGORY [cycle for all selected ldsities where check_area_domain_category = FALSE]')
                for r in res_l9_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get area domain category [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_area_domain_category(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_area_domain_category(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - AREA DOMAIN CATEGORY [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('ADC_LABEL_EN: %s' % (res_c1_sdb_get_list[i]['label_en']))                        
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_area_domain_category__etl_id order by etl_area_domain_category__etl_id) from analytical.fn_etl_get_area_domain_category(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get area domain categories [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domain_categories(%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domain_categories(%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domain_categories(%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domain_categories(%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save area domain category [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                #print(res_c9_tdb_save)
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save area domain category [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check area domain category [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_area_domain_category(%s,%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_area_domain_category(%s,%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label_en'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the area domain category')
                                    for cc in res_c3_tdb_get_list:
                                        print('ADC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save area domain category [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area_domain category [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain category [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of area domain category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('ADC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s AD_LABEL_EN: %s' % (ccc['area_domain_category'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en'], ccc['area_domain__label_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the area domain category')
                                        for cc in res_c3_tdb_get_list:
                                            print('ADC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save area domain category [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                            #print(res_c9_tdb_save)
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save area domain category [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save area domain category [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of area domain category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain category [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 10 and ETL of AREA DOMAIN CATEGORY HIERARCHY]
            ###############################################################################
            print('ETL - LDSITIES [node 10 and ETL of AREA DOMAIN CATEGORY HIERARCHY]')
            print('--------------------------------------------------------------')
            ##############################################    Node 10
            sql_cmd_l10_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_adc_hierarchy = false;'''
            #sql_cmd_l10_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_adc_hierarchy = false;'''
            data_l10 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l10_check = run_sql(conn_src,sql_cmd_l10_check,data_l10)[0][0]
            #print(res_l10_check)
            if res_l10_check == 0:
                print('Node 10 - Is the TRUE value filled in for all selected ldsities in column check_adc_hierarchy? -> YES -> next step: CHECK ADC2CLASSIFICATION_RULE')
            else:
                print('Node 10 - Is the TRUE value filled in for all selected ldsities in column check_adc_hierarchy? -> NO')
                sql_cmd_l10_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_adc_hierarchy = false order by ldsity;'''
                #sql_cmd_l10_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_adc_hierarchy = false order by ldsity;'''
                res_l10_list = run_sql(conn_src,sql_cmd_l10_list,data_l10)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - AREA DOMAIN CATEGORY HIERARCHY [cycle for all selected ldsities where check_adc_hierarchy = FALSE]')
                for r in res_l10_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get area domain category hierarchy [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_adc_hierarchy(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_adc_hierarchy(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - AREA DOMAIN CATEGORY HIERARCHY [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_adc_hierarchy__etl_id order by etl_adc_hierarchy__etl_id) from analytical.fn_etl_get_adc_hierarchy(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get area domain category hierarchies [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 5
                                print('Node 5 - Save area domain category hierarchy [TARGET DB]')
                                sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_adc_hierarchy(%s,%s,%s,%s);'''
                                data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                                res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                                #print(res_c5_tdb_save)
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save area domain category hierarchy [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc_hierarchy(%s,%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable'],res_c5_tdb_save)
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 6
                                # check that number of rows in res_c3_tdb_get_list must be only one row
                                if res_c3_tdb_get_count == 1:
                                    print('Node 6 - Is returned one row in node 3? -> YES')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Save area domain category hierarchy [SOURCE DB]')
                                    sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc_hierarchy(%s,%s,%s,%s);'''
                                    data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable'],res_c3_tdb_get_list[0]['etl_id'])
                                    res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                    #print(res_c7_sdb_save)
                                else:
                                    print('Node 6 - Is returned one row in node 3? -> NO')
                                    print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                    sys.exit(1)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 11 and ETL of ADC2CLASSIFICATION_RULE]
            ###############################################################################
            print('ETL - LDSITIES [node 11 and ETL of ADC2CLASSIFICATION_RULE]')
            print('--------------------------------------------------------------')
            ##############################################    Node 11
            sql_cmd_l11_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_adc2classification_rule = false;'''
            #sql_cmd_l11_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_adc2classification_rule = false;'''
            data_l11 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l11_check = run_sql(conn_src,sql_cmd_l11_check,data_l11)[0][0]
            #print(res_l11_check)
            if res_l11_check == 0:
                print('Node 11 - Is the TRUE value filled in for all selected ldsities in column check_adc2classification_rule? -> YES -> next step: CHECK ADC2CLASSRULE2PANEL_REFYEARSET')
            else:
                print('Node 11 - Is the TRUE value filled in for all selected ldsities in column check_adc2classification_rule? -> NO')
                sql_cmd_l11_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_adc2classification_rule = false order by ldsity;'''
                #sql_cmd_l11_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_adc2classification_rule = false order by ldsity;'''
                res_l11_list = run_sql(conn_src,sql_cmd_l11_list,data_l11)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - ADC2CLASSIFICATION_RULE [cycle for all selected ldsities where check_adc2classification_rule = FALSE]')
                for r in res_l11_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get adc2classification_rule [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_adc2classification_rule(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_adc2classification_rule(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - ADC2CLASSIFICATION_RULE [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('ADC2CLASSIFICATION_RULE: %s' % (res_c1_sdb_get_list[i]['classification_rule']))                         
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_adc2classification_rule__etl_id order by etl_adc2classification_rule__etl_id) from analytical.fn_etl_get_adc2classification_rule(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get adc2classification_rules [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save adc2classification_rule [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_adc2classification_rule(%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__refyearset2panel'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                #print(res_c9_tdb_save)
                                #print(res_c9_tdb_save[0]['adc2classification_rule'])
                                #print(res_c9_tdb_save[0]['etl_id'])
                                #print(res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'])
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                                ########################################## II. Cycle Node 12
                                print('Node 12 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                #print(res_c12_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check adc2classification_rule [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_adc2classification_rule(%s,%s,%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_adc2classification_rule(%s,%s,%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the adc2classification_rule')
                                    for cc in res_c3_tdb_get_list:
                                        print('ADC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    ADC_LABEL_EN: %s' % (cc['adc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['area_domain_category__label_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save adc2classification_rule [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_adc2classification_rule(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__refyearset2panel'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                        #print(res_c9_tdb_save)[0]['etl_id']
                                        #print(res_c9_tdb_save)[0]['cm_adc2classrule2panel_refyearset__etl_id']
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)                            
                                        ########################################## II. Cycle Node 12
                                        print('Node 12 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                        sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                        data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                        res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                        #print(res_c12_sdb_save)                            
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of adc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('ADC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    ADC_LABEL_EN: %s' % (ccc['adc2classification_rule'], ccc['etl_id'], ccc['classification_rule'], ccc['ldsity_object__label_en'], ccc['area_domain_category__label_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the adc2classification_rule')
                                        for cc in res_c3_tdb_get_list:
                                            print('ADC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    ADC_LABEL_EN: %s' % (cc['adc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['area_domain_category__label_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save adc2classification_rule [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_adc2classification_rule(%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__refyearset2panel'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                            #print(res_c9_tdb_save)[0]['etl_id']
                                            #print(res_c9_tdb_save)[0]['cm_adc2classrule2panel_refyearset__etl_id']
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                            ########################################## II. Cycle Node 12
                                            print('Node 12 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                            sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                            data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                            res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                            #print(res_c12_sdb_save)                                 
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of adc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################


            
            ###############################################################################
            # LDSITIES [node 12 and ETL of ADC2CLASSRULE2PANEL_REFYEARSET]
            ###############################################################################
            print('ETL - LDSITIES [node 12 and ETL of ADC2CLASSRULE2PANEL_REFYEARSET]')
            print('--------------------------------------------------------------')
            ##############################################    Node 12
            sql_cmd_l12_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_adc2classrule2panel_refyearset = false;'''
            #sql_cmd_l12_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_adc2classrule2panel_refyearset = false;'''
            data_l12 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l12_check = run_sql(conn_src,sql_cmd_l12_check,data_l12)[0][0]
            #print(res_l12_check)
            if res_l12_check == 0:
                print('Node 12 - Is the TRUE value filled in for all selected ldsities in column check_adc2classrule2panel_refyearset? -> YES -> next step: ETL SUB POPULATION')
            else:
                print('Node 12 - Is the TRUE value filled in for all selected ldsities in column check_adc2classrule2panel_refyearset? -> NO')
                sql_cmd_l12_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_adc2classrule2panel_refyearset = false order by ldsity;'''
                #sql_cmd_l12_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_adc2classrule2panel_refyearset = false order by ldsity;'''
                res_l12_list = run_sql(conn_src,sql_cmd_l12_list,data_l12)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - ADC2CLASSRULE2PANEL_REFYEARSET [cycle for all selected ldsities where check_adc2classrule2panel_refyearset = FALSE]')
                for r in res_l12_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get adc2classrule2panel_refyearset [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_adc2classrule2panel_refyearset(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_adc2classrule2panel_refyearset(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - ADC2CLASSRULE2PANEL_REFYEARSET [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('ADC2CLASSRULE2PANEL_REFYEARSET [id]: %s' % (res_c1_sdb_get_list[i]['id']))
                            print('ADC2CLASSRULE2PANEL_REFYEARSET [etl_adc2classification_rule__etl_id]: %s' % (res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id']))   
                            print('ADC2CLASSRULE2PANEL_REFYEARSET [refyearset2panel]: %s' % (res_c1_sdb_get_list[i]['refyearset2panel']))    
                            print('ADC2CLASSRULE2PANEL_REFYEARSET [adc2classrule2panel_refyearset]: %s' % (res_c1_sdb_get_list[i]['adc2classrule2panel_refyearset']))                         
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_adc2classrule2panel_refyearset__etl_id order by etl_adc2classrule2panel_refyearset__etl_id) from analytical.fn_etl_get_adc2classrule2panel_refyearset(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get adc2classrule2panel_refyearsets [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 5
                                print('Node 5 - Save adc2classrule2panel_refyearset [TARGET DB]')
                                sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                                res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                                #print(res_c5_tdb_save)
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['adc2classrule2panel_refyearset'],res_c5_tdb_save,res_c1_sdb_get_list[i]['etl_adc2classification_rule__id'])
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 6
                                # check that number of rows in res_c3_tdb_get_list must be only one row
                                if res_c3_tdb_get_count == 1:
                                    print('Node 6 - Is returned one row in node 3? -> YES')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                    sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                    data_c7_sdb_save = (res_c1_sdb_get_list[i]['adc2classrule2panel_refyearset'],res_c3_tdb_get_list[0]['etl_id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__id'])
                                    res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                    #print(res_c7_sdb_save)
                                else:
                                    print('Node 6 - Is returned one row in node 3? -> NO')
                                    print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                    sys.exit(1)
            print('--------------------------------------------------------------')
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################
            ##############################################################################################################################################################
            ##############################################################################################################################################################



            ### SPC ###
            ##############################################################################################################################################################
            ##############################################################################################################################################################
            ###############################################################################
            # LDSITIES [node 8 and ETL of SUB POPULATION]
            ###############################################################################
            print('--------------------------------------------------------------')
            print('ETL - LDSITIES [node 8 and ETL of SUB POPULATION]')
            print('--------------------------------------------------------------')
            ##############################################    Node 8
            sql_cmd_l8_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_sub_population = false;'''
            data_l8 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l8_check = run_sql(conn_src,sql_cmd_l8_check,data_l8)[0][0]
            #print(res_l8_check)
            if res_l8_check == 0:
                print('Node 8 - Is the TRUE value filled in for all selected ldsities in column check_sub_population? -> YES -> next step: CHECK SUB POPULATION CATEGORY')
            else:
                print('Node 8 - Is the TRUE value filled in for all selected ldsities in column check_sub_population? -> NO')
                sql_cmd_l8_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_sub_population = false order by ldsity;'''
                res_l8_list = run_sql(conn_src,sql_cmd_l8_list,data_l8)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - SUB POPULATION [cycle for all selected ldsities where check_sub_population = FALSE]')
                for r in res_l8_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get sub population [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_sub_population(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_sub_population(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - SUB POPULATION [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('SP_LABEL_EN: %s' % (res_c1_sdb_get_list[i]['label_en']))                        
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_sub_population__etl_id order by etl_sub_population__etl_id) from analytical.fn_etl_get_sub_population(%s,null::integer,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,)
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get sub populations [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_populations(%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_populations(%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],)
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_populations(%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_populations(%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save sub population [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population(%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                #print(res_c9_tdb_save)
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save sub population [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check sub population [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_sub_population(%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_sub_population(%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label_en'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the sub population')
                                    for cc in res_c3_tdb_get_list:
                                        print('SP: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save sub population [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of sub population [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('SP: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['sub_population'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the sub population')
                                        for cc in res_c3_tdb_get_list:
                                            print('SP: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save sub population [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population(%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                            #print(res_c9_tdb_save)
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save sub population [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save sub population [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of sub population [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 9 and ETL of SUB POPULATION CATEGORY]
            ###############################################################################
            print('ETL - LDSITIES [node 9 and ETL of SUB POPULATION CATEGORY]')
            print('--------------------------------------------------------------')
            ##############################################    Node 9
            sql_cmd_l9_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_sub_population_category = false;'''
            data_l9 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l9_check = run_sql(conn_src,sql_cmd_l9_check,data_l9)[0][0]
            #print(res_l9_check)
            if res_l9_check == 0:
                print('Node 9 - Is the TRUE value filled in for all selected ldsities in column check_sub_population_category? -> YES -> next step: CHECK SUB POPULATION CATEGORY HIERARCHY')
            else:
                print('Node 9 - Is the TRUE value filled in for all selected ldsities in column check_sub_population_category? -> NO')
                sql_cmd_l9_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_sub_population_category = false order by ldsity;'''
                res_l9_list = run_sql(conn_src,sql_cmd_l9_list,data_l9)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - SUB POPULATION CATEGORY [cycle for all selected ldsities where check_sub_population_category = FALSE]')
                for r in res_l9_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get sub population category [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_sub_population_category(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_sub_population_category(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - SUB POPULATION CATEGORY [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('SPC_LABEL_EN: %s' % (res_c1_sdb_get_list[i]['label_en']))                        
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_sub_population_category__etl_id order by etl_sub_population_category__etl_id) from analytical.fn_etl_get_sub_population_category(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get sub population categories [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_population_categories(%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_population_categories(%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_population_categories(%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_population_categories(%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save sub population category [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                #print(res_c9_tdb_save)
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save sub population category [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check sub population category [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_sub_population_category(%s,%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_sub_population_category(%s,%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label_en'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the sub population category')
                                    for cc in res_c3_tdb_get_list:
                                        print('SPC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save sub population category [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population category [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population category [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of sub population category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('SPC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s AD_LABEL_EN: %s' % (ccc['sub_population_category'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en'], ccc['sub_population__label_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the sub population category')
                                        for cc in res_c3_tdb_get_list:
                                            print('SPC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save sub population category [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                            #print(res_c9_tdb_save)
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save sub population category [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save sub population category [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of sub population category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population category [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 10 and ETL of SUB POPULATION CATEGORY HIERARCHY]
            ###############################################################################
            print('ETL - LDSITIES [node 10 and ETL of SUB POPULATION CATEGORY HIERARCHY]')
            print('--------------------------------------------------------------')
            ##############################################    Node 10
            sql_cmd_l10_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_spc_hierarchy = false;'''
            data_l10 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l10_check = run_sql(conn_src,sql_cmd_l10_check,data_l10)[0][0]
            #print(res_l10_check)
            if res_l10_check == 0:
                print('Node 10 - Is the TRUE value filled in for all selected ldsities in column check_spc_hierarchy? -> YES -> next step: CHECK SPC2CLASSIFICATION_RULE')
            else:
                print('Node 10 - Is the TRUE value filled in for all selected ldsities in column check_spc_hierarchy? -> NO')
                sql_cmd_l10_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_spc_hierarchy = false order by ldsity;'''
                res_l10_list = run_sql(conn_src,sql_cmd_l10_list,data_l10)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - SUB POPULATION CATEGORY HIERARCHY [cycle for all selected ldsities where check_spc_hierarchy = FALSE]')
                for r in res_l10_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get sub population category hierarchy [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_spc_hierarchy(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_spc_hierarchy(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - SUB POPULATION CATEGORY HIERARCHY [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_spc_hierarchy__etl_id order by etl_spc_hierarchy__etl_id) from analytical.fn_etl_get_spc_hierarchy(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get sub population category hierarchies [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 5
                                print('Node 5 - Save sub population category hierarchy [TARGET DB]')
                                sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_spc_hierarchy(%s,%s,%s,%s);'''
                                data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                                res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                                #print(res_c5_tdb_save)
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save sub population category hierarchy [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc_hierarchy(%s,%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable'],res_c5_tdb_save)
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 6
                                # check that number of rows in res_c3_tdb_get_list must be only one row
                                if res_c3_tdb_get_count == 1:
                                    print('Node 6 - Is returned one row in node 3? -> YES')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Save sub population category hierarchy [SOURCE DB]')
                                    sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc_hierarchy(%s,%s,%s,%s);'''
                                    data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable'],res_c3_tdb_get_list[0]['etl_id'])
                                    res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                    #print(res_c7_sdb_save)
                                else:
                                    print('Node 6 - Is returned one row in node 3? -> NO')
                                    print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                    sys.exit(1)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 11 and ETL of SPC2CLASSIFICATION_RULE]
            ###############################################################################
            print('ETL - LDSITIES [node 11 and ETL of SPC2CLASSIFICATION_RULE]')
            print('--------------------------------------------------------------')
            ##############################################    Node 11
            sql_cmd_l11_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_spc2classification_rule = false;'''
            data_l11 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l11_check = run_sql(conn_src,sql_cmd_l11_check,data_l11)[0][0]
            #print(res_l11_check)
            if res_l11_check == 0:
                print('Node 11 - Is the TRUE value filled in for all selected ldsities in column check_spc2classification_rule? -> YES -> next step: CHECK SPC2CLASSRULE2PANEL_REFYEARSET')
            else:
                print('Node 11 - Is the TRUE value filled in for all selected ldsities in column check_spc2classification_rule? -> NO')
                sql_cmd_l11_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_spc2classification_rule = false order by ldsity;'''
                res_l11_list = run_sql(conn_src,sql_cmd_l11_list,data_l11)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - SPC2CLASSIFICATION_RULE [cycle for all selected ldsities where check_spc2classification_rule = FALSE]')
                for r in res_l11_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get spc2classification_rule [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_spc2classification_rule(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_spc2classification_rule(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - SPC2CLASSIFICATION_RULE [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('SPC2CLASSIFICATION_RULE: %s' % (res_c1_sdb_get_list[i]['classification_rule']))                         
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_spc2classification_rule__etl_id order by etl_spc2classification_rule__etl_id) from analytical.fn_etl_get_spc2classification_rule(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get spc2classification_rules [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save spc2classification_rule [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_spc2classification_rule(%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__refyearset2panel'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                #print(res_c9_tdb_save)
                                #print(res_c9_tdb_save[0]['spc2classification_rule'])
                                #print(res_c9_tdb_save[0]['etl_id'])
                                #print(res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'])
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                                ########################################## II. Cycle Node 12
                                print('Node 12 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                #print(res_c12_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check spc2classification_rule [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_spc2classification_rule(%s,%s,%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_spc2classification_rule(%s,%s,%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the spc2classification_rule')
                                    for cc in res_c3_tdb_get_list:
                                        print('SPC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    SPC_LABEL_EN: %s' % (cc['spc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['sub_population_category__label_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save spc2classification_rule [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_spc2classification_rule(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__refyearset2panel'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                        #print(res_c9_tdb_save)[0]['etl_id']
                                        #print(res_c9_tdb_save)[0]['cm_spc2classrule2panel_refyearset__etl_id']
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)                            
                                        ########################################## II. Cycle Node 12
                                        print('Node 12 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                        sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                        data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                        res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                        #print(res_c12_sdb_save)                            
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of spc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('SPC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    SPC_LABEL_EN: %s' % (ccc['spc2classification_rule'], ccc['etl_id'], ccc['classification_rule'], ccc['ldsity_object__label_en'], ccc['sub_population_category__label_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the spc2classification_rule')
                                        for cc in res_c3_tdb_get_list:
                                            print('SPC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    SPC_LABEL_EN: %s' % (cc['spc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['sub_population_category__label_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save spc2classification_rule [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_spc2classification_rule(%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__refyearset2panel'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                            #print(res_c9_tdb_save)[0]['etl_id']
                                            #print(res_c9_tdb_save)[0]['cm_spc2classrule2panel_refyearset__etl_id']
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                            ########################################## II. Cycle Node 12
                                            print('Node 12 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                            sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                            data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                            res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                            #print(res_c12_sdb_save)                                 
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of spc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 12 and ETL of SPC2CLASSRULE2PANEL_REFYEARSET]
            ###############################################################################
            print('ETL - LDSITIES [node 12 and ETL of SPC2CLASSRULE2PANEL_REFYEARSET]')
            print('--------------------------------------------------------------')
            ##############################################    Node 12
            sql_cmd_l12_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_spc2classrule2panel_refyearset = false;'''
            data_l12 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l12_check = run_sql(conn_src,sql_cmd_l12_check,data_l12)[0][0]
            #print(res_l12_check)
            if res_l12_check == 0:
                print('Node 12 - Is the TRUE value filled in for all selected ldsities in column check_spc2classrule2panel_refyearset? -> YES -> next step: CHECK ETL_LDSITY__ETL_ID')
            else:
                print('Node 12 - Is the TRUE value filled in for all selected ldsities in column check_spc2classrule2panel_refyearset? -> NO')
                sql_cmd_l12_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,null::integer[]) where ldsity in (select unnest(%s)) and check_spc2classrule2panel_refyearset = false order by ldsity;'''
                res_l12_list = run_sql(conn_src,sql_cmd_l12_list,data_l12)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - SPC2CLASSRULE2PANEL_REFYEARSET [cycle for all selected ldsities where check_spc2classrule2panel_refyearset = FALSE]')
                for r in res_l12_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get spc2classrule2panel_refyearset [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_spc2classrule2panel_refyearset(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_spc2classrule2panel_refyearset(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - SPC2CLASSRULE2PANEL_REFYEARSET [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            print('-----------------------------------')
                            print('SPC2CLASSRULE2PANEL_REFYEARSET [id]: %s' % (res_c1_sdb_get_list[i]['id']))
                            print('SPC2CLASSRULE2PANEL_REFYEARSET [etl_spc2classification_rule__etl_id]: %s' % (res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id']))   
                            print('SPC2CLASSRULE2PANEL_REFYEARSET [refyearset2panel]: %s' % (res_c1_sdb_get_list[i]['refyearset2panel']))    
                            print('SPC2CLASSRULE2PANEL_REFYEARSET [spc2classrule2panel_refyearset]: %s' % (res_c1_sdb_get_list[i]['spc2classrule2panel_refyearset']))
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_spc2classrule2panel_refyearset__etl_id order by etl_spc2classrule2panel_refyearset__etl_id) from analytical.fn_etl_get_spc2classrule2panel_refyearset(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get spc2classrule2panel_refyearsets [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 5
                                print('Node 5 - Save spc2classrule2panel_refyearset [TARGET DB]')
                                sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                                res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                                #print(res_c5_tdb_save)
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['spc2classrule2panel_refyearset'],res_c5_tdb_save,res_c1_sdb_get_list[i]['etl_spc2classification_rule__id'])
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 6
                                # check that number of rows in res_c3_tdb_get_list must be only one row
                                if res_c3_tdb_get_count == 1:
                                    print('Node 6 - Is returned one row in node 3? -> YES')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                    sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                    data_c7_sdb_save = (res_c1_sdb_get_list[i]['spc2classrule2panel_refyearset'],res_c3_tdb_get_list[0]['etl_id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__id'])
                                    res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                    #print(res_c7_sdb_save)
                                else:
                                    print('Node 6 - Is returned one row in node 3? -> NO')
                                    print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                    sys.exit(1)
            print('--------------------------------------------------------------')
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################
            ##############################################################################################################################################################
            ##############################################################################################################################################################



            ###############################################################################
            # LDSITIES [part from node 13 to node 15]
            ###############################################################################
            print('ETL - LDSITIES [part from node 13 to node 15]')
            print('--------------------------------------------------------------')
            ##############################################    Node 13
            print('Node 13 - Get ldsities4object [SOURCE DB]')
            sql_cmd_l13_count_check_ldsity_part = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_ldsity_part = false;'''
            sql_cmd_l13_count_etl_ldsity__etl_id = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where etl_ldsity__etl_id is null;'''
            sql_cmd_l13_list = '''select * from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where etl_ldsity__etl_id is null;'''
            data_l13 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l13_count_check_ldsity_part = run_sql(conn_src,sql_cmd_l13_count_check_ldsity_part,data_l13)[0][0]
            res_l13_count_etl_ldsity__etl_id = run_sql(conn_src,sql_cmd_l13_count_etl_ldsity__etl_id,data_l13)[0][0]
            res_l13_list = run_sql(conn_src,sql_cmd_l13_list,data_l13)
            #print(res_l13_count_check_ldsity_part)
            #print(res_l13_count_etl_ldsity__etl_id)
            #print(res_l13_list)
            ##############################################    Node 14
            if res_l13_count_check_ldsity_part > 0:
                print('Node 14 - Is the TRUE value in node 13 filled in for all ldsities in column check_ldsity_part? -> NO -> STOP process ETL !!!')
                sys.exit(1)
            else:
                print('Node 14 - Is the TRUE value in node 13 filled in for all ldsities in column check_ldsity_part? -> YES')
                ##############################################    Node 15
                if res_l13_count_etl_ldsity__etl_id > 0:
                    print('Node 15 - Is the value ETL_ID in node 13 filled in for all ldsities in column etl_ldsity__etl_id? -> NO -> next step: ETL LDSITIES [Cycle for all ldsities in node 13 where etl_ldsity__etl_id IS NULL]')
                    #########################################################
                    ##############################################  CYCLE
                    print('CYCLE - LDSITIES [cycle for all ldsities where etl_ldsity__etl_id IS NULL]')
                    for i, r in enumerate(res_l13_list):
                        print('-----------------------------------')
                        print('LDSITY = %s' % (res_l13_list[i]['ldsity']))
                        print('LDSITY = %s' % (res_l13_list[i]['ldsity_label_en']))
                        ########################################## Cycle Node 1
                        # preparing cmd and data [array of ETL_IDs] for Node 1
                        #sql_cmd_c1_sdb_get_etl_id = '''select array_agg(etl_ldsity__etl_id order by etl_ldsity__etl_id) from analytical.fn_etl_get_ldsities4object(%s,%s,null::boolean,null::integer[],true);'''     # tady 4 argument ma byt nejspis null::integer[]
                        sql_cmd_c1_sdb_get_etl_id = '''select * from analytical.fn_etl_get_etl_ids_of_ldsities(%s);'''
                        #data_c1_sdb_get_etl_id = (node7_conn,lo4_input_ldsity_object)
                        data_c1_sdb_get_etl_id = (node7_conn,)
                        res_c1_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c1_sdb_get_etl_id,data_c1_sdb_get_etl_id)[0][0]
                        #print(res_c1_sdb_get_etl_id)

                        print('Node 1 - Get ldsities [TARGET DB]')
                        if res_c1_sdb_get_etl_id == None:
                            sql_cmd_c1_tdb_get_count = '''select count(*) from target_data.fn_import_get_ldsities(%s,%s,null::integer[]);'''
                            sql_cmd_c1_tdb_get_list = '''select * from target_data.fn_import_get_ldsities(%s,%s,null::integer[]);'''
                            data_c1_tdb_get = (res_l13_list[i]['ldsity_object__import'],res_l13_list[i]['ldsity'],)
                        else:
                            sql_cmd_c1_tdb_get_count = '''select count(*) from target_data.fn_import_get_ldsities(%s,%s,%s);'''
                            sql_cmd_c1_tdb_get_list = '''select * from target_data.fn_import_get_ldsities(%s,%s,%s);'''
                            data_c1_tdb_get = (res_l13_list[i]['ldsity_object__import'],res_l13_list[i]['ldsity'],res_c1_sdb_get_etl_id)

                        res_c1_tdb_get_count = run_sql(conn_dest,sql_cmd_c1_tdb_get_count,data_c1_tdb_get)[0][0]
                        res_c1_tdb_get_list = run_sql(conn_dest,sql_cmd_c1_tdb_get_list,data_c1_tdb_get)
                        #print(res_c1_tdb_get_count)
                        #print(res_c1_tdb_get_list)
                        ########################################## Cycle Node 2         II. Cycle Node 4
                        if res_c1_tdb_get_count == 0:
                            print('Node 2 - Any record returned? -> NO')
                            ########################################## Cycle Node 7     Cycle II. Node 9
                            print('Node 7 - Save ldsity [TARGET DB]')
                            sql_cmd_c7_tdb_save = '''select * from target_data.fn_import_save_ldsity(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
                            data_c7_tdb_save = (res_l13_list[i]['ldsity'],res_l13_list[i]['ldsity_label'],res_l13_list[i]['ldsity_description'],res_l13_list[i]['ldsity_label_en'],res_l13_list[i]['ldsity_description_en'],res_l13_list[i]['ldsity_object__import'],res_l13_list[i]['ldsity_column_expression'],res_l13_list[i]['unit_of_measure__import'],res_l13_list[i]['area_domain_category__import'],res_l13_list[i]['sub_population_category__import'],res_l13_list[i]['definition_variant__import'])
                            res_c7_tdb_save =run_sql(conn_dest,sql_cmd_c7_tdb_save,data_c7_tdb_save)[0][0]
                            #print(res_c7_tdb_save)
                            ########################################## Cycle Node 9     II. Cycle Node 11
                            print('Node 9 - Save ldsity [SOURCE DB]')
                            sql_cmd_c9_sdb_save = '''select * from analytical.fn_etl_save_ldsity(%s,%s,%s);'''
                            data_c9_sdb_save = (res_l13_list[i]['ldsity'],res_c7_tdb_save,res_l13_list[i]['etl_ldsity_object__id'])
                            res_c9_sdb_save = run_sql(conn_src,sql_cmd_c9_sdb_save,data_c9_sdb_save)[0][0]
                            #print(res_c9_sdb_save)
                        else:
                            print('Node 2 - Any record returned? -> YES')
                            ########################################## Cycle Node 3     II. Cycle Node 5
                            print('Node 3 - Check ldsity [TARGET DB]')
                            sql_cmd_c3_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_ldsity(%s,%s,%s);'''
                            sql_cmd_c3_tdb_check_list = '''select * from target_data.fn_import_check_ldsity(%s,%s,%s);'''
                            data_c3_tdb_check = (res_l13_list[i]['ldsity'],res_l13_list[i]['ldsity_object__import'],res_l13_list[i]['ldsity_label_en'])
                            res_c3_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c3_tdb_check_etl_id,data_c3_tdb_check)[0][0]
                            res_c3_tdb_check_list = run_sql(conn_dest,sql_cmd_c3_tdb_check_list,data_c3_tdb_check)
                            ########################################## Cycle Node 4     II. Cycle Node 6
                            if res_c3_tdb_check_etl_id == None:
                                print('Node 4 - Exists etl_id? -> NO')
                                ########################################## Cycle Node 5     II. Cycle Node 7
                                print('Node 5 - Using labels and descriptions, manually search and assign the ldsity')
                                for cc in res_c1_tdb_get_list:
                                    print('L: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s   LO_LABEL_EN: %s C_EXP: %s   UoM_LABEL_EN: %s' % (cc['ldsity'], cc['id'], cc['label'], cc['description'], cc['label_en'], cc['description_en'], cc['ldsity_object__label_en'], cc['column_expression'], cc['unit_of_measure__label_en']))
                                ########################################## Cycle Node 6     II. Cycle Node 8
                                while True:
                                    input_c6_tdb = input("Node 6 - Exists etl_id? (y/n):\n")
                                    if not (input_c6_tdb == 'y' or input_c6_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c6_tdb == 'n':
                                    print('NO')
                                    ########################################## Cycle Node 7     II. Cycle Node 9
                                    print('Node 7 - Save ldsity [TARGET DB]')
                                    sql_cmd_c7_tdb_save = '''select * from target_data.fn_import_save_ldsity(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
                                    data_c7_tdb_save = (res_l13_list[i]['ldsity'],res_l13_list[i]['ldsity_label'],res_l13_list[i]['ldsity_description'],res_l13_list[i]['ldsity_label_en'],res_l13_list[i]['ldsity_description_en'],res_l13_list[i]['ldsity_object__import'],res_l13_list[i]['ldsity_column_expression'],res_l13_list[i]['unit_of_measure__import'],res_l13_list[i]['area_domain_category__import'],res_l13_list[i]['sub_population_category__import'],res_l13_list[i]['definition_variant__import'])
                                    res_c7_tdb_save =run_sql(conn_dest,sql_cmd_c7_tdb_save,data_c7_tdb_save)[0][0]
                                    #print(res_c7_tdb_save)
                                    ########################################## Cycle Node 9     II. Cycle Node 11
                                    print('Node 9 - Save ldsity [SOURCE DB]')
                                    sql_cmd_c9_sdb_save = '''select * from analytical.fn_etl_save_ldsity(%s,%s,%s);'''
                                    data_c9_sdb_save = (res_l13_list[i]['ldsity'],res_c7_tdb_save,res_l13_list[i]['etl_ldsity_object__id'])
                                    res_c9_sdb_save = run_sql(conn_src,sql_cmd_c9_sdb_save,data_c9_sdb_save)[0][0]
                                    #print(res_c9_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## Cycle Node 9     II. Cycle Node 11
                                    print('Node 9 - Save ldsity [SOURCE DB]')
                                    input_c9_tdb = input("Node 9 - Set ETL_ID of ldsity [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c9_sdb_save = '''select * from analytical.fn_etl_save_ldsity(%s,%s,%s);'''
                                    data_c9_sdb_save = (res_l13_list[i]['ldsity'],input_c9_tdb,res_l13_list[i]['etl_ldsity_object__id'])
                                    res_c9_sdb_save = run_sql(conn_src,sql_cmd_c9_sdb_save,data_c9_sdb_save)[0][0]
                                    #print(res_c9_sdb_save)
                            else:
                                print('Node 4 - Exists etl_id? -> YES')
                                ########################################## Cycle Node 8     II. Cycle Node 10
                                for ccc in res_c3_tdb_check_list:
                                    print('L: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s   LO_LABEL_EN: %s C_EXP: %s   UoM_LABEL_EN: %s' % (ccc['ldsity'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en'], ccc['ldsity_object__label_en'], ccc['column_expression'], ccc['unit_of_measure__label_en']))
            
                                while True:
                                    input_c8_tdb = input("Node 8 - Is etl_id correct? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## Cycle Node 5     II. Cycle Node 7
                                    print('Node 5 - Using labels and descriptions, manually search and assign the ldsity')
                                    for cc in res_c1_tdb_get_list:
                                        print('L: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s   LO_LABEL_EN: %s C_EXP: %s   UoM_LABEL_EN: %s' % (cc['ldsity'], cc['id'], cc['label'], cc['description'], cc['label_en'], cc['description_en'], cc['ldsity_object__label_en'], cc['column_expression'], cc['unit_of_measure__label_en']))
                                    ########################################## Cycle Node 6     II. Cycle Node 8
                                    while True:
                                        input_c6_tdb = input("Node 6 - Exists etl_id? (y/n):\n")
                                        if not (input_c6_tdb == 'y' or input_c6_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c6_tdb == 'n':
                                        print('NO')
                                        ########################################## Cycle Node 7     II. Cycle Node 9
                                        print('Node 7 - Save ldsity [TARGET DB]')
                                        sql_cmd_c7_tdb_save = '''select * from target_data.fn_import_save_ldsity(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'''
                                        data_c7_tdb_save = (res_l13_list[i]['ldsity'],res_l13_list[i]['ldsity_label'],res_l13_list[i]['ldsity_description'],res_l13_list[i]['ldsity_label_en'],res_l13_list[i]['ldsity_description_en'],res_l13_list[i]['ldsity_object__import'],res_l13_list[i]['ldsity_column_expression'],res_l13_list[i]['unit_of_measure__import'],res_l13_list[i]['area_domain_category__import'],res_l13_list[i]['sub_population_category__import'],res_l13_list[i]['definition_variant__import'])
                                        res_c7_tdb_save =run_sql(conn_dest,sql_cmd_c7_tdb_save,data_c7_tdb_save)[0][0]
                                        #print(res_c7_tdb_save)
                                        ########################################## Cycle Node 9     II. Cycle Node 11
                                        print('Node 9 - Save ldsity [SOURCE DB]')
                                        sql_cmd_c9_sdb_save = '''select * from analytical.fn_etl_save_ldsity(%s,%s,%s);'''
                                        data_c9_sdb_save = (res_l13_list[i]['ldsity'],res_c7_tdb_save,res_l13_list[i]['etl_ldsity_object__id'])
                                        res_c9_sdb_save = run_sql(conn_src,sql_cmd_c9_sdb_save,data_c9_sdb_save)[0][0]
                                        #print(res_c9_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## Cycle Node 9     II. Cycle Node 11
                                        print('Node 9 - Save ldsity [SOURCE DB]')
                                        input_c9_tdb = input("Node 9 - Set ETL_ID of ldsity [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c9_sdb_save = '''select * from analytical.fn_etl_save_ldsity(%s,%s,%s);'''
                                        data_c9_sdb_save = (res_l13_list[i]['ldsity'],input_c9_tdb,res_l13_list[i]['etl_ldsity_object__id'])
                                        res_c9_sdb_save = run_sql(conn_src,sql_cmd_c9_sdb_save,data_c9_sdb_save)[0][0]
                                        #print(res_c9_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## Cycle Node 9     II. Cycle Node 11
                                    print('Node 9 - Save ldsity [SOURCE DB]')
                                    sql_cmd_c9_sdb_save = '''select * from analytical.fn_etl_save_ldsity(%s,%s,%s);'''
                                    data_c9_sdb_save = (res_l13_list[i]['ldsity'],res_c3_tdb_check_etl_id,res_l13_list[i]['etl_ldsity_object__id'])
                                    res_c9_sdb_save = run_sql(conn_src,sql_cmd_c9_sdb_save,data_c9_sdb_save)[0][0]
                                    #print(res_c9_sdb_save)
                        #########################################################
                        #########################################################
                else:
                    print('Node 15 - Is the value ETL_ID in node 13 filled in for all ldsities in column etl_ldsity__etl_id? -> YES -> next step: ETL VERSION')
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 16 and ETL of VERSION]
            ###############################################################################
            print('ETL - LDSITIES [node 16 and ETL of VERSION]')
            print('--------------------------------------------------------------')
            ##############################################    Node 16
            sql_cmd_l16_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_version = false;'''
            data_l16 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l16_check = run_sql(conn_src,sql_cmd_l16_check,data_l16)[0][0]
            #print(res_l16_check)
            if res_l16_check == 0:
                print('Node 16 - Is the TRUE value filled in for all selected ldsities in column check_version? -> YES -> next step: CHECK LDSITY2PANEL_REFYEARSET_VERSION')
            else:
                print('Node 16 - Is the TRUE value filled in for all selected ldsities in column check_version? -> NO')
                sql_cmd_l16_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_version = false order by ldsity;'''
                res_l16_list = run_sql(conn_src,sql_cmd_l16_list,data_l16)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - VERSION [cycle for all selected ldsities where check_version = FALSE]')
                for r in res_l16_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get version [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_version(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_version(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - VERSION [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_version__etl_id order by etl_version__etl_id) from analytical.fn_etl_get_version(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get versions [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_versions(%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_versions(%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],)
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_versions(%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_versions(%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 9
                                print('Node 9 - Save version [TARGET DB]')
                                sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_version(%s,%s,%s,%s,%s);'''
                                data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                #print(res_c9_tdb_save)
                                ########################################## II. Cycle Node 11
                                print('Node 11 - Save version [SOURCE DB]')
                                sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_version(%s,%s,%s);'''
                                data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                #print(res_c11_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 5
                                print('Node 5 - Check version [TARGET DB]')
                                sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_version(%s,%s);'''
                                sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_version(%s,%s);'''
                                data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label_en'])
                                res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                                res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                                ########################################## II. Cycle Node 6     #686r
                                if res_c5_tdb_check_etl_id == None:
                                    print('Node 6 - Exists etl_id? -> NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the version')
                                    for cc in res_c3_tdb_get_list:
                                        print('V: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['id'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save version [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_version(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save version [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_version(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save version [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of version [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_version(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('Node 6 - Exists etl_id? -> YES')
                                    ########################################## II. Cycle Node 10
                                    for ccc in res_c5_tdb_check_list:
                                        print('V: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['id'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                    
                                    while True:
                                        input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                        if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c10_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 7
                                        print('Node 7 - Using labels and descriptions, manually search and assign the version')
                                        for cc in res_c3_tdb_get_list:
                                            print('V: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['id'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                        ########################################## II. Cycle Node 8
                                        while True:
                                            input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                            if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break

                                        if input_c8_tdb == 'n':
                                            print('NO')
                                            ########################################## II. Cycle Node 9
                                            print('Node 9 - Save version [TARGET DB]')
                                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_version(%s,%s,%s,%s,%s);'''
                                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                            #print(res_c9_tdb_save)
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save version [SOURCE DB]')
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_version(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                        else:
                                            print('YES')
                                            ########################################## II. Cycle Node 11
                                            print('Node 11 - Save version [SOURCE DB]')
                                            input_c11_tdb = input("Node 11 - Set ETL_ID of version [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_version(%s,%s,%s);'''
                                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                            #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save version [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_version(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [node 17 and ETL of LDSITY2PANEL_REFYEARSET_VERSION]
            ###############################################################################
            print('ETL - LDSITIES [node 17 and ETL of LDSITY2PANEL_REFYEARSET_VERSION]')
            print('--------------------------------------------------------------')
            ##############################################    Node 17
            sql_cmd_l17_check = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_ldsity2panel_refyearset_version = false;'''
            data_l17 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l17_check = run_sql(conn_src,sql_cmd_l17_check,data_l17)[0][0]
            #print(res_l17_check)
            if res_l17_check == 0:
                print('Node 17 - Is the TRUE value filled in for all selected ldsities in column check_ldsity2panel_refyearset_version? -> YES -> next step: LDSITIES Node 18 - get_ldsities4object')
            else:
                print('Node 17 - Is the TRUE value filled in for all selected ldsities in column check_ldsity2panel_refyearset_version? -> NO')
                sql_cmd_l17_list = '''select ldsity from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s) where check_ldsity2panel_refyearset_version = false order by ldsity;'''
                res_l17_list = run_sql(conn_src,sql_cmd_l17_list,data_l17)
                #########################################################
                ##############################################  I. CYCLE
                print('I. CYCLE - LDSITY2PANEL_REFYEARSET_VERSION [cycle for all selected ldsities where check_ldsity2panel_refyearset_version = FALSE]')
                for r in res_l17_list:
                    print('LDSITY = %s' % (r['ldsity']))
                    ########################################## I. Cycle Node 1
                    print('Node 1 - Get ldsity2panel_refyearset_version [SOURCE DB]')
                    sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_ldsity2panel_refyearset_version(%s,%s,false);'''
                    sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_ldsity2panel_refyearset_version(%s,%s,false);'''
                    data_c1_sdb_get = (node7_conn,r['ldsity'])
                    res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                    res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                    #print(res_c1_sdb_get_count)
                    #print(res_c1_sdb_get_list)
                    ########################################## I. Cycle Node 2
                    if res_c1_sdb_get_count == 0:
                        print('Node 2 - Any record returned? -> NO')
                    else:
                        print('Node 2 - Any record returned? -> YES')
                        ########################################## II. Cycle Node 3
                        print('II. CYCLE - LDSITY2PANEL_REFYEARSET_VERSION [cycle for all rows from NODE 1]')
                        for i, rr in enumerate(res_c1_sdb_get_list):
                            # preparing cmd and data [array of ETL_IDs] for Node 3
                            sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_ldsity2panel_refyearset_version__etl_id order by etl_ldsity2panel_refyearset_version__etl_id) from analytical.fn_etl_get_ldsity2panel_refyearset_version(%s,%s,true);'''
                            data_c3_sdb_get_etl_id = (node7_conn,r['ldsity'])
                            res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                            print('Node 3 - Get ldsity2panel_refyearset_versions [TARGET DB]')
                            if res_c3_sdb_get_etl_id == None:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_ldsity2panel_refyearset_versions(%s,%s,%s,%s,null::integer[]);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_ldsity2panel_refyearset_versions(%s,%s,%s,%s,null::integer[]);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_ldsity__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c1_sdb_get_list[i]['etl_version__etl_id'])
                            else:
                                sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_ldsity2panel_refyearset_versions(%s,%s,%s,%s,%s);'''
                                sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_ldsity2panel_refyearset_versions(%s,%s,%s,%s,%s);'''
                                data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_ldsity__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c1_sdb_get_list[i]['etl_version__etl_id'],res_c3_sdb_get_etl_id)

                            res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                            res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                            #print(res_c3_tdb_get_count)
                            #print(res_c3_tdb_get_list)
                            ########################################## II. Cycle Node 4
                            if res_c3_tdb_get_count == 0:
                                print('Node 4 - Any record returned? -> NO')
                                ########################################## Cycle II. Node 5
                                print('Node 5 - Save ldsity2panel_refyearset_version [TARGET DB]')
                                sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_ldsity2panel_refyearset_version(%s,%s,%s,%s);'''
                                data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_ldsity__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c1_sdb_get_list[i]['etl_version__etl_id'])
                                res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                                #print(res_c5_tdb_save)
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save ldsity2panel_refyearset_version [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_ldsity2panel_refyearset_version(%s,%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_ldsity__id'],res_c1_sdb_get_list[i]['etl_version__id'],res_c5_tdb_save)
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 4 - Any record returned? -> YES')
                                ########################################## II. Cycle Node 6
                                # check that number of rows in res_c3_tdb_get_list must be only one row
                                if res_c3_tdb_get_count == 1:
                                    print('Node 6 - Is returned one row in node 3? -> YES')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Save ldsity2panel_refyearset_version [SOURCE DB]')
                                    sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_ldsity2panel_refyearset_version(%s,%s,%s,%s);'''
                                    data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_ldsity__id'],res_c1_sdb_get_list[i]['etl_version__id'],res_c3_tdb_get_list[0]['etl_id'])
                                    res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                    #print(res_c7_sdb_save)
                                else:
                                    print('Node 6 - Is returned one row in node 3? -> NO')
                                    print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                    sys.exit(1)
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################################################################



            ###############################################################################
            # LDSITIES [part node 18 and node 19]
            ###############################################################################
            print('ETL - LDSITIES [part node 18 and node 19]')
            print('--------------------------------------------------------------')
            ##############################################    Node 18
            print('Node 18 - Get ldsities4object [SOURCE DB]')
            sql_cmd_l18_count_check_ldsity = '''select count(*) from analytical.fn_etl_get_ldsities4object(%s,%s,false,%s);'''
            data_l18 = (node7_conn,lo4_input_ldsity_object,lst)
            res_l18_count_check_ldsity = run_sql(conn_src,sql_cmd_l18_count_check_ldsity,data_l18)[0][0]
            #print(res_l18_count_check_ldsity)
            ##############################################    Node 19
            if res_l18_count_check_ldsity == 0:
                print('Node 19 - Any record returned? -> NO -> next step: ETL of AD ATTRIBUTES.')
            else:
                print('Node 19 - Any record returned? -> YES -> NO COMMIT and STOP process ETL !!!')
                sys.exit(1)
        print('--------------------------------------------------------------')
        ###############################################################################
        ###############################################################################
    ###############################################################################
    ###############################################################################
else:
    ###############################################################################
    # Get ETL_ID of LDSITY OBJECT that have already been ETLed !!! #HERE#
    ###############################################################################
    print('ETL - AREA DOMAIN or SUB POPULATION attributes')
    print('Get ETL_ID of LDSITY OBJECT that have already been ETLed')
    print('--------------------------------------------------------------')
    ##############################################    Node 1
    print('Node 1 - Informative list of ldsity objects than have already been ELTed [SOURCE DB]')
    sql_cmd_lo1_count = '''select count(*) from analytical.fn_etl_get_ldsity_objects(%s,%s,%s);'''
    sql_cmd_lo1_list = '''select ldsity_object,label,description,label_en,description_en,table_name,upper_object__id,upper_object__etl_id,areal_or_population__id,areal_or_population__label,areal_or_population__etl_id,column4upper_object,filter,etl_ldsity_object__id,etl_ldsity_object__etl_id,check_ldsities,check_upper_object from analytical.fn_etl_get_ldsity_objects(%s,%s,%s) order by ldsity_object;'''
    data_false = 'false'
    data_true = 'true'
    data_lo1 = (node7_conn,data_true,data_false)
    res_lo1_count = run_sql(conn_src, sql_cmd_lo1_count, data_lo1)[0][0]
    res_lo1_list = run_sql(conn_src, sql_cmd_lo1_list, data_lo1)
    #print(res_lo1_count)
    ###############################################     Node 2
    if res_lo1_count > 0:
        print('Node 2 - Exists any record? -> YES')
        ###############################################     Node 3
        print('Node 3 - List of ldsity objects than have already been ELTed [SOURCE DB]:')
        for r in res_lo1_list:
            print('LDSITY_OBJECT: %s   LABEL: %s   LABEL_EN: %s    AoP_LABEL: %s  ETL_LO__ETL_ID: %s' % (r['ldsity_object'], r['label'], r['label_en'], r['areal_or_population__label'], r['etl_ldsity_object__etl_id']))
        ###############################################     Node 4
        lo4_input_ldsity_object = input("Node 4 - Choose ldsity object for ETL of area domain or sub population attributes?\n")
        sql_cmd_lo4_check_exists_etl_id = '''select count(*) from analytical.fn_etl_get_ldsity_objects(%s,%s,%s,%s) where etl_ldsity_object__etl_id is not null;'''
        data_lo4 = (node7_conn,data_true,data_false,lo4_input_ldsity_object)
        res_lo4_check_exists_etl_id = run_sql(conn_src,sql_cmd_lo4_check_exists_etl_id,data_lo4)[0][0]
        #print(res_lo4_check_exists_etl_id)
        ###############################################     Node 5
        if res_lo4_check_exists_etl_id == 0:
            print('Node 5 - Exists etl_id of selected ldsity object? -> NO -> STOP ETL process!')
            sys.exit(1)
        else:
            print('Node 5 - Exists etl_id of selected ldsity object? -> YES -> next step: ETL - AREA DOMAIN or SUB POPULATION attriubutes')
    else:
        print('Node 2 - Exists any record? -> NO -> next step: STOP ETL process. Not exist any ETL_ID of ldsity object that have already been ETLed.')
        sys.exit(1)
    ###############################################################################
###############################################################################
###############################################################################



###############################################################################
# Node B
###############################################################################
while True:
    input_node_b = input("Node B - Do you want to solve an ETL of AREA DOMAIN ATTRIBUTES for selected ldsity object? (y/n):\n")
    if not (input_node_b == 'y' or input_node_b == 'n'):
        print("Sorry, not known.")
        continue
    else:
        break
print('--------------------------------------------------------------')
###############################################################################



###############################################################################
# ETL AD attributes
###############################################################################
if input_node_b == 'y':
    print('ETL AD ATTRIBUTES')
    print('--------------------------------------------------------------')
    ##############################################    Node 1
    print('Node 1 - get_ad_attributes4ldsity_object [SOURCE DB]')
    sql_cmd_ad_attr_1_get_count = '''select count(*) from analytical.fn_etl_get_ad_attributes4ldsity_object(%s,%s,null::integer[],null::boolean);'''
    sql_cmd_ad_attr_1_get_list = '''select * from analytical.fn_etl_get_ad_attributes4ldsity_object(%s,%s,null::integer[],null::boolean);'''
    data_ad_attr_1 = (node7_conn,lo4_input_ldsity_object)
    res_ad_attr_1_count = run_sql(conn_src,sql_cmd_ad_attr_1_get_count,data_ad_attr_1)[0][0]
    res_ad_attr_1_list = run_sql(conn_src,sql_cmd_ad_attr_1_get_list,data_ad_attr_1)
    #print(res_ad_attr_1_count)
    ########################################## Node 2
    if res_ad_attr_1_count == 0:
        print('Node 2 - Any record returned? -> NO [not exists any AD ATTRIBUTE for ETL]')
    else:
        print('Node 2 - Any record returned? -> YES')
        ########################################## Node 2
        print('Node 3 - Display list of AD ATTRIBUTES from node 1:')
        for r in res_ad_attr_1_list:
            print('LO__ID: %s  AD__ID: %s   AD__L: %s   ADC__ID: %s   CHECK_ATTR: %s' % (r['lo__id'], r['ad__id'], r['ad__label_en'], r['adc__id'], r['check__attr'])) 
        ###############################################     Node 4
        print('Node 4 - choose AD ATTRIBUTES for ETL [SOURCE DB] -> next step: Node 5')
        lst_attr = []
        # number of elements as input
        n = int(input("Enter number of AD ATTRIBUTES and IDs [SOURCE DB]: "))
        # iterating till the range
        for i in range(0, n):
            ele = int(input())
            lst_attr.append(ele) # adding the element
        print(lst_attr)

        if lst_attr == []:
            ###############################################     Node 5
            print('Node 5 - Exists any AD attribute for ETL? -> NO -> next step: ETL of SP ATTRIBUTES')
        else:
            print('Node 5 - Exists any AD attribute for ETL? -> YES -> next step: AD [cycle for all selected AD ATTRIBUTES from node 4]')
            # user list from node 4
            sql_cmd_ad_attr_4_get_list_selected = '''select * from analytical.fn_etl_get_ad_attributes4ldsity_object(%s,%s,null::integer[],null::boolean) where ad__id in (select unnest(%s));'''
            data_ad_attr_4_selected = (node7_conn,lo4_input_ldsity_object,lst_attr)
            res_ad_attr_4_list_selected = run_sql(conn_src,sql_cmd_ad_attr_4_get_list_selected,data_ad_attr_4_selected)
            #print(res_ad_attr_4_list_selected) -- list for cycle I.
            #######################################################################################
            # AD
            #######################################################################################
            ########################################################
            ############################################### I. CYCLE
            print('--------------------------------------------------------------')
            print('I. CYCLE - AREA DOMAIN')
            for r in res_ad_attr_4_list_selected:
                print('I. CYCLE - AD__ID = %s' % (r['ad__id']))
                print('I. CYCLE - AD__L = %s' % (r['ad__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_area_domain_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_area_domain_attr(%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_area_domain_attr(%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,r['adc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]    
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ############################################### I. CYCLE Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next AD attribute]')    # next r in I. CYCLE
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ############################################### II. CYCLE Node 3
                    print('II. CYCLE - AREA DOMAIN [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        print('II. CYCLE - AD__ID = %s' % (res_c1_sdb_get_list[i]['id']))
                        print('II. CYCLE - AD__L = %s' % (res_c1_sdb_get_list[i]['label_en']))
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_area_domain__etl_id order by etl_area_domain__etl_id) from analytical.fn_etl_get_area_domain_attr(%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]
                        #print(res_c3_sdb_get_etl_id)

                        print('Node 3 - Get area domains [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domains(%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domains(%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],)
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domains(%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domains(%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)                        
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 9
                            print('Node 9 - Save area domain [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain(%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                            #print(res_c9_tdb_save)
                            ########################################## II. Cycle Node 11
                            print('Node 11 - Save area domain [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 5
                            print('Node 5 - Check area domain [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_area_domain(%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_area_domain(%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label_en'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## II. Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the area domain')
                                for cc in res_c3_tdb_get_list:
                                    print('AD: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                ########################################## II. Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 9
                                    print('Node 9 - Save area domain [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain(%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                    #print(res_c9_tdb_save)
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save area_domain [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save area domain [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## II. Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('AD: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['area_domain'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the area domain')
                                    for cc in res_c3_tdb_get_list:
                                        print('AD: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save area domain [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save area domain [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            #######################################################################################
            # ADC
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - AREA DOMAIN CATEGORY')
            for r in res_ad_attr_4_list_selected:
                print('I. CYCLE - AD = %s' % (r['ad__id']))
                print('I. CYCLE - L = %s' % (r['ad__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_area_domain_category_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_area_domain_category_attr(%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_area_domain_category_attr(%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,r['adc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next AD attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - AREA DOMAIN CATEGORY [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        print('II. CYCLE - ADC__ID = %s' % (res_c1_sdb_get_list[i]['id']))
                        print('II. CYCLE - ADC__L = %s' % (res_c1_sdb_get_list[i]['label_en']))
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_area_domain_category__etl_id order by etl_area_domain_category__etl_id) from analytical.fn_etl_get_area_domain_category_attr(%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get area domain categories [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domain_categories(%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domain_categories(%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_area_domain_categories(%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_area_domain_categories(%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 9
                            print('Node 9 - Save area domain category [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                            #print(res_c9_tdb_save)
                            ########################################## II. Cycle Node 11
                            print('Node 11 - Save area domain category [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 5
                            print('Node 5 - Check area domain category [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_area_domain_category(%s,%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_area_domain_category(%s,%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label_en'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## II. Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the area domain category')
                                for cc in res_c3_tdb_get_list:
                                    print('ADC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                ########################################## II. Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 9
                                    print('Node 9 - Save area domain category [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                    #print(res_c9_tdb_save)
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save area_domain category [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save area domain category [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of area domain category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## II. Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('ADC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s AD_LABEL_EN: %s' % (ccc['area_domain_category'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en'], ccc['area_domain__label_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the area domain category')
                                    for cc in res_c3_tdb_get_list:
                                        print('ADC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['area_domain_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save area domain category [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain category [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save area domain category [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of area domain category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save area domain category [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_area_domain_category(%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_area_domain__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            #######################################################################################
            # ADC HIERARCHY
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - AREA DOMAIN CATEGORY HIERARCHY')
            for r in res_ad_attr_4_list_selected:
                print('I. CYCLE - AD = %s' % (r['ad__id']))
                print('I. CYCLE - L = %s' % (r['ad__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_adc_hierarchy_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_adc_hierarchy_attr(%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_adc_hierarchy_attr(%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,r['adc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next AD attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - AREA DOMAIN CATEGORY HIERARCHY [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_adc_hierarchy__etl_id order by etl_adc_hierarchy__etl_id) from analytical.fn_etl_get_adc_hierarchy_attr(%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get area domain category hierarchies [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc_hierarchies(%s,%s,%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 5
                            print('Node 5 - Save area domain category hierarchy [TARGET DB]')
                            sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_adc_hierarchy(%s,%s,%s,%s);'''
                            data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                            res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                            #print(res_c5_tdb_save)
                            ########################################## II. Cycle Node 7
                            print('Node 7 - Save area domain category hierarchy [SOURCE DB]')
                            sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc_hierarchy(%s,%s,%s,%s);'''
                            data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable'],res_c5_tdb_save)
                            res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                            #print(res_c7_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 6
                            # check that number of rows in res_c3_tdb_get_list must be only one row
                            if res_c3_tdb_get_count == 1:
                                print('Node 6 - Is returned one row in node 3? -> YES')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save area domain category hierarchy [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc_hierarchy(%s,%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_area_domain_category__id__variable'],res_c3_tdb_get_list[0]['etl_id'])
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 6 - Is returned one row in node 3? -> NO')
                                print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                sys.exit(1)
            print('--------------------------------------------------------------')
            #######################################################################################
            # ADC2CLASSIFICATION RULE
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - ADC2CLASSIFICATION RULE')
            for r in res_ad_attr_4_list_selected:
                print('I. CYCLE - AD = %s' % (r['ad__id']))
                print('I. CYCLE - L = %s' % (r['ad__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_adc2classification_rule_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_adc2classification_rule_attr(%s,%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_adc2classification_rule_attr(%s,%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,lo4_input_ldsity_object,r['adc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next AD attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - ADC2CLASSIFICATION_RULE [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_adc2classification_rule__etl_id order by etl_adc2classification_rule__etl_id) from analytical.fn_etl_get_adc2classification_rule_attr(%s,%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,lo4_input_ldsity_object)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get adc2classification_rules [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classification_rules(%s,%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 9
                            print('Node 9 - Save adc2classification_rule [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_adc2classification_rule(%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__refyearset2panel'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                            #print(res_c9_tdb_save)
                            #print(res_c9_tdb_save[0]['adc2classification_rule'])
                            #print(res_c9_tdb_save[0]['etl_id'])
                            #print(res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'])
                            ########################################## II. Cycle Node 11
                            print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                            ########################################## II. Cycle Node 12
                            print('Node 12 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                            sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                            data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                            res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                            #print(res_c12_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 5
                            print('Node 5 - Check adc2classification_rule [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_adc2classification_rule(%s,%s,%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_adc2classification_rule(%s,%s,%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## II. Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the adc2classification_rule')
                                for cc in res_c3_tdb_get_list:
                                    print('ADC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    ADC_LABEL_EN: %s' % (cc['adc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['area_domain_category__label_en']))
                                ########################################## II. Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 9
                                    print('Node 9 - Save adc2classification_rule [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_adc2classification_rule(%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__refyearset2panel'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                    #print(res_c9_tdb_save)[0]['etl_id']
                                    #print(res_c9_tdb_save)[0]['cm_adc2classrule2panel_refyearset__etl_id']
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)                            
                                    ########################################## II. Cycle Node 12
                                    print('Node 12 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                    sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                    data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                    res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                    #print(res_c12_sdb_save)                            
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of adc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## II. Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('ADC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    ADC_LABEL_EN: %s' % (ccc['adc2classification_rule'], ccc['etl_id'], ccc['classification_rule'], ccc['ldsity_object__label_en'], ccc['area_domain_category__label_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the adc2classification_rule')
                                    for cc in res_c3_tdb_get_list:
                                        print('ADC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    ADC_LABEL_EN: %s' % (cc['adc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['area_domain_category__label_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save adc2classification_rule [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_adc2classification_rule(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_area_domain_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__refyearset2panel'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                        #print(res_c9_tdb_save)[0]['etl_id']
                                        #print(res_c9_tdb_save)[0]['cm_adc2classrule2panel_refyearset__etl_id']
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                        ########################################## II. Cycle Node 12
                                        print('Node 12 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                        sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                        data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_adc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_adc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                        res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                        #print(res_c12_sdb_save)                                 
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of adc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save adc2classification_rule [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_adc2classification_rule(%s,%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_area_domain_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            #######################################################################################
            # ADC2CLASSRULE2PANEL_REFYEARSET
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - ADC2CLASSRULE2PANEL_REFYEARSET')
            for r in res_ad_attr_4_list_selected:
                print('I. CYCLE - AD = %s' % (r['ad__id']))
                print('I. CYCLE - L = %s' % (r['ad__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_adc2classrule2panel_refyearset_att [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(%s,%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(%s,%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,lo4_input_ldsity_object,r['adc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next AD attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - ADC2CLASSRULE2PANEL_REFYEARSET [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_adc2classrule2panel_refyearset__etl_id order by etl_adc2classrule2panel_refyearset__etl_id) from analytical.fn_etl_get_adc2classrule2panel_refyearset_attr(%s,%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,lo4_input_ldsity_object)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get adc2classrule2panel_refyearsets [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 5
                            print('Node 5 - Save adc2classrule2panel_refyearset [TARGET DB]')
                            sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                            data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                            res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                            #print(res_c5_tdb_save)
                            ########################################## II. Cycle Node 7
                            print('Node 7 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                            sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                            data_c7_sdb_save = (res_c1_sdb_get_list[i]['adc2classrule2panel_refyearset'],res_c5_tdb_save,res_c1_sdb_get_list[i]['etl_adc2classification_rule__id'])
                            res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                            #print(res_c7_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 6
                            # check that number of rows in res_c3_tdb_get_list must be only one row
                            if res_c3_tdb_get_count == 1:
                                print('Node 6 - Is returned one row in node 3? -> YES')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save adc2classrule2panel_refyearset [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_adc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['adc2classrule2panel_refyearset'],res_c3_tdb_get_list[0]['etl_id'],res_c1_sdb_get_list[i]['etl_adc2classification_rule__id'])
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 6 - Is returned one row in node 3? -> NO')
                                print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                sys.exit(1)
            print('--------------------------------------------------------------')
            #######################################################################################
            print('Node 6 - get_ad_attributes4ldsity_object [SOURCE DB]')
            sql_cmd_ad_attr_6_get_count_selected = '''select count(*) from analytical.fn_etl_get_ad_attributes4ldsity_object(%s,%s,%s,false);'''
            data_ad_attr_6_selected = (node7_conn,lo4_input_ldsity_object,lst_attr)
            res_ad_attr_6_count_selected = run_sql(conn_src,sql_cmd_ad_attr_6_get_count_selected,data_ad_attr_6_selected)[0][0]
            #print(res_ad_attr_6_count_selected)

            if res_ad_attr_6_count_selected == 0:
                print('Node 7 - Is returned any row in node 6? -> NO -> next step: ETL of SP ATTRIBUTES')
            else:
                print('Node 7 - Is returned any row in node 6? -> YES -> next step: NO COMMIT and STOP process ETL')
                sys.exit(1)
            print('--------------------------------------------------------------')
            #######################################################################################
print('--------------------------------------------------------------')
###############################################################################



###############################################################################
# Node C
###############################################################################
while True:
    input_node_c = input("Node C - Do you want to solve an ETL of SUB POPULATION ATTRIBUTES for selected ldsity object? (y/n):\n")
    if not (input_node_c == 'y' or input_node_c == 'n'):
        print("Sorry, not known.")
        continue
    else:
        break
print('--------------------------------------------------------------')
###############################################################################



###############################################################################
# ETL SP attributes
###############################################################################
if input_node_c == 'y':
    print('ETL SP ATTRIBUTES')
    print('--------------------------------------------------------------')
    ##############################################    Node 1
    print('Node 1 - get_sp_attributes4ldsity_object [SOURCE DB]')
    sql_cmd_sp_attr_1_get_count = '''select count(*) from analytical.fn_etl_get_sp_attributes4ldsity_object(%s,%s,null::integer[],null::boolean);'''
    sql_cmd_sp_attr_1_get_list = '''select * from analytical.fn_etl_get_sp_attributes4ldsity_object(%s,%s,null::integer[],null::boolean);'''
    data_sp_attr_1 = (node7_conn,lo4_input_ldsity_object)
    res_sp_attr_1_count = run_sql(conn_src,sql_cmd_sp_attr_1_get_count,data_sp_attr_1)[0][0]
    res_sp_attr_1_list = run_sql(conn_src,sql_cmd_sp_attr_1_get_list,data_sp_attr_1)
    #print(res_sp_attr_1_count)
    ########################################## Node 2
    if res_sp_attr_1_count == 0:
        print('Node 2 - Any record returned? -> NO [not exists any SP ATTRIBUTE for ETL]')
    else:
        print('Node 2 - Any record returned? -> YES')
        ########################################## Node 2
        print('Node 3 - Display list of SP ATTRIBUTES from node 1:')
        for r in res_sp_attr_1_list:
            print('LO__ID: %s  SP__ID: %s   SP__L: %s   SPC__ID: %s   CHECK_ATTR: %s' % (r['lo__id'], r['sp__id'], r['sp__label_en'], r['spc__id'], r['check__attr'])) 
        ###############################################     Node 4
        print('Node 4 - choose SP ATTRIBUTES for ETL [SOURCE DB] -> next step: Node 5')
        lst_attr = []
        # number of elements as input
        n = int(input("Enter number of SP ATTRIBUTES and IDs [SOURCE DB]: "))
        # iterating till the range
        for i in range(0, n):
            ele = int(input())
            lst_attr.append(ele) # adding the element
        print(lst_attr)

        if lst_attr == []:
            ###############################################     Node 5
            print('Node 5 - Exists any SP attribute for ETL? -> NO -> next step: COMMIT and the END of process ETL')
        else:
            print('Node 5 - Exists any SP attribute for ETL? -> YES -> next step: SP [cycle for all selected SP ATTRIBUTES from node 4]')
            # user list from node 4
            sql_cmd_sp_attr_4_get_list_selected = '''select * from analytical.fn_etl_get_sp_attributes4ldsity_object(%s,%s,null::integer[],null::boolean) where sp__id in (select unnest(%s));'''
            data_sp_attr_4_selected = (node7_conn,lo4_input_ldsity_object,lst_attr)
            res_sp_attr_4_list_selected = run_sql(conn_src,sql_cmd_sp_attr_4_get_list_selected,data_sp_attr_4_selected)
            #print(res_sp_attr_4_list_selected)
            #######################################################################################
            # SP
            #######################################################################################
            ########################################################
            ############################################### I. CYCLE
            print('--------------------------------------------------------------')
            print('I. CYCLE - SUB POPULATION')
            for r in res_sp_attr_4_list_selected:
                print('I. CYCLE - SP__ID = %s' % (r['sp__id']))
                print('I. CYCLE - SP__L = %s' % (r['sp__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_sub_population_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_sub_population_attr(%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_sub_population_attr(%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,r['spc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]    
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ############################################### I. CYCLE Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next SP attribute]')    # next r in I. CYCLE
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ############################################### II. CYCLE Node 3
                    print('II. CYCLE - SUB POPULATION [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        print('II. CYCLE - SP__ID = %s' % (res_c1_sdb_get_list[i]['id']))
                        print('II. CYCLE - SP__L = %s' % (res_c1_sdb_get_list[i]['label_en']))
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_sub_population__etl_id order by etl_sub_population__etl_id) from analytical.fn_etl_get_sub_population_attr(%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]
                        #print(res_c3_sdb_get_etl_id)

                        print('Node 3 - Get sub populations [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_populations(%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_populations(%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],)
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_populations(%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_populations(%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)                        
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 9
                            print('Node 9 - Save sub population [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population(%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                            #print(res_c9_tdb_save)
                            ########################################## II. Cycle Node 11
                            print('Node 11 - Save sub population [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                            data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 5
                            print('Node 5 - Check sub population [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_sub_population(%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_sub_population(%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label_en'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## II. Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the sub population')
                                for cc in res_c3_tdb_get_list:
                                    print('SP: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                ########################################## II. Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 9
                                    print('Node 9 - Save sub population [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population(%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                    #print(res_c9_tdb_save)
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save sub population [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save sub population [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of sub population [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## II. Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('SP: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (ccc['sub_population'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the sub population')
                                    for cc in res_c3_tdb_get_list:
                                        print('SP: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save sub population [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c9_tdb_save)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of sub population [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                        data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],input_c11_tdb)
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save sub population [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population(%s,%s,%s);'''
                                    data_c11_sdb_save = (node7_conn,res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id)
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            #######################################################################################
            # SPC
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - SUB POPULATION CATEGORY')
            for r in res_sp_attr_4_list_selected:
                print('I. CYCLE - SP = %s' % (r['sp__id']))
                print('I. CYCLE - L = %s' % (r['sp__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_sub_population_category_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_sub_population_category_attr(%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_sub_population_category_attr(%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,r['spc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next SP attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - SUB POPULATION CATEGORY [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        print('II. CYCLE - SPC__ID = %s' % (res_c1_sdb_get_list[i]['id']))
                        print('II. CYCLE - SPC__L = %s' % (res_c1_sdb_get_list[i]['label_en']))
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_sub_population_category__etl_id order by etl_sub_population_category__etl_id) from analytical.fn_etl_get_sub_population_category_attr(%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get sub population categories [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_population_categories(%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_population_categories(%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_sub_population_categories(%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_sub_population_categories(%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 9
                            print('Node 9 - Save sub population category [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                            #print(res_c9_tdb_save)
                            ########################################## II. Cycle Node 11
                            print('Node 11 - Save sub population category [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 5
                            print('Node 5 - Check sub population category [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_sub_population_category(%s,%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_sub_population_category(%s,%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label_en'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## II. Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the sub population category')
                                for cc in res_c3_tdb_get_list:
                                    print('SPC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                ########################################## II. Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 9
                                    print('Node 9 - Save sub population category [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                    #print(res_c9_tdb_save)
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save sub population category [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save sub population category [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of sub population category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## II. Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('SPC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s SP_LABEL_EN: %s' % (ccc['sub_population_category'], ccc['etl_id'], ccc['label'], ccc['description'], ccc['label_en'], ccc['description_en'], ccc['sub_population__label_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the sub population category')
                                    for cc in res_c3_tdb_get_list:
                                        print('SPC: %s ETL_ID: %s   LABEL: %s   DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (cc['sub_population_category'], cc['etl_id'], cc['label'], cc['description'], cc['label_en'], cc['description_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save sub population category [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select etl_id from target_data.fn_import_save_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population__etl_id'],res_c1_sdb_get_list[i]['label'],res_c1_sdb_get_list[i]['description'],res_c1_sdb_get_list[i]['label_en'],res_c1_sdb_get_list[i]['description_en'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)[0][0]
                                        #print(res_c9_tdb_save)
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population category [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save sub population category [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of sub population category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save sub population category [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_sub_population_category(%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_sub_population__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            #######################################################################################
            # SPC HIERARCHY
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - SUB POPULATION CATEGORY HIERARCHY')
            for r in res_sp_attr_4_list_selected:
                print('I. CYCLE - SP = %s' % (r['sp__id']))
                print('I. CYCLE - L = %s' % (r['sp__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_spc_hierarchy_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_spc_hierarchy_attr(%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_spc_hierarchy_attr(%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,r['spc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next SP attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - SUB POPULATION CATEGORY HIERARCHY [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_spc_hierarchy__etl_id order by etl_spc_hierarchy__etl_id) from analytical.fn_etl_get_spc_hierarchy_attr(%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get sub population category hierarchies [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc_hierarchies(%s,%s,%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 5
                            print('Node 5 - Save sub population category hierarchy [TARGET DB]')
                            sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_spc_hierarchy(%s,%s,%s,%s);'''
                            data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id__variable'],res_c1_sdb_get_list[i]['dependent'])
                            res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                            #print(res_c5_tdb_save)
                            ########################################## II. Cycle Node 7
                            print('Node 7 - Save sub population category hierarchy [SOURCE DB]')
                            sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc_hierarchy(%s,%s,%s,%s);'''
                            data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable'],res_c5_tdb_save)
                            res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                            #print(res_c7_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 6
                            # check that number of rows in res_c3_tdb_get_list must be only one row
                            if res_c3_tdb_get_count == 1:
                                print('Node 6 - Is returned one row in node 3? -> YES')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save sub population category hierarchy [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc_hierarchy(%s,%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable_superior'],res_c1_sdb_get_list[i]['etl_sub_population_category__id__variable'],res_c3_tdb_get_list[0]['etl_id'])
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 6 - Is returned one row in node 3? -> NO')
                                print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                sys.exit(1)
            print('--------------------------------------------------------------')
            #######################################################################################
            # SPC2CLASSIFICATION RULE
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - SPC2CLASSIFICATION RULE')
            for r in res_sp_attr_4_list_selected:
                print('I. CYCLE - SP = %s' % (r['sp__id']))
                print('I. CYCLE - L = %s' % (r['sp__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_spc2classification_rule_attr [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_spc2classification_rule_attr(%s,%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_spc2classification_rule_attr(%s,%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,lo4_input_ldsity_object,r['spc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next SP attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - SPC2CLASSIFICATION_RULE [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_spc2classification_rule__etl_id order by etl_spc2classification_rule__etl_id) from analytical.fn_etl_get_spc2classification_rule_attr(%s,%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,lo4_input_ldsity_object)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get spc2classification_rules [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classification_rules(%s,%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 9
                            print('Node 9 - Save spc2classification_rule [TARGET DB]')
                            sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_spc2classification_rule(%s,%s,%s,%s,%s);'''
                            data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__refyearset2panel'])
                            res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                            #print(res_c9_tdb_save)
                            #print(res_c9_tdb_save[0]['spc2classification_rule'])
                            #print(res_c9_tdb_save[0]['etl_id'])
                            #print(res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'])
                            ########################################## II. Cycle Node 11
                            print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                            sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                            data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                            res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                            #print(res_c11_sdb_save)
                            ########################################## II. Cycle Node 12
                            print('Node 12 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                            sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                            data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                            res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                            #print(res_c12_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 5
                            print('Node 5 - Check spc2classification_rule [TARGET DB]')
                            sql_cmd_c5_tdb_check_etl_id = '''select etl_id from target_data.fn_import_check_spc2classification_rule(%s,%s,%s,%s);'''
                            sql_cmd_c5_tdb_check_list = '''select * from target_data.fn_import_check_spc2classification_rule(%s,%s,%s,%s);'''
                            data_c5_tdb_check = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'])
                            res_c5_tdb_check_etl_id = run_sql(conn_dest,sql_cmd_c5_tdb_check_etl_id,data_c5_tdb_check)[0][0]
                            res_c5_tdb_check_list = run_sql(conn_dest,sql_cmd_c5_tdb_check_list,data_c5_tdb_check)
                            ########################################## II. Cycle Node 6
                            if res_c5_tdb_check_etl_id == None:
                                print('Node 6 - Exists etl_id? -> NO')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Using labels and descriptions, manually search and assign the spc2classification_rule')
                                for cc in res_c3_tdb_get_list:
                                    print('SPC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    SPC_LABEL_EN: %s' % (cc['spc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['sub_population_category__label_en']))
                                ########################################## II. Cycle Node 8
                                while True:
                                    input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                    if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c8_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 9
                                    print('Node 9 - Save spc2classification_rule [TARGET DB]')
                                    sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_spc2classification_rule(%s,%s,%s,%s,%s);'''
                                    data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__refyearset2panel'])
                                    res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                    #print(res_c9_tdb_save)[0]['etl_id']
                                    #print(res_c9_tdb_save)[0]['cm_spc2classrule2panel_refyearset__etl_id']
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)                            
                                    ########################################## II. Cycle Node 12
                                    print('Node 12 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                    sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                    data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                    res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                    #print(res_c12_sdb_save)                            
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                    input_c11_tdb = input("Node 11 - Set ETL_ID of spc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
                            else:
                                print('Node 6 - Exists etl_id? -> YES')
                                ########################################## II. Cycle Node 10
                                for ccc in res_c5_tdb_check_list:
                                    print('SPC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    SPC_LABEL_EN: %s' % (ccc['spc2classification_rule'], ccc['etl_id'], ccc['classification_rule'], ccc['ldsity_object__label_en'], ccc['sub_population_category__label_en']))
                                
                                while True:
                                    input_c10_tdb = input("Node 10 - Is etl_id correct? (y/n):\n")
                                    if not (input_c10_tdb == 'y' or input_c10_tdb == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                if input_c10_tdb == 'n':
                                    print('NO')
                                    ########################################## II. Cycle Node 7
                                    print('Node 7 - Using labels and descriptions, manually search and assign the spc2classification_rule')
                                    for cc in res_c3_tdb_get_list:
                                        print('SPC2CR: %s ETL_ID: %s   CR: %s   LO_LABEL_EN: %s    SPC_LABEL_EN: %s' % (cc['spc2classification_rule'], cc['etl_id'], cc['classification_rule'], cc['ldsity_object__label_en'], cc['sub_population_category__label_en']))
                                    ########################################## II. Cycle Node 8
                                    while True:
                                        input_c8_tdb = input("Node 8 - Exists etl_id? (y/n):\n")
                                        if not (input_c8_tdb == 'y' or input_c8_tdb == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if input_c8_tdb == 'n':
                                        print('NO')
                                        ########################################## II. Cycle Node 9
                                        print('Node 9 - Save spc2classification_rule [TARGET DB]')
                                        sql_cmd_c9_tdb_save = '''select * from target_data.fn_import_save_spc2classification_rule(%s,%s,%s,%s,%s);'''
                                        data_c9_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_sub_population_category__etl_id'],res_c1_sdb_get_list[i]['etl_ldsity_object__etl_id'],res_c1_sdb_get_list[i]['classification_rule'],res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__refyearset2panel'])
                                        res_c9_tdb_save =run_sql(conn_dest,sql_cmd_c9_tdb_save,data_c9_tdb_save)
                                        #print(res_c9_tdb_save)[0]['etl_id']
                                        #print(res_c9_tdb_save)[0]['cm_spc2classrule2panel_refyearset__etl_id']
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c9_tdb_save[0]['etl_id'],res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                        ########################################## II. Cycle Node 12
                                        print('Node 12 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                        sql_cmd_c12_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                        data_c12_sdb_save = (res_c1_sdb_get_list[i]['cm_spc2classrule2panel_refyearset__id'],res_c9_tdb_save[0]['cm_spc2classrule2panel_refyearset__etl_id'],res_c11_sdb_save)
                                        res_c12_sdb_save = run_sql(conn_src,sql_cmd_c12_sdb_save,data_c12_sdb_save)
                                        #print(res_c12_sdb_save)                                 
                                    else:
                                        print('YES')
                                        ########################################## II. Cycle Node 11
                                        print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                        input_c11_tdb = input("Node 11 - Set ETL_ID of spc2classification_rule [choosing from TARGET DB and saving into SOURCE DB]:\n")
                                        sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                        data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],input_c11_tdb,res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                        res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                        #print(res_c11_sdb_save)
                                else:
                                    print('YES')
                                    ########################################## II. Cycle Node 11
                                    print('Node 11 - Save spc2classification_rule [SOURCE DB]')
                                    sql_cmd_c11_sdb_save = '''select * from analytical.fn_etl_save_spc2classification_rule(%s,%s,%s,%s);'''
                                    data_c11_sdb_save = (res_c1_sdb_get_list[i]['id'],res_c5_tdb_check_etl_id,res_c1_sdb_get_list[i]['etl_sub_population_category__id'],res_c1_sdb_get_list[i]['etl_ldsity_object__id'])
                                    res_c11_sdb_save = run_sql(conn_src,sql_cmd_c11_sdb_save,data_c11_sdb_save)[0][0]
                                    #print(res_c11_sdb_save)
            print('--------------------------------------------------------------')
            #######################################################################################
            # SPC2CLASSRULE2PANEL_REFYEARSET
            ####################################################################################### 
            ########################################################
            ############################################### I. CYCLE
            print('I. CYCLE - SPC2CLASSRULE2PANEL_REFYEARSET')
            for r in res_sp_attr_4_list_selected:
                print('I. CYCLE - SP = %s' % (r['sp__id']))
                print('I. CYCLE - L = %s' % (r['sp__label_en']))
                ############################################### I. CYCLE Node 1
                print('Node 1 - get_spc2classrule2panel_refyearset_att [SOURCE DB]')
                sql_cmd_c1_sdb_get_count = '''select count(*) from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(%s,%s,%s,false);'''
                sql_cmd_c1_sdb_get_list = '''select * from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(%s,%s,%s,false);'''
                data_c1_sdb_get = (node7_conn,lo4_input_ldsity_object,r['spc__id'])
                res_c1_sdb_get_count = run_sql(conn_src,sql_cmd_c1_sdb_get_count,data_c1_sdb_get)[0][0]
                res_c1_sdb_get_list = run_sql(conn_src,sql_cmd_c1_sdb_get_list,data_c1_sdb_get)
                #print(res_c1_sdb_get_count)
                #print(res_c1_sdb_get_list)
                ########################################## I. Cycle Node 2
                if res_c1_sdb_get_count == 0:
                    print('Node 2 - Any record returned? -> NO [next SP attribute]')
                else:
                    print('Node 2 - Any record returned? -> YES')
                    ########################################## II. Cycle Node 3
                    print('II. CYCLE - SPC2CLASSRULE2PANEL_REFYEARSET [cycle for all rows from NODE 1]')
                    for i, rr in enumerate(res_c1_sdb_get_list):
                        # preparing cmd and data [array of ETL_IDs] for Node 3
                        sql_cmd_c3_sdb_get_etl_id = '''select array_agg(etl_spc2classrule2panel_refyearset__etl_id order by etl_spc2classrule2panel_refyearset__etl_id) from analytical.fn_etl_get_spc2classrule2panel_refyearset_attr(%s,%s,null::integer[],true);'''
                        data_c3_sdb_get_etl_id = (node7_conn,lo4_input_ldsity_object)
                        res_c3_sdb_get_etl_id = run_sql(conn_src,sql_cmd_c3_sdb_get_etl_id,data_c3_sdb_get_etl_id)[0][0]

                        print('Node 3 - Get spc2classrule2panel_refyearsets [TARGET DB]')
                        if res_c3_sdb_get_etl_id == None:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,null::integer[]);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                        else:
                            sql_cmd_c3_tdb_get_count = '''select count(*) from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                            sql_cmd_c3_tdb_get_list = '''select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(%s,%s,%s,%s);'''
                            data_c3_tdb_get = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'],res_c3_sdb_get_etl_id)

                        res_c3_tdb_get_count = run_sql(conn_dest,sql_cmd_c3_tdb_get_count,data_c3_tdb_get)[0][0]
                        res_c3_tdb_get_list = run_sql(conn_dest,sql_cmd_c3_tdb_get_list,data_c3_tdb_get)
                        #print(res_c3_tdb_get_count)
                        #print(res_c3_tdb_get_list)
                        ########################################## II. Cycle Node 4
                        if res_c3_tdb_get_count == 0:
                            print('Node 4 - Any record returned? -> NO')
                            ########################################## Cycle II. Node 5
                            print('Node 5 - Save spc2classrule2panel_refyearset [TARGET DB]')
                            sql_cmd_c5_tdb_save = '''select etl_id from target_data.fn_import_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                            data_c5_tdb_save = (res_c1_sdb_get_list[i]['id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__etl_id'],res_c1_sdb_get_list[i]['refyearset2panel'])
                            res_c5_tdb_save =run_sql(conn_dest,sql_cmd_c5_tdb_save,data_c5_tdb_save)[0][0]
                            #print(res_c5_tdb_save)
                            ########################################## II. Cycle Node 7
                            print('Node 7 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                            sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                            data_c7_sdb_save = (res_c1_sdb_get_list[i]['spc2classrule2panel_refyearset'],res_c5_tdb_save,res_c1_sdb_get_list[i]['etl_spc2classification_rule__id'])
                            res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                            #print(res_c7_sdb_save)
                        else:
                            print('Node 4 - Any record returned? -> YES')
                            ########################################## II. Cycle Node 6
                            # check that number of rows in res_c3_tdb_get_list must be only one row
                            if res_c3_tdb_get_count == 1:
                                print('Node 6 - Is returned one row in node 3? -> YES')
                                ########################################## II. Cycle Node 7
                                print('Node 7 - Save spc2classrule2panel_refyearset [SOURCE DB]')
                                sql_cmd_c7_sdb_save = '''select * from analytical.fn_etl_save_spc2classrule2panel_refyearset(%s,%s,%s);'''
                                data_c7_sdb_save = (res_c1_sdb_get_list[i]['spc2classrule2panel_refyearset'],res_c3_tdb_get_list[0]['etl_id'],res_c1_sdb_get_list[i]['etl_spc2classification_rule__id'])
                                res_c7_sdb_save = run_sql(conn_src,sql_cmd_c7_sdb_save,data_c7_sdb_save)[0][0]
                                #print(res_c7_sdb_save)
                            else:
                                print('Node 6 - Is returned one row in node 3? -> NO')
                                print('In node 3 is returned more than one row! -> STOP ETL process !!!')
                                sys.exit(1)
            print('--------------------------------------------------------------')
            #######################################################################################
            print('Node 6 - get_sp_attributes4ldsity_object [SOURCE DB]')
            sql_cmd_sp_attr_6_get_count_selected = '''select count(*) from analytical.fn_etl_get_sp_attributes4ldsity_object(%s,%s,%s,false);'''
            data_sp_attr_6_selected = (node7_conn,lo4_input_ldsity_object,lst_attr)
            res_sp_attr_6_count_selected = run_sql(conn_src,sql_cmd_sp_attr_6_get_count_selected,data_sp_attr_6_selected)[0][0]
            #print(res_sp_attr_6_count_selected)

            if res_sp_attr_6_count_selected == 0:
                print('Node 7 - Is returned any row in node 6? -> NO -> next step: COMMIT and the END of process ETL')
            else:
                print('Node 7 - Is returned any row in node 6? -> YES -> next step: NO COMMIT and STOP process ETL')
                sys.exit(1)
            print('--------------------------------------------------------------')
            #######################################################################################
print('--------------------------------------------------------------')
###############################################################################



# dbname=contrib_regression_etl port=5433 user=vagrant host=localhost



###############################################################################
#######################################################   COMMIT CONFIRMATION
while True:
    commit_conn = input("Confirm a commit of ETL process? (y/n):\n")
    if not (commit_conn == 'y' or commit_conn == 'n'):
        print("Sorry, not known.")
        continue
    else:
        break

if commit_conn == 'y':
    print('ETL process was commited.')
    conn_dest.commit()
    conn_src.commit()
elif commit_conn == 'n':
    print('ETL process was canceled !!!')
###############################################################################



###############################################################################
###########################################################     DB CONN CLOSE
conn_src.close()
conn_dest.close()
###############################################################################
