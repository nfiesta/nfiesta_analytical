--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_area_domain_category(integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_area_domain_category
(
	_area_domain_category	integer,
	_etl_id					integer,
	_etl_area_domain		integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _area_domain_category is null
	then
		raise exception 'Error 01: fn_etl_save_area_domain_category: Input argument _area_domain_category must not by NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_area_domain_category: Input argument _etl_id must not by NULL!';
	end if;

	if _etl_area_domain is null
	then
		raise exception 'Error 03: fn_etl_save_area_domain_category: Input argument _etl_area_domain must not by NULL!';
	end if;

	if not exists (select t.* from analytical.c_area_domain_category as t where t.id = _area_domain_category)
	then
		raise exception 'Error 04: fn_etl_save_area_domain_category: Given area domain category (%) does not exist in c_area_domain_category table.', _area_domain_category;
	end if;	

	insert into analytical.t_etl_area_domain_category(area_domain_category, etl_id, etl_area_domain)
	select _area_domain_category, _etl_id, _etl_area_domain
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) is
'Function inserts a record into table t_etl_area_domain_category based on given parameters.';

alter function analytical.fn_etl_save_area_domain_category(integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_area_domain_category(integer, integer, integer) to public;