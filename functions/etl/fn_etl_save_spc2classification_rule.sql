--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) CASCADE;

create or replace function analytical.fn_etl_save_spc2classification_rule
(
	_spc2classification_rule		integer,
	_etl_id							integer,
	_etl_sub_population_category	integer,
	_etl_ldsity_object				integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _spc2classification_rule is null
	then
		raise exception 'Error 01: fn_etl_save_spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
	end if; 

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_spc2classification_rule: Input argument _etl_id must not be NULL!';
	end if; 

	if _etl_sub_population_category is null
	then
		raise exception 'Error 03: fn_etl_save_spc2classification_rule: Input argument _etl_sub_population_category must not be NULL!';
	end if;

	if _etl_ldsity_object is null
	then
		raise exception 'Error 04: fn_etl_save_spc2classification_rule: Input argument _etl_ldsity_object must not be NULL!';
	end if; 

	insert into analytical.t_etl_spc2classification_rule(spc2classification_rule, etl_id, etl_sub_population_category, etl_ldsity_object)
	select _spc2classification_rule, _etl_id, _etl_sub_population_category, _etl_ldsity_object
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) is
'Function inserts a record into table t_etl_spc2classification_rule based on given parameters.';

alter function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_save_spc2classification_rule(integer, integer, integer, integer) to public;