--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_informations_attr
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_sub_population_informations_attr(integer, integer) CASCADE;

create or replace function analytical.fn_etl_get_sub_population_informations_attr
(
	_export_connection	integer,
	_ldsity_object		integer
)
returns table
(
	lo__id								integer,
	lo__label							varchar,
	lo__description						text,
	lo__label_en						varchar,
	lo__description_en					text,
	aop__id								integer,
	aop__label							varchar,
	aop__description					text,
	sp__id								integer,
	sp__label							varchar,
	sp__description						text,
	sp__label_en						varchar,
	sp__description_en					text,
	spc__id								integer,
	spc__sub_population					integer,
	spc__label							varchar,
	spc__description					text,
	spc__label_en						varchar,
	spc__description_en					text,
	rule__id							integer,
	rule__sub_population_category		integer,
	rule__classification_rule			text,
	etl_ldsity_object__id				integer,
	etl_ldsity_object__etl_id			integer,
	etl_sub_population__id				integer,
	etl_sub_population__etl_id			integer,
	etl_sub_population_category__id		integer,
	etl_sub_population_category__etl_id	integer,
	etl_spc2classification_rule__id		integer,
	etl_spc2classification_rule__etl_id	integer
)
as
$$
declare
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_informations_attr: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_informations_attr: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_informations_attr: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_sub_population_informations_attr: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		if not exists (select t3.* from analytical.t_etl_ldsity_object as t3 where t3.export_connection = _export_connection and t3.ldsity_object = _ldsity_object)
		then
			raise exception 'Error 05: fn_etl_get_sub_population_informations_attr: For given export connection (%) and ldsity object (%) does not exist record in t_etl_ldsity_object table.', _export_connection, _ldsity_object;
		end if;	
		
		return query
		with
		w1 as	(						
				select
						t4.id as lo__id,
						t4.label as lo__label,
						t4.description as lo__description,
						t4.label_en as lo__label_en,
						t4.description_en as lo__description_en,
						-----------------------------------------
						t5.id as aop__id,
						t5.label as aop__label,
						t5.description as aop__description,
						-----------------------------------------
						t3.id as sp__id,
						t3.label as sp__label,
						t3.description as sp__description,
						t3.label_en as sp__label_en,
						t3.description_en as sp__description_en,
						-----------------------------------------
						t2.id as spc__id,
						t2.sub_population as spc__sub_population,
						t2.label as spc__label,
						t2.description as spc__description,
						t2.label_en as spc__label_en,
						t2.description_en as spc__description_en,
						-----------------------------------------
						t1.rule__id,
						t1.rule__sub_population_category,
						t1.rule__classification_rule,
						-----------------------------------------
						t6.id as etl_ldsity_object__id,
						t6.etl_id as etl_ldsity_object__etl_id
				from
					(
					select
							cmspc2cr.id as rule__id,
							cmspc2cr.sub_population_category as rule__sub_population_category,
							cmspc2cr.ldsity_object as rule__ldsity_object,
							cmspc2cr.classification_rule as rule__classification_rule
					from
							analytical.cm_spc2classification_rule as cmspc2cr
					inner join 	analytical.cm_spc2classrule2panel_refyearset as t2
					on 		cmspc2cr.id = t2.spc2classification_rule

					where
							cmspc2cr.ldsity_object = _ldsity_object
					) as t1
				
				inner join	analytical.c_sub_population_category as t2 on t1.rule__sub_population_category = t2.id
				inner join	analytical.c_sub_population as t3 on t2.sub_population = t3.id
				inner join	analytical.c_ldsity_object as t4 on t1.rule__ldsity_object = t4.id
				inner join	analytical.c_areal_or_population as t5 on t4.areal_or_population = t5.id
				inner join	(select telo.* from analytical.t_etl_ldsity_object as telo where telo.export_connection = _export_connection) as t6 on t1.rule__ldsity_object = t6.ldsity_object
				)
		,w2 as	(
				select
						w1.*,
						a1.id as etl_sub_population__id,
						a1.etl_id as etl_sub_population__etl_id
				from
						w1
				
				left join	(select * from analytical.t_etl_sub_population where export_connection = _export_connection) as a1 on w1.sp__id = a1.sub_population
				)
		,w3 as	(
				select
						w2.*,
						a2.id as etl_sub_population_category__id,
						a2.etl_id as etl_sub_population_category__etl_id
				from
						w2
						
				left join	(
							select tespc.* from analytical.t_etl_sub_population_category as tespc
							where tespc.etl_sub_population in (select w2.etl_sub_population__id from w2)
							) as a2
							on w2.etl_sub_population__id = a2.etl_sub_population and w2.spc__id = a2.sub_population_category
				)
		,w4 as	(
				select
						w3.*,
						a3.id as etl_spc2classification_rule__id,
						a3.etl_id as etl_spc2classification_rule__etl_id
				from
						w3
				left join	(
							select * from analytical.t_etl_spc2classification_rule
							where etl_ldsity_object in (select distinct w3.etl_ldsity_object__id from w3)
							and etl_sub_population_category in (select w3.etl_sub_population_category__id from w3)
							) as a3
							on w3.etl_sub_population_category__id = a3.etl_sub_population_category
							and w3.etl_ldsity_object__id = a3.etl_ldsity_object
							and w3.rule__id = a3.spc2classification_rule					
				)
		select
				w4.lo__id,
				w4.lo__label,
				w4.lo__description,
				w4.lo__label_en,
				w4.lo__description_en,
				w4.aop__id,
				w4.aop__label,
				w4.aop__description,
				w4.sp__id,
				w4.sp__label,
				w4.sp__description,
				w4.sp__label_en,
				w4.sp__description_en,
				w4.spc__id,
				w4.spc__sub_population,
				w4.spc__label,
				w4.spc__description,
				w4.spc__label_en,
				w4.spc__description_en,
				w4.rule__id,
				w4.rule__sub_population_category,
				w4.rule__classification_rule,
				w4.etl_ldsity_object__id,
				w4.etl_ldsity_object__etl_id,
				w4.etl_sub_population__id,
				w4.etl_sub_population__etl_id,
				w4.etl_sub_population_category__id,
				w4.etl_sub_population_category__etl_id,
				w4.etl_spc2classification_rule__id,
				w4.etl_spc2classification_rule__etl_id
		from
				w4 order by w4.rule__id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) is
'Function returns sub population informations for given input arguments: export connection and ldsity object.';

alter function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to adm_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to data_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to usr_analytical;
grant execute on function analytical.fn_etl_get_sub_population_informations_attr(integer, integer) to public;

