--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_areal_or_population(integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_areal_or_population
(
	_export_connection	integer,
	_etl				boolean default null::boolean
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	etl_areal_or_population__id		integer,
	etl_areal_or_population__etl_id	integer
)
as
$$
declare
		_cond		text;
begin
		if not exists (select t.* from analytical.t_export_connection as t where t.id = _export_connection)
		then
			raise exception 'Error 01: fn_etl_get_areal_or_population: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_areal_or_population__etl_id is not null';
			else
				_cond := 'w3.etl_areal_or_population__etl_id is null';
			end if;
		end if;
				
		return query execute
		'
		with
		w1 as	(
				select * from analytical.c_areal_or_population
				)
		,w2 as	(
				select * from analytical.t_etl_areal_or_population
				where export_connection = $1
				)
		,w3 as	(
				select
					w1.*,
					w2.id as etl_areal_or_population__id,
					w2.etl_id as etl_areal_or_population__etl_id
				from
					w1 left join w2 on w1.id = w2.areal_or_population
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label as label_en,
				w3.description as description_en,
				w3.etl_areal_or_population__id,
				w3.etl_areal_or_population__etl_id
		from
				w3 where '|| _cond ||'
		order
				by w3.id
		'
		using _export_connection;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_areal_or_population(integer, boolean) is
'Function returns information records of areal or population.';

alter function analytical.fn_etl_get_areal_or_population(integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_areal_or_population(integer, boolean) to public;

