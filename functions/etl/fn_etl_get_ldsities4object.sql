--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsities4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[]) CASCADE;

create or replace function analytical.fn_etl_get_ldsities4object
(
	_export_connection	integer,
	_ldsity_object		integer,
	_etl				boolean default null::boolean,
	_ldsities			integer[] default null::integer[],
	_etl_ldsity__etl_id	boolean default null::boolean

)
returns table
(
	export_connection						integer,
	------------------------------------------------
	ldsity_object							integer,
	ldsity_object_label						varchar,
	ldsity_object_description				text,
	ldsity_object_label_en					varchar,
	ldsity_object_description_en			text,
	etl_ldsity_object__id					integer,
	etl_ldsity_object__etl_id				integer,
	------------------------------------------------
	ldsity 									integer,
	ldsity_label							varchar,
	ldsity_description						text,
	ldsity_label_en							varchar,
	ldsity_description_en					text,
	ldsity_column_expression				text,
	------------------------------------------------
	check_unit_of_measure					boolean,
	check_definition_variant				boolean,
	------------------------------------------------
	check_area_domain						boolean,
	check_area_domain_category				boolean,
	check_adc2classification_rule			boolean,
	check_adc_hierarchy						boolean,
	check_adc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_sub_population					boolean,
	check_sub_population_category			boolean,
	check_spc2classification_rule			boolean,
	check_spc_hierarchy						boolean,
	check_spc2classrule2panel_refyearset	boolean,
	------------------------------------------------
	check_version							boolean,
	check_ldsity2panel_refyearset_version	boolean,
	etl_ldsity__id							integer,
	etl_ldsity__etl_id						integer,
	check_ldsity							boolean,
	check_ldsity_part						boolean,
	------------------------------------------------
	ldsity_object__import					integer,
	unit_of_measure__import					integer,
	definition_variant__import				integer[],
	area_domain_category__import			integer[],
	sub_population_category__import			integer[]
)
as
$$
declare
		_ldsity_object_label				varchar;
		_ldsity_object_description			text;
		_ldsity_object_label_en				varchar;
		_ldsity_object_description_en		text;
		_etl_ldsity_object__id				integer;
		_etl_ldsity_object__etl_id			integer;
		_ldsity_object_upper_object			integer;
		_ldsity_array						integer[];
		_cond_1								text;
		_cond_2 							text;
		_data4import						boolean;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsities4object: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity_object is null
		then
			raise exception 'Error 02: fn_etl_get_ldsities4object: Input argument _ldsity_object must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ldsities4object: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity_object as t2 where t2.id = _ldsity_object)
		then
			raise exception 'Error 04: fn_etl_get_ldsities4object: Given ldsity object (%) does not exist in c_ldsity_object table.', _ldsity_object;
		end if;
	
		select
				clo.label,
				clo.description,
				clo.label_en,
				clo.description_en,
				clo.upper_object
		from
				analytical.c_ldsity_object as clo where clo.id = _ldsity_object
		into
				_ldsity_object_label,
				_ldsity_object_description,
				_ldsity_object_label_en,
				_ldsity_object_description_en,
				_ldsity_object_upper_object;

		select telo.id, telo.etl_id from analytical.t_etl_ldsity_object as telo
		where telo.export_connection = _export_connection
		and telo.ldsity_object = _ldsity_object
		into
			_etl_ldsity_object__id,
			_etl_ldsity_object__etl_id;
			
		if _ldsity_object_upper_object is null
		then
			return query
			with
			w1 as	(
					select
						0 as export_connection,
						------------------------------------------------
						null::integer as ldsity_object,
						null::varchar as ldsity_object_label,
						null::text as ldsity_object_description,
						null::varchar as ldsity_object_label_en,
						null::text as ldsity_object_description_en,
						null::integer as etl_ldsity_object__id,
						null::integer as etl_ldsity_object__etl_id,
						------------------------------------------------
						null::integer as ldsity,
						null::varchar as ldsity_label,
						null::text as ldsity_description,
						null::varchar as ldsity_label_en,
						null::text as ldsity_description_en,
						null::text as ldsity_column_expression,
						------------------------------------------------
						null::boolean as check_unit_of_measure,
						null::boolean as check_definition_variant,
						------------------------------------------------
						null::boolean as check_area_domain,
						null::boolean as check_area_domain_category,
						null::boolean as check_adc2classification_rule,
						null::boolean as check_adc_hierarchy,
						null::boolean as check_adc2classrule2panel_refyearset,
						------------------------------------------------
						null::boolean as check_sub_population,
						null::boolean as check_sub_population_category,
						null::boolean as check_spc2classification_rule,
						null::boolean as check_spc_hierarchy,
						null::boolean as check_spc2classrule2panel_refyearset,
						------------------------------------------------
						null::boolean as check_version,
						null::boolean as check_ldsity2panel_refyearset_version,
						null::integer as etl_ldsity__id,
						null::integer as etl_ldsity__etl_id,
						null::boolean as check_ldsity,
						null::boolean as check_ldsity_part,
						------------------------------------------------
						null::integer as ldsity_object__import,
						null::integer as unit_of_measure__import,
						null::integer[] as definition_variant__import,
						null::integer[] as area_domain_category__import,
						null::integer[] as sub_population_category__import
					)
			select * from w1 where w1.export_connection = _export_connection;
					
		else
			if _ldsities is null
			then
				select array_agg(cl.id order by cl.id) from analytical.c_ldsity as cl
				where cl.ldsity_object = _ldsity_object
				into _ldsity_array;

				_data4import := false;
			else
				_ldsity_array := _ldsities;
				_data4import := true;
			end if;
		
			if _ldsity_array is null
			then
				-- for given ldsity object not exist any ldsity => function must return zero rows
				return query
				with
				w1 as	(
						select
							0 as export_connection,
							------------------------------------------------
							null::integer as ldsity_object,
							null::varchar as ldsity_object_label,
							null::text as ldsity_object_description,
							null::varchar as ldsity_object_label_en,
							null::text as ldsity_object_description_en,
							null::integer as etl_ldsity_object__id,
							null::integer as etl_ldsity_object__etl_id,
							------------------------------------------------
							null::integer as ldsity,
							null::varchar as ldsity_label,
							null::text as ldsity_description,
							null::varchar as ldsity_label_en,
							null::text as ldsity_description_en,
							null::text as ldsity_column_expression,
							------------------------------------------------
							null::boolean as check_unit_of_measure,
							null::boolean as check_definition_variant,
							------------------------------------------------
							null::boolean as check_area_domain,
							null::boolean as check_area_domain_category,
							null::boolean as check_adc2classification_rule,
							null::boolean as check_adc_hierarchy,
							null::boolean as check_adc2classrule2panel_refyearset,
							------------------------------------------------
							null::boolean as check_sub_population,
							null::boolean as check_sub_population_category,
							null::boolean as check_spc2classification_rule,
							null::boolean as check_spc_hierarchy,
							null::boolean as check_spc2classrule2panel_refyearset,
							------------------------------------------------
							null::boolean as check_version,
							null::boolean as check_ldsity2panel_refyearset_version,
							null::integer as etl_ldsity__id,
							null::integer as etl_ldsity__etl_id,
							null::boolean as check_ldsity,
							null::boolean as check_ldsity_part,
							------------------------------------------------
							null::integer as ldsity_object__import,
							null::integer as unit_of_measure__import,
							null::integer[] as definition_variant__import,
							null::integer[] as area_domain_category__import,
							null::integer[] as sub_population_category__import
						)
				select * from w1 where w1.export_connection = _export_connection;				
			else
				if _etl is null
				then
					_cond_1 := 'TRUE';
				else
					if _etl = true
					then
						_cond_1 := 'w3.check_ldsity = true';
					else
						_cond_1 := 'w3.check_ldsity = false';
					end if;
				end if;

				if _etl_ldsity__etl_id is null
				then
					_cond_2 := 'TRUE';
				else
					if _etl_ldsity__etl_id = true
					then
						_cond_2 := 'w3.etl_ldsity__etl_id is not null';
					else
						_cond_2 := 'w3.etl_ldsity__etl_id is null';
					end if;
				end if;
			
				return query execute
				'
				with
				w1 as	(select unnest($2) as ldsities)
				,w2 as	(select analytical.fn_etl_get_ldsity($1,w1.ldsities,$10) as res from w1)
				,w3 as	(
						select		
								(w2.res).export_connection,
								-------------------------------------------
								$3 as ldsity_object,
								$4 as ldsity_object_label,
								$5 as ldsity_object_description,
								$6 as ldsity_object_label_en,
								$7 as ldsity_object_description_en,
								$8 as etl_ldsity_object__id,
								$9 as etl_ldsity_object__etl_id,	
								-------------------------------------------
								(w2.res).ldsity,
								(w2.res).label					as ldsity_label,
								(w2.res).description			as ldsity_description,
								(w2.res).label_en				as ldsity_label_en,
								(w2.res).description_en			as ldsity_description_en,
								(w2.res).column_expression		as ldsity_column_expression,
								-------------------------------------------
								(w2.res).check_unit_of_measure,
								(w2.res).check_definition_variant,
								-------------------------------------------
								(w2.res).check_area_domain,
								(w2.res).check_area_domain_category,
								(w2.res).check_adc2classification_rule,
								(w2.res).check_adc_hierarchy,
								(w2.res).check_adc2classrule2panel_refyearset,
								-------------------------------------------
								(w2.res).check_sub_population,
								(w2.res).check_sub_population_category,
								(w2.res).check_spc2classification_rule,
								(w2.res).check_spc_hierarchy,
								(w2.res).check_spc2classrule2panel_refyearset,
								-------------------------------------------
								(w2.res).check_version,
								(w2.res).check_ldsity2panel_refyearset_version,
								(w2.res).etl_ldsity__id,
								(w2.res).etl_ldsity__etl_id,
								(w2.res).check_ldsity,
								(w2.res).check_ldsity_part,
								-------------------------------------------
								(w2.res).ldsity_object__import,
								(w2.res).unit_of_measure__import,
								(w2.res).definition_variant__import,
								(w2.res).area_domain_category__import,
								(w2.res).sub_population_category__import
						from
								w2
						)
					select
							w3.export_connection,
							------------------------------------------------
							w3.ldsity_object,
							w3.ldsity_object_label,
							w3.ldsity_object_description,
							w3.ldsity_object_label_en,
							w3.ldsity_object_description_en,
							w3.etl_ldsity_object__id,
							w3.etl_ldsity_object__etl_id,
							------------------------------------------------
							w3.ldsity,
							w3.ldsity_label,
							w3.ldsity_description,
							w3.ldsity_label_en,
							w3.ldsity_description_en,
							w3.ldsity_column_expression,
							------------------------------------------------
							w3.check_unit_of_measure,
							w3.check_definition_variant,
							------------------------------------------------
							w3.check_area_domain,
							w3.check_area_domain_category,
							w3.check_adc2classification_rule,
							w3.check_adc_hierarchy,
							w3.check_adc2classrule2panel_refyearset,
							------------------------------------------------
							w3.check_sub_population,
							w3.check_sub_population_category,
							w3.check_spc2classification_rule,
							w3.check_spc_hierarchy,
							w3.check_spc2classrule2panel_refyearset,
							------------------------------------------------
							w3.check_version,
							w3.check_ldsity2panel_refyearset_version,
							w3.etl_ldsity__id,
							w3.etl_ldsity__etl_id,
							w3.check_ldsity,
							w3.check_ldsity_part,
							------------------------------------------------
							w3.ldsity_object__import,
							w3.unit_of_measure__import,
							w3.definition_variant__import,
							w3.area_domain_category__import,
							w3.sub_population_category__import
					from
							w3
					where
							'|| _cond_1 ||'
					and
							'|| _cond_2 ||'
					order
							by w3.ldsity
					'
					using
							_export_connection,
							_ldsity_array,
							_ldsity_object,
							_ldsity_object_label,
							_ldsity_object_description,
							_ldsity_object_label_en,
							_ldsity_object_description_en,
							_etl_ldsity_object__id,
							_etl_ldsity_object__etl_id,
							_data4import;
			end if;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) is
'Function returns information of ldsities for given ldsity object.';

alter function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsities4object(integer, integer, boolean, integer[], boolean) to public;

