--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_ldsity2panel_refyearset_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) CASCADE;

create or replace function analytical.fn_etl_get_ldsity2panel_refyearset_version
(
	_export_connection	integer,
	_ldsity				integer,
	_etl				boolean default null::boolean
)
returns table
(
	id											integer,
	ldsity										integer,
	refyearset2panel							integer,
	version										integer,
	etl_ldsity__id								integer,
	etl_ldsity__etl_id							integer,
	etl_version__id								integer,
	etl_version__etl_id							integer,
	etl_ldsity2panel_refyearset_version__id		integer,
	etl_ldsity2panel_refyearset_version__etl_id	integer
)
as
$$
declare
		_ldsity_object	integer;
		_cond			text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_ldsity2panel_refyearset_version: Input argument _export_connection must not be NULL!';
		end if;
	
		if _ldsity is null
		then
			raise exception 'Error 02: fn_etl_get_ldsity2panel_refyearset_version: Input argument _ldsity must not be NULL!';
		end if;

		if not exists (select t1.* from analytical.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 03: fn_etl_get_ldsity2panel_refyearset_version: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		if not exists (select t2.* from analytical.c_ldsity as t2 where t2.id = _ldsity)
		then
			raise exception 'Error 04: fn_etl_get_ldsity2panel_refyearset_version: Given ldsity (%) does not exist in c_ldsity table.', _ldsity;
		end if;	

		select cl.ldsity_object from analytical.c_ldsity as cl where cl.id = _ldsity
		into _ldsity_object;
	
		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w3.etl_ldsity2panel_refyearset_version__etl_id is not null';
			else
				_cond := 'w3.etl_ldsity2panel_refyearset_version__etl_id is null';
			end if;
		end if;	
			
		return query execute
		'
		with
		w1 as	(select * from analytical.cm_ldsity2panel_refyearset_version where ldsity = $2)
		,w2 as	(
				select
						w1.id,
						w1.ldsity,
						w1.refyearset2panel,
						w1.version,
						t1.id as etl_ldsity__id,
						t1.etl_id as etl_ldsity__etl_id,
						t2.id as etl_version__id,
						t2.etl_id as etl_version__etl_id
				from
						w1
				inner join	(	
							select * from analytical.t_etl_ldsity where etl_ldsity_object =
							(select id from analytical.t_etl_ldsity_object where export_connection = $1 and ldsity_object = $3)
							and ldsity = $2
							) as t1
							on w1.ldsity = t1.ldsity
							
				inner join	(select * from analytical.t_etl_version where export_connection = $1) as t2
							on w1.version = t2.version
				)
		,w3 as	(
				select
						w2.id,
						w2.ldsity,
						w2.refyearset2panel,
						w2.version,
						w2.etl_ldsity__id,
						w2.etl_ldsity__etl_id,
						w2.etl_version__id,
						w2.etl_version__etl_id,
						telprv.id as etl_ldsity2panel_refyearset_version__id,
						telprv.etl_id as etl_ldsity2panel_refyearset_version__etl_id
				from w2
				left join	analytical.t_etl_ldsity2panel_refyearset_version as telprv
							on w2.id = telprv.ldsity2panel_refyearset_version
							and w2.etl_ldsity__id = telprv.etl_ldsity
							and w2.etl_version__id = telprv.etl_version
				)
		select
				w3.id,
				w3.ldsity,
				w3.refyearset2panel,
				w3.version,
				w3.etl_ldsity__id,
				w3.etl_ldsity__etl_id,
				w3.etl_version__id,
				w3.etl_version__etl_id,
				w3.etl_ldsity2panel_refyearset_version__id,
				w3.etl_ldsity2panel_refyearset_version__etl_id							
		from
				w3 where '|| _cond ||'
		order
				by w3.ldsity, w3.refyearset2panel;
		'
		using _export_connection, _ldsity, _ldsity_object;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) is
'Function returns information records of mapping between ldsity and combinations of panel, reference year sets and versions.';

alter function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) owner to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to adm_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to data_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to usr_analytical;
grant execute on function analytical.fn_etl_get_ldsity2panel_refyearset_version(integer, integer, boolean) to public;

