--
-- Copyright 2017, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: analytical.fn_f_c_strip_object_ftgm_before_delete()

-- DROP FUNCTION analytical.fn_f_c_strip_object_ftgm_before_delete();

CREATE OR REPLACE FUNCTION analytical.fn_f_c_strip_object_ftgm_before_delete()
  RETURNS trigger AS
$BODY$
	BEGIN
		RAISE EXCEPTION 
			'fn_f_c_strip_object_ftgm_before_delete: '
			'Záznam (id = %) v tabulce f_c_strip_object_ftgm není možné smazat.', 
			OLD.id; 
		RETURN OLD;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
COMMENT ON FUNCTION analytical.fn_f_c_strip_object_ftgm_before_delete() IS 'Funkce triggeru, která zabraňuje smazání záznamu z tabulky f_c_strip_object_ftgm. Při pokusu o smazání záznamů vyvolává výjimku. ';

GRANT EXECUTE ON FUNCTION analytical.fn_f_c_strip_object_ftgm_before_delete() TO public;


