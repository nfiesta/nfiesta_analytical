--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

UPDATE analytical.c_ldsity SET 	label = concat(label,' (zákopek)'), description = concat(description,' (zákopek)') WHERE id = 58;
UPDATE analytical.c_ldsity SET 	label = concat(label,' (sonda)'), description = concat(description,' (sonda)') WHERE id = 60;
UPDATE analytical.c_ldsity SET 	label = replace(label, 'kmene.','kmene, norm nom'), 
				description = concat(description, ' Objem normalizován na jeden rok na základě skutečné délky periody od koncového měření do počátečního.')
WHERE id = 31;

ALTER TABLE analytical.f_p_stems_dynamic ADD COLUMN time_span_nominal integer;
COMMENT ON COLUMN analytical.f_p_stems_dynamic.time_span_nominal IS 'Nominální délka periody mezi počátečním a koncovým měřením ve dnech.';

UPDATE analytical.f_p_stems_dynamic SET time_span_nominal = 5*365.25;

ALTER TABLE analytical.f_p_stems_dynamic ALTER COLUMN time_span SET NOT NULL;
ALTER TABLE analytical.f_p_stems_dynamic ALTER COLUMN time_span_nominal SET NOT NULL;

--ALTER TABLE analytical.c_ldsity ADD CONSTRAINT ukey__c_ldsity__label UNIQUE (label);
--ALTER TABLE analytical.c_ldsity ADD CONSTRAINT ukey__c_ldsity__description UNIQUE (description);

INSERT INTO analytical.c_definition_variant (id, label, description, label_en, description_en)
VALUES 
	(7700, 	'normalizace (nominální délka periody)', 'Normalizace na jeden rok provedena na základě nominální délky periody mezi koncovým a počátečním šetřením.',
		'normalization (nominal time span)', 'The normalization to one year is based on the nominal time span between the end and the begin of the measurement.');

INSERT INTO analytical.c_ldsity(id,label,description,ldsity_object,column_expression,unit_of_measure,area_domain_category,sub_population_category,definition_variant,label_en,description_en) VALUES
(237,	'přírůst biomasy s generalizovanou lokální hustotou, norm nom',
	'Přírůst biomasy vyjádřený jako hmotnost sušiny v tunách. Příspěvek je na všech kmenech počítán jako rozdíl mezi generalizovanou hustotou v druhém (okamžik těžby/mortality nebo koncové období) a prvním okamžiku (počáteční období). Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními.',
	1800,'(3652.5 * (above_bio_gldsity_increment_t_ha/time_span_nominal)) / 10.0',1000,'{1111}',NULL,'{6200,7700,7300}',
	'biomass increment with generalized local density, norm nom',
	'Increment of biomass, measured in tons of dry matter. Contribution of each stem is calculated as a difference of generalized local density in the second (time of cut/mortality or the end measurement) and the first measurement (begin measurement). Volume normalized to one year based on the nominal time span.'),
(238,	'přírůst ÚLT b.k. s generalizovanou lokální hustotou norm nom',
	'Přírůst podle tabulek ÚLT bez kůry. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Příspěvek je na všech kmenech počítán jako rozdíl mezi generalizovanou hustotou v druhém (okamžik těžby/mortality nebo koncové období) a prvním okamžiku (počáteční období).',
	1800,'365.25 * (gldsity_increment_ult_bk_m3_ha/time_span_nominal::double precision)',800,'{1111}',NULL,'{500,7700,7300}',
	'increment ULT u.b. with generalized local density norm nom',
	'increment ULT under bark. Volume normalized to one year based on the nominal time span. Contribution of each stem is calculated as a difference of generalized local density in the second (time of cut/mortality or the end measurement) and the first measurement (begin measurement).'),
(239,	'přírůst ÚLT s k. s generalizovanou lokální hustotou norm nom',
	'Přírůst podle tabulek ÚLT s kůrou. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Příspěvek je na všech kmenech počítán jako rozdíl mezi generalizovanou hustotou v druhém (okamžik těžby/mortality nebo koncové období) a prvním okamžiku (počáteční období).',
	1800,'365.25 * (gldsity_increment_ult_sk_m3_ha/time_span_nominal::double precision)',1700,'{1111}',NULL,'{500,7700,7300}',
	'increment ULT o.b. with generalized local density norm nom',
	'increment ULT over bark. Volume normalized to one year based on the nominal time span. Contribution of each stem is calculated as a difference of generalized local density in the second (time of cut/mortality or the end measurement) and the first measurement (begin measurement).'),
(240,	'přírůst ČSOT b.k. s generalizovanou lokální hustotou norm nom',
	'Přírůst podle tabulek ČSOT bez kůry. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Příspěvek je na všech kmenech počítán jako rozdíl mezi generalizovanou hustotou v druhém (okamžik těžby/mortality nebo koncové období) a prvním okamžiku (počáteční období).',
	1800,'365.25 * (gldsity_increment_csot_bk_m3_ha/time_span_nominal::double precision)',800,'{1111}',NULL,'{600,7700,7300}',
	'increment CSOT u.b. with generalized local density norm nom',
	'increment CSOT under bark. Volume normalized to one year based on the nominal time span. Contribution of each stem is calculated as a difference of generalized local density in the second (time of cut/mortality or the end measurement) and the first measurement (begin measurement).'),
(241,	'přírůst ČSOT s k. s generalizovanou lokální hustotou norm nom',
	'Přírůst podle tabulek ČSOT s kůrou. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Příspěvek je na všech kmenech počítán jako rozdíl mezi generalizovanou hustotou v druhém (okamžik těžby/mortality nebo koncové období) a prvním okamžiku (počáteční období).',
	1800,'365.25 * (gldsity_increment_csot_sk_m3_ha/time_span_nominal::double precision)',1700,'{1111}',NULL,'{600,7700,7300}',
	'increment CSOT o.b. with generalized local density norm nom',
	'increment CSOT over bark. Volume normalized to one year based on the nominal time span. Contribution of each stem is calculated as a difference of generalized local density in the second (time of cut/mortality or the end measurement) and the first measurement (begin measurement).'),

(242,	'generalizovaný objem ÚLT těž./odum. kmene s přírůst., b.k., norm nom',
	'Objem ÚLT bez kůry těž./odum. kmene s přírůstem do poloviny periody. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Generalizovaná lokální hustota.',
	1800,'365.25 * (gldsity_cut_mid_ult_bk_m3_ha/time_span_nominal::double precision)',800,'{1111}','{1895}','{500,800,7700,7000}',
	'cut/dead stem generalized volume ULT u.b., with growth, norm nom',
	'Cut or dead stem volume ULT under bark. Volume calculated to the time of assumed cut (half of the period between measurements). Volume normalized to one year based on the nominal time span. Generalized local density.'),
(243,	'generalizovaný objem ÚLT těž./odum. kmene s přírůst., s k., norm nom',
	'Objem ÚLT s kůrou těž./odum. kmene s přírůstem do poloviny periody. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Generalizovaná lokální hustota.',
	1800,'365.25 * (gldsity_cut_mid_ult_sk_m3_ha/time_span_nominal::double precision)',1700,'{1111}','{1895}','{500,800,7700,7000}',
	'cut/dead stem generalized volume ULT o.b., with growth, norm nom',
	'Cut or dead stem volume ULT over bark. Volume calculated to the time of assumed cut (half of the period between measurements). Volume normalized to one year based on the nominal time span. Generalized local density.'),
(244,	'generalizovaný objem ČSOT těž./odum. kmene s přírůst., b.k., norm nom',
	'Objem ČSOT bez kůry těž./odum. kmene s přírůstem do poloviny periody. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Generalizovaná lokální hustota.',
	1800,'365.25 * (gldsity_cut_mid_csot_bk_m3_ha/time_span_nominal::double precision)',800,'{1111}','{1895}','{600,800,7700,7000}',
	'cut/dead stem generalized volume CSOT u.b., with growth, norm nom',
	'Cut or dead stem volume CSOT under bark. Volume calculated to the time of assumed cut (half of the period between measurements). Volume normalized to one year based on the nominal time span. Generalized local density.'),
(245,	'generalizovaný objem ČSOT těž./odum. kmene s přírůst., s k., norm nom',
	'Objem ČSOT s kůrou těž./odum. kmene s přírůstem do poloviny periody. Objem normalizován na jeden rok na základě nominální délky periody mezi měřeními. Generalizovaná lokální hustota.',
	1800,'365.25 * (gldsity_cut_mid_csot_sk_m3_ha/time_span_nominal::double precision)',1700,'{1111}','{1895}','{600,800,7700,7000}',
	'cut/dead stem generalized volume CSOT o.b., with growth, norm nom',
	'Cut or dead stem volume CSOT over bark. Volume calculated to the time of assumed cut (half of the period between measurements). Volume normalized to one year based on the nominal time span. Generalized local density.'),
(246,	'hmotnost nadzemní biomasy těž./odum. kmene s přírůstem, generalizovaná hustota, norm nom',
	'Hmotnost sušiny nadzemní biomasy těž./odum. kmene s přírůstem do poloviny periody. Výpočet biomasy na úrovni jedince hroubí je proveden za pomocí alometrických rovnic. Generalizovaná lokální hustota. Biomasa normalizována na jeden rok na základě nominální délky periody mezi měřeními.',
	1800,'365.25 * (above_bio_gldsity_mid_t_ha/time_span_nominal::double precision)',1000,'{1111}','{1895}','{800,6200,7000,7700}',
	'AGB of cut/dead stems, generalized local density, norm nom',
	'Above ground biomass of cut or dead stems, measured in tons of dry matter. Generalized local density. Biomass normalized to one year based on the nominal time span.');
