--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE analytical.t_plots_field_data ADD COLUMN altitude_m double precision;
COMMENT ON COLUMN analytical.t_plots_field_data.altitude_m IS 'Nadmořská výška z tabulky t_plots_derived (DTM), odvozeno pro tvorbu klasifikačního pravidla v kombinaci s ostatními sloupci tabulky.';

GRANT SELECT,REFERENCES ON TABLE analytical.t_plots_field_data_dynamic TO usr_analytical;
GRANT SELECT ON SEQUENCE analytical.t_plots_field_data_change_id_seq TO usr_analytical;
GRANT SELECT,REFERENCES ON TABLE analytical.t_plots_derived_dynamic TO usr_analytical;
GRANT SELECT ON SEQUENCE analytical.t_plots_derived_change_id_seq TO usr_analytical;
