--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- classification rules for dynamic tables
ALTER TABLE analytical.t_plots_field_data_dynamic ADD european_forest_type integer;

ALTER TABLE analytical.t_plots_field_data_dynamic ADD CONSTRAINT
fkey__t_plots_field_data_dynamic__european_forest_type FOREIGN KEY (european_forest_type)
REFERENCES analytical.c_european_forest_type(id);

INSERT INTO analytical.cm_adc2classification_rule (id, ldsity_object, area_domain_category, classification_rule)
VALUES
(1222,2000,37101,'european_forest_type IN (1,2)'),
(1223,2000,37102,'european_forest_type IN (3,4,5,6,7,8)'),
(1224,2000,37103,'european_forest_type IN (9,10,11)'),
(1225,2000,37104,'european_forest_type IN (12,13)'),
(1226,2000,37105,'european_forest_type IN (14,15,16,17,18,19,20,21,22)'),
(1227,2000,37106,'european_forest_type IN (23,24,25,26,27,28,29)'),
(1228,2000,37107,'european_forest_type IN (30,31,32,33,34,35,36,37)'),
(1229,2000,37108,'european_forest_type IN (38,39,40,41,42,43,44,45)'),
(1230,2000,37109,'european_forest_type IN (46,47,48,49,50)'),
(1231,2000,37110,'european_forest_type IN (51,52,53,54,55,56,57,58,59,60,61)'),
(1232,2000,37111,'european_forest_type IN (62,63,64,65,66)'),
(1233,2000,37112,'european_forest_type IN (67,68,69)'),
(1234,2000,37113,'european_forest_type IN (70,71,72,73,74)'),
(1235,2000,37114,'european_forest_type IN (75,76)'),

(1236,2000,37201,'european_forest_type = 1'),
(1237,2000,37202,'european_forest_type = 2'),
(1238,2000,37203,'european_forest_type = 3'),
(1239,2000,37204,'european_forest_type = 4'),
(1240,2000,37205,'european_forest_type = 5'),
(1241,2000,37206,'european_forest_type = 6'),
(1242,2000,37207,'european_forest_type = 7'),
(1243,2000,37208,'european_forest_type = 8'),
(1244,2000,37209,'european_forest_type = 9'),
(1245,2000,37210,'european_forest_type = 10'),
(1246,2000,37211,'european_forest_type = 11'),
(1247,2000,37212,'european_forest_type = 12'),
(1248,2000,37213,'european_forest_type = 13'),
(1249,2000,37214,'european_forest_type = 14'),
(1250,2000,37215,'european_forest_type = 15'),
(1251,2000,37216,'european_forest_type = 16'),
(1252,2000,37217,'european_forest_type = 17'),
(1253,2000,37218,'european_forest_type = 18'),
(1254,2000,37219,'european_forest_type = 19'),
(1255,2000,37220,'european_forest_type = 20'),
(1256,2000,37221,'european_forest_type = 21'),
(1257,2000,37222,'european_forest_type = 22'),
(1258,2000,37223,'european_forest_type = 23'),
(1259,2000,37224,'european_forest_type = 24'),
(1260,2000,37225,'european_forest_type = 25'),
(1261,2000,37226,'european_forest_type = 26'),
(1262,2000,37227,'european_forest_type = 27'),
(1263,2000,37228,'european_forest_type = 28'),
(1264,2000,37229,'european_forest_type = 29'),
(1265,2000,37230,'european_forest_type = 30'),
(1266,2000,37231,'european_forest_type = 31'),
(1267,2000,37232,'european_forest_type = 32'),
(1268,2000,37233,'european_forest_type = 33'),
(1269,2000,37234,'european_forest_type = 34'),
(1270,2000,37235,'european_forest_type = 35'),
(1271,2000,37236,'european_forest_type = 36'),
(1272,2000,37237,'european_forest_type = 37'),
(1273,2000,37238,'european_forest_type = 38'),
(1274,2000,37239,'european_forest_type = 39'),
(1275,2000,37240,'european_forest_type = 40'),
(1276,2000,37241,'european_forest_type = 41'),
(1277,2000,37242,'european_forest_type = 42'),
(1278,2000,37243,'european_forest_type = 43'),
(1279,2000,37244,'european_forest_type = 44'),
(1280,2000,37245,'european_forest_type = 45'),
(1281,2000,37246,'european_forest_type = 46'),
(1282,2000,37247,'european_forest_type = 47'),
(1283,2000,37248,'european_forest_type = 48'),
(1284,2000,37249,'european_forest_type = 49'),
(1285,2000,37250,'european_forest_type = 50'),
(1286,2000,37251,'european_forest_type = 51'),
(1287,2000,37252,'european_forest_type = 52'),
(1288,2000,37253,'european_forest_type = 53'),
(1289,2000,37254,'european_forest_type = 54'),
(1290,2000,37255,'european_forest_type = 55'),
(1291,2000,37256,'european_forest_type = 56'),
(1292,2000,37257,'european_forest_type = 57'),
(1293,2000,37258,'european_forest_type = 58'),
(1294,2000,37259,'european_forest_type = 59'),
(1295,2000,37260,'european_forest_type = 60'),
(1296,2000,37261,'european_forest_type = 61'),
(1297,2000,37262,'european_forest_type = 62'),
(1298,2000,37263,'european_forest_type = 63'),
(1299,2000,37264,'european_forest_type = 64'),
(1300,2000,37265,'european_forest_type = 65'),
(1301,2000,37266,'european_forest_type = 66'),
(1302,2000,37267,'european_forest_type = 67'),
(1303,2000,37268,'european_forest_type = 68'),
(1304,2000,37269,'european_forest_type = 69'),
(1305,2000,37270,'european_forest_type = 70'),
(1306,2000,37271,'european_forest_type = 71'),
(1307,2000,37272,'european_forest_type = 72'),
(1308,2000,37273,'european_forest_type = 73'),
(1309,2000,37274,'european_forest_type = 74'),
(1310,2000,37275,'european_forest_type = 75'),
(1311,2000,37276,'european_forest_type = 76');
