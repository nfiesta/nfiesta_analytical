--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the 'Licence');
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an 'AS IS' basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

UPDATE analytical.cm_spc2classification_rule SET classification_rule =  'stem_category IN (100,200,300,400,500,600,700,800,900)'
WHERE id = 1895;

-- create lookups
CREATE TABLE analytical.c_drupoz_kod
(
	id int4 NOT NULL,
	"label" varchar(100) NOT NULL,
	description text NULL,
	CONSTRAINT pkey__c_drupoz_kod PRIMARY KEY (id)
);

COMMENT ON TABLE analytical.c_drupoz_kod IS 'Číselník druhů pozemků podle KN.';
COMMENT ON COLUMN analytical.c_drupoz_kod.id IS 'Kód kategorie';
COMMENT ON COLUMN analytical.c_drupoz_kod.label IS 'Popis kategorie';
COMMENT ON COLUMN analytical.c_drupoz_kod.description IS 'Podrobná definice kategorie';
COMMENT ON CONSTRAINT pkey__c_drupoz_kod ON analytical.c_drupoz_kod IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

INSERT INTO analytical.c_drupoz_kod(id,label,description)
VALUES 
(2,	'orná půda','orná půda'),
(3,	'chmelnice','chmelnice'),
(4,	'vinice','vinice'),
(5,	'zahrada','zahrada'),
(6,	'ovocný sad','ovocný sad'),
(7,	'trvalý travní porost','trvalý travní porost'),
(8,	'trvalý travní porost','trvalý travní porost'),
(10,	'lesní pozemek','lesní pozemek'),
(11,	'vodní plocha','vodní plocha'),
(13,	'zastavěná plocha a nádvoří','zastavěná plocha a nádvoří'),
(14,	'ostatní plocha','ostatní plocha');

CREATE TABLE analytical.c_zpvypa_kod
(
	id int4 NOT NULL,
	"label" varchar(100) NOT NULL,
	description text NULL,
	CONSTRAINT pkey__c_zpvypa_kod PRIMARY KEY (id)
);

COMMENT ON TABLE analytical.c_zpvypa_kod IS 'Číselník způsobu využití pozemku podle KN.';
COMMENT ON COLUMN analytical.c_zpvypa_kod.id IS 'Kód kategorie';
COMMENT ON COLUMN analytical.c_zpvypa_kod.label IS 'Popis kategorie';
COMMENT ON COLUMN analytical.c_zpvypa_kod.description IS 'Podrobná definice kategorie';
COMMENT ON CONSTRAINT pkey__c_zpvypa_kod ON analytical.c_zpvypa_kod IS 'Jednoznačný identifikátor, primární klíč tabulky číselníku';

INSERT INTO analytical.c_zpvypa_kod(id,label,description)
VALUES
(1,	'skleník, pařeniště','skleník, pařeniště'),
(2,	'školka','školka'),
(3,	'plantáž dřevin','plantáž dřevin'),
(4,	'les jiný než hospodářský','les jiný než hospodářský'),
(5,	'lesní pozemek, na kterém je budova','lesní pozemek, na kterém je budova'),
(6,	'rybník','rybník'),
(7,	'koryto vodního toku přirozené nebo upravené','koryto vodního toku přirozené nebo upravené'),
(8,	'koryto vodního toku umělé', 'koryto vodního toku umělé'),
(9,	'vodní nádrž přírodní','vodní nádrž přírodní'),
(10,	'vodní nádrž umělá','vodní nádrž umělá'),
(11,	'zamokřená plocha','zamokřená plocha'),
(12,	'společný dvůr','společný dvůr'),
(13,	'zbořeniště','zbořeniště'),
(14,	'dráha','dráha'),
(15,	'dálnice','dálnice'),
(16,	'silnice','silnice'),
(17,	'ostatní komunikace','ostatní komunikace'), 
(18,	'ostatní dopravní plocha','ostatní dopravní plocha'),
(19,	'zeleň','zeleň'),
(20,	'sportoviště a rekreační plocha','sportoviště a rekreační plocha'),
(21,	'pohřebiště','pohřebiště'),
(22,	'kulturní a osvětová plocha','kulturní a osvětová plocha'),
(23,	'manipulační plocha','manipulační plocha'),
(24,	'dobývací prostor','dobývací prostor'),
(25,	'skládka','skládka'),
(26,	'jiná plocha','jiná plocha'),
(27,	'neplodná půda','neplodná půda'),
(28,	'vodní plocha, na které je budova','vodní plocha, na které je budova'),
(29,	'fotovoltaická elektrárna','fotovoltaická elektrárna'),
(30,	'mez, stráň','mez, stráň');


ALTER TABLE analytical.t_plots_derived ADD COLUMN land_category integer;
ALTER TABLE analytical.t_plots_derived ADD COLUMN land_use integer;

COMMENT ON COLUMN analytical.t_plots_derived.land_category IS 'Identifikace druhu pozemku, který je zjišťován na základě průniku s daty KN (k 1.1. - první sezóna, k 1.7.druhá sezóna šetření NIL). Cizí klíč na pole ID do číselníku analytical.c_drupoz_kod.';
COMMENT ON COLUMN analytical.t_plots_derived.land_use IS 'Identifikace způsobu využití pozemku, který je zjišťován na základě průniku s daty KN (k 1.1. - první sezóna, k 1.7.druhá sezóna šetření NIL). Cizí klíč na pole ID do číselníku analytical.c_zpvypa_kod.';

ALTER TABLE analytical.t_plots_derived ADD CONSTRAINT fkey__land_category__c_drupoz_kod
FOREIGN KEY (land_category)
REFERENCES analytical.c_drupoz_kod(id)
ON DELETE NO ACTION ON UPDATE CASCADE;

ALTER TABLE analytical.t_plots_derived ADD CONSTRAINT fkey__land_use__c_zpvypa_kod
FOREIGN KEY (land_use)
REFERENCES analytical.c_zpvypa_kod(id)
ON DELETE NO ACTION ON UPDATE CASCADE;
